# Medusa

## 1 Configuration

### 1a. Quick Setup
Run
```bash
bash setup.sh
```
This will:
* prompt you for the location of the Z3 jar. Z3 installation instructions are in
  Section 1b (Z3 Setup)
* compile java source files used in testing (located in
  `resource/java/test-src`)
* build Medusa as a fat jar (i.e., with all its dependencies)

If you run this option, skip to the Section 2 (Running).

### 1b. Z3 Setup
Before building Medusa you'll need to set up Z3. First, download the appropriate
Z3 binary from the [Z3 releases page][z3-release] or get the most recent build
by [compiling Z3 from source][z3-github].

After either downloading and extracting or building, make note of the location
of the `com.microsoft.z3.jar` file that is sitting in `z3/build` and place the
absolute path in the file `z3_location` in the project root. You may need to
create this file.


```bash
echo "/path/to/z3/build/com.microsoft.z3.jar" > z3_location
```

### 1c. Testing Infrastructure
Before running tests we need to compile some resources. From the project root,
run:

```bash
cd resources/scripts
compile-tests.sh
compile-triangle.sh
compile-fork-strat-tests.sh
```

This will compile the source files needed for testing.

## 2. Building
Create a runnable jar, run

```bash
gradle clean fatJar -x test    # The tests take about 5 minutes, so we exclude them
```

This will create `build/libs/edu.umass.medusa.jar`.

## 3. Running
Currently there is a primitive CLI that analyzes multiple mutants of a single
original method, which may be invoked by the following command:

```bash
java -jar build/libs/medusa-all-0.1.0.jar path/to/OriginalClass.class method-desc path/to/mutants1 [path/to/mutants2 ...]
```

The parameters:

* `path/to/OriginalClass.class` is the path to the original classfile
* `method-desc` is the *method descriptor* which unambiguously indicates which
  method is being mutated. (run the cli without any parameters for more info)
* `path/to/mutants_n` points to a file or directory where Medusa will
  recursively collect all classfiles to treat as mutants.


### 3a. An Example
If you ran the scripts in **Configuration->Testing Infrastructure** above then
you can test the CLI with the following command:

```bash
java -jar build/libs/medusa-all-0.1.0.jar resources/java/test-classes/triangle/Triangle.class "classify(III)I" resources/java/test-classes/triangle/mutants/1/triangle/Triangle.class resources/java/test-classes/triangle/mutants/43/triangle/Triangle.class
```

which should output

```bash
1) NOT EQUIVALENT
2) EQUIVALENT
```

### 3b. A Bigger Example
Instead of running on two individual targets we can simply point to the mutants
directory and Medusa will collect all the classfiles:

```bash
java -jar build/libs/medusa-all-0.1.0.jar resources/java/test-classes/triangle/Triangle.class "classify(III)I" resources/java/test-classes/triangle/mutants
```

This should take a minute or two to run.

## 4. Metrics
Medusa also has some built in metrics, including analyzer modules and a simple
timer utility.

### 4a. Applicability Analyzer
At her heart, Medusa reasons about bytecode. Certain bytecodes can always be
reasoned about, such as `iadd`, etc, while others may only be reasoned about in
certain situations, such as `invokestatic`. The Applicability Analyzer is fed a
root directory `path` and looks at all classfiles in the directory tree rooted
at `path`. The analyzer reads in each method in each class and if it encounters
a bytecode instruction that it cannot reason about it marks the method. At the
end of the process, Medusa reports how many methods were unmarked.

#### An Example
Running
```bash
java -jar build/libs/medusa analyze applicability build/classes/
```
will analyze Medusa's applicability on the current build of Medusa. This will
output four sections:

1. **Bot Sets:** These are minimal sets of bytecode whose implementation will
   increase applicability. The current list should not be treated as exhaustive.
2. **Unimplemented Count:** This section consists of a table of unimplemented
   opcodes, how many methods they were encountered in, and what percentage of
   methods they were encountered int.
3. **Applicable Methods:** This is a convenience list of methods that Medusa can
   currently reason about

4. **Summary:** Some statistics, including how many classes were analyzed, the
   total number of applicable methods encountered, and what percentage of
   methods that Medusa may be applied to.

### 4b. Timer
Medusa can also time different strategies. There are two strategies implemented
currently, fork and naive. The naive strategy does the most obvious thing and
runs pretty slowly, having no real optimizations. The fork strategy uses the
first-order cfg-mutation structure of many of the mutants to increase the
efficiency.

#### An Example

Run the following
```bash
java -jar $MEDUSA time resources/java/test-classes/triangle/Triangle.class "classify(III)I" resources/java/test-classes/triangle/mutants --strategy naive
```

which will output something like

```bash
strategy type:            naive
filter type:              first-order
included setup?           false
reasoned about:           125 of 125 mutants
total time:               257807ms
average time per mutant:  2062.456ms/mutant
```

Then, for the comparison, run
```bash
java -jar $MEDUSA time resources/java/test-classes/triangle/Triangle.class "classify(III)I" resources/java/test-classes/triangle/mutants --filter all
```

which should output something along the lines of

```bash
strategy type:            constraint-forking
filter type:              first-order
included setup?           false
reasoned about:           72 of 125 mutants
total time:               52266ms
average time per mutant:  725.917ms/mutant
```

Here the fork strategy has yielded an almost 3x speedup from the naive strategy,
though the fork strategy works particularly well against methods such as
`Triangle.classify()` which have a lot of basic blocks and control flow.

[z3-release]: https://github.com/Z3Prover/z3/releases
[z3-github]: https://github.com/Z3Prover/z3
