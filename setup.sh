printf "Enter the absolute path to the z3 jar file: "
read line

echo "$line" > z3_location

cd resources/scripts
./compile-tests.sh
./compile-triangle.sh
./compile-fork-strat-tests.sh
cd ../..

gradle clean fatJar -x test    # The tests take about 5 minutes, so we exclude them
