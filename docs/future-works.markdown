# Future works for Medusa

## Medusa as a Service

Structural data is lost (or
at least obscured) by compiling the AST to bytecode:
for instance, I may want to apply different techniques
to foreach loops than I do to while loops since their use
cases tend to be different.

Additionally, Medusa is best at chomping on smaller loop
free programs, and the control-flow structure that is
apparent in the AST must be re-discovered by Medusa/Z3,
and this can be costly.

Finally, our end goal for Medusa is to have her hooked in
to Major so that Major can ask questions to Medusa
ideally during mutant generation, and definitely before
executing tests on the generated mutants.

With these things in mind, it would be nice to be able to
send Medusa a fragment of a method compiled down to
JVM bytecode to do smaller, low-level equivalence
searches.

For example, consider the program

```java
int[] square(int[] arr) {
    int[] result = new int[arr.length];
    for (int i = 0; i < arr.length; ++i) {
        result[i] = arr[i] * arr[i];
    }
}
```

A little high-level reasoning tells me that the `i`th entry
in result depends only on the `i`th entry of arr, and that
this is a candidate for our foldability approach; I do this
reasoning looking at the AST, using data-flow analysis.
I then create a new method, call it `square_loop_body`:

```java
// input is the method state
void square_loop_body(int i, int arr_i) {
    // result_i is a global variable in the class
    // we are compiling, representing state. This
    // is basically an SSA assignment.
    result_i = arr_i * arr_i;
}
```

Since we are working with state, we will want to reflect all
state updates as global variables in some new class we
are compiling (thus, `result_i` isn't declared as an int, and
we don't return its value). Rather than explicitly creating
a new Java class and compiling that, it would be great
to get into the Javac compiler and just get the bytecode
needed for the body of the method we factored out, providing
JavaC with whatever it needs to compile the body of the method.

We do the same with our mutant, say:

```java
void square_loop_body_m(int i, int arr_i) {
    result_i = arr_i + arr_i;
}
```
and check if these are equivalent with Medusa (including state).

I anticipate this increasing efficiency and applicability.

First, we can work with higher level language abstractions (for
loops, for each loops, if/if-else statements, etc) which may be
easier to reason about. Second, we are not having to compile
every single mutant program in its entirety, but rather only fragments
that we are interested in. Both of these speak to an increase in
efficiency. As for applicability, the above example demonstrates
how we can reason about programs that loop over arrays.

In addition to increased efficiency and applicability, this also aligns
with our use cases.

## Mutant Generation as a Game

Another thought: it would be fun to take the game your colleague made
and replace both players with automated systems... Medusa versus Major?
Alice creates mutants automatically that are not killed by the existing test suite,
and Bob may either:
1. offer a "proof" of equivalence, or
2. offer a test that kills the mutant.

Scores can be kept by:
1. Alice gets 1 point for each unkilled mutant she generates
2. Bob gets 1 point for each equivalent mutant hi finds

Then, if we wanna be super cool kids and use ML, we can run some
reinforcement learning to determine Alice's choice function so that she
can try to only generate tricky non-equivalent mutants.
