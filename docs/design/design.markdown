# Medusa Architecture and Design
***Note: This document goes into more depth and generality than we want to
employ, especially in our first build. Idea pruning will be important before we
start to build***
## Architecture

### API
The API reflects Medusa's input/output capabilities and is how it is called into
and out of from other programs. The API takes inputs and feeds them into the
[Pipeline](#the-pipeline).

**TODO:** What should this interface be? We need Rene's help for this

### The Pipeline
The Pipeline is a composition of [Pipes](#pipes) which are referred to as the
*plumbing* of the pipeline. The Pipeline will be invoked by the API and data
will be passed into it. Program data (i.e., bytecode, CFG, AST, ...) will
undergo transformations from one format to another. Along the way information
that may be helpful to different tactics will be computed and stored in a
`Context`.

Here is a brief sketch of the pipeline
```
               +----+           +----+            +----+             +----+
  +-----------/     |----------/     |-----------/     |------------/     |
  | Bytecode        | Bytecode       |  CFG            |    CFG 2         |
  | Processor  ===> |     2     ===> | Processor  ===> | Constraint  ===> |
  |                 |    CFG         |                 | Generators       |
  +-----+-----\     |----------\     +-----------\     |------------\     |
        |      +----+           +----|    |       +----+  ^          +----+
        |                                 |               |
        | Decompile                       |      +--------+--+
        +--------+                        +----> | AST / CFG |
                 |                        +----> | Structure +
                 v                        |      |   Assoc   |
 From API     +-----+   +---------------+ |      +-----------+
------------> | AST |-->| AST Processor |-+
              +-----+   +---------------+
+---------+
| Context | (available throughout to all phases of pipeline)
+---------+
```

Note that we treat the pipeline as if it is acting on a single method at a time.
We should consider generalizing this so that it can act on a variety of classes
at once.

### Program Formats
There are several program formats. These will be produced and modified in the
[Pipeline](#the-pipeline) and stored in the Context as they are
produced/modified.
<!-- TODO -->
#### Java Source Format
We should never see this...

#### Classfile Format
This is a `String` containing the bytes from a binary .classfile as it would be
stored on disk. We rely on ASM for anything .classfile related

#### Bytecode Format
Note to be confused with [Classfile Format](#classfile-format), Bytecode Format
refers to the storage of the bytecode in memory in a semi-structured way
that reflects the syntax of the .classfile specification (probably via ASM).
<!-- TODO: Move to Specs -->

This should include
* Storing each bytecode instruction in an enumerable type (i.e., `enum Bytecode
  {...}`)
* The constant pool of the .classfile, preferably with the *transitive closure*
  of all constants computed (the constant pool will often break up constant
  definitions by factoring out parts of the definition that are used by multiple
  constants; this defines a digraph that must be traversed to get the normal
  form of the constant, and this normal form should be computed AOT)
* **Addressess** for each instruction

Additionally, this should be
* **Easily searchable and modifiable:**
  Certain optimizations (Unboxing for instance) involve searching for patterns
  and modifying the bytecode.

  Some sort of pattern matching?

#### CFG Format
Bytecode will be transformed into a CFG. Medusa should be able to traverse and
modify at the CFG level.

#### CFG Diff Format
Medusa will compute constraints from a method's CFG. To represent a first order
CFG mutant it suffices to include a reference to the original CFG, a reference
to the mutated basic block, and the new basic block to replace it.

We should research the best encoding for CFGDiffs of generalized non-FO mutants.
It may be best to find a minimal subgraph that contains all mutated basic blocks
and that maybe satisfies some additional simplifying properties. This will
exploit the hypothesized locality property of FO CFG mutants formed from FO AST
mutants, namely that FO mutations of an AST will yield CFG mutations that are
tightly clustered in the corresponding mutant.

***Note that updating short circuiting operators will in general alter the
structure of the CFG (remove/add blocks). As such this generalization is very
important to have in the medium to long term development cycle.***

#### Constraint Generation Format
***NOTE: Medusa uses constraint generators to approximate looping structures
(and possibly recursive or nested method calls in the future). Other approaches
have unrolled loops to a pre-specified depth prior to constraint generation
which works in many cases. However, Medusa's strategies and tactics will use
the SUT's structural information when determining how to best approach
constraints.***

***This generalization allows us to***
1. ***Customize the unrolling behavior on a mutant-by-mutant basis.***
2. ***Consider structural information and other data, possibly including feedback
   from Z3 at runtime.***

Medusa needs to be able to generate/update constraints for different constructs
at different times. Thus, rather than issuing a `single` copy of constraints it
makes sense to create and cache the constraints as needed.

For instance, consider modelling the following
```java
  int foo(int a, int b) {
    return add(add(a, b), add(a, b));
  }

  int add(int a, int b) {
    return a + b;
  }
```

This has three distinct copies of `add` and each will need its own sets of
constraints.

Note that the Generator format can be used to model looping and recursive
programs with *arbitrary accurracy*, and this can be done *dynamically* and
*without recomputing*. This seems very powerful.

There are three types of constraint generators

##### Class Constraint Generators
We basically model classes as structs with fields.

##### Method Constraint Generators
These wrap a system of `Block Constraint Generators`.
This should support an API such as

```java
    generate(loopUnroll=10, ...);
```

which can automatically generate a set of constraints. However we can also
dynamically generate the Block Constraints as needed, enabling **middle out**
constraining as well as other potential techniques.

The BlockConstraintGenerators will be arranged to match the nodes of the CFG
from which they are generated---this correspondence should be stored.

##### Block Constraint Generators
These create [`BlockConstraints`](#block-constraints) which contain the bytecode
level constraints of a program. Block constraints will be connected by
EdgeConstraints which will be produced as needed by Medusa.

#### Constraints
This is the final program format. Constraints should be cacheable.
##### Class Constraints
##### Method Constraints
This is a wrapper around a system of `BlockConstraints` and determines
method-level reasoning. Some things tracked by a `MethodConstraint` are:
* Shape of `State`: each basic block transforms state. We know at compile time
  what resources are needed for local state but global state isn't known until
  runtime.
* Handle for input parameters and return value, as well as tracking if an
  exception has been thrown
* Managing `EdgeConstraints`

##### Block Constraints
A `BlockConstraint` represents the bytecode constraints of a single basic block.
The API will include

```java
class BlockConstraint {
  /**
   * Return the initial {@code State} object of this {@code BlockConstraint}
   */
  public State getInitialState();

  /**
   * Return the final {@code State} object of this {@code BlockConstraint}
   */
  public State getFinalState();

  /**
   * Set the {@code trueSuccessor} field to be {@code b} and return the
   * generated EdgeConstraint.
   *
   * @param b  a block constraint to set as the successor of this. Must have the
   *           same parent method
   * @return   the generated EdgeConstraint predicated on this.condition.
   */
  public EdgeConstraint setTrueSuccessor(BlockConstraint b);

  /**
   * Set the {@code falseSuccessor} field to be {@code b} and return the
   * generated EdgeConstraint. This handles state propagation and control flow.
   *
   * @param b  a block constraint to set as the successor of this. Must have the
   *           same parent method
   * @return   the generated EdgeConstraint predicated on this.condition.
   */
  public EdgeConstraint setFalseSuccessor(BlockConstraint b);

}
```

#### Parallel Data
Some data, such as the `Context` and `AST`, will pass through the Pipeline in
parallel to the above-mentioned program formatting. These will also need to be
processed somehow.

### Solver
(name pending)

The solver gets a `Context` with a bunch of program formats (including
`ConstraintGenerators`) as well as a set of mutants. Its only goal is to try to
prove `EQUIV` or `NEQUIV(input_witnessing_non_equivalence)` for each of the
mutants.

A solver uses `Strategies` to determine how to solve. Each `Strategy` determines
a general approach to proving mutant equivalence. A default strategy will be
supplied by Medusa but other strategies may be invoked by the user.

#### Strategies

A strategy uses data computed in the pipeline to choose certain `Tactics` to
operate on each `(Method, Mutant)` pair. Thus a `Strategy`'s execution will look
something like this:
```java
  for (Method method : ctx.methods) {
    for (Mutant mutant : method.mutants) {
      if (property1(method)) {
        if (isFirstOrderCFGMutant(method, mutant)) {
          tryTactics(tactic1, tactic2, tactic3);
        }
        else {
          abort(mutant);  // We cannot reason
        }
      }
      else {
        // ... Test for other things
      }
    }
  }
```

#### Tactics
A tactic is a low level reasoning agent. An atomic tactic is called a `Tactical`
and is not composed of any other tactics. Otherwise a `Tactic` may employ other
`Tactic`s to help it (but these should not loop or have cyclic
dependencies---this should be enforced somehow?)

Each tactic works on a single mutant and tries to prove or disprove mutant
equivalence.

```java
interface Tactic {

    /** Run tactic on mutant {@code m}
     * @param m     a mutant to analyze
     * @return    EQUIV        if we have proven equivalent,
     *           NEQUIV(model) if proven not equivalent,
     *          UNKNOWN(data)  otherwise
     */
    TacticResult run(Mutant m);
}
```

## Design

### The Pipeline's Design
#### Pipes
Each pipe can (roughly) be thought of as a function from [program
format](#program-formats) `F` to format `G`; this can be reflected in the type
system by `Pipe<F, G>` if we deem it worth while but since our pipeline will be
relatively static this shouldn't be necessary.

Pipes come in two forms:

1. `TransformationPipe<F, G> extends Pipe<F, G>` transforms the input format
   into an output format (say a .classfile into ASM bytecode). These are
   responsible for storing the newly created format `G` into the context to be
   looked up later.
2. `ProcPipe<F> extends Pipe<F, F>` collects/processes data about the current
   format that is being inspected. It doesn't update the program format; instead
   it updates the `Context` with relevant data and/or annotates the current
   program format.

Each pipe has a reference to its successor but has no knowledge of its
predecessor. Diagramatically, this looks like:

```
     PIPE 1           PIPE 2
                 +-----            +-----
   -------------/  ---------------/

P1 IN  ===> P1 OUT / P2 IN ===> P2 OUT ==> ...

   -------------\  ---------------\
                 +-----            +-----
```

This is reflected in the `Pipe` interface:

```
public interface Pipe<F, G> {
    public G flow(F input, Context ctx, Z3 z3);
}
```

This may either be recursively invoked or invoked by a `ManualPlumber` (poor
bloke...).

##### BytecodeProcessor
This processes ASM bytecode. This involves
* Performing bytecode simplifications
  - Unboxing
  - Ad hoc method resolution (Integer.toInt(), for example)
* Collect any relevant bytecode statistics
  - Instructions that may not be implementable
  - Instructions that are *definitely* not implementable

##### Bytecode2CFGPipe
Transforms bytecode to CFG.

##### CFGProcessor
This does some processing of the control flow graph. This involves
* Mapping the CFG Structure to AST structure and vice versa:
  - Where are the heads of loops?
  - Where are loop exits/backwards edges?
  - What blocks correspond to short circuiting? This may be important if we
    have a mutant: `if (a && b) {...} => `if (a == b) {...}` as this will update
    CFG structure and we (currently) will not be able to analyze without mapping
    the new CFG to the old CFG, exploiting locallity properties.
##### CFG2ConstraintGenerators
Once the CFG is processed we are ready to use it to create a constraint
generator. We should consider object-closures and create
`ClassConstraintGenerator`s:

```java
public class ClassConstraintGenerator {
  MethodConstraintGenerator[] methodGenerators;
  FieldConstraintGenerator[] fieldGenerators;   // Track class global state
}
```

To constrain a (non-static) method we then need to pass a `this` parameter.
To prove equivalence of method `m` and mutant `n` we then need to write boolean
statement:
```
(assert (= m's-inputs n's-inputs))
(assert (!= m's-execption-bits n's-exception-bits))
(check-sat)
(assert (!= m's-return-value n's-return-value))
(assert (!= m's-this-fields n's-this-fields))
(check-sat)
```
The above is not quite correct but is close enough to give a general idea.
Comparing return values is predicated on exceptional behavior (we don't care
about return values if there was an unhandled exception...) but we *do* care
about state (since even after an exception is thrown a class can catch it and
then query state)

We shouldn't include this much detail in our first implementation but
object-closures will be key to unlocking Java.


##### AST
An AST is either provided or decompiled

##### AST Processor
There is some data that is easily seen in the AST. For instance, Medusa can
detect **simple loops**, loops of the form:

```java
for (int i = CONST_INIT; i < BOUND; i += CONST_INCR){
    // BODY THAT DOES NOT ALTER i
}
```

If these are found then Medusa can unroll these ahead of time. This type of data
can also eventually be used for subgoal generation.

Further, Medusa may be able to detect a `map`-like function:

```java
for (int i = 0; i < array.length(); ++i){
    outputArray[i] = f(array[i]);
}
```

Mutants of this form (in particular, of `f`) depend solely on the operations of
`f` on single elements and this gives us a special case that is easy to prove.


<!-- END PIPELINE -->

### Solver
(name pending)

The solver solves; it is the target of the Pipeline and the brain behind Medusa.
Invoked by the final pipe in the pipeline, the Solver is supplied with a
`Context` containing relevant data (including all forms of the program visited
in the pipeline), a Z3 instance, `ConstraintGenerator`s instances for all
original programs.


