# Medusa Architecture

# Outline
1. [Outline](#outline)
2. [Execution Walkthrough](#execution-walkthrough)
3. [High Level Design Sketch](#high-level-design-sketch)
    - [Data Formatting](#data-formatting)
    - [Medusa's Reasoning Agents: Strategies and Tactics](#medusa-s-reasoning-agents-strategies-and-tactics)
3. [Program Formats](#program-formats)
    - [Original Program Format](#original-program-format)
    - [Mutant Program Format](#mutant-program-format)
4. [Internal Components](#internal-components)
    - [Strategies](#strategies)
    - [Tactics](#tactics)
    - [Contexts](#context)
    - [Class](#class)
    - [Method](#method)
    - [Control Flow Graphs](#control-flow-graph)
    - [Mutant](#mutant)
    - [Diffs](#diff)
    - [Constraint Generators](#constraint-generators)
    - [Constraints](#constraints)
5. [API](#api)
6. [Some Thoughts](#some-thoughts)

# Execution Walkthrough
**NOTE: This is out of phase with newer design ideas!!!** The spirit of design
is the same but after some iterative passes I've fleshed out some terminology
and relations between different moving parts. Thus a **TODO** is to update this
to reflect later sections.


1. **Initialize Medusa Session**
2. **Query Medusa**

    **Inputs:**
    1. _Program/programs (bytecode format)_
    2. _Mutant Set_
        1. How is this specified?
    3. _Optional Data_
    4. _Options_
        1. Strategies and Tactics
        2. Loop Unrolling Depth
        3. Timeout
        4. etc.
3. **Prepass**

    **TODO: This should become the Pipeline**
    1. _Simplify_
        1. Unbox boxed types
        2. Other ad-hoc methods
    2. _Create CFG_
        1. CFG is created for each method
        2. For each mutation, record only the blocks that differ
           (that is, compute the [`CFGDiff`](#cfg-diffs)
    3. _Determine Structure_
        1. Loops?
            - Are loops simple?
            - Can we guarantee termination?
            - Special structure (i.e., are they a *map* or a *fold*?)
        2. Method calls?
           - Call depth
        3. Dependencies?
            1. Other classes?
            2. In-class methods?
            3. Make stubs as needed--these may be inlined or we may
               be able to avoid using them.
        4. Number of branching instructions
            - forward?
            - backwards?
        5. AST, if present, will be used. Otherwise, decompile to get an AST
            1. Map bytecode/CFG to AST structure
            2. Calculate forward/backwards edges

                A *forward edge* is an edge from blocks *B* to *B'* such that
                any execution trace that passes through both blocks must
                always pass through *B* before *B'*. A *backwards edge* the
                dual of a *forward edge*; that is, it is the opposite of a
                forward edge.
    4. _Determine Applicability_
        1. unimplemented instructions?
            - Can we avoid these?
        2. calls to classes we haven't modelled?
            - Can we avoid these?
        3. we may be optimistic with some features
    5. _Create Constraint Generators_
    5. _Record Stubs_
        1. If we see a call to `Foo.bar` then we should enter this into a
          table---we may generate constraints for it later.
4. **Employ Strategies**
5. **Report Results**

# High Level Design Sketch


![Basic Design](media/high-level-design.png)

**_A rough outline of Medusa's preliminary design_**

Roughly, Medusa will be broken into the following parts:

* Data/Program Formatting and Transformation
* Medusa's Reasoning Agents: Strategies and Tactics
* The API

Possibly divided into a separate library, Medusa will have an associated set of
**Research Tools**---these include `SourceAnalyses` which calculate data about
source trees (percentage of methods that can be reasoned about under certain
assumptions, percentage of methods with loops/method calls, etc). The Research
Tools will not affect Medusa's execution directly but will be used employed by
her developers to design strategies and tactics and guide development.

## Source Analysis
Analysis that tell us about certain properties of input program.
Some of them could be (and could be strategies in themself):

1. Number of classes, methods in a class file.
2. Number of instructions in the bytecode of a method.
3. Maximum nesting depth and number of loops inside a method.
3. Is it worth trying to generate constraints and solve them?
4. Heuristic that given a loop outputs the number of times it should be unrolled.
   This may help our strategies in later parts.

## Data Formatting
Medusa will get a program as input and will *reflect* its structure in
[`ConstraintGenerators`](#constraint-generators) and [`Constraints`](#constraints).
A `ContrloFlowGraph` is equivalent to the original program (all execution data
can be retrieved) while a `Constraint` may be viewed as an *approximation* of the
original program. The `ConstraintGenerator`, then, is an *approximator* of the
program and can approximate looping programs with arbitrary precision (assuming
that other than the presence of loops the program can be modelled).

This is just a thought but may be useful to us. Like LLVM has an IR which is treated
as a common currency between all the various analysis and optimizations, we could do
the same. An IR for e.g. could have all the necessary information like the source code,
AST, and the bytecode of the mutant, which is serialized/deserialized to disk.
This will increase the size of each mutant but would save on recomputing various
important details regarding a mutant.
I'm thinking of a binary file with sections denoted to each such information about the mutant
and a module that can read/write to them.
This could then be made more general, and not only worry about Java files.
We could have a common IR (like LLVM and many other compilers).
Every source/bytecode is converted to this IR and this is passed between various stages of MEDUSA.
We should attempt (not now) to a general format and move away from the JVM specific arguments.

### The Pipeline
There is a central flow of data, starting with a classfile (or perhaps with Java
source code) and ending with a set of constraints. This can be visualized as

                                  /    CFG   \
    CLASSFILE => ASM BYTECODE => |            | => CONSTRAINT GENERATORS ==> CONSTRAINTS
                                  \ CFGDiffs /

Mutant information will be injected into the Pipeline at some point and this
will be transformed into a set of [`CFGDiffs`](#cfg-diffs) which will in turn be
transformed into constraint generators. These mutant constraint generators
differ from the original program's constraint generators in that the entirety of
the constraints need not be generated, only the constraints for the mutated
blocks.

### Transformers
As data goes down the Pipeline there are certain places where simplification may
take place. For instance, to unbox boxed types we should transform at the ASM
Bytecode level:

                   +----------+                   +-----------+
    ASM BYTECODE ==| Unboxing |==> ASM BYTECODE ==| CFG Maker |==> CFG ==> ...
                   +----------+                   +-----------+

These would be composable.

### Parallel Data
While the Pipeline comprises the most important segment of Medusa's program
transformations, there is some program data that may be passed in or derived
along the way that propagates in parallel to the rest of the pipeline.

#### Context
Throughout her work Medusa will need to store data for later use. For instance,
as Medusa moves down the Pipeline she may discover that a block is the head or
tail of a loop, and this will affect her future decisions.

All of this can be stored into a `Context` object which is similar to a
compiler's `Context`.

#### AST
A program's AST, while not necessary for Medusa's operation,  will provide extra
information for a [`Strategy`](#strategies) to act on. An AST can be passed in
or generated along the way, and by the time `Constraints` are generated this
should be available for Medusa to reference.

How should this AST be represented, like a Java specific AST?
Clang for example can dump a complete textual representation of a C/C++ source file,
which is language agnostic.

#### Annotated CFG
After the CFG is built all subsequent phases of the Pipeline depend on it. It
will be useful to have a way to annotate the CFG and have this annotated graph
be used directly by the different pieces of Medusa.

The exact placement of this (perhaps as a `Block ==> Annotation` map in
`Context`?) is TBD, but if we are bringing the CFG lib into Medusa then we can
add annotation support directly.

## Medusa's Reasoning Agents: Strategies and Tactics
Medusa's `Strategies` and `Tactics` will be used to implement Medusa's higher
reasoning---that is, they will be used to decide how Z3 should reason about a
program. They are in essence *heuristics* detailing what approaches Medusa
thinks is the most efficient or will have the best results.

### High Level Reasoning: Strategies
Z3 will be reasoning about `Constraints` but Medusa will use other formats of
the program to guide Z3's reasoning. This external guidance will take the form of
[`Strategies`](#strategies). A `Strategy` will take data about the world,
say the CFG, the AST, and perhaps some derived data such as the call depth
or the bytecode dependency graph of a method, and use this information to
determine how best to guide Z3. `Strategies` invoke the use of Medusa
[`Tactics`](#tactics) to perform actual computations with Z3. See the
[Strategies](#strategies) section for an example.

### Middle Level Reasoning: Tactics
Once a `Strategy` has chewed on a program and its relevant data for a bit it will
decide on a series of `Tactic`s to employ (not to be confused with
Z3 tactics). A `Tactic` will be some atomic or near-atomic algorithm that is
executable on a single mutant. See the section on [Tactics](#tactics) for an
example.

### Low Level Reasoning
At the bottom of the chain lies Z3. Z3 executes all formal proofs, and while
Medusa may nudge it in a certain direction or only give it certain data, Z3 is
responsible for any formal reasoning.

# Program Formats
There will be several transformations that take place on our program. We list
the formats, in order of creation, that a program will be in. The formats differ
for original programs and mutants.

## Original Program Format
Note that item 1 and perhaps item 2 will be handled by whoever is calling in to
Medusa. However it shouldn't bee too hard to support passing in a file name and
a list of mutated files for Medusa to check. Medusa will probably start simple
and expand the API as needed.

1. **Java Classfile:** this will be read in and converted to an internal form

2. **Bytecode:** the result of reading a classfile into memory (via ASM)

3. **Control Flow Graph:** store the ASM bytecode into a control flow graph

4. **AST:** if provided this will be matched with the CFG generated in step 4;
   otherwise Medusa will attempt to decompile to obtain an AST. This is optional
   and allows for AST hints

5. **Constraint Producers:** mirror the CFG structure for block/edge
   constraints---this will allow for loop unrolling. Likewise a method may be
   called more than once (say via recursion or in a situation such as
   ```java
   int blah() {
       int x = foo(1);
       int y = foo(2);
       return foo(x + y);
   }
   ```
  When inlining `foo` it we will need to generate identical constraints multiple
  times.

6. <a name="program-format-constraint-producers"></a> **Constraints:** this is a
   simplified, first-order approximation of a program.

## Mutant Program Format
1. **Input:** there are multiple ways to specify a mutant, either via source
   code, or by mutant operator, or by mutated bytecode. In any event this will
   be translated into a [`CFGDiff`](#cfg-diffs) which will be used to create a
   `CFGMutant`.

2. **CFGMutant:** This is a representation of the Mutant in CFG format. In
   particular this uses the knowledge that a method `m` is a mutant of some
   other method `p` and only the `CFGDiff` is specified. This provides enough
   data to completely reconstruct `m` and also makes it easy to fork
   constraints.

3. **Constraint Producers:** mirror the CFG structure of the mutant. When a
   block or edge constraint is produced it will return a cached version of the
   original program's constraint if the block us unmutated or a new version if
   the block is mutated or if the state may differ from the original output.

   The exact way that this happens is TBD

4. **Constraints:** see [Original Program Format](#program-format-constraint-producers),
   part 5.

Again, we should discuss this in context of a IR format (which for now could be simply a file with the bytecode and source).

# Internal Components
## Strategies
A session will have a single strategy that determines how Medusa will try to
solve the mutant equivalence problem. This strategy may be composed of several
sub-strategies predicated on certain properties. Eventually strategies bottom
out into [`Tactics` ](#tactics).

For example, a strategy may look like the following (put into scripting language
for ease of reading but these may be hard coded)
```python
# Medusa Sample Strategy
MAX_CALL_DEPTH = 1
if call_depth(method) > MAX_CALL_DEPTH:
    abandon("Call depth %d exceeded max: %d", callDepth(method), MAX_CALL_DEPTH)

preprocess(INLINE, UNBOX)

if (has_loops(method)):
    nested, unnested = filter(mutants, has_ancestor(LOOP))
    nested_results = use_tactic(MIDDLE_OUT_EXPAND_UP, method, nested)
    unnested_results = use_tactic(MIDDLE_OUT_EXPAND_DOWN, method, unnested)
    results = nested_results + unnested_results

else:
    results = use_tactic(FORK, method, mutants)

print("Equivalent:     " + results.equivalent)
print("Non-Equivalent: " + results.nonEquivalent)
print("Unknown:        " + results.unknown)
```

Here a `Strategy` is applied to a tuple `(Method, MutantSet)` and says that if
we have too deep of a call depth (i.e., any method we call calls another
method), then we should abandon with an informative message. Otherwise we
preprocess our code, inlining method calls and unboxing any boxed primitives.
Then, if the method has any loops we will test if a mutant is inside of a loop
or not and use different tactics accordingly.
Otherwise, if the method contains no loops we use the Forking and Cacheing
tactic. Finally we collect results and print what we have found.

## Tactics
A `Tactic` may be composable from subtactics but in general `Tactic`s should be
thought of as atomic. `Tactic`s are invoked by a [`Strategy`](#strategy)
and used to execute calculations involving Z3.

Each tactic operates on a single method constraint and a single mutant's
constraint:

```python
# Medusa MIDDLE_OUT_EXPAND_DOWN Tactic
# This tactic fixes the top block of a mutant and expands downward. If a loop
# tail is reached the loop is unrolled further, up to a total of `MAX_UNROLL`
# times.

# A Parameter for this tactic---can be set externally
MAX_UNROLL = 10

def apply(original, mutant):
    """
    Apply this tactic to the original and the mutant
    :param original: original program
    :param mutant: the mutated program
    :returns: one of EQUIV, NOT_EQUIV(witnessing-args), UNKNOWN([message])
    """
    z3.push()
    bndry_top = mutant.get_top_boundary()  # Should be a single block, fixed
    block_top = bndry_top[0]
    bndry_bot = mutant.get_bot_boundary()  # May be multiple blocks
    result = None
    while result is None:
        new_bndry = set()
        for block in bndry_bot:
            if is_loop_tail(block):
                unroll(block, MAX_UNROLL)    # This is magic for now
            else:
                z3.assert(block.out_edges)
                new_bndry.add_all(block.successors)
        if new_bndry.is_empty():
            # We haven't propagated anything
            break
        bndry_bot = update_boundaries(boundary_bot, new_bndry)

        z3.push()
        for (orig_block, mut_block) in bndry_bot:
            # Here we are being loose with state equality. Really we want to
            # test for state equivalence which is same return value/exception
            # codition OR state equivalence.
            z3.assert(z3.mkNeq(orig_block.final_state, mut_block.final_state))

        status = z3.check_sat()
        if status is SAT:
            continue
        elif status is UNSAT:
            result = EQUIVALENT
        else:
            result = UNKNONW

        z3.pop()    # Pop the equivalence testing scope
    z3.pop()
```

Some thought should be put in to how we can mix and match tactics, but it is
possible that this can be done at the Strategy level.

## Context
The `Context` is responsible for tracking data about a session. This involves
mapping entities in our object language (classes, methods, etc) to their
various generated information, including:

- Constraint Generators
- Constraints
- Mutant Set

This is to be developed further.

## Class
A `Class` represents a class that we are reasoning about. We should spend some
time developing what traits of a class should be tracked, how we should separate
`static` from non-`static` methods, etc.

A class will have a lookup table to each of its methods and fields---it may be
possible to use ASM's Class representation, or possibly wrap it to provide extra
data. This is TBD

## Method
A `Method` represents a Java method. It comprises some data such as if it is
static, final, private, etc., as well as its signature, return type, which class
it belongs to, overriding data, etc.

In addition to the above metadata a `Method` will have a reference to its
bytecode and its generated `CFG`.

## Control Flow Graph
A `CFG` is an organized form of the executable code of a [Method](#method) and
is the primary source code format Medusa uses to generate constraints.

## Mutant
A mutant represents a mutated [`Method`](#method) in our source code. This class
is abstract and will be extended by `CFGMutant` and possibly by `ASTMutant`.
These may possibly be `FirstOrderCFGMutant`, etc..., though whether this is set
by a flag or reflected in our type system is yet to be determined.

A `Mutant` should comprise a [`Method`](#method) or a
[`CFG`](#control-flow-graph) and an appropriately typed [`Diff`](#diff) (i.e., a
`CFGMutant` will have a [`CFGDiff`](#cfg-diffs))

## Diff
Represents a diff of a program in some format

### CFG Diffs
A `CFGDiff` represents the difference in the CFG from the original program and
the mutant and contains a reference back to the original `CFG`. A `CFGMutant`
will comprise a `CFGDiff` and some meta data.

### AST Diffs
Represents the mutation of the AST---this may or may not be used

## Constraint Generators
We may need more than one instance of a constraint; for example, loop
unrolloing requires the loop body be constrained multiple times.

Rather than creating constraints directly from source code objects (i.e.,
classes, methods, basic blocks, etc) we will create generators that can create
multiple parametrized versions of the constraint.

### Block Constraint Generator
Generates `BlockConstraint` instances. Some thought needs to be put into exactly
how we will parametrize these. For instance, consider the following code

```java
int x = 0;
for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 10; ++j) {
        x += i * j;
    }
}
```

If we try to unroll the inner loop then we need to know where control flow
should continue to after the loop is done executing: in particular, we need to
know which iteration of the outer for loop we are in.

### Method Constraint Generator
Generates `MethodConstraint` instances. For now we will not support method
unrolling (similar to loop unrolling) but we should think about how we will
handle method inlining.

## Constraints
We will be generating constraints to be asserted to Z3 instances. The various
`Constraint` classes will be wrappers around these.

### Class Constraints
### Method Constraints
### Block Constraints

# API
Medusa will be configurable and extensible but should have a simple API to
present to the world. The user should be able to pass in a class and some
mutants and Medusa will just figure out what it can.

The exact format of what Medusa can accept is an open question. There are also
questions as to what extra data it can optionally receive (AST, etc)

# Some Thoughts
## Factor out common processes
* Block constraints and edge constraints should be generated AOT.
    - This is especially important for the original method since this will be
      used repeatedly.
    - Less important for mutants since they are constrained once and no savings
      are to be had
* If possible, all block constraints and edge constraints of _original method_
  should be constrained AOT
    - The original method will never change

## Combining Middle Out and Constraint Forking
It will be nice to combine these two approaches. For instance, if there is a
subgraph of a CFG inside of a loop that is mutated we can use constriant
forking on the subgraph and use middle out to propagate downwards.

## Rolling Down vs Rolling Up
When we unroll a loop we may either assert the input state of the top block
of the first iteration or we may constrain the final states of the bot blocks
of the last iteration---this second option assumes the loop terminates. The
only difference between these two constraints is the initial/final state
assertions. Thus with relatively little extra overhead we can try to unroll
both up and down.

## Block Reordering and Refining
This is definitely in the *future work* category but it is worth putting
down on paper in case there is a blindingly obvious application.

We can *refine* a basic block by splitting it. Thus if there are unmodeled
instructions in the first half of a basic block that are not present in the
second half of a basic block we may be able to model the second half of the
basic block.

More interesting, we can rearrange instructions (if they commute with other
instructions, say) and we might be able to ignore troublesome sequences of
instructions. This will involve a dependency analysis of the bytecode but.
Consider the following java method:

```java
    int blah(int a, int b) {
      int x = a + b;
      System.out.println("Some message");
      x *= 2;
      return x;
    }
```

which has bytecode

```asm
  int blah(int, int);
    Code:
       0: iload_1
       1: iload_2
       2: iadd
       3: istore_3
       4: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #3                  // String Some message
       9: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      12: iload_3
      13: iconst_2
      14: imul
      15: istore_3
      16: iload_3
      17: ireturn
```

The calls to `println` do not depend in any way on any values so as long as they
are not mutated we needn't model them. Thus we could rearrange and refine our basic
block into:

```asm
  int blah(int, int);
    BLOCK 1:
       4: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #3                  // String Some message
       9: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
    BLOCK 2:
       0: iload_1
       1: iload_2
       2: iadd
       3: istore_3
      12: iload_3
      13: iconst_2
      14: imul
      15: istore_3
      16: iload_3
      17: ireturn
```

and model the second block.


## Some thoughts
In my opinion, we should differentiate between what is really fundamental to MEDUSA and what
is language specific. Right now MEDUSA works only with JVM bytecode, but this need not be always
the case. It would be simpler to separate away the non-essential (reading/writing bytecode, AST etc)
than what is essential like being able to generate and solving constraints, the strategies and tactics used. I would be up to discuss about this.

<style type="text/css">
    ol ol    {list-style-type: lower-alpha; }
    ol ol ol {list-style-type: lower-roman;}
    h4 {color: #777776; font-style: italic;}
</style>

