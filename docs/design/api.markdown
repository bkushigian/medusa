# Medusa APIs

This document details Medusa's APIs, and is subject to change with design iterations.

## Context

This class stores information that may be used later and is shared between the phases of pipeline. Therefore it needs to be general enough to store and retrieve information.

```
Context
    @return true if property set, false otherwise
    bool setProperty(String propname, String propval)
    String getProperty(String propname)
```

## CFG

We can use the CFG representation provided by the controlflow library, as it provides all basic functionalities. It will be augmented with functions important to our use case.

## Code

Although the controlflow library provides a neat CFG representation of the bytecode, it still requires one to go over all instructions in the bytecode to find the ones that belong to the current bytecode. This is inefficient (in case of multiple block traversals), and error prone (wrong loop conditions, etc). This class returns the instructions in a given basic block, and may cache results. It may be augmented further with other bytecode operations. This may also allow peephole optimizations on the bytecode.

```
Code
    Bytecode[] getBytecode(BasicBlock bb)
```

## Constraint Generation Format and Queries

It will be immensely helpful to pin point the constraints that were generated from a particular bytecode section. For e.g. if one wants to find the branching and straight line constraints, it should not be more difficult than a simple API call.

```
ConstraintQuery
    Constraint[] getConstraints(Class class)
    Constraint[] getConstraints(Method method)
    Constraint[] getConstraints(BasicBlock bb)
    Constraint   getConstraints(Instruction inst)

Constraint
    @TODO: what is a great way to return constraints? sexps?
    getStraightlineConstraint()
    getBranchingConstraint()
```

## Class Constraint Generators
### Method Constraint Generators
### Basic Block Constraint Generators

This class takes a class in bytecode or CFG form and generates constraints for it.
@TODO: need to rethink this.

```
ClassConstraintGen
    ConstraintQuery generateConstraints(Context context, String path)
    ConstraintQuery generateConstraints(Context context, CFG cfg)
```

## Constraint

This is an important class and determines how easy/difficult it is to map and generate constraints from bytecode.
It should provide a simple interface which allows representation of various constraints without being tied to a specific solver tool.
For e.g. if we want to assert a constraint that two symbols `a` and `b` are equal, we may say something like `Constraint.makeEqual(a, b)`.
This class will maintain constraints in memory which can either be serialized or turned to actual constraints for various solvers.
This class could use a factory pattern.

```
Constraint
    Constraint makeEqual(Symbol a, Symbol b)
    ...
    Constraint makeIfThenElse(Constraint cond, Constraint true, Constraint false)
```

## Solver

```
Solver
    Status solve(Constraint[] constraints)
    Model getModel()
```
