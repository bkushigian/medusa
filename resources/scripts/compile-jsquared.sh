#!/usr/bin/env bash

# Get script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Move to the test-src directory and compile the original Triangle program
cd "$DIR/../java/test-src"
javac "jsquared21/JSquared21Samples.java" -d "../test-classes"

# Compile each of the mutants
for mdir in `ls jsquared21/mutants`; do
    source="jsquared21/mutants/$mdir/JSquared21Samples.java"
    dest="../test-classes/jsquared21/mutants/$mdir"
    echo "Compiling mutant $mdir via \"javac $source -d $dest\""
    javac $source -d $dest
done

echo "Done"
