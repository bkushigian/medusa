#!/usr/bin/env bash

# Get script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Move to the test-src directory and compile the original Triangle program
cd "$DIR/../java/test-src"
javac "triangle/Triangle.java" -d "../test-classes"

# Compile each of the mutants
for mdir in `ls triangle/mutants`; do
    source="triangle/mutants/$mdir/triangle/Triangle.java"
    dest="../test-classes/triangle/mutants/$mdir"
    echo "Compiling mutant $mdir via \"javac $source -d $dest\""
    javac $source -d $dest
done

echo "Done"
