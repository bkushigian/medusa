#!/usr/bin/env bash

# Get script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Move to the test-src directory and compile the original Triangle program
cd "$DIR/../java/test-src/fork-strat-tests"
pwd
javac "ForkStrategyTest.java" -d "../../test-classes/fork-strat-tests"

# Compile each of the mutants
for mdir in `ls mutants`; do
    pwd
    source="mutants/$mdir/ForkStrategyTest.java"
    dest="../../test-classes/fork-strat-tests/mutants/$mdir"
    echo "Compiling mutant $mdir via \"javac $source -d $dest\""
    javac $source -d $dest
done

echo "Done"

# Move to the test-src directory and compile the original Triangle program
cd "../fork-strat-tests2"
pwd
javac "ForkStrategyTest.java" -d "../../test-classes/fork-strat-tests2"

# Compile each of the mutants
for mdir in `ls mutants`; do
    pwd
    source="mutants/$mdir/ForkStrategyTest.java"
    dest="../../test-classes/fork-strat-tests2/mutants/$mdir"
    echo "Compiling mutant $mdir via \"javac $source -d $dest\""
    javac $source -d $dest
done

echo "Done"
