public class CallDepths {
  public int depth0() {
    return 0;
  }

  public int depth1() {
    depth0();
    return depth0() + 1;
  }

  public int depth2() {
    depth1();
    depth0();
    return depth1() + 1;
  }

  public int depth3() {
    depth0();
    depth0();
    depth1();
    depth2();
    return depth2() + 1;
  }

  public int depth4() {
    depth0();
    depth0();
    depth1();
    depth2();
    depth3();
    depth2();
    return depth3() + 1;
  }

  public int recursive() {
    return recursive();
  }

  public int callsRecursive() {
    return recursive();
  }
}
