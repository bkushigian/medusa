//package triangle;

/**
 * An implementation that classifies triangles.
 */
public class TriangleModified {
    /**
     * These constants gives the type of the triangle.
     */
    public static final int Type_INVALID = 0,
            Type_SCALENE = 1,
            Type_EQUILATERAL = 2,
            Type_ISOSCELES = 3;

    /**
     * This static method does the actual classification of a triangle, given the lengths
     * of its three sides.
     */
    public static int classify(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return Type_INVALID;
        }
        int trian = 0;
        if (a == b) {
            trian = trian + 1;
        }
        if (a == c) {
            trian = trian + 2;
        }
        if (b == c) {
            trian = trian + 3;
        }
        if (trian == 0) {
            if (a + b <= c || a + c <= b || b + c <= a) {
                return Type_INVALID;
            } else {
                return Type_SCALENE;
            }
        }
        if (trian > 3) {
            return Type_EQUILATERAL;
        }
        if (trian == 1 && a + b > c) {
            return Type_ISOSCELES;
        } else if (trian == 2 && a + c > b) {
            return Type_ISOSCELES;
        } else if (trian == 3 && b + c > a) {
            return Type_ISOSCELES;
        } else {
            return Type_INVALID;
        }
    }
}
