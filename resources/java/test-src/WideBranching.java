public class WideBranching {
  public int maxOfFour(int a, int b, int c, int d) {
    if (a < b) {
      if (b < c) {
        if (c < d) {
          return d;
        }
        else {
          return c;
        }
      }         // b < c
      else {
        if (b < d) {
          return d;
        }
        else {
          return b;
        }
      }         // !(b < c)
    }           // a < b
    else {
      if (a < c) {
        if (c < d) {
          return d;
        }
        else {
          return c;
        }
      }
      else {
        if (a < d) {
          return d;
        }
        else {
          return a;
        }
      }
    }
  }
}
