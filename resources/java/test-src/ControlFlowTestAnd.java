class ControlFlowTestAnd {
    /**
     * this returns -1 if there are any duplicated entries. Otherwise
     * it retuns the max
     */
    public int f(int a, int b, int c){
        int result = -1;
        if (a < b && b < c){
            result = c;
        }
        if (b < a && a < c){
            result = c;
        }
        if (b < c && c < a){
            result = a;
        }
        if (c < b && b < a){
            return a;
        }
        if (a < c && c < b){
            result = b;
        }
        if (c < a && a < b){
            result = b;
        }
        return result;
    }
}
