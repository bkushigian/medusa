public class PipeASM2CFGTestClass {
    public int add(int a, int b) {
        return a + b;
    }

    public boolean and(boolean a, boolean b) {
        return a && b;
    }

}