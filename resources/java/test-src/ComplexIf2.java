public class ComplexIf2 {
    int f(int a, int b) {
        if (a > b) {
            return a + 1;
        } else if (b < a+2) {
            return b;
        } else {
            return a - b;
        }
    }
}
