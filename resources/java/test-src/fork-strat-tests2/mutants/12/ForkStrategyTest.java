// MUTANT 12
public class ForkStrategyTest { 
  int maxOfThree(int a, int b, int c) {
    int biggest = a;
    if (b > biggest) {
      biggest = b;
    }
    if (c > biggest) {
      biggest += c - biggest; // Very artificial equivalent mutant :)
    }
    return biggest;
  }
}
