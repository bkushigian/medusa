// MUTANT 16
public class ForkStrategyTest { 
  int maxOfThree(int a, int b, int c) {
    int biggest = a;
    if (b > biggest) {
      biggest = b;
    }
    if (c > biggest) {
      biggest = a;
    }
    return biggest;
  }
}
