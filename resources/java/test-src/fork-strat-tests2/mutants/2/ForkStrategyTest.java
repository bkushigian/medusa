public class ForkStrategyTest {
  int maxOfThree(int a, int b, int c) {
    int biggest = c;
    if (b > biggest) {
      biggest = b;
    }
    if (c > biggest) {
      biggest = c;
    }
    return biggest;
  }
}
