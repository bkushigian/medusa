// MUTANT 19
public class ForkStrategyTest { 
  int maxOfThree(int a, int b, int c) {
    int biggest = a;
    if (b > biggest) {
      biggest += b - biggest;
    }
    if (c > biggest) {
      biggest = c;
    }
    return biggest;
  }
}
