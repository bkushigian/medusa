public class Unboxing {

  Integer integerUnboxing() {
    int prim = 1;
    Integer boxed = prim;
    int unboxed = boxed;
    return boxed;
  }

  Boolean booleanUnboxing() {
    boolean prim = true;
    Boolean boxed = prim;
    boolean unboxed = boxed;
    return boxed;
  }

  Float floatUnboxing() {
    float prim = 1.0f;
    Float boxed = prim;
    float unboxed = boxed;
    return boxed;
  }

  Double doubleUnboxing() {
    double prim = 1.0;
    Double boxed = prim;
    double unboxed = boxed;
    return boxed;
  }

  Byte byteUnboxing() {
    byte prim = 1;
    Byte boxed = prim;
    byte unboxed = boxed;
    return boxed;
  }

  Short shortUnboxing() {
    short prim = 1;
    Short boxed = prim;
    short unboxed = boxed;
    return boxed;
  }

  Long longUnboxing() {
    long prim = 1;
    Long boxed = prim;
    long unboxed = boxed;
    return boxed;
  }

  Long longUnboxingWithLDC() {
    long prim = 12345;
    Long boxed = prim;
    long unboxed = boxed;
    return boxed;
  }

  Character charUnboxing() {
    char prim = 1;
    Character boxed = prim;
    char unboxed = boxed;
    return boxed;
  }
}
