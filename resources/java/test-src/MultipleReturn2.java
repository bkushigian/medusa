// comparing MultipleReturn with this mutant should give satisfiable
public class MultipleReturn2 {
    int f(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }
}
