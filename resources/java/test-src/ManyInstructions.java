public class ManyInstructions {
    public int instructions(int a, int b){
        int i = 32;
        int j = 1024 + i;
        switch(a){
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            default:
                return i + j;
        }
    }
}
