public class LongSamples {
  public long add(long a, long b) {
    return a + b;
  }

  public long max(long a, long b) {
    if (a < b) return b;
    return a;
  }

  public long max(long a, long b, long c) {
    long max = a;
    if (a < b) max = b;
    if (b < c) max = c;
    return max;
  }

  public long max(long a, long b, long c, long d) {
    if (b > a) a = b;
    if (c > a) a = c;
    if (d > a) a = d;
    return a;
  }
}
