/**
 * A collection of samples taken from an intro java GitHub repo by user
 * jsquared21:
 * <link>https://github.com/jsquared21/Intro-to-Java-Programming</link>
 */
public class JSquared21Samples {

  /**
   * Given a float `t`, return -1 for frozen, 0 for liquid, 1 for gas
   */
  int waterState(float t) {
    if (t <  32.0f) return -1;   // Solid
    if (t < 212.0f) return 0;    // Liquid
    return 1;                    // Gas
  }

  /**
   * Given the month as an int (1 for Jan, 2 for Feb, ...) and a boolean
   * answering the question "is this a leapyear?", return the number of days in
   * the year.
   */
  int daysInMonth(int m, boolean leapyear) {
    if (m < 1 || m > 12)
      return 0; // error
    if (m > 7) {
      if (m % 2 == 0) {
        return 31;
      }
      return 30;
    } else if (m % 2 == 0) {
      if (m == 2) {
        return 28 + (leapyear ? 1 : 0);
      }
      return 30;
    }
    return 31;
  }

  float fileTaxesStatus0(float income) {
    float tax = 0;
    tax += (income <= 8350) ? income * 0.10f : 8350 * 0.10f;
    if (income > 8350)
      tax += (income <= 33950) ? (income - 8350) * 0.15f :
        25600 * 0.15f;
    if (income > 33950)
      tax += (income <= 82250) ? (income - 33950) * 0.25f :
        48300 * 0.25f;
    if (income > 82250)
      tax += (income <= 171550) ? (income - 82250) * 0.28f :
        89300 * 0.28f;
    if (income > 171550)
      tax += (income < 372950) ? (income - 171550) * 0.33f :
        201400 * 0.33f;
    if (income > 372950)
      tax += (income - 372950) * 0.35f;
    return tax;

  }
  /**
   * Given the tax status and the income, compute total income taxes.
   * Status:
   *  0: single filer
   *  1: married filer jointly or qualifying widow(er)
   *  2: married separately
   *  3: head of household
   */
  int fileTaxes(int income, int status) {
    // https://github.com/jsquared21/Intro-to-Java-Programming/blob/master/Exercise_03/Exercise_03_13/Exercise_03_13.java

    // Compute tax
    float tax = 0;
    if (status == 0) {
        tax += (income <= 8350) ? income * 0.10f : 8350 * 0.10f;
        if (income > 8350)
          tax += (income <= 33950) ? (income - 8350) * 0.15f :
            25600 * 0.15f;
        if (income > 33950)
          tax += (income <= 82250) ? (income - 33950) * 0.25f :
            48300 * 0.25f;
        if (income > 82250)
          tax += (income <= 171550) ? (income - 82250) * 0.28f :
            89300 * 0.28f;
        if (income > 171550)
          tax += (income <= 372950) ? (income - 171550) * 0.33f :
            201400 * 0.33f;
        if (income > 372950)
          tax += (income - 372950) * 0.35f;
      } else if (status == 1) {
        tax += (income <= 16700) ? income * 0.10f : 16700 * 0.10f;
        if (income > 16700)
          tax += (income <= 67900) ? (income - 16700) * 0.15f :
            (67900 - 16700) * 0.15f;
        if (income > 67900)
          tax += (income <= 137050) ? (income - 67900) * 0.25f :
            (137050 - 67900) * 0.25f;
        if (income > 137050)
          tax += (income <= 208850) ? (income - 137050) * 0.28f :
            (208850 - 137050) * 0.28f;
        if (income > 208850)
          tax += (income <= 372950) ? (income - 208850) * 0.33f :
            (372950 - 208850) * 0.33f;
        if (income > 372950)
          tax += (income - 372950) * 0.35f;
      } else if (status == 2) {
        tax += (income <= 8350) ? income * 0.10f : 8350 * 0.10f;
        if (income > 8350)
          tax += (income <= 33950) ? (income - 8350) * 0.15f :
            (33950 - 8350) * 0.15f;
        if (income > 33950)
          tax += (income <= 68525) ? (income - 33950) * 0.25f :
            (68525 - 33950) * 0.25f;
        if (income > 68525)
          tax += (income <= 104425) ? (income - 68525) * 0.28f :
            (104425 - 68525) * 0.28f;
        if (income > 104425)
          tax += (income <= 186475) ? (income - 104425) * 0.33f :
            (186475 - 104425) * 0.33f;
        if (income > 186475)
          tax += (income - 186475) * 0.35f;
      } else if (status == 3) {
        tax += (income <= 11950) ? income * 0.10f : 11950 * 0.10f;
        if (income > 11950)
          tax += (income <= 45500) ? (income - 11950) * 0.15f :
            (45500 - 11950) * 0.15f;
        if (income > 45500)
          tax += (income <= 117450) ? (income - 45500) * 0.25f :
            (117450 - 45500) * 0.25f;
        if (income > 117450)
          tax += (income <= 190200) ? (income - 117450) * 0.28f :
            (190200 - 117450) * 0.28f;
        if (income > 190200)
          tax += (income <= 372950) ? (income - 190200) * 0.33f :
            (372950 - 190200) * 0.33f;
        if (income > 372950)
          tax += (income - 372950) * 0.35f;
    } else {
      return -1; // error
    }
    return (int)tax;
  }

  /**
   * 1: rock, 2: paper, 3:scissor
   * @return 1 if p1 wins, 2 if p2 wins, 0 otherwise
   */
  int rockPaperScissor(int p1, int p2) {
    if (p1 == p2) return 0;
    if ((p1 + 1) % 3 == p2) return 2;
    return 1;
  }

  boolean onLine(float x0, float y0, float x1, float y1, float x2, float y2) {
    //https://github.com/jsquared21/Intro-to-Java-Programming/blob/master/Exercise_03/Exercise_03_34/Exercise_03_34.java
      return !(((x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0)) > 0.0f ||
          ((x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0)) < 0.0f ||
          (x2 < x0) || (y2 < y0) || (x2 > x1) || (y2 > y1));
  }

}
