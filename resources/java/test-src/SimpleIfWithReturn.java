public class SimpleIfWithReturn {
    public int f(int a) {
        if (a <= 2 + a) {
            if (a == 0) {
                return 0;
            } 
            return 2;
        }
        return 1;
    }
}
