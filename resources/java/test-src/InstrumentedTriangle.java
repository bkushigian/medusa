public class InstrumentedTriangle {

    // This is like the original triangle method but
    // here we can query information about which return
    // statement was executed.
    public static int classify(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return 0;
        }
        int trian = 0;
        if (a == b) {
            trian = trian + 1;
        }
        if (a == c) {
            trian = trian + 2;
        }
        if (b == c) {
            trian = trian + 3;
        }
        if (trian == 0) {
            if (a + b <= c || a + c <= b || b + c <= a) {
                return 2;
            } else {
                return 3;
            }
        }
        if (trian > 3) {
            return 4;
        }
        if (trian == 1 && a + b > c) {
            return 5;
        } else if (trian == 2 && a + c > b) {
            return 6;
        } else if (trian == 3 && b + c > a) {
            return 7;
        } else {
            return 8;
        }
    }
}
