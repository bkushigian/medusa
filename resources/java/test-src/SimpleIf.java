public class SimpleIf {
    int f(int a, int b) {
        int c;

        if (a > 2) {
            c = 3;
        } else {
            c = 4;
        }

        return c;
    }
}
