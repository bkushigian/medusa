public class StatefulExample {
    public int statefulMax(int a, int b, int c){
        int result = a;
        if (a < b){
            result = b;
        }
        if (b < c){
            result = c;
        }
        if (c < a){
            if (b < a){
                result = a;
            }
        }
        return result;
    }
}
