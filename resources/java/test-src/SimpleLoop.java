class SimpleLoop {
    public static int sumUpto(int n) {
        int sum = 0;

        for (int i = 1; i < n; ++i) {
            sum += 1;
        }

        return sum;
    }

    // mutant that decrements sum var inside loop
    // is a valid FO mutant
    public static int sumUpto1(int n) {
        int sum = 0;

        for (int i = 1; i < n; ++i) {
            sum -= 1;
        }

        return sum;
    }

    // mutant that adds one extra instruction
    // to test different number of instructions
    // which do not change the control flow/ FO mutant property
    // is a valid FO mutant
    public static int sumUpto2(int n) {
        int sum = 0;

        for (int i = 1; i < n; ++i) {
            sum -= 1;
            sum += 0;
        }

        return sum;
    }
}
