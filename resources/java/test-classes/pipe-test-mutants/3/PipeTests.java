public class PipeTests {
    public Integer plus_boxed(Integer a, Integer b) {
        return b + a;
    }

    public int plus_unboxed(int a, int b){
        return b + a;
    }
}
