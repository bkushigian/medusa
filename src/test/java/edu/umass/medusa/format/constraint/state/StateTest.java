package edu.umass.medusa.format.constraint.state;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import edu.umass.medusa.util.Z3Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StateTest {
    Z3Session z3;
    State state;
    Z3Session.Z3Constants consts;

    @BeforeEach
    void setUp() {
        z3 = new Z3Session();
        state = new State(z3, 2);
        consts = z3.constants;
    }

    @Test
    void testOpstack1() {
        state.push(consts.ICONST_1);
        state.push(consts.ICONST_2);
        state.push(z3.add(state.pop(), state.pop()));
        checkOpstack(consts.ICONST_3);
    }

    @Test
    void testOpstack2() {
        state.push(consts.ICONST_1);
        state.push(consts.ICONST_2);
        state.push(z3.add(state.pop(), state.pop()));
        state.push(consts.ICONST_1);
        state.push(consts.ICONST_2);
        state.push(z3.add(state.pop(), state.pop()));
        state.push(z3.add(state.pop(), state.pop()));
        checkOpstack(consts.ICONST_6);
    }

    @Test
    void testVarTable1() {
        state.push(consts.ICONST_M1);
        state.push(consts.ICONST_2);
        state.push(z3.add(state.pop(), state.pop()));
        state.store(0, state.pop());
        checkVarTable(0, consts.ICONST_1);
    }

    @Test
    void testVarTable2() {
        state.push(consts.ICONST_1);
        state.push(consts.ICONST_2);
        state.push(z3.add(state.pop(), state.pop()));
        state.store(0, state.pop());
        checkVarTable(0, consts.ICONST_3);
        state.push(state.load(0));
        state.store(1, state.pop());
        checkVarTable(0, consts.ICONST_3);
        checkVarTable(1, consts.ICONST_3);
        state.push(state.load(0));
        state.push(state.load(1));
        state.push(z3.add(state.pop(), state.pop()));
        checkOpstack(consts.ICONST_6);
        state.store(0, state.pop());
        checkVarTable(0, consts.ICONST_6);
    }

    @Test
    void testPropagateState1() {
        z3.push();
        State state2 = new State(z3, 2);
        state.push(consts.ICONST_1);                       // 1
        state.push(consts.ICONST_2);                       // 1 2
        state.push(z3.add(state.pop(), state.pop()));      // 3
        z3.addConstraints(state.getConstraints());
        z3.addConstraints(state.propagateState(state2));
        checkOpstack(state2, consts.ICONST_3);
        z3.pop();
    }

    @Test
    void testPropagateState2() {
        z3.push();
        State state2 = new State(z3, 2);
        state.push(consts.ICONST_1);
        state.push(consts.ICONST_1);
        state.push(z3.add(state.pop(), state.pop()));
        state.store(0, state.pop());

        z3.addConstraints(state2.getConstraints());
        z3.addConstraints(state.propagateState(state2));
        checkVarTable(0, consts.ICONST_2);

        z3.pop();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                      HELPER METHODS FOR TESTING                        //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * A convenience method that ensures that a state's opstack has the
     * correct top value
     * @param state
     * @param expected
     */
    void checkOpstack(State state, BitVecExpr expected) {
        final BoolExpr constraints = state.getConstraints();
        z3.push(constraints);
        check(z3.mkEq(expected, state.peek()));
        z3.pop();
    }

    void checkOpstack(BitVecExpr bv) {
        checkOpstack(state, bv);
    }

    /**
     * A convenience method that ensures that a state's vartable has the
     * correct current value
     * @param index
     * @param expected
     */
    void checkVarTable(State state, int index, BitVecExpr expected) {
        final BoolExpr constraints = state.getConstraints();
        z3.push(constraints);
        check(z3.mkEq(expected, state.load(index)));
        z3.pop();
    }

    void checkVarTable(int index, BitVecExpr bv) {
        checkVarTable(state, index, bv);
    }

    void check(BoolExpr assertion) {
        assertTrue(z3.isUnsat(z3.mkNot(assertion)), () ->
                "Model:\n\n" + z3.getSortedModelString() + "\n\nConstraints: " + z3.z3.toString());
        assertTrue(z3.isSat(assertion));
    }
}