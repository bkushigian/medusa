package edu.umass.medusa.format.constraint;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.pipeline.Pipeline;
import edu.umass.medusa.util.TestUtil;
import edu.umass.medusa.util.Z3Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class BlockConstraintTest {
    static ClassIdentity pipeTestsClassId;
    static ClassIdentity triangleClassId;
    static MutantClassIdentity[] mutants;
    static Z3Session z3;

    @BeforeAll
    static void setUp() throws IOException {
        z3 = new Z3Session();
        Pipeline pipeline = new Pipeline(z3);

        pipeTestsClassId = new ClassIdentity("PipeTests", TestUtil.TEST_CLASSES);
        triangleClassId = new ClassIdentity("Triangle", TestUtil.TRIANGLE_CLASSES);

        // Handle triangle tests
        File[] triangleMutantFiles = TestUtil.getTriangleMutantFiles();
        mutants = new MutantClassIdentity[triangleMutantFiles.length + 1];
        int i = 1;
        for (File f : triangleMutantFiles) {
            MutantClassIdentity mutantId = new MutantClassIdentity(
                    "Triangle",
                    f.getParentFile().getAbsolutePath(),
                    triangleClassId,
                    "classify(III)I");
            triangleClassId.getMethod("classify(III)I").addMutant(mutantId.mutantId);
            mutants[i++] = mutantId;
        }

        pipeline.add(pipeTestsClassId);
        pipeline.add(triangleClassId);

        //TODO: Fix
        //pipeline.flow();
        System.out.println();

    }

    @Test
    void free() {
    }

    @Test
    void isFree() {
    }

    @Test
    void getFree() {
    }

    @Test
    void setTrueSucc() {
    }

    @Test
    void setFalseSucc() {
    }

    @Test
    void setMutantSucc() {
    }

    @Test
    void getSuccessors() {
    }

    @Test
    void getBlockConstraints() {
    }

    @Test
    void getTrueEdgeConstraint() {
    }

    @Test
    void getFalseEdgeConstraint() {
    }

    @Test
    void getMutantEdgeConstraint() {
    }

    void init(String classFile) {

    }
}