package edu.umass.medusa.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TypeUtilTest {
    @Test
    void testPrimAndBoxed() {
        assertEquals(TypeUtil.BYTE_PRIM_TYPE, TypeUtil.parseTypeDesc("B"));
        assertEquals(TypeUtil.SHORT_PRIM_TYPE, TypeUtil.parseTypeDesc("S"));
        assertEquals(TypeUtil.INTEGER_PRIM_TYPE, TypeUtil.parseTypeDesc("I"));
        assertEquals(TypeUtil.LONG_PRIM_TYPE, TypeUtil.parseTypeDesc("J"));
        assertEquals(TypeUtil.FLOAT_PRIM_TYPE, TypeUtil.parseTypeDesc("F"));
        assertEquals(TypeUtil.DOUBLE_PRIM_TYPE, TypeUtil.parseTypeDesc("D"));
        assertEquals(TypeUtil.CHAR_PRIM_TYPE, TypeUtil.parseTypeDesc("C"));
        assertEquals(TypeUtil.BOOLEAN_PRIM_TYPE, TypeUtil.parseTypeDesc("Z"));

        Class clazz = Integer.class;

        assertEquals(TypeUtil.BYTE_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Byte.class)));
        assertEquals(TypeUtil.SHORT_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Short.class)));
        assertEquals(TypeUtil.INTEGER_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Integer.class)));
        assertEquals(TypeUtil.LONG_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Long.class)));
        assertEquals(TypeUtil.FLOAT_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Float.class)));
        assertEquals(TypeUtil.DOUBLE_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Double.class)));
        assertEquals(TypeUtil.CHAR_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Character.class)));
        assertEquals(TypeUtil.BOOLEAN_BOXED_TYPE, TypeUtil.parseTypeDesc(getDesc(Boolean.class)));

        String paramDesc = "(Ljava/lang/Byte;Ljava/lang/Short;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Character;Ljava/lang/Boolean;)I";
        TypeUtil.Type[] params = TypeUtil.parseMethodDesc(paramDesc);
        TypeUtil.Type[] expected = { TypeUtil.BYTE_BOXED_TYPE, TypeUtil.SHORT_BOXED_TYPE,
                TypeUtil.INTEGER_BOXED_TYPE, TypeUtil.LONG_BOXED_TYPE, TypeUtil.DOUBLE_BOXED_TYPE,
                TypeUtil.FLOAT_BOXED_TYPE, TypeUtil.CHAR_BOXED_TYPE, TypeUtil.BOOLEAN_BOXED_TYPE,
                TypeUtil.INTEGER_PRIM_TYPE};

        assertEquals(expected.length, params.length);
        for (int i = 0; i < params.length; ++i) {
            assertEquals(expected[i], params[i]);
        }

        paramDesc = "([I[S[Ljava/lang/Integer;[Ljava/lang/Short;[[I)V";
        params = TypeUtil.parseMethodDesc(paramDesc);
        TypeUtil.Type[] expected2 = {
                TypeUtil.parseTypeDesc("[I"),
                TypeUtil.parseTypeDesc("[S"),
                TypeUtil.parseTypeDesc("[Ljava/lang/Integer;"),
                TypeUtil.parseTypeDesc("[Ljava/lang/Short;"),
                TypeUtil.parseTypeDesc("[[I"),
                TypeUtil.parseTypeDesc("V")
        };

        assertEquals(expected2.length, params.length);
        for (int i = 0; i < params.length; ++i) {
            assertEquals(expected2[i], params[i]);
        }
    }

    @Test
    void getPrimFromDesc() {
    }

    String getDesc(Class c) {
        return (new StringBuilder("L"))
                .append(c.getName().replace('.', '/'))
                .append(';')
                .toString();
    }
}