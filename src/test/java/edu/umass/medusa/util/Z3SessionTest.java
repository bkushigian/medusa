package edu.umass.medusa.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Status;
import org.junit.jupiter.api.*;

class Z3SessionTest {
    private static Z3Session z3;
    private static BoolExpr trueAndTrue;
    private static BoolExpr trueAndFalse;

    @BeforeAll
    static void initAll() {
        z3 = new Z3Session();
        trueAndTrue = z3.ctx.mkAnd(z3.ctx.mkBool(true), z3.ctx.mkBool(true));
        trueAndFalse = z3.ctx.mkAnd(z3.ctx.mkBool(true), z3.ctx.mkBool(false));
    }

    @BeforeEach
    void setup() {
        z3.z3.push();
    }

    @AfterEach
    void tearDown() {
        z3.z3.pop();
    }

    @Test
    void z3BasicsSatTest() {
        z3.z3.add(trueAndTrue);
        Status status = z3.z3.check();
        assertEquals(Status.SATISFIABLE, status);
    }

    @Test
    void z3BasicsUnsatTest() {
        z3.z3.add(trueAndFalse);
        Status status = z3.z3.check();
        assertEquals(Status.UNSATISFIABLE, status);
    }

}