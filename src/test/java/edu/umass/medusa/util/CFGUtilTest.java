package edu.umass.medusa.util;

import de.codesourcery.asm.controlflow.ControlFlowAnalyzer;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.tree.ClassNode;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class CFGUtilTest {

    @Test
    void test_countDifferingBlocksReturnsZeroOnSameMethod() throws Exception {
        ClassNode cn1 = ASMUtil.readClass(new File(TestUtil.TRIANGLE));
        ClassIdentity cid1 = new ClassIdentity(cn1);
        MethodIdentity mid1 = cid1.getMethod("classify(III)I");
        ControlFlowAnalyzer analyzer1 = new ControlFlowAnalyzer();
        mid1.cfg = analyzer1.analyze(mid1.name, mid1.method);

        ClassNode cn2 = ASMUtil.readClass(new File(TestUtil.TRIANGLE));
        ClassIdentity cid2 = new ClassIdentity(cn2);
        MethodIdentity mid2 = cid2.getMethod("classify(III)I");
        ControlFlowAnalyzer analyzer2 = new ControlFlowAnalyzer();
        mid2.cfg = analyzer2.analyze(mid2.name, mid2.method);

        assertNotNull(mid1.cfg);
        assertNotNull(mid2.cfg);
        assertEquals(0, CFGUtil.countDifferingBlocks(mid1, mid2));
    }

    @Test
    void test_triangleMutant14IsFOMutant() throws Exception {
        ClassNode cn1 = ASMUtil.readClass(new File(TestUtil.TRIANGLE));
        ClassIdentity cid1 = new ClassIdentity(cn1);
        MethodIdentity mid1 = cid1.getMethod("classify(III)I");
        ControlFlowAnalyzer analyzer1 = new ControlFlowAnalyzer();
        mid1.cfg = analyzer1.analyze(mid1.name, mid1.method);

        ClassNode cn2 = ASMUtil.readClass(TestUtil.getTriangleMutant(14));
        ClassIdentity cid2 = new ClassIdentity(cn2);
        MethodIdentity mid2 = cid2.getMethod("classify(III)I");
        ControlFlowAnalyzer analyzer2 = new ControlFlowAnalyzer();
        mid2.cfg = analyzer2.analyze(mid2.name, mid2.method);

        assertFalse(CFGUtil.isIsomorphic(mid1.cfg, mid2.cfg));
        assertFalse(CFGUtil.isAtomicCFGMutant(mid1, mid2));
    }

    @Test
    void test_loopsShouldBeIsomorphicToSelves() throws Exception {
        ClassNode cn = ASMUtil.readClass("SimpleLoop", TestUtil.TEST_CLASSES);
        ClassIdentity cid = new ClassIdentity(cn);
        MethodIdentity mid1 = cid.getMethod("sumUpto(I)I");
        ControlFlowAnalyzer analyzer1 = new ControlFlowAnalyzer();
        mid1.cfg = analyzer1.analyze(mid1.name, mid1.method);

        assertTrue(CFGUtil.isIsomorphic(mid1.cfg, mid1.cfg));
        assertFalse(CFGUtil.isAtomicCFGMutant(mid1, mid1));
    }

    @Test
    void testSimpleLoop1() throws Exception {
        ClassNode cn = ASMUtil.readClass("SimpleLoop", TestUtil.TEST_CLASSES);
        ClassIdentity cid = new ClassIdentity(cn);

        MethodIdentity mid1 = cid.getMethod("sumUpto(I)I");
        ControlFlowAnalyzer analyzer1 = new ControlFlowAnalyzer();
        mid1.cfg = analyzer1.analyze(mid1.name, mid1.method);

        MethodIdentity mid2 = cid.getMethod("sumUpto1(I)I");
        ControlFlowAnalyzer analyzer2 = new ControlFlowAnalyzer();
        mid2.cfg = analyzer2.analyze(mid2.name, mid2.method);

        assertTrue(CFGUtil.isIsomorphic(mid1.cfg, mid2.cfg));
        assertTrue(CFGUtil.isAtomicCFGMutant(mid1, mid2));
    }

    @Test
    void testSimpleLoop2() throws Exception {
        ClassNode cn = ASMUtil.readClass("SimpleLoop", TestUtil.TEST_CLASSES);
        ClassIdentity cid = new ClassIdentity(cn);

        MethodIdentity mid1 = cid.getMethod("sumUpto(I)I");
        ControlFlowAnalyzer analyzer1 = new ControlFlowAnalyzer();
        mid1.cfg = analyzer1.analyze(mid1.name, mid1.method);

        MethodIdentity mid2 = cid.getMethod("sumUpto2(I)I");
        ControlFlowAnalyzer analyzer2 = new ControlFlowAnalyzer();
        mid2.cfg = analyzer2.analyze(mid2.name, mid2.method);

        assertTrue(CFGUtil.isIsomorphic(mid1.cfg, mid2.cfg));
        assertTrue(CFGUtil.isAtomicCFGMutant(mid1, mid2));
    }
}
