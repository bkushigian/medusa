package edu.umass.medusa.pipeline;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FPExpr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Status;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.util.TestUtil;
import edu.umass.medusa.util.Z3Session;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

public abstract class ConstraintTests {
  protected Z3Session z3;
  protected Map<Integer, Expr> params;
  protected Model model;
  protected Integer accessModifier;
  protected Pipeline pipeline;
  protected ClassIdentity classId;
  protected MethodIdentity methodId;
  protected MethodConstraint methodConstraint;
  protected String path;
  protected String message;
  protected String positiveAssertionString;
  protected String negativeAssertionString;
  protected String assertionString;
  protected Status status;
  protected Status positiveStatus;
  protected Status negativeStatus;
  protected boolean autoGenMessages = true;

  @BeforeEach
  protected void setUp() {
    z3 = new Z3Session();
    pipeline = new Pipeline(z3);
    message = "";
  }

  @AfterEach
  protected void tearDown() {
    params = null;
    model = null;
  }


  /**
   * Load a class file. Each file loaded should have precisely one non-constructor method which
   * we load as the method identity
   * @param name
   * @param location
   */
  protected void loadClassFile(String name, String location) {
    String sanitized = name.endsWith(".class") ? name.substring(0, name.length() - 6) : name;
    path = Paths.get(location, name).toString();
    try {
      classId = new ClassIdentity(sanitized, location);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Couldn't open file " + location + "/" + name);
    }

    pipeline.add(classId);
    pipeline.flow();

    methodId = null;
    for (MethodIdentity mid : classId.methods) {
      if (! "<init>()V".equals(mid.name)) {
        methodId = mid;
        break;
      }
    }

    if (methodId == null) {
      throw new RuntimeException("Unable to find a unique non-constructor method in " + location + "/" + name);
    }
    methodConstraint = methodId.gen.generate();
    accessModifier = methodId.method.access;
  }

  protected void loadClassFile(String name) throws Exception {
    loadClassFile(name, TestUtil.TEST_CLASSES);
  }

  /**
   * This is a helper function that calls both {@code makePositiveAssertion} and
   * {@code makeNegativeAssertion}. This is strictly for convenience and is
   * equivalent to calling the two methods in succession while ignoring their
   * outputs (this is saved in {@code positiveStatus} and {@code negativeStatus}
   *
   * @param inputs the inputs to the method to be called
   * @param output the expected output
   */
  protected void makeDualAssertions(Integer[] inputs, Integer output){
    makePositiveAssertion(inputs, output);
    positiveAssertionString = assertionString;
    makeNegativeAssertion(inputs, output);
    negativeAssertionString = assertionString;
  }

  /**
   * Make assertions that the following inputs yield the following output.
   * If this returns SATISFIABLE then there is some model for these assertions.
   *
   * If this returns UNSATISFIABLE then there is no model that satisfies these
   * assertions.
   *
   * If this returns UNKNOWN then all bets are off! Good luck to ya! However,
   * restricting to BitVecs we have a (PSPACE complete) decision procedure, so
   * we should be good...
   *
   * @param inputs
   * @param output
   *
   * @return Status resulting from the assertions
   */
  protected Status makePositiveAssertion(Integer[] inputs, Integer output) {

    z3.push();
    z3.addConstraints(methodConstraint.getBlockConstraints(), methodConstraint.getEdgeConstraints());
    z3.addConstraints(methodConstraint.executed);
    z3.addConstraints(z3.mkEq(z3.mkJavaInt("ret"), methodConstraint.getReturnExpr()));
    for (int i = 0; i < inputs.length; ++i) {
      z3.addConstraints(z3.mkEq(methodConstraint.getParam(i), z3.mkJavaInt(inputs[i])));
    }

    // Assert the return value
    z3.addConstraints(z3.mkEq(methodConstraint.getReturnExpr(), z3.mkJavaInt(output)));
    status = z3.check();
    if (status == Status.UNSATISFIABLE){
      message = autoGenMessages && status == Status.UNSATISFIABLE ? "\nIn " + path + ":\n\n"
          + "    System is over constrained from positive assertion. The following input\n"
          + "    should yield the following output:\n\n"
          + "                  INPUT: " + Arrays.toString(inputs) + "\n"
          + "        EXPECTED OUTPUT: " + output + "\n"
          + "            CONSTRAINTS:\n" + z3.z3.toString() + "\n\n"
          : "";
    }
    assertionString = z3.z3.toString();
    z3.pop();
    positiveStatus = status;
    return status;
  }


  /**
   * this asserts that the inputs CANNOT lead to the selected output
   * @param inputs
   * @param output
   * @return
   */
  protected Status makeNegativeAssertion(Integer[] inputs, Integer output) {
    z3.push();
    z3.addConstraints(methodConstraint.getBlockConstraints(), methodConstraint.getEdgeConstraints());
    z3.addConstraints(methodConstraint.executed);
    z3.addConstraints(z3.mkEq(z3.mkJavaInt("ret"), methodConstraint.getReturnExpr()));
    for (int i = 0; i < inputs.length; ++i) {
      // TODO: Fix
      z3.addConstraints(z3.mkEq(methodConstraint.getParam(i), z3.mkJavaInt(inputs[i])));
    }

    // Assert the negation of the return value
    z3.addConstraints(z3.mkNot(z3.mkEq(methodConstraint.getReturnExpr(), z3.mkJavaInt(output))));
    status = z3.check();
    if (status == Status.SATISFIABLE) {
      model = z3.getModel();
      message = autoGenMessages && status == Status.SATISFIABLE ? "\nIn " + path + ":\n\n"
          + "    System is under constrained from negative assertion. The following should never happen:\n\n"
          + "                  INPUT: " + Arrays.toString(inputs) + "\n"
          + "        EXPECTED OUTPUT: " + output + "\n\n"
          + "    However the following model provides a counter example:\n\n"
          + "                  MODEL:\n" + z3.getSortedModelString() + "\n"
          + "            CONSTRAINTS:\n" + z3.z3.toString() + "\n\n"
          : "";
    }
    assertionString = z3.z3.toString();
    z3.pop();
    negativeStatus = status;
    return status;
  }

  protected void makeDualAssertions(Float[] inputs, Float output){
    makePositiveAssertion(inputs, output);
    positiveAssertionString = assertionString;
    makeNegativeAssertion(inputs, output);
    negativeAssertionString = assertionString;
  }

  /**
   * Make assertions that the following inputs yield the following output.
   * If this returns SATISFIABLE then there is some model for these assertions.
   *
   * If this returns UNSATISFIABLE then there is no model that satisfies these
   * assertions.
   *
   * If this returns UNKNOWN then all bets are off! Good luck to ya! However,
   * restricting to BitVecs we have a (PSPACE complete) decision procedure, so
   * we should be good...
   *
   * @param inputs
   * @param output
   *
   * @return Status resulting from the assertions
   */
  protected Status makePositiveAssertion(Float[] inputs, Float output) {
    z3.push();
    final BitVecExpr bvret = z3.mkJavaInt(z3.nameGen.get("bvreturn"));
    final FPExpr fpret = z3.mkJavaFloat("freturn");
    z3.addConstraints(methodConstraint.getBlockConstraints(), methodConstraint.getEdgeConstraints());
    z3.addConstraints(methodConstraint.executed);
    z3.addConstraints(z3.mkEq(bvret, methodConstraint.getReturnExpr()));
    z3.addConstraints(z3.mkEq(fpret, z3.ctx.mkFPToFP(bvret, z3.javaFloatSort)));
    for (int i = 0; i < inputs.length; ++i) {
      final BitVecExpr bv = z3.mkJavaInt(z3.nameGen.get("bvparam"));
      final FPExpr fp = z3.mkJavaFloat(z3.nameGen.get("fparam"));
      z3.addConstraints(z3.mkEq(methodConstraint.getParam(i), bv));
      z3.addConstraints(z3.mkEq(fp, z3.ctx.mkFPToFP(bv, z3.javaFloatSort)));
      z3.addConstraints(z3.mkEq(fp, z3.mkJavaFloat(inputs[i])));
    }

    // Assert the return value
    z3.addConstraints(z3.ctx.mkFPEq(fpret, z3.mkJavaFloat(output)));
    status = z3.check();
    if (status == Status.UNSATISFIABLE){
      message = autoGenMessages ? "\nIn " + path + ":\n\n"
          + "    System is over constrained from positive assertion. The following input\n"
          + "    should yield the following output:\n\n"
          + "                  INPUT: " + Arrays.toString(inputs) + "\n"
          + "        EXPECTED OUTPUT: " + output + "\n"
          + "            CONSTRAINTS:\n" + z3.z3.toString() + "\n\n"
          : "";
    }
    assertionString = z3.z3.toString();
    z3.pop();
    positiveStatus = status;
    return status;
  }

  /**
   * this asserts that the inputs CANNOT lead to the selected output
   * @param inputs
   * @param output
   * @return
   */
  protected Status makeNegativeAssertion(Float[] inputs, Float output) {
    z3.push();
    final BitVecExpr bvret = z3.mkJavaInt(z3.nameGen.get("bvret"));
    final FPExpr fpret = z3.mkJavaFloat(z3.nameGen.get("fpret"));
    z3.addConstraints(methodConstraint.getBlockConstraints(), methodConstraint.getEdgeConstraints());
    z3.addConstraints(methodConstraint.executed);
    z3.addConstraints(z3.mkEq(bvret, methodConstraint.getReturnExpr()));
    z3.addConstraints(z3.mkEq(fpret, z3.ctx.mkFPToFP(bvret, z3.javaFloatSort)));

    for (int i = 0; i < inputs.length; ++i) {
      final BitVecExpr bv = z3.mkJavaInt(z3.nameGen.get("bv-param"));
      final FPExpr fp = z3.mkJavaFloat(z3.nameGen.get("fp-param"));
      z3.addConstraints(z3.mkEq(methodConstraint.getParam(i), bv));
      z3.addConstraints(z3.mkEq(fp, z3.ctx.mkFPToFP(bv, z3.javaFloatSort)));
      z3.addConstraints(z3.mkEq(fp, z3.mkJavaFloat(inputs[i])));
    }

    // Assert the negation of the return value
    z3.addConstraints(z3.mkNot(z3.ctx.mkFPEq(fpret, z3.mkJavaFloat(output))));
    status = z3.check();
    if (status == Status.SATISFIABLE) {
      model = z3.getModel();
      message = autoGenMessages ? "\nIn " + path + ":\n\n"
          + "    System is under constrained from negative assertion. The following should never happen:\n\n"
          + "                  INPUT: " + Arrays.toString(inputs) + "\n"
          + "        EXPECTED OUTPUT: " + output + "\n\n"
          + "    However the following model provides a counter example:\n\n"
          + "                  MODEL:\n" + z3.getSortedModelString() + "\n"
          + "            CONSTRAINTS:\n" + z3.z3.toString() + "\n\n"
          : "";
    }
    assertionString = z3.z3.toString();
    z3.pop();
    negativeStatus = status;
    return status;
  }
}
