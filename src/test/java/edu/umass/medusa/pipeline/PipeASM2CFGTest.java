package edu.umass.medusa.pipeline;

import de.codesourcery.asm.controlflow.ControlFlowGraph;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.util.TestUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PipeASM2CFGTest {
    static ClassIdentity pipeTestsClassId;
    static ClassIdentity triangleClassId;
    static ClassIdentity[] mutants;
    static Map<String, MethodIdentity> methods = new HashMap<>();

    @BeforeAll
    static void setUp() throws IOException {
        // Handle pipe tests
        pipeTestsClassId = new ClassIdentity("PipeTests", TestUtil.TEST_CLASSES);
        (new PipeASM2CFG()).flow(pipeTestsClassId);
        for (MethodIdentity method : pipeTestsClassId.methods) {
            methods.put(method.name, method);
            ControlFlowGraph cfg = method.cfg;
        }

        // Handle triangle tests
        triangleClassId = new ClassIdentity("Triangle", TestUtil.TRIANGLE_CLASSES);
        File[] triangleMutantFiles = TestUtil.getTriangleMutantFiles();
        mutants = new ClassIdentity[triangleMutantFiles.length + 1];
        mutants[0] = triangleClassId;
        int i = 1;
        for (File f : triangleMutantFiles) {
            ClassIdentity mutantId = new ClassIdentity("Triangle", f.getParentFile().getAbsolutePath());
            System.out.println("MutantID:" +  mutantId.name);
            mutants[i++] = mutantId;
        }

    }

    //@Test
    void flow_ASM2CFG_plus_boxed() {
        // TODO: Fix
        final MethodIdentity methId = methods.get("plus_boxed");
        assertEquals(3, methId.cfg.getAllNodes().size());
    }

    //@Test
    void flow_ASM2CFG_plus_unboxed() {
        // TODO: Fix
        final MethodIdentity methId = methods.get("plus_unboxed");
        assertEquals(3, methId.cfg.getAllNodes().size());
    }

    @Test
    void flow_unbox() {
        final MethodIdentity methId = methods.get("plus_boxed");
    }

    //@Test
    void randoCommando() throws IOException {
        // TODO: Fix
        ClassIdentity cid2 = new ClassIdentity("PipeTests", TestUtil.TEST_CLASSES);
        Boolean eq = cid2.classNode.equals(pipeTestsClassId.classNode);
        System.out.println(eq);
        assertEquals(cid2.classNode, pipeTestsClassId.classNode);
    }
}