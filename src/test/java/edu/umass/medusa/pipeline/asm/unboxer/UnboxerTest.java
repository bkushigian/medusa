package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.ASMUtil;
import edu.umass.medusa.util.TestUtil;
import edu.umass.medusa.util.bytecode.BytecodeUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.objectweb.asm.tree.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UnboxerTest {
  // original and unboxed versions
  static ClassNode unboxed;
  static ClassNode boxed;

  @BeforeAll
  static void setUp() throws IOException {
    unboxed = ASMUtil.readClass("Unboxing", TestUtil.TEST_CLASSES);
    boxed = ASMUtil.readClass("Unboxing", TestUtil.TEST_CLASSES);

    // FIXME: This cast vvv shouldn't be needed, but a time-saving hack to avoid fixing gradle rn.
    //     For some reason, asm is being pulled in from controlflow, not from our explicit
    //     dependency...
    for (MethodNode mn : (List<MethodNode>)unboxed.methods) {
      if (mn.name.contains("integer")) {
        (new IntegerUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("boolean")){
        (new BooleanUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("short")){
        (new ShortUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("byte")){
        (new ByteUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("long")){
        //(new LongUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("float")){
        (new FloatUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("double")){
        //(new DoubleUnboxer()).unboxMethod(mn);
      }
      else if (mn.name.contains("char")){
        (new CharUnboxer()).unboxMethod(mn);
      }
    }
  }

  @Test
  void test_ByteUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("byteUnboxing", "()B");
    InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
        BytecodeUtil.istore(1),
        BytecodeUtil.iload(1),
        BytecodeUtil.istore(2),
        BytecodeUtil.iload(2),
        BytecodeUtil.istore(3),
        BytecodeUtil.iload(2),
        BytecodeUtil.ireturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Test
  void test_byteUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("byteUnboxing", "()B");
    assertEquals("()B", mn.desc);
  }


  @Test
  void test_ShortUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("shortUnboxing", "()S");
    InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
        BytecodeUtil.istore(1),
        BytecodeUtil.iload(1),
        BytecodeUtil.istore(2),
        BytecodeUtil.iload(2),
        BytecodeUtil.istore(3),
        BytecodeUtil.iload(2),
        BytecodeUtil.ireturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Test
  void test_shortUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("shortUnboxing", "()S");
    assertEquals(mn.desc, "()S");
  }

  @Test
  void test_IntegerUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("integerUnboxing", "()I");
    InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
        BytecodeUtil.istore(1),
        BytecodeUtil.iload(1),
        BytecodeUtil.istore(2),
        BytecodeUtil.iload(2),
        BytecodeUtil.istore(3),
        BytecodeUtil.iload(2),
        BytecodeUtil.ireturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Disabled
  @Test
  void test_LongUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("longUnboxing", "()L");
    assertEquals(mn.desc, "()L");
  }

  @Disabled
  @Test
  void test_LongUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("longUnboxing", "()L");
        /* TODO: Adjust this for longs, including param offsets
        InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
                BytecodeUtil.istore(1),
                BytecodeUtil.iload(1),
                BytecodeUtil.istore(2),
                BytecodeUtil.iload(2),
                BytecodeUtil.istore(3),
                BytecodeUtil.iload(2),
                BytecodeUtil.ireturn());

        final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
        final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
        assertInsnsEqual(expectedNodes, actualNodes);
        */
  }

  @Test
  void test_integerUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("integerUnboxing", "()I");
    assertEquals(mn.desc, "()I");
  }

  @Test
  void test_CharUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("charUnboxing", "()C");
    InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
        BytecodeUtil.istore(1),
        BytecodeUtil.iload(1),
        BytecodeUtil.istore(2),
        BytecodeUtil.iload(2),
        BytecodeUtil.istore(3),
        BytecodeUtil.iload(2),
        BytecodeUtil.ireturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Test
  void test_charUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("charUnboxing", "()C");
    assertEquals(mn.desc, "()C");
  }

  @Test
  void test_BooleanUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("booleanUnboxing", "()Z");
    InsnList expected = makeInsnList(BytecodeUtil.iconst_1(),
        BytecodeUtil.istore(1),
        BytecodeUtil.iload(1),
        BytecodeUtil.istore(2),
        BytecodeUtil.iload(2),
        BytecodeUtil.istore(3),
        BytecodeUtil.iload(2),
        BytecodeUtil.ireturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Test
  void test_booleanUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("booleanUnboxing", "()Z");
    assertEquals(mn.desc, "()Z");
  }

  @Test
  void test_FloatUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("floatUnboxing", "()F");
    InsnList expected = makeInsnList(BytecodeUtil.fconst_1(),
        BytecodeUtil.fstore(1),
        BytecodeUtil.fload(1),
        BytecodeUtil.fstore(2),
        BytecodeUtil.fload(2),
        BytecodeUtil.fstore(3),
        BytecodeUtil.fload(2),
        BytecodeUtil.freturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Test
  void test_floatUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("floatUnboxing", "()F");
    assertEquals(mn.desc, "()F");
  }



  @Disabled
  @Test
  void test_DoubleUnboxShouldRemoveASTORE() {
    final MethodNode mn = getUnboxedMethod("doubleUnboxing", "()D");
    InsnList expected = makeInsnList(BytecodeUtil.dconst_1(),
        BytecodeUtil.dstore(1),
        BytecodeUtil.dload(1),
        BytecodeUtil.dstore(2),
        BytecodeUtil.dload(2),
        BytecodeUtil.dstore(3),
        BytecodeUtil.dload(2),
        BytecodeUtil.dreturn());

    final AbstractInsnNode[] expectedNodes = strippedInsnsArray(expected);
    final AbstractInsnNode[] actualNodes = strippedInsnsArray(mn.instructions);
    assertInsnsEqual(expectedNodes, actualNodes);
  }

  @Disabled
  @Test
  void test_doubleUnboxingShouldUpdateDesc() {
    final MethodNode mn = getUnboxedMethod("doubleUnboxing", "()D");
    assertEquals(mn.desc, "()D");
  }

  ////////////////////////////////////////////////////////////////////////////////
  //                              HELPER METHODS                                //
  ////////////////////////////////////////////////////////////////////////////////

  MethodNode getBoxedMethod(String name, String desc) {
    // FIXME: Gradle issues cause type erasure (and thus the following cast...)
    for (MethodNode mn : (List<MethodNode>)boxed.methods) {
      if (mn.name.equals(name) && mn.desc.equals(desc)) {
        return mn;
      }
    }
    throw new RuntimeException("Couldn't find method " + name + desc);
  }

  MethodNode getUnboxedMethod(String name, String desc) {
    // FIXME: Gradle issues cause type erasure (and thus the following cast)
    for (MethodNode mn : (List<MethodNode>)unboxed.methods) {
      if (mn.name.equals(name) && mn.desc.equals(desc)) {
        return mn;
      }
    }
    throw new RuntimeException("Couldn't find method " + name + desc);
  }

  AbstractInsnNode[] strippedInsnsArray(InsnList instructions) {
    List<AbstractInsnNode> list = new ArrayList<>();
    for (AbstractInsnNode node : instructions.toArray()) {
      if (node.getOpcode() >= 0) {
        list.add(node);
      }
    }
    return list.toArray(new AbstractInsnNode[list.size()]);
  }

  InsnList makeInsnList(AbstractInsnNode ... nodes) {
    InsnList result = new InsnList();
    for (AbstractInsnNode node : nodes) {
      result.add(node);
    }
    return result;

  }

  void assertInsnsEqual(AbstractInsnNode[] expected, AbstractInsnNode[] actual) {
    assertEquals(expected.length, actual.length);

    for (int i = 0; i < expected.length; ++i) {
      AbstractInsnNode exp = expected[i];
      AbstractInsnNode act = actual[i];

      int expOpcode = exp.getOpcode();
      int actOpcode = act.getOpcode();
      assertEquals(expOpcode, actOpcode);
      if (exp instanceof VarInsnNode) {
        VarInsnNode exp_ = (VarInsnNode)exp;
        VarInsnNode act_ = (VarInsnNode)act;
        assertEquals(exp_.var, act_.var);
      }
      else if (exp instanceof IntInsnNode) {
        IntInsnNode exp_ = (IntInsnNode)exp;
        IntInsnNode act_ = (IntInsnNode)act;
        assertEquals(exp_.operand, act_.operand);
      }
    }
  }
}