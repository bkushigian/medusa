package edu.umass.medusa.pipeline;

import com.microsoft.z3.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class allows for easy generation of test cases, giving automatically
 * generated feedback in the form of models and proofs.
 *
 * There are a large number of global variables to allow for easy auto-test generation
 * and minimal test writing/design. This has the added bonus of making tests easy to
 * read and reason about.
 */

// TODO: This was just added. This needs to be adopted
public class MethodConstraintsTest extends ConstraintTests {

  /**
   * Use this code as an example. The basic pattern is to
   * <ol>
   *     <li>
   *         Load a class file with {@code loadClassFile},
   *     </li>
   *     <li>
   *         Make a <em>positive</em> Z3 assertion, which says that the
   *         assertions made are not so tight as to restrict our modeled
   *         JVM from executing correctly (this should return SATISFIABLE)
   *     </li>
   *     <li>
   *         Make a <em>negative</em> Z3 assertion, which says that the
   *         assertions made in Z3 are tight enough to not allow any
   *         incorrect execution in our modeled JVM (this should return
   *         UNSATISFIABLE)
   *     </li>
   * </ol>
   *
   * Our code to be modelled here is:
   * <pre>
   *  int f(int a, int b) {
   *      int c;
   *      if (a > 2)
   *          c = 3;
   *      else
   *          c = 4;
   *      return c;
   *  }
   * </pre>
   *
   * NOTE: There is a helper function {@code makeDualAssertions()} that updates
   * class fields {@code positiveStatus} and {@code negativeStatus} that serves
   * as a shortcut for the above pattern.
   *
   * @throws Exception
   */
  @Test
  public void testSimpleIf1() throws Exception {
    loadClassFile("SimpleIf.class");

    /* In our below code, we assert that given an input of {0,0} to the method
     * being modelled, there is some possible execution in our modelled JVM
     * that outputs 4 (which is what is expected); this is our positive assertion.
     * If this is possible we are over constrained
     */
    status = makePositiveAssertion(new Integer[]{0,0}, 4);
    assertEquals(Status.SATISFIABLE, status, message);


    /* Next, we assert that for input {0,0} is <em>not</em> a possible execution
     * in our modelled JVM that outputs 4. If this is satisfiable then we are
     * under constrained.
     */
    status = makeNegativeAssertion(new Integer[]{0,0}, 4);
    assertEquals(Status.UNSATISFIABLE, status, message);
  }

  @Test
  public void testSimpleIf2() throws Exception {
    loadClassFile("SimpleIf.class");

    makeDualAssertions(new Integer[]{3,0}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testSimpleIf3() throws Exception {
    loadClassFile("SimpleIf.class");

    makeDualAssertions(new Integer[]{3,-1}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testSimpleIf4() throws Exception {
    loadClassFile("SimpleIf.class");
    makeDualAssertions(new Integer[]{0,3}, 4);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }


  /*
   * ComplexIf:
   *
   * int f(int a, int b) {
   *     if (a > b) {
   *         return a + 1;
   *     } else if (b < a + 2) {
   *         return b;
   *     } else {
   *       return a + b;
   *     }
   * }
   *
   */

  @Test
  public void testComplexIf1() throws Exception {
    loadClassFile("ComplexIf.class");
    makeDualAssertions(new Integer[]{1,0}, 2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testComplexIf2() throws Exception {
    loadClassFile("ComplexIf.class");
    makeDualAssertions(new Integer[]{2,2},2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }


  @Test
  public void testComplexIf3() throws Exception {
    loadClassFile("ComplexIf.class");
    makeDualAssertions(new Integer[]{0,2},2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }


  @Test
  public void testControlFlowTestAnd1() throws Exception {
    loadClassFile("ControlFlowTestAnd.class");
    makeDualAssertions(new Integer[]{0,0,0},-1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);

  }

  @Test
  public void testControlFlowTestAnd2() throws Exception {
    loadClassFile("ControlFlowTestAnd.class");
    makeDualAssertions(new Integer[]{2,1,0},2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }


  @Test
  public void testControlFlowTestAnd3() throws Exception {
    loadClassFile("ControlFlowTestAnd.class");
    makeDualAssertions(new Integer[]{1,2,0},2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);

  }


  @Test
  public void testControlFlowTestAnd4() throws Exception {
    loadClassFile("ControlFlowTestAnd.class");
    makeDualAssertions(new Integer[]{1,0,2},2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);

  }

  /*
   * MinimalBrokenExample:
   *
   * int minimal(int a, int b) {
   *     if (a <= 0 || b <= 0) {
   *         return 0;
   *     }
   *     return 1;
   * }
   */
  @Test
  public void testMinimalBrokenExample1() throws Exception {
    loadClassFile("MinimalBrokenExample.class");
    makeDualAssertions(new Integer[]{0, 0}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testMinimalBrokenExample2() throws Exception {
    loadClassFile("MinimalBrokenExample.class");
    makeDualAssertions(new Integer[]{1, 1}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testMinimalBrokenExample3() throws Exception {
    loadClassFile("MinimalBrokenExample.class");
    makeDualAssertions(new Integer[]{0, 1}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testMinimalBrokenExample4() throws Exception {
    loadClassFile("MinimalBrokenExample.class");
    makeDualAssertions(new Integer[]{1, 0}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  /*
   * MultipleReturn.java:
   *
   *  int f(int a, int b) {
   *      if (a > b) {
   *          return a;
   *      } else {
   *          return b;
   *      }
   *  }
   */
  @Test
  public void testMultipleReturn1() throws Exception {
    loadClassFile("MultipleReturn.class");
    makeDualAssertions(new Integer[]{1, 0}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testMultipleReturn2() throws Exception {
    loadClassFile("MultipleReturn.class");
    makeDualAssertions(new Integer[]{0, 1}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testMultipleReturn3() throws Exception {
    loadClassFile("MultipleReturn.class");
    makeDualAssertions(new Integer[]{0, 2}, 2);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  /*
   * SimplerIf.java:
   *
   * int checkIfZero(int a) {
   *     if (a == 0) return 1;
   *     return 0;
   * }
   */
  @Test
  public void testSimplerIf1() throws Exception {
    loadClassFile("SimplerIf.class");

    makeDualAssertions(new Integer[]{0}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testSimplerIf2() throws Exception {
    loadClassFile("SimplerIf.class");

    makeDualAssertions(new Integer[]{1}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testSimplerIf3() throws Exception {
    loadClassFile("SimplerIf.class");

    makeDualAssertions(new Integer[]{2}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testSimplerIf4() throws Exception {
    loadClassFile("SimplerIf.class");

    makeDualAssertions(new Integer[]{-1}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample1() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{0,0,0}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample2() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{1,0,0}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample3() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{0,1,0}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample4() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{0,0,1}, 1);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample5() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{1,2,3}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample6() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{1,3,2}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample7() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{2,3,1}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testStatefulExample8() throws Exception {
    loadClassFile("StatefulExample.class");
    makeDualAssertions(new Integer[]{2,1,3}, 3);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  /*
   * TriangleModified.java:
   *
   * public static final int
   *        Type_INVALID = 0,
   *        Type_SCALENE = 1,
   *        Type_EQUILATERAL = 2,
   *        Type_ISOSCELES = 3;
   *
   * public static int classify(int a, int b, int c) {
   *     if (a <= 0 || b <= 0 || c <= 0) {
   *         return Type_INVALID;
   *     }
   *     int trian = 0;
   *     if (a == b) {
   *         trian = trian + 1;
   *     }
   *     if (a == c) {
   *         trian = trian + 2;
   *     }
   *     if (b == c) {
   *         trian = trian + 3;
   *     }
   *     if (trian == 0) {
   *         if (a + b <= c || a + c <= b || b + c <= a) {
   *             return Type_INVALID;
   *         } else {
   *             return Type_SCALENE;
   *         }
   *     }
   *     if (trian > 3) {
   *         return Type_EQUILATERAL;
   *     }
   *     if (trian == 1 && a + b > c) {
   *         return Type_ISOSCELES;
   *     } else if (trian == 2 && a + c > b) {
   *         return Type_ISOSCELES;
   *     } else if (trian == 3 && b + c > a) {
   *         return Type_ISOSCELES;
   *     } else {
   *         return Type_INVALID;
   *     }
   *  }
   */

  /**
   * The following are for ease of writing tests
   */
  private Integer INVALID = 0, SCALENE = 1, EQUILATERAL = 2, ISOSCELES = 3;

  @Test
  public void testTriangleModifiedInvalid1() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{0, 0, 0}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid2() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{0, 1, 1}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid3() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{1, 0, 1}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid4() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{1, 1, 0}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid5() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{3, 1, 1}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid6() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{1, 3, 1}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedInvalid7() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{1, 1, 3}, INVALID);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedEquilateral1() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{1, 1, 1}, EQUILATERAL);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedEquilateral2() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{5, 5, 5}, EQUILATERAL);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedIsosceles1() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{2, 2, 3}, ISOSCELES);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedIsosceles2() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{2, 3, 2}, ISOSCELES);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedIsosceles3() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{3, 2, 2}, ISOSCELES);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedScalene1() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{2, 3, 4}, SCALENE);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedScalene2() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{4, 2, 3}, SCALENE);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test
  public void testTriangleModifiedScalene3() throws Exception {
    loadClassFile("TriangleModified.class");
    makeDualAssertions(new Integer[]{4, 3, 2}, SCALENE);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }
}
