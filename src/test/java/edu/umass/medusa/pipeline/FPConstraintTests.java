package edu.umass.medusa.pipeline;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.microsoft.z3.*;
import org.junit.jupiter.api.Test;

public class FPConstraintTests extends ConstraintTests {

  @Test public void testFPAddition1() throws Exception {
    loadClassFile("FPAdder.class");
    makeDualAssertions(new Float[]{1.0f, 1.0f}, 2.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPAddition2() throws Exception {
    loadClassFile("FPAdder.class");
    makeDualAssertions(new Float[]{2.0f, 3.0f}, (2.0f + 3.0f));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPAddition3() throws Exception {
    loadClassFile("FPAdder.class");
    makeDualAssertions(new Float[]{1.123f, -3.321f}, (1.123f + (-3.321f)));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPSubtraction1() throws Exception {
    loadClassFile("FPSubtracter.class");
    makeDualAssertions(new Float[]{1.0f, 1.0f}, 1.0f - 1.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPSubtraction2() throws Exception {
    loadClassFile("FPSubtracter.class");
    makeDualAssertions(new Float[]{2.0f, 3.0f}, (2.0f - 3.0f));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPSubtraction3() throws Exception {
    loadClassFile("FPSubtracter.class");
    makeDualAssertions(new Float[]{-1.123f, 3.321f}, (-1.123f - (3.321f)));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPMultiplication1() throws Exception {
    loadClassFile("FPMultiplier.class");
    makeDualAssertions(new Float[]{1.0f, 1.0f}, 1.0f * 1.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPMultiplication2() throws Exception {
    loadClassFile("FPMultiplier.class");
    makeDualAssertions(new Float[]{2.0f, 3.0f}, (2.0f * 3.0f));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPMultiplication3() throws Exception {
    loadClassFile("FPMultiplier.class");
    makeDualAssertions(new Float[]{-1.123f, -3.321f}, (-1.123f * (-3.321f)));
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPNegation1() throws Exception {
    loadClassFile("FPNegater.class");
    makeDualAssertions(new Float[]{-1.123f}, 1.123f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPNegation2() throws Exception {
    loadClassFile("FPNegater.class");
    makeDualAssertions(new Float[]{1.123f}, -1.123f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPNegation3() throws Exception {
    loadClassFile("FPNegater.class");
    makeDualAssertions(new Float[]{12345.12345f}, -12345.12345f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPNegation4() throws Exception {
    loadClassFile("FPNegater.class");
    makeDualAssertions(new Float[]{0.0f}, 0.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPLT1() throws Exception {
    loadClassFile("FPLT.class");
    makeDualAssertions(new Float[]{0.0f, 1.0f}, 1.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPLT2() throws Exception {
    loadClassFile("FPLT.class");
    makeDualAssertions(new Float[]{1.0f, 0.0f}, 0.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPLT3() throws Exception {
    loadClassFile("FPLT.class");
    makeDualAssertions(new Float[]{-30.0f, 1.0f}, 1.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

  @Test public void testFPLT4() throws Exception {
    loadClassFile("FPLT.class");
    makeDualAssertions(new Float[]{30.0f, -1.0f}, 0.0f);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }

}
