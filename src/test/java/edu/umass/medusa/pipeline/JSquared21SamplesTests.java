package edu.umass.medusa.pipeline;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.microsoft.z3.Status;
import org.junit.jupiter.api.Test;

public class JSquared21SamplesTests extends ConstraintTests {
  @Test public void test_fileTaxes1() throws Exception {
    loadClassFile("JSquared21Samples.class");
    makeDualAssertions(new Integer[]{0, 0}, 0);
    assertEquals(Status.SATISFIABLE, positiveStatus, message);
    assertEquals(Status.UNSATISFIABLE, negativeStatus, message);
  }
}
