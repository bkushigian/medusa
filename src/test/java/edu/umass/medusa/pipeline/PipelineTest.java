package edu.umass.medusa.pipeline;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Status;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.util.TestUtil;
import edu.umass.medusa.util.Z3Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PipelineTest {
    static Z3Session z3;

    // Triangle
    static Pipeline trianglePipeline;
    static ClassIdentity triangleClassId;
    static MutantClassIdentity[] triangleMutants;
    static HashMap<Integer, MutantIdentity> triangleMutantMap;
    static HashMap<Integer, Boolean> triangleEquivalence;
    static final int[] triangleMutantArgs = IntStream.range(1, 125).toArray();

    // PipeTests
    static Pipeline pipeTestsPipeline;
    static ClassIdentity pipeTestsClassId;
    static MutantClassIdentity[] pipeTestsMutants;
    static HashMap<Integer, MutantIdentity> pipeTestsMutantMap;
    static HashMap<Integer, Boolean> pipeTestsEquivalence;

    @BeforeAll
    static void setUp() throws IOException {
        z3 = new Z3Session();

        // Set Up Triangle
        triangleMutantMap = new HashMap<>();
        triangleEquivalence = TestUtil.triangleEquivalence();
        triangleClassId = new ClassIdentity("Triangle", TestUtil.TRIANGLE_CLASSES);
        trianglePipeline = new Pipeline(z3);

        // Handle triangle tests
        File[] triangleMutantFiles = TestUtil.getTriangleMutantFiles();
        triangleMutants = new MutantClassIdentity[triangleMutantFiles.length];
        int i = 0;
        int TAIL_LENGTH = "/triangle/triangle.class".length();
        for (File f : triangleMutantFiles) {
            int pathLength = f.getPath().length();
            int trimmedLength = pathLength - TAIL_LENGTH;
            int lastSlash = f.getPath().lastIndexOf('/', trimmedLength - 1);
            String trimmed = f.getPath().substring(lastSlash + 1, trimmedLength);
            MutantClassIdentity mutantId = new MutantClassIdentity(
                    "Triangle",
                    f.getParentFile().getAbsolutePath(),
                    triangleClassId,
                    "classify(III)I");
            triangleClassId.getMethod("classify(III)I").addMutant(mutantId.mutantId);
            triangleMutants[i++] = mutantId;
            triangleMutantMap.put(Integer.parseInt(trimmed), mutantId.mutantId);
        }

        trianglePipeline.add(triangleClassId);

        trianglePipeline.flow();
        System.out.println();

        pipeTestsMutantMap = new HashMap<>();
        pipeTestsEquivalence = TestUtil.pipelineEquivalence();
        pipeTestsClassId = new ClassIdentity("PipeTests", TestUtil.TEST_CLASSES);
        pipeTestsPipeline = new Pipeline(z3);

        File[] pipeTestsMutantFiles = TestUtil.getMutantFiles("PipeTests", TestUtil.TEST_CLASSES + "/pipe-test-mutants", "");
        pipeTestsMutants = new MutantClassIdentity[pipeTestsMutantFiles.length];

        i = 0;
        TAIL_LENGTH = "/PipeTests.class".length();
        for (File f : pipeTestsMutantFiles) {
            int pathLength = f.getPath().length();
            int trimmedLength = pathLength - TAIL_LENGTH;
            int lastSlash = f.getPath().lastIndexOf('/', trimmedLength - 1);
            String trimmed = f.getPath().substring(lastSlash + 1, trimmedLength);
            MutantClassIdentity mutantId = new MutantClassIdentity(
                    "PipeTests",
                    f.getParentFile().getAbsolutePath(),
                    pipeTestsClassId,
                    "plus_boxed(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;");
            pipeTestsClassId.getMethod("plus_boxed(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;").addMutant(mutantId.mutantId);
            pipeTestsMutants[i++] = mutantId;
            pipeTestsMutantMap.put(Integer.parseInt(trimmed), mutantId.mutantId);
        }

        pipeTestsPipeline.add(pipeTestsClassId);
        pipeTestsPipeline.flow();

    }

    @ParameterizedTest(name = "testTriangle(mutant #{arguments}) ")
    @ValueSource(ints = {
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,
            39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,
            74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,
            107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125})

    void testTriangle(int i) throws IOException {
        final MethodIdentity methodId = triangleClassId.getMethod("classify(III)I");
        final MutantIdentity mutantId = triangleMutantMap.get(i);
        boolean equiv = triangleEquivalence.get(i);

        assertMutantEquivalence(methodId, mutantId, equiv);
    }

    @ParameterizedTest(name = "testPipeTest(mutant #{arguments})")
    @ValueSource(ints = {1, 2, 3})
    void test_PipelineShouldHandleIntegerUnboxing(int i) {
        final MethodIdentity methodId = pipeTestsClassId.getMethod("plus_boxed(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;");
        final MutantIdentity mutantId = pipeTestsMutantMap.get(i);
        boolean equiv = pipeTestsEquivalence.get(i);

        assertMutantEquivalence(methodId, mutantId, equiv);
    }

    /**
     * To assert that two programs are equivalent we must
     * <ol>
     *     <li>Assert their input params are identical</li>
     *     <li>Assert their outputs are different</li>
     *     <li>Constrain their blocks</li>
     *     <li>Constrain their edges</li>
     *     <li>Mark them as excecuted</li>
     * </ol>
     * @param orig
     * @param mut
     * @param equiv
     */
    private void assertMutantEquivalence(MethodIdentity orig, MutantIdentity mut, boolean equiv) {

        final MethodConstraint origConst = orig.gen.generate();
        final MethodConstraint mutConst = mut.gen.generate();

        final BoolExpr blockConstraints = origConst.getBlockConstraints();
        final BoolExpr edgeConstraints = origConst.getEdgeConstraints();

        final BoolExpr mutantBlockConstraints = mutConst.getBlockConstraints();
        final BoolExpr mutantEdgeConstraints = mutConst.getEdgeConstraints();

        final BoolExpr origExecuted = origConst.executed;
        final BoolExpr mutExecuted = mutConst.executed;

        final BoolExpr paramIdentification = origConst.identifyInputParams(mutConst);
        final BoolExpr retValSeparation = origConst.separateReturnValues(mutConst);

        z3.push();
        z3.addConstraints(blockConstraints, edgeConstraints, mutantBlockConstraints,
                mutantEdgeConstraints, paramIdentification, retValSeparation, origExecuted, mutExecuted);
        final Status check = z3.check();
        String constraintString = z3.z3.toString();
        String modelString = "";
        if (check == Status.SATISFIABLE) {
            modelString = z3.getSortedModelString();
        }
        z3.pop();

        if (equiv) {
            assertEquals(check, Status.UNSATISFIABLE,
                    "Separated return values from equivalent mutant.  Model:\n\n"
                            + modelString + "\n\nConstraints:\n\n" + constraintString);
        }

        else {
            assertEquals(check, Status.SATISFIABLE,
                    "Could not find parameters to separate return values of non-equivalent mutant. Constraints: \n\n"
                            + constraintString);
        }
    }
}