package edu.umass.medusa.solver;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Status;
import de.codesourcery.asm.controlflow.ControlFlowAnalyzer;
import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.Edge;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.constraint.BytecodeConstrainer;
import edu.umass.medusa.format.constraint.state.State;
import edu.umass.medusa.util.Z3Session;
import org.junit.jupiter.api.BeforeEach;
import static edu.umass.medusa.util.bytecode.BytecodeUtil.*;


import org.junit.jupiter.api.Test;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.AnalyzerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BytecodeConstrainerTest {
    Z3Session z3;
    State state;
    BytecodeConstrainer bc;
    Z3Session.Z3Constants consts;

    MethodNode methodNode;
    ControlFlowGraph cfg;

    @BeforeEach
    void setUp() {
        z3 = new Z3Session();
        state = new State(z3, 2);
        bc = new BytecodeConstrainer(z3);
        consts = z3.constants;
    }
    ////////////////////////////////////////////////////////////////////////////
    //                           INTEGER TESTS                                //
    //  This section tests individual integer bytecode ops for correctness by //
    //   issuing instructions to the constrainer to update a state and then   //
    //  checking Z3 for Sat/Unsat output. These are very simple by and large. //
    //         Later we use  more comprehensive and holistic methods.         //
    ////////////////////////////////////////////////////////////////////////////

    @Test
    void testLongConstraints() {
        emit(lconst_0());
        emit(lconst_1());
        emit(ladd());
        checkOpstackLong(consts.LCONST_1);
        emit(lconst_1());
        emit(ladd());
        checkOpstackLong(consts.LCONST_2);
        emit(lconst_1());
        emit(lsub());
        checkOpstackLong(consts.LCONST_1);
        emit(lconst_1());
        emit(ldiv());
        checkOpstackLong(consts.LCONST_1);
        emit(lconst_1());
        emit(ladd());
        emit(lconst_1());
        emit(ladd());
        emit(lconst_1());
        emit(ladd());
        checkOpstackLong(consts.LCONST_4);
        emit(lconst_1());
        emit(lconst_1());
        emit(ladd());
        emit(ldiv());
        checkOpstackLong(consts.LCONST_2);
    }

    /**
     * Store an integer constant
     */
    @Test
    void testIConst() {
        emit(iconst_0());
        checkOpstack(consts.ICONST_0);
        emit(iconst_1());
        checkOpstack(consts.ICONST_1);
        emit(iconst_2());
        checkOpstack(consts.ICONST_2);
        emit(iconst_3());
        checkOpstack(consts.ICONST_3);
        emit(iconst_4());
        checkOpstack(consts.ICONST_4);
        emit(iconst_5());
        checkOpstack(consts.ICONST_5);
        emit(iconst_m1());
        checkOpstack(consts.ICONST_M1);
    }

    @Test
    void testIAdd() {
        emit(iconst_1(), iconst_1(),iadd());
        checkOpstack(consts.ICONST_2);

        emit(iconst_2(), iadd());
        checkOpstack(consts.ICONST_4);

        emit(iconst_4(), iadd());
        checkOpstack(consts.ICONST_8);

        emit(iconst_m1(), iadd());
        checkOpstack(consts.ICONST_7);
    }

    @Test
    void testISub() {
        emit(iconst_1(), iconst_1(), isub());
        checkOpstack(consts.ICONST_0);

        emit(iconst_m1(), isub());
        checkOpstack(consts.ICONST_1);
    }

    @Test
    void testIMul() {
        emit(iconst_1(), iconst_2(), imul());
        checkOpstack(consts.ICONST_2);

        emit(iconst_2(), imul());
        checkOpstack(consts.ICONST_4);

    }

    @Test
    void testIDiv() {
        emit(iconst_1(), iconst_1(), idiv());
        checkOpstack(consts.ICONST_1);
        state.pop();

        emit(iconst_5(), iconst_2(), idiv());
        checkOpstack(consts.ICONST_2);
        state.pop();

        emit(sipush(100), sipush(50), idiv());
        checkOpstack(consts.ICONST_2);

        emit(bipush(12), iconst_5(), idiv());
        checkOpstack(consts.ICONST_2);
    }

    @Test
    void testIRem() {
        emit(iconst_5(), iconst_3(), irem());
        checkOpstack(consts.ICONST_2);

        emit(iconst_5(), iconst_5(), irem());
        checkOpstack(consts.ICONST_0);

        emit(iconst_m1(), iconst_5(), irem());
        checkOpstack(consts.ICONST_M1);
    }

    @Test
    void testIShl() {
        emit(iconst_1(), iconst_2(), ishl());
        checkOpstack(consts.ICONST_4);

        emit(iconst_1(), ishl());
        checkOpstack(consts.ICONST_8);

        emit(iconst_1(), ishl());
        checkOpstack(consts.ICONST_16);

        emit(iconst_1(), ishl());
        checkOpstack(consts.ICONST_32);
    }

    @Test
    void testIShr() {
        emit(bipush(32), iconst_1(), ishr());
        checkOpstack(consts.ICONST_16);

        emit(iconst_1(), ishr());
        checkOpstack(consts.ICONST_8);

        emit(iconst_1(), ishr());
        checkOpstack(consts.ICONST_4);

        emit(iconst_1(), ishr());
        checkOpstack(consts.ICONST_2);

        emit(iconst_1(), bipush(31), ishl(),  // Should make negative
             bipush(31), ishr());             // Should be -1
        checkOpstack(consts.ICONST_M1);
    }

    @Test
    void testIUshr() {
        emit(bipush(32), iconst_1(), iushr());
        checkOpstack(consts.ICONST_16);

        emit(iconst_1(), iushr());
        checkOpstack(consts.ICONST_8);

        emit(iconst_1(), iushr());
        checkOpstack(consts.ICONST_4);

        emit(iconst_1(), iushr());
        checkOpstack(consts.ICONST_2);
        emit(iconst_1(), bipush(31), ishl(),  // Should make negative
                bipush(31), iushr());         // Should be 1
        checkOpstack(consts.ICONST_1);
    }

    @Test
    void testIAnd() {
        //TODO
    }

    @Test
    void testIOr() {
        //TODO
    }

    @Test
    void testIXor() {
        //TODO
    }

    @Test
    void testINeg() {
        //TODO
    }

    @Test
    void testIInc() {
        //TODO
    }

    ////////////////////////////////////////////////////////////////////////////
    //                              CFG TESTS                                 //
    //  In this section we define simple MethodNode objects and analyze them  //
    //  to build a CFG. From this we then detect flow of information through  //
    //   our system to determine if our state is over or under constrained.   //
    ////////////////////////////////////////////////////////////////////////////

    //@Test
    void testHolistic1() throws AnalyzerException {
        // TODO: Fix
        LabelNode l1 = new LabelNode(), l2 = new LabelNode(), l3 = new LabelNode();
        testHolistic( consts.ICONST_1, null, 0,
                // BLOCK 1
                iconst_0(),    // 0: 0
                iconst_1(),    // 1: 0 1
                iconst_2(),    // 2: 0 1 2
                iadd(),        // 3: 0 3
                iconst_3(),    // 4: 0 3 3
                if_icmpeq(l1), // 5: 0      => GOTO Label 1
                iconst_1(),    // 6: 0 1
                iadd(),        // 7: 1
                l1             // 8: Should be 0, may be 1
        );
    }

    //@Test
    void testHolistic2() throws AnalyzerException {
        // TODO: Fix
        LabelNode l1 = new LabelNode(), l2 = new LabelNode(), l3 = new LabelNode();
        testHolistic( consts.ICONST_2, null, 0,
                // BLOCK 1
                iconst_1(),  // 0: 1
                iconst_2(),  // 1: 1 2
                iadd(),      // 2: 3
                ifeq(l1),    // 3:     THIS GOES TO INSTRUCTION 4, BLOCK 2
                // BLOCK 2
                iconst_2(),  // 4: 2
                go2(l2),     // 5: ...
                // BLOCK 3
                l1,          // 6:
                iconst_3(),  // 7: 3
                // BLOCK 4
                l2           // 8:
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //                      HELPER METHODS FOR TESTING                        //
    ////////////////////////////////////////////////////////////////////////////

    void emit(AbstractInsnNode ... nodes) {
        emit(state, nodes);
    }

    void emit(State state, AbstractInsnNode ... nodes) {
        for (AbstractInsnNode n : nodes) {
            emit(state, n);
        }
    }

    void emit(AbstractInsnNode insn) {
        bc.emit(state, insn);
    }

    void emit(State state, AbstractInsnNode insn) {
        bc.emit(state, insn);
    }

    /**
     * A convenience method that ensures that a state's opstack has the
     * correct top value
     * @param state
     * @param expected
     */
    void checkOpstack(State state, BitVecExpr expected) {
        final BoolExpr constraints = state.getConstraints();
        z3.push(constraints);
        check(z3.mkEq(expected, state.peek()));
        z3.pop();
    }

    void checkOpstack(BitVecExpr bv) {
        checkOpstack(state, bv);
    }

    void checkOpstackLong(BitVecExpr bv) {
        checkOpstackLong(state, bv);
    }

    void checkOpstackLong(State state, BitVecExpr expected) {
        final BoolExpr constraints = state.getConstraints();
        z3.push(constraints);
        check(z3.mkEq(expected, state.peekLong()));
        z3.pop();
    }

    /**
     * A convenience method that ensures that a state's vartable has the
     * correct current value
     * @param index
     * @param expected
     */
    void checkVarTable(State state, int index, BitVecExpr expected) {
        final BoolExpr constraints = state.getConstraints();
        z3.push(constraints);
        check(z3.mkEq(expected, state.load(index)));
        z3.pop();
    }

    void checkVarTable(int index, BitVecExpr bv) {
        checkVarTable(state, index, bv);
    }

    void check(BoolExpr assertion) {
        assertTrue(z3.isUnsat(z3.mkNot(assertion)),
                () -> "System is under constrained:\n\n" + z3.z3.getModel().toString());
        assertTrue(z3.isSat(assertion),
                "System is over constrained");
    }

    /**
     * Create a new MethodNode out of a sequence of instructions
     * @param instructions
     * @return
     */
    MethodNode createMethodNode(int maxLocals, AbstractInsnNode ... instructions) {
        MethodNode method = new MethodNode();
        method.maxLocals = maxLocals;
        for (AbstractInsnNode insn : instructions) {
            method.instructions.add(insn);
        }
        method.tryCatchBlocks = new ArrayList<>();
        return method;
    }

    /**
     * Create a new CFG out of a method node
     * @param method
     * @return
     * @throws AnalyzerException
     */
    ControlFlowGraph createCFG(MethodNode method) throws AnalyzerException {
        ControlFlowAnalyzer analyzer = new ControlFlowAnalyzer();
        final ControlFlowGraph graph = analyzer.analyze("me", method);
        return graph;
    }


    /**
     * Given a sequence of instructions, an expected top-of-stack value and an expected
     * array of variable table entries, create constraints for the instructions and assert
     * that Medusa constrains the system so that expectedStack and expectedVTEntries are
     * the only possible values of the constrained state.
     *
     * @param expectedStack
     * @param expectedVTEntries
     * @param instructions
     * @throws AnalyzerException
     */
    void testHolistic(BitVecExpr expectedStack, BitVecExpr[] expectedVTEntries, int maxLocals,
                      AbstractInsnNode ... instructions) throws AnalyzerException {
        initTestingEnvironment(maxLocals, instructions);
        holisticHelper(expectedStack, expectedVTEntries);
    }

    /**
     * This takes a list of instruction nodes, creates a {@code MethodNode}, uses these
     * to create a {@code ControlFlowGraph}, and stores these in the {@code method} and
     * {@code cfg} fields of this class respectively.
     */
    void initTestingEnvironment(int maxLocals, AbstractInsnNode ... instructions) throws AnalyzerException {
        methodNode = createMethodNode(maxLocals, instructions);
        cfg = createCFG(methodNode);
    }

    void holisticHelper(BitVecExpr expectedStack, BitVecExpr[] expectedVTEntries) {

        final Set<IBlock> nodes = cfg.getAllNodes();
        HashMap<IBlock, State> node2state = new HashMap<>();

        // Create states, emit block constraints, and add them to Z3
        for (IBlock block : nodes) {
            final State state = new State(z3, methodNode.maxLocals);
            node2state.put(block, state);
            bc.constrainBlock(state, methodNode, block);
            z3.addConstraints(state.getConstraints());
        }

        // Compute edge constraints, and add them to Z3
        for (IBlock block : nodes) {
            if (block.getRegularSuccessorCount() == 0) { // EXIT block
                continue;
            }

            State state = node2state.get(block);
            final Set<Edge> edges = block.getEdges();
            State trueSucc = null, falseSucc = null;

            if (block.getRegularSuccessorCount() == 1) {
                for (Edge e : edges) {
                    if (e.src == block) {
                        trueSucc = node2state.get(e.dst);
                        break;
                    }
                }
            }
            else if (block.getRegularSuccessorCount() == 2){
                for (Edge e : edges) {
                    if ("true".equals(e.metaData)) {
                        trueSucc = node2state.get(e.dst);
                    } else if ("false".equals(e.metaData)) {
                        falseSucc = node2state.get(e.dst);
                    }
                }
            } else {
                System.err.println("Cannot handle switch statements");
                assertFalse(true);
            }

            if (trueSucc == null) {
                continue;
            }

            final BoolExpr exp = state.constrainSuccessors(trueSucc, falseSucc);
            state.printConstraints("Block: " + block.toString());
            if (exp != null) {
                z3.addConstraints(exp);
            }
        }

        IBlock lastBlock = cfg.getBlockForInstruction(methodNode.instructions.size() - 1);
        State lastState = node2state.get(lastBlock);

        if (expectedStack != null) {
            checkOpstack(lastState, expectedStack);
        }

        if (expectedVTEntries != null) {
            for (int i = 0; i < expectedVTEntries.length; ++i) {
                if (expectedVTEntries[i] != null) {
                    checkVarTable(lastState, i, expectedVTEntries[i]);
                }
            }
        }
    }
}