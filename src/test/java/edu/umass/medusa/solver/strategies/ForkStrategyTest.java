package edu.umass.medusa.solver.strategies;

import com.microsoft.z3.Statistics;
import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.pipeline.Pipeline;
import edu.umass.medusa.solver.result.SolverResult;
import edu.umass.medusa.util.CFGUtil;
import edu.umass.medusa.util.Pair;
import edu.umass.medusa.util.TestUtil;
import edu.umass.medusa.util.Z3Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ForkStrategyTest {

    static Z3Session z3;
    static Pipeline pipeline;
    static ForkStrategy strategy;

    /* Static testing fields
     * For each testing class, include the following fields. These should
     * be added to TestUtil for convenience.
     *
     * Each of these should be initiated in setUpAll()
     */

    // CLASS IDs
    static ClassIdentity triangleClassId;
    static ClassIdentity forkStrategyClassId;
    static ClassIdentity forkStrategy2ClassId;

    // MUTANT FILES
    static File[] triangleMutantFiles;
    static File[] forkStrategyMutantFiles;
    static File[] forkStrategy2MutantFiles;

    // EQUIVALENCE
    static Map<Integer, Boolean> triangleEquivalence;
    static Map<Integer, Boolean> forkStrategyEquivalence;
    static Map<Integer, Boolean> forkStrategy2Equivalence;

    @BeforeAll
    static void setUpAll() throws IOException {
        z3 = new Z3Session();
        pipeline = new Pipeline(z3);
        strategy = new ForkStrategy(z3);

        System.out.println("Initializing Fork Strategy Test. This will take some time...");
        final long t0 = System.currentTimeMillis();
        triangleClassId = new ClassIdentity(new File(TestUtil.TRIANGLE));
        forkStrategyClassId = new ClassIdentity(new File(TestUtil.FORK_STRATEGY));
        forkStrategy2ClassId = new ClassIdentity(new File(TestUtil.FORK_STRATEGY2));

        triangleMutantFiles = TestUtil.getTriangleMutantFiles();
        forkStrategyMutantFiles = TestUtil.getForkStrategyMutantFiles();
        forkStrategy2MutantFiles = TestUtil.getForkStrategy2MutantFiles();

        triangleEquivalence = TestUtil.triangleEquivalence();
        forkStrategyEquivalence = TestUtil.forkStrategyEquivalence();
        forkStrategy2Equivalence = TestUtil.forkStrategy2Equivalence();

        System.out.print("    Adding Mutants...");
        addMutants(triangleClassId, "classify(III)I", triangleMutantFiles);
        addMutants(forkStrategyClassId, "max(II)I", forkStrategyMutantFiles);
        addMutants(forkStrategy2ClassId, "maxOfThree(III)I", forkStrategy2MutantFiles);
        // MethodIdentity method = forkStrategy2ClassId.getMethod("maxOfThree(III)I");
        // for (File cf : forkStrategy2MutantFiles) {
        //     if (cf.getPath().contains("12"))
        //         method.addMutant(cf);
        // }
        System.out.println("DONE");

        System.out.print("    Adding classes to pipeline...");

        // ADD ALL CLASS IDS TO PIPELINE HERE
        pipeline.add(triangleClassId);
        pipeline.add(forkStrategyClassId);
        pipeline.add(forkStrategy2ClassId);

        // END ADD CLASS IDS TO PIPELINE
        System.out.println("DONE");

        System.out.print("    Flowing pipeline...");
        pipeline.flow();
        final long tf = System.currentTimeMillis();
        System.out.println("DONE");
        System.out.println("Initialization took " + (tf - t0) + "ms");
    }

    static void addMutants(ClassIdentity cid, String desc, File[] mutantFiles) throws IOException {
        MethodIdentity method = cid.getMethod(desc);
        for (File cf : mutantFiles) {
            method.addMutant(cf);
        }
    }

    @BeforeEach
    public void beforeEach() {
        z3.z3.reset();
        strategy.reset();
    }

    @AfterEach
    public void tearDown() {
        final Statistics statistics = z3.z3.getStatistics();
        System.out.println(statistics);
    }

    @Test
    public void testCalculateDomSets1() throws IOException {
        final ClassIdentity cid = loadOrigClassFile(new File(TestUtil.TEST_CLASSES + "/SimpleIfWithReturn"));
        final MethodIdentity mid = cid.getMethod("f(I)I");
        final Pipeline pipeline = new Pipeline(z3);
        final ForkStrategy strategy = new ForkStrategy(z3);

        pipeline.add(cid);
        pipeline.flow();

        final Deque<IBlock> ordering = strategy.calculateBlockOrder(mid);

        final ControlFlowGraph cfg = mid.cfg;

        IBlock top = cfg.getStart();
        IBlock bot = cfg.getEnd();
        IBlock b1 = cfg.getBlockForInstruction(2);
        IBlock b2 = cfg.getBlockForInstruction(23);
        IBlock b3 = cfg.getBlockForInstruction(9);
        IBlock b4 = cfg.getBlockForInstruction(18);
        IBlock b5 = cfg.getBlockForInstruction(13);

        Map<IBlock, Integer> indexes = new HashMap<>();
        int i = ordering.size();
        for (IBlock b : ordering) {
            indexes.put(b, --i);
        }

        int iTop = indexes.get(top), iBot = indexes.get(bot), i1 = indexes.get(b1), i2 = indexes.get(b2),
                i3 = indexes.get(b3), i4 = indexes.get(b4), i5 = indexes.get(b5);

        for (IBlock b : ordering) {
            assertTrue(indexes.get(top) <= indexes.get(b));
            assertTrue(indexes.get(bot) >= indexes.get(b));
        }

        assertTrue(i1 < i2);
        assertTrue(i1 < i3);
        assertTrue(i1 < i4);
        assertTrue(i1 < i5);
        assertTrue(i3 < i4);
        assertTrue(i3 < i5);

    }

    @Test
    public void testCalculateDomSets2() throws IOException {
        final ClassIdentity cid = loadOrigClassFile(new File(TestUtil.TEST_CLASSES + "/WideBranching"));
        final MethodIdentity mid = cid.getMethod("maxOfFour(IIII)I");
        final Pipeline pipeline = new Pipeline(z3);
        final ForkStrategy strategy = new ForkStrategy(z3);

        pipeline.add(cid);
        pipeline.flow();

        final Deque<IBlock> ordering = strategy.calculateBlockOrder(mid);
        final ControlFlowGraph cfg = mid.cfg;

        IBlock top = cfg.getStart();
        IBlock bot = cfg.getEnd();
        IBlock b1 = cfg.getBlockForInstruction(2);
        IBlock b2 = cfg.getBlockForInstruction(42);
        IBlock b3 = cfg.getBlockForInstruction(7);
        IBlock b4 = cfg.getBlockForInstruction(62);
        IBlock b5 = cfg.getBlockForInstruction(47);
        IBlock b6 = cfg.getBlockForInstruction(27);
        IBlock b7 = cfg.getBlockForInstruction(12);
        IBlock b8 = cfg.getBlockForInstruction(72);
        IBlock b9 = cfg.getBlockForInstruction(67);
        IBlock b10 = cfg.getBlockForInstruction(57);
        IBlock b11 = cfg.getBlockForInstruction(52);
        IBlock b12 = cfg.getBlockForInstruction(37);
        IBlock b13 = cfg.getBlockForInstruction(32);
        IBlock b14 = cfg.getBlockForInstruction(22);
        IBlock b15 = cfg.getBlockForInstruction(17);

        IBlock[] blocks = {b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15};

        Map<IBlock, Integer> idxs = new HashMap<>();
        int i = ordering.size();
        for (IBlock b : ordering) {
            idxs.put(b, --i);
        }

        int iTop = idxs.get(top), iBot = idxs.get(bot),
                i1 = idxs.get(b1), i2 = idxs.get(b2), i3 = idxs.get(b3), i4 = idxs.get(b4), i5 = idxs.get(b5),
                i6 = idxs.get(b6), i7 = idxs.get(b7), i8 = idxs.get(b8), i9 = idxs.get(b9), i10 = idxs.get(b10),
                i11 = idxs.get(b11), i12 = idxs.get(b12), i13 = idxs.get(b13), i14 = idxs.get(b14), i15 = idxs.get(b15);

        for (IBlock b : ordering) {
            assertTrue(idxs.get(top) <= idxs.get(b));
            assertTrue(idxs.get(bot) >= idxs.get(b));
        }

        assertTrue(i1 < i2);
        assertTrue(i1 < i3);
        assertTrue(i1 < i4);
        assertTrue(i1 < i5);
        assertTrue(i1 < i6);
        assertTrue(i1 < i7);
        assertTrue(i1 < i8);
        assertTrue(i1 < i9);
        assertTrue(i1 < i10);
        assertTrue(i1 < i11);
        assertTrue(i1 < i12);
        assertTrue(i1 < i13);
        assertTrue(i1 < i14);
        assertTrue(i1 < i15);

        assertTrue(i2 < i4);
        assertTrue(i2 < i5);
        assertTrue(i2 < i8);
        assertTrue(i2 < i9);
        assertTrue(i2 < i10);
        assertTrue(i2 < i11);

        assertTrue(i3 < i6);
        assertTrue(i3 < i7);
        assertTrue(i3 < i12);
        assertTrue(i3 < i13);
        assertTrue(i3 < i14);
        assertTrue(i3 < i15);

        assertTrue(i4 < i8);
        assertTrue(i4 < i9);

        assertTrue(i5 < i10);
        assertTrue(i5 < i11);

        assertTrue(i6 < i12);
        assertTrue(i6 < i13);

        assertTrue(i7 < i14);
        assertTrue(i7 < i15);

    }

    @Test
    void test_forkStrategyTriangleTest() throws IOException {
        runTest(triangleClassId,
                "classify(III)I",
                triangleMutantFiles,
                71,
                triangleEquivalence);
    }

    @Test
    void test_forkStrategyForkStrategyTest() throws IOException {
        runTest(forkStrategyClassId,
                "max(II)I",
                forkStrategyMutantFiles,
                79,
                forkStrategyEquivalence);
    }

    @Test
    void test_forkStrategy2ForkStrategyTest() throws IOException {
        runTest(forkStrategy2ClassId,
                "maxOfThree(III)I",
                forkStrategy2MutantFiles,
                80,
                forkStrategy2Equivalence);
    }
    /**
     * @param cid ClassIdentity to run test on
     * @param methodDesc Method desc
     * @param mutantFiles Array of mutant files
     * @param uglyHackToTellWhereInThePathStringTheMutantNumberIsLocated An ugly hack to tell where in the path string
     *                                                                   the mutant number is located
     * @param equivalence A map from Integers to Booleans specifying if mutant i is an equivalent mutant or not
     * @throws IOException
     */
    void runTest(ClassIdentity cid, String methodDesc, File[] mutantFiles, int uglyHackToTellWhereInThePathStringTheMutantNumberIsLocated,
                 Map<Integer, Boolean> equivalence) throws IOException {
        final MethodIdentity method = cid.getMethod(methodDesc);

        List<Error> errors = new ArrayList<>();

        System.out.print("Applying fork strategy...");
        final long t0 = System.currentTimeMillis();
        strategy.applyToMethod(method);
        final long tf = System.currentTimeMillis();
        System.out.println("Done");
        System.out.println("    NUMBER OF MUTANTS REASONED ABOUT: " + strategy.reasonedAbout.size());
        System.out.println("                          TOTAL TIME: "  + (tf - t0) + " ms");
        System.out.println("                        AVERAGE TIME: "  + ((double)(tf - t0)) / strategy.reasonedAbout.size() + " ms/mutant");

        final Map<MutantIdentity, Integer> mutantIndices = new HashMap<>();
        final List<Pair<MutantIdentity, Integer>> pairs = method.mutants.stream()
                .filter(m -> CFGUtil.isIsomorphic(method.cfg, m.cfg))
                .map(m -> new Pair<>(m, m.path))
                .map(p -> new Pair<>(p.first, p.second.substring(uglyHackToTellWhereInThePathStringTheMutantNumberIsLocated, p.second.indexOf("/", uglyHackToTellWhereInThePathStringTheMutantNumberIsLocated))))
                .map(p -> new Pair<>(p.first, Integer.parseInt(p.second)))
                .collect(Collectors.toList());

        for (Pair<MutantIdentity, Integer> p : pairs) {
            mutantIndices.put(p.first, p.second);
        }


        for (MutantIdentity mid : strategy.reasonedAbout) {
            Integer num = mutantIndices.get(mid);
            final Boolean equiv = equivalence.get(num);
            final SolverResult result = mid.getResult();
            System.out.print("Testing mutant " + num + "...");
            if (equiv != result.isEquiv()) {
                System.out.println("FAIL");
                errors.add(new Error("testTriangle", mid, num, equiv, result));
            } else {
                System.out.println("PASS");
            }
        }

        if (!errors.isEmpty()) {
            for (Error e : errors) {
                System.out.println(e.toString());
            }
        }
        System.out.println("SUMMARY");
        System.out.println("Total of " + errors.size() + " errors:");
        for (Error e : errors) {
            System.out.println("    " + e.num + ": expected " + (e.equiv? "EQUIV" : "NEQUIV") + ", actual: " + e.result.toString());
        }
        assertEquals(0, errors.size());
    }

    /**
     * Load a class file and returns the ClassIdentity.
     * @param path
     */
    private ClassIdentity loadOrigClassFile(File path) throws IOException {
        String location = path.getParent();
        String name = path.getName();
        String sanitized = name.endsWith(".class") ? name.substring(0, name.length() - 6) : name;
        return new ClassIdentity(sanitized, location);
    }

    private MutantClassIdentity loadMutantClassFile(File path, ClassIdentity orig, String desc) throws IOException {
        String location = path.getParent();
        String name = path.getName();
        String sanitized = name.endsWith(".class") ? name.substring(0, name.length() - 6) : name;
        return new MutantClassIdentity(sanitized, location, orig, desc);
    }

    class Error {
        public String testName;
        public final MutantIdentity mid;
        public final int num;
        public final boolean equiv;
        public final SolverResult result;

        Error(String testName, MutantIdentity mid, int num, boolean equiv, SolverResult result) {
            this.testName = testName;
            this.mid = mid;
            this.num = num;
            this.equiv = equiv;
            this.result = result;
        }

        @Override
        public String toString() {
            String sb = "Failed test " + testName + " on mutant " + num +
                    "\nExpected " + (equiv ? "EQUIV" : "NEQUIV") + " but found " +
                    result.toString() + " instead.\n" +
                    "ASSERTION STRING: " + result.assertionString +
                    "\n================================================================================\n" +
                    "MODEL STRING: " +
                    (result.isNotEquiv() ? Z3Session.getSortedModelString(result.getModel()) : "N/A");
            return sb;

        }
    }
}