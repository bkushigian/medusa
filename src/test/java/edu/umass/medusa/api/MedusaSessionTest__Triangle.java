package edu.umass.medusa.api;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.util.TestUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.NoSuchFileException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MedusaSessionTest__Triangle {
    private static File[] triangleMutantFiles;
    private static MedusaSession session;
    private static final String classOffsetMethodName = "triangle/Triangle.classify(III)I";

    @BeforeAll
    public static void setUp() throws NoSuchFileException {
        triangleMutantFiles = TestUtil.getTriangleMutantFiles();
    }

    @BeforeEach
    public void beforeEach() {
        session = new MedusaSession(new File(TestUtil.TRIANGLE), null);
        for (File mf : triangleMutantFiles) {
            session.addMutant(classOffsetMethodName, mf);
        }
    }

    @Test
    public void testAddMutant() throws NoSuchFileException {
        assertEquals(125, session.mutants.size());

        final ClassIdentity triangleClassId = session.getClassId("triangle/Triangle");
        assertNotNull(triangleClassId);

        final MethodIdentity classifyMethodId = triangleClassId.getMethod("classify(III)I");
        assertNotNull(classifyMethodId);

        final Set<MutantIdentity> mutants = classifyMethodId.mutants;
        assertEquals(125, mutants.size());
    }

    @Test
    void testPipeline() {
        session.getPipeline().flow();
        System.out.println(session);
        /* TODO: Finish this test --- find some assertions to make here.
                 It would be good to have structural/isomorphism data to assert.
                 Special attention should be paid to short circuiting mutants as this
                 may change the structure of the graph.
        */
        final MethodIdentity classify = session.getClassId("triangle/Triangle").getMethod("classify(III)I");
        final MethodIdentity constructor = session.getClassId("triangle/Triangle").getMethod("<init>()V");
        assertEquals(125, classify.mutants.size());
        assertEquals(0, constructor.mutants.size());
    }



}