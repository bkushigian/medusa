package edu.umass.medusa;

import com.microsoft.z3.Params;
import de.codesourcery.asm.controlflow.ControlFlowGrapher;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.pipeline.Pipeline;
import edu.umass.medusa.solver.strategies.ForkStrategy;
import edu.umass.medusa.solver.strategies.ImprovedNaiveStrategy;
import edu.umass.medusa.solver.strategies.NaiveStrategy;
import edu.umass.medusa.solver.strategies.Strategy;
import edu.umass.medusa.util.FSUtil;
import edu.umass.medusa.util.Z3Session;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * A simple command line interface for Medusa.
 */
public class MedusaCLI {

  File origFile;
  String desc;
  File[] mutantFiles;
  Z3Session z3;
  Pipeline pipeline;
  ClassIdentity orig;
  MethodIdentity origMethod;
  MutantIdentity[] mutants;
  MethodConstraint origConstraint;
  Strategy strategy;
  String strategyString = "fork"; // DEFAULT
  int timeout = 0;
  int verbosity = 0;

  public MedusaCLI(String origPath, String desc, String... mutants) {
    this.origFile = new File(origPath);
    this.mutantFiles = Arrays.stream(mutants).map(File::new).toArray(File[]::new);
    this.desc = desc;
    this.z3 = new Z3Session();
    this.pipeline = new Pipeline(z3);
    this.orig = null;
    this.mutants = new MutantIdentity[mutants.length];
  }

  public MedusaCLI(String... args) {
    this.z3 = new Z3Session();
    this.pipeline = new Pipeline(z3);
    this.orig = null;
    this.mutants = null;
    this.origFile = null;
    this.mutantFiles = null;
    this.desc = null;

    handleArgs(args);

    this.mutants = new MutantIdentity[mutantFiles.length];
  }

  /**
   * Handle the args of the MedusaCLI. Any mode-related args (i.e., running some sort of analysis,
   * are handled outside of this method. This is for once we know that we are running one of our
   * strategies).
   */
  public void handleArgs(final String[] args) {
    if (args.length < 2) {
      usage();
    }

    int i = 0;
    boolean parsingFlags = true;

    while (i < args.length && parsingFlags) {
      final String arg = args[i++];
      switch (arg) {
        case "--strategy":
          strategyString = args[i++];
          break;
        case "--verbosity":
        case "-v":
          try {
            verbosity = Integer.parseInt(args[i++]);
          } catch (Exception e) {
            usage("Couldn't parse verbosity as an int: " + args[i - 1]);
          }
          break;
        case "--desc":
        case "-d":
          desc = args[i++];
          break;
        case "--timeout":
          try {
            timeout = Integer.parseInt(args[i++]);
          } catch (Exception e) {
            usage("Couldn't parse timeout as int: " + args[i - 1]);
          }
          break;
        case "--path":
        case "-p":
          origFile = new File(args[i++]);
        case "--help":
        case "-h":
          usage();

        default:
          parsingFlags = false;
          --i;
          break;
      }
    }

    if (origFile == null) {
      origFile = new File(args[i++]);
    }
    if (desc == null) {
      desc = args[i++];
    }

    if (i < args.length) {
      File[] bases = Arrays.stream(Arrays.copyOfRange(args, i, args.length))
          .map(File::new)
          .toArray(File[]::new);
      try {
        mutantFiles = FSUtil.collectClassfiles(bases).toArray(new File[0]);
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      mutantFiles = new File[0];
    }

    switch (strategyString) {
      case "naive":
        strategy = new NaiveStrategy(z3);
        break;

      case "cache":
      case "caching":
      case "improved":
      case "improved-naive":
        strategy = new ImprovedNaiveStrategy(z3);
        break;

      default:
        System.err.println("Unrecognized strategy: " + strategyString + ", defaulting to \"fork\"");
      case "default":
      case "fork":
        strategy = new ForkStrategy(z3);
    }
    strategy.setVerbosity(verbosity);
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      usage();
    } else if (args.length == 1) {
      if ("controlflow".equals(args[0])) {
        try {
          ControlFlowGrapher.main(args);
        } catch (Exception e) {
          e.printStackTrace();
        }
        System.exit(1);
      }
      if (!"time".equals(args[0])) {
        usage();
      }
    }

    System.out.println("Mode arge: " + args[0]);
    if (args[0].equals("analyze")) {
      switch (args[1]) {
        case "applicability":
          String[] appArgs = Arrays.copyOfRange(args, 2, args.length);
          edu.umass.medusa.metrics.applicability.ApplicabilityAnalyzer.main(appArgs);
          return;

        case "branch":
        case "jumps":
          String[] jumpArgs = Arrays.copyOfRange(args, 2, args.length);
          edu.umass.medusa.metrics.branch.JumpInsnAnalyzer.main(jumpArgs);
          return;

        default:
          usage("Unrecognized analysis \"" + args[1] + "\', aborting");

      }
    } else if (args[0].equals("time")) {
      String[] timeArgs = Arrays.copyOfRange(args, 1, args.length);
      edu.umass.medusa.metrics.timing.StrategyTimer.main(timeArgs);
      return;
    } else if (args[0].equals("controlflow")) {
      System.out.println(">>>Creating CFG<<<");

      try {
        String[] cfgargs = Arrays.copyOfRange(args, 1, args.length);
        ControlFlowGrapher.main(cfgargs);
        for (String s : cfgargs) {
          System.out.println(s);
        }
        return;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    try {
      MedusaCLI cli = new MedusaCLI(args);
      cli.run();
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(2);
    }
  }

  public int run() throws IOException {
    this.orig = loadOrigClassFile(this.origFile);
    this.origMethod = orig.getMethod(desc);

    if (origMethod == null) {
      throw new RuntimeException("Couldn't find method with descriptor \"" + desc + "\"");
    }

    // Ensure that there is a method with proper description
    if (orig.getMethod(desc) == null) {
      throw new RuntimeException(
          String.format("Class %s does not have a method with description %s",
              origFile.getName(),
              desc));
    }

    System.out.println("=== MEDUSA ===");
    System.out.println("  Strategy: " + strategyString);
    System.out.println("  Class:    " + origFile.getName());
    System.out.println("  Method:   " + desc);
    System.out.println("  Mutants:  " + mutantFiles.length);

    // Populate the mutants array
    Arrays.sort(mutantFiles);
    int i = 0;
    for (File mf : mutantFiles) {
      MutantClassIdentity mcid = loadMutantClassFile(mf, orig, desc);
      MutantIdentity mid = mcid.mutantId;
      mid.path = mf.getPath();
      if (!mid.name.equals(desc)) {
        throw new RuntimeException("Desc strings do not match\n" +
            "Expected: " + desc +
            "\n  Actual:" + mid.desc);
      }
      origMethod.addMutant(mid);
      mutants[i++] = mid;
    }

    pipeline.add(orig);
    pipeline.flow();

    if (timeout > 0) {
      final Params params = z3.ctx.mkParams();
      params.add("timeout", timeout);
      z3.z3.setParameters(params);
    }

    strategy.applyToMethod(origMethod);
    printResults();

    return 0;           // SUCCESS
  }

  public void printResults() {
    int i = 0;
    for (MutantIdentity mid : mutants) {
      System.out.println(
          String.format("%s\t%s", mutantFiles[i++].toString(), mid.getResult().toString()));
    }
  }

  /**
   * Load a class file and returns the ClassIdentity.
   */
  private ClassIdentity loadOrigClassFile(File path) throws IOException {
    String location = path.getParent();
    String name = path.getName();
    String sanitized = name.endsWith(".class") ? name.substring(0, name.length() - 6) : name;
    return new ClassIdentity(sanitized, location);
  }

  private MutantClassIdentity loadMutantClassFile(File path, ClassIdentity orig, String desc)
      throws IOException {
    String location = path.getParent();
    String name = path.getName();
    String sanitized = name.endsWith(".class") ? name.substring(0, name.length() - 6) : name;
    return new MutantClassIdentity(sanitized, location, orig, desc);
  }

  private static void usage() {
    usage("");
  }

  private static void usage(String message) {
    if (message != null && !message.isEmpty()) {
      System.err.println(message);
    }
    StringBuilder sb = new StringBuilder(
        "MedusaCLI usage: java MedusaCLI [--strategy STRATEGY_STRING] orig desc [mutant1 mutant2 ...]\n");
    sb.append("                 java MedusaCLI analyze jumps [source-root]\n");
    sb.append("                 java MedusaCLI analyze applicability [--path source-root]\n")
        .append(
            "                                                      [--filter (all|static|local-static)]\n")
        .append(
            "                                                      [--instructions instrs]\n\n");
    sb.append("                 java MedusaCLI time [OPTIONS] orig desc mutants...\n");

    sb.append("PARAMETERS\n");
    sb.append("==========\n");
    sb.append("strategy: one of \"fork\", \"cache\", or \"naive\" (default is \"fork\"). This determines\n");
    sb.append("          medusa will run.\n");
    sb.append("    orig: the original class file that is being constrained.\n");
    sb.append("  method: the description string of the method being constrained. For a method\n");
    sb.append("              int foo(Integer i, boolean b){...}\n");
    sb.append("          the description would be\n");
    sb.append("              foo(Ljava/lang/Integer;Z)I\n");
    sb.append("          See below for more details\n");
    sb.append(
        "  mutant: path to the class of the nth mutant to constrain. This must be a mutant\n");
    sb.append("          of the class specified by `origFile` and must mutate the method\n");
    sb.append("          specified by `desc`\n\n");
    sb.append("METHOD DESCRIPTIONS\n");
    sb.append("===================\n");
    sb.append("Method descriptions have the following format\n");
    sb.append("    method-name(arg1arg2arg3...)return-type\n");
    sb.append("where each argument and return type may be a primitive or a reference type.\n");
    sb.append("Primitives have single letter descriptions:\n\n");
    sb.append("    B: byte\n");
    sb.append("    S: short\n");
    sb.append("    I: int\n");
    sb.append("    J: long\n");
    sb.append("    F: float\n");
    sb.append("    D: double\n");
    sb.append("    C: char\n");
    sb.append("    Z: boolean\n\n");
    sb.append(
        "and non-array reference types have descriptions of the form \"Lpath/to/ClassName;\".\n");
    sb.append("For an array type has a description formed by prefixing a '[' symbol to T's\n");
    sb.append("description. Thus, an array of java.lang.Integers has description\n");
    sb.append("    [Ljava/lang/Integer;\n");
    sb.append("while a two dimensional array of primitive ints will have a description of\n");
    sb.append("    [[I\n");

    System.err.println(sb.toString());

    System.exit(1);
  }
}
