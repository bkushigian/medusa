package edu.umass.medusa.api;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.pipeline.Pipeline;
import edu.umass.medusa.solver.MedusaSolver;
import edu.umass.medusa.util.ASMUtil;
import edu.umass.medusa.util.Pair;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.tree.ClassNode;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * This wraps data needed for Medusa TODO: Clean up interface, encapsulate
 */
public class MedusaSession {

  public final Z3Session z3;
  public final Pipeline pipeline;
  public final Map<String, ClassIdentity> classMap;
  public final Set<MutantIdentity> mutants;
  private final MedusaSolver solver;
  private final Set<ClassIdentity> classes;

  /**
   * Begin a Medusa Session
   *
   * @param classFiles a set of class files to consider
   * @param mutantFiles a set of (class-offset-method-name, mutant class file) pairs
   *
   * Note that a unique class-offset method name is formed by taking the package/name/ClassName
   * concatenated with the methodName, separated by a dot, concatenated with the method description
   * (ArgString)ReturnString.
   *
   * For instance, if a class MathUtils exists in package com.example.utils, and there is a method
   * <pre>
   *     int max(int a, int b) {
   *         return a > b ? a : b;
   *     }
   * </pre>
   * then the unique class-offset method name would be "com/example/utils/MathUtils.max(II)I
   */
  public MedusaSession(Set<File> classFiles, Set<Pair<String, File>> mutantFiles) {
    z3 = new Z3Session();
    classMap = new HashMap<>();
    mutants = new HashSet<>();

    for (File cp : classFiles) {
      addClass(cp);
    }

    if (mutantFiles != null) {
      addMutantFiles(mutantFiles);
    }

    classes = new HashSet<ClassIdentity>(classMap.values());

    solver = new MedusaSolver(z3);
    pipeline = new Pipeline(z3, classes);
  }

  public MedusaSession(File classFile, Set<Pair<String, File>> mutantFiles) {
    z3 = new Z3Session();
    classMap = new HashMap<>();
    mutants = new HashSet<>();
    addClass(classFile);

    if (mutantFiles != null) {
      addMutantFiles(mutantFiles);
    }

    classes = new HashSet<ClassIdentity>(classMap.values());
    solver = new MedusaSolver(z3);
    pipeline = new Pipeline(solver, classes);
  }

  public boolean addClass(File classFile) {
    try {
      ClassNode cn = ASMUtil.readClass(classFile);
      ClassIdentity cid = new ClassIdentity(cn);
      classMap.put(cn.name, cid);
    } catch (IOException e) {
      System.err.println(String.format("Couldn't open file %s: %s", classFile, e.getMessage()));
      return false;
    }
    return true;
  }

  public boolean addMutant(String classOffsetMethodName, File mutantFile) {
    final String[] split = classOffsetMethodName.split("\\.");
    ClassIdentity originalClass = classMap.get(split[0]);
    if (originalClass == null) {
      return false;
    }

    final MethodIdentity method = originalClass.getMethod(classOffsetMethodName);

    if (method == null) {
      return false;
    }

    try {
      ClassNode mutantNode = ASMUtil.readClass(mutantFile);
      MutantClassIdentity mutantClassId = new MutantClassIdentity(mutantNode, originalClass,
          classOffsetMethodName);
      method.addMutant(mutantClassId.mutantId);
      mutants.add(mutantClassId.mutantId);
      mutantClassId.mutantId.path = mutantFile.getPath();
    } catch (IOException e) {
      System.err.println("Couldn't find mutant file " + mutantFile.getPath());
      return false;
    }
    return true;
  }

  public boolean addMutantFiles(Set<Pair<String, File>> mutantFiles) {
    boolean result = true;
    for (Pair<String, File> p : mutantFiles) {
      String absMethodName = p.first;
      File mutFile = p.second;
      result ^= addMutant(absMethodName, mutFile);
    }
    return result;
  }

  public ClassIdentity getClassId(String className) {
    return classMap.get(className);
  }

  public Pipeline getPipeline() {
    return pipeline;
  }
}
