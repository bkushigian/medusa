/**
 * This package defines the API to be invoked by Medusa users.
 */
package edu.umass.medusa.api;