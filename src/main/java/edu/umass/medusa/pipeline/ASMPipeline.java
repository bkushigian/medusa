package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.pipeline.asm.ProcessASM;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ASMPipeline {

  Set<ClassIdentity> classes;

  public ASMPipeline() {
    this(new HashSet<>());
  }

  public ASMPipeline(Set<ClassIdentity> classes) {
    this.classes = classes;
  }

  public boolean add(ClassIdentity cid) {
    return classes.add(cid);
  }

  public boolean addAll(Collection<ClassIdentity> cids) {
    return classes.addAll(cids);
  }

  /**
   * Invoke the pipeline, making all suitable transformations, and call into the {@code
   * MedusaSolver}
   */
  public void flow() {
    (new ProcessASM()).flow(classes);
  }

  public void reset() {
    classes = new HashSet<>();
  }
}

