package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.ClassConstraintGen;
import edu.umass.medusa.format.constraint.MethodConstraintGen;
import edu.umass.medusa.format.constraint.MutantConstraintGen;
import edu.umass.medusa.util.Z3Session;

public class PipeCFG2ConstraintGen extends TransformationPipe {

  private final Z3Session z3;

  public PipeCFG2ConstraintGen(Z3Session z3) {
    this.z3 = z3;
  }

  @Override
  public void flow(ClassIdentity input) {
    input.gen = new ClassConstraintGen(input, z3);
    super.flow(input);
  }

  @Override
  protected void flow(MethodIdentity method) throws Exception {
    super.flow(method);

    if (method.gen == null && !method.name.contains("<init>")) {
      method.gen = new MethodConstraintGen(method, z3);
    }
  }

  @Override
  protected void flow(MutantIdentity mutant) throws Exception {
    // TODO: Handle mutants
    mutant.gen = new MutantConstraintGen(mutant, z3);
  }
}
