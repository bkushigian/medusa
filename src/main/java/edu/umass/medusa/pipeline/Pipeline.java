package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.pipeline.asm.ProcessASM;
import edu.umass.medusa.solver.MedusaSolver;
import edu.umass.medusa.util.Z3Session;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A {@code Pipeline} transforms initial bytecode in the ASM format to TODO: Clean up the
 * constructors. There should be one, or at most two, public constructors -> Make it parametrized by
 * a MedusaSession?
 */
public class Pipeline {

  Z3Session z3;
  Set<ClassIdentity> classes;
  MedusaSolver medusaSolver;

  public Pipeline(Z3Session z3) {
    this(z3, new HashSet<ClassIdentity>());
  }

  /**
   * Create a {@code Pipeline} object
   *
   * @param classes {@code ClassIdentity} set to operate on
   */
  public Pipeline(Set<ClassIdentity> classes) {
    this(new Z3Session(), classes);
  }

  public Pipeline(Z3Session z3, Set<ClassIdentity> classes) {
    this(new MedusaSolver(z3), classes);
  }

  public Pipeline(MedusaSolver solver, Set<ClassIdentity> classes) {
    this.classes = classes;
    z3 = solver.z3;
    medusaSolver = solver;
  }

  public boolean add(ClassIdentity cid) {
    return classes.add(cid);
  }

  public boolean addAll(Collection<ClassIdentity> cids) {
    return classes.addAll(cids);
  }

  /**
   * Invoke the pipeline, making all suitable transformations, and call into the {@code
   * MedusaSolver}
   */
  public void flow() {
    (new ProcessASM()).flow(classes);
    (new PipeASM2CFG()).flow(classes);
    (new ProcessCFG()).flow(classes);
    (new PipeCFG2ConstraintGen(z3)).flow(classes);
  }

  public void reset() {
    classes = new HashSet<>();
  }
}
