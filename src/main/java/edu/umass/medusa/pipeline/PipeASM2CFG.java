package edu.umass.medusa.pipeline;

import de.codesourcery.asm.controlflow.ControlFlowAnalyzer;
import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.exceptions.NotImplementedException;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.util.TypeUtil;
import edu.umass.medusa.util.TypeUtil.Type;
import edu.umass.medusa.util.bytecode.BytecodeUtil;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * Transforms bytecode to CFG
 */
public class PipeASM2CFG extends TransformationPipe {

  ControlFlowAnalyzer analyzer = new ControlFlowAnalyzer();

  /**
   * Flow a method through the pipe
   */
  @Override
  public void flow(final MethodIdentity method) throws Exception {
    super.flow(method);
    method.cfg = analyzer.analyze(method.name, method.method);
    //computeStackSizes(method.cfg);
  }

  /**
   * Flow a mutant through the pipe
   */
  @Override
  public void flow(final MutantIdentity mutant) throws Exception {
    mutant.cfg = analyzer.analyze(mutant.owner, mutant.method);
    computeStackSizes(mutant.cfg);
  }

  private void computeStackSizes(final ControlFlowGraph cfg) {
    final MethodNode method = cfg.getMethod();
    final InsnList instructions = method.instructions;
    final Set<IBlock> visited = new HashSet<>();
    final Deque<IBlock> toVisit = new ArrayDeque<>();
    final IBlock start = cfg.getStart();
    start.storeProperty("initial-stack-size", 0);
    toVisit.add(cfg.getStart());

    // Invariant: each element in toVisit has property "initial-stack-size" set correctly and
    //     "stack-size-delta" unset (i.e., null), and items are removed from toVisit only when
    //     the correct value of a "stack-size-delta" is known.
    while (!toVisit.isEmpty()) {
      final IBlock b = toVisit.poll();
      visited.add(b);

      final Integer delta = computeDelta(b, instructions);
      final Integer termStackSize = (Integer)b.getProperty("initial-stack-size") + delta;
      b.storeProperty("stack-size-delta", delta);
      for (IBlock succ : b.getRegularSuccessors()) {
        if (!visited.contains(succ) && !toVisit.contains(succ)) {     // XXX: Slow lookup in toVisit
          succ.storeProperty("initial-stack-size", termStackSize);
          toVisit.add(succ);
        } else {
          assert termStackSize.equals(succ.getProperty("initial-stack-size"));
        }
      }
    }
  }

  private int computeDelta(IBlock block, InsnList instructions) {
    int delta = 0;
    for (Integer idx : block.getInstructionNums()) {
      /* TODO: Check for special instructions, including:
       *    - DUP2
       *    - DUP2_X1
       *    - DUP2_X2
       *    - ATHROW
       *    - INVOKEDYNAMIC
       *    - INVOKEINTERFACE
       *    - INVOKESPECIAL
       *    - INVOKESTATIC
       *    - INVOKEVIRTUAL
       *    - MULTIANEWARRAY
       *    - POP2
       */

      final AbstractInsnNode insn = instructions.get(idx);
      switch (insn.getOpcode()) {
        case Opcodes.DUP2:
          // TODO:
          throw new NotImplementedException("DUP2 instruction is not implemented");
        case Opcodes.DUP2_X1:
          // TODO:
          throw new NotImplementedException("DUP2_X1 instruction is not implemented");
        case Opcodes.DUP2_X2:
          // TODO:
          throw new NotImplementedException("DUP2_X2 instruction is not implemented");
        case Opcodes.ATHROW:
          // TODO:
          throw new NotImplementedException("ATHROW instruction is not implemented");
        case Opcodes.INVOKEDYNAMIC:
          // TODO:
          throw new NotImplementedException("INVOKEDYNAMIC instruction is not implemented");
        case Opcodes.INVOKEINTERFACE:
          // TODO:
          throw new NotImplementedException("INVOKEINTERFACE instruction is not implemented");
        case Opcodes.INVOKESPECIAL:
          // TODO:
          final MethodInsnNode n = (MethodInsnNode)insn;
          final Type[] types = TypeUtil.parseMethodDesc(n.desc);
          throw new NotImplementedException("INVOKESPECIAL instruction is not implemented");
        case Opcodes.INVOKESTATIC:
          // TODO:
          throw new NotImplementedException("INVOKESTATIC instruction is not implemented");
        case Opcodes.INVOKEVIRTUAL:
          // TODO:
          throw new NotImplementedException("INVOKEVIRTUAL instruction is not implemented");
        case Opcodes.MULTIANEWARRAY:
          // TODO:
          throw new NotImplementedException("MULTIANEWARRAY instruction is not implemented");
        case Opcodes.POP2:
          // TODO:
          throw new NotImplementedException("POP2 instruction is not implemented");
        default:
          break;
      }
      final int insnDelta = BytecodeUtil.stackSizeDelta(insn);
      if (insnDelta != Integer.MIN_VALUE) {
        delta += insnDelta;
      } else {
        throw new RuntimeException("Tried to obtain stack size delta of special instruction.");
      }
    }
    return delta;
  }
}
