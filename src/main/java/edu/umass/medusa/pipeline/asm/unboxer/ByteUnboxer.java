package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class ByteUnboxer extends Unboxer {

  public ByteUnboxer() {
    super("Byte", "byte", TypeUtil.BYTE_BOXED_TYPE);
  }
}
