package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class IntegerUnboxer extends Unboxer {

  public IntegerUnboxer() {
    super("Integer", "int", TypeUtil.INTEGER_BOXED_TYPE);
  }
}
