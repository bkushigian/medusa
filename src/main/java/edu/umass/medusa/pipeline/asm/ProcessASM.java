package edu.umass.medusa.pipeline.asm;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.pipeline.ProcPipe;
import edu.umass.medusa.pipeline.asm.unboxer.IntegerUnboxer;

/**
 * Performs simplifications and extracts information on bytecode.
 */
public class ProcessASM extends ProcPipe {

  private ConstructorTransformer ct = new ConstructorTransformer();
  private IntegerUnboxer iub = new IntegerUnboxer();

  @Override
  public void flow(ClassIdentity input) {
    ct.flow(input);
    iub.flow(input);
  }
}
