package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class FloatUnboxer extends Unboxer {

  public FloatUnboxer() {
    super("Float", "float", TypeUtil.FLOAT_BOXED_TYPE);
  }
}
