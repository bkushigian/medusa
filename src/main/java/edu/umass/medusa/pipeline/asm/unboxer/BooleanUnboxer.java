package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class BooleanUnboxer extends Unboxer {

  public BooleanUnboxer() {
    super("Boolean", "boolean", "Z", TypeUtil.BOOLEAN_BOXED_TYPE);
  }
}
