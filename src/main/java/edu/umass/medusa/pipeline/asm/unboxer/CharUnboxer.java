package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class CharUnboxer extends Unboxer {

  public CharUnboxer() {
    super("Character", "char", TypeUtil.CHAR_BOXED_TYPE);
  }
}
