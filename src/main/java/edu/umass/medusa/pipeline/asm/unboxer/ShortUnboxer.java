package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.util.TypeUtil;

/**
 * This TransformationPipe attempts to unbox all {@code Integer} references
 */
public class ShortUnboxer extends Unboxer {

  public ShortUnboxer() {
    super("Short", "short", TypeUtil.SHORT_BOXED_TYPE);
  }
}
