package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.pipeline.TransformationPipe;
import edu.umass.medusa.util.TypeUtil;
import edu.umass.medusa.util.bytecode.BytecodeUtil;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

public abstract class Unboxer extends TransformationPipe {

  /**
   * The name of the class that is being unboxed (java/lang/Integer, ...)
   */
  final String className;

  /**
   * The primitive desc string (I for Integer, J for Long, ...)
   */
  final String primDesc;

  /**
   * The primitive name (int for Integer, long for Long, ...)
   */
  final String primName;

  /**
   * this is the name of the xxxValue() method (intValue() for Integer, longValue() for Long, ...)
   */
  final String primValueName;

  /**
   * This is the name of the valueOf() method
   */
  final String valueOfName;

  /**
   * the number of vartable slots that a primitive stores. int, byte, short, etc each take 1 slot
   * while longs and doubles take two slots.
   */
  final protected int primWidth = 1;

  final TypeUtil.Type type;

  protected Unboxer(String className, String primName, TypeUtil.Type type) {
    this(className, primName, primName.substring(0, 1).toUpperCase(), type);
  }

  protected Unboxer(String className, String primName, String primDesc, TypeUtil.Type type) {
    // TODO: Change type's type to BoxType or PrimType
    this.className = className.startsWith("java/lang/") ? className : "java/lang/" + className;
    this.primDesc = primDesc.toUpperCase();
    this.primName = primName;
    this.type = type;

    primValueName = primName + "Value";
    valueOfName = "valueOf";
  }

  @Override
  protected void flow(MutantIdentity mutant) {
    unboxMethod(mutant);
  }

  @Override
  protected void flow(MethodIdentity method) throws Exception {
    super.flow(method);
    unboxMethod(method);
  }

  public void unboxMethod(MethodIdentity method) {
    unboxMethod(method.method);
  }

  public void unboxMethod(MethodNode method) {
    final InsnList instructions = method.instructions;
    AbstractInsnNode n1 = instructions.getFirst();

    while (n1 != null) {
      if (n1.getOpcode() < 0) {
        n1 = n1.getNext();
        continue;
      }
      final AbstractInsnNode[] abstractInsnNodes = instructions.toArray();

      /*
       * If the next instruction is an aload then we will have a reference on the stack and it is
       * possibly an unboxing operation. We handle the following case:
       *
       * (1) An xxxValue() call is about to be made (i.e., an intValue())
       */
      if (n1.getOpcode() == Opcodes.ALOAD) {
        int loadIndex = ((VarInsnNode) n1).var;

        /*
         * (1) Check for calls of the form xxValue() (i.e., intValue(), longValue(), etc). These will
         *     have bytecode sequences of the form
         *
         *          ALOAD(var=n)
         *          INVOKEVIRTUAL(owner=java/lang/ClassName,name=primValue)
         *
         *     and should be replaced by an appropriate LOAD instruction and the INVOKEVIRTUAL should be
         *     stripped:
         *
         *          ILOAD(var=n)
         */
        if (n1.getNext().getOpcode() == Opcodes.INVOKEVIRTUAL) {
          MethodInsnNode next = (MethodInsnNode) n1.getNext();
          if (next.owner.equals(className) && next.name.equals(primValueName)) {
            n1 = n1.getPrevious();
            instructions.remove(n1.getNext());
            instructions.remove(next);
            instructions.insert(n1, BytecodeUtil.getLoad(primName, loadIndex));

            n1 = n1.getNext();
          }
        } else if (n1.getNext().getOpcode() == Opcodes.ARETURN) {
          final TypeUtil.Type[] sig = TypeUtil.parseMethodDesc(method.desc);
          TypeUtil.Type returnType = sig[sig.length - 1];
          if (returnType.equals(type)) {
            n1 = n1.getPrevious();
            instructions.remove(n1.getNext());                                  // Repeal
            instructions.insert(n1, BytecodeUtil.getLoad(primName, loadIndex)); // ... and replace
            n1 = n1.getNext();
            instructions.remove(n1.getNext());                                  // remove areturn
            instructions.insert(n1, BytecodeUtil.getReturn(primName));          // ... and replace
          }
        }

      } else if (n1.getOpcode() == Opcodes.INVOKESTATIC) {
        MethodInsnNode mNode = (MethodInsnNode) n1;
        if (mNode.owner.equals(className) && mNode.name.equals(valueOfName)) {
          n1 = n1.getPrevious();
          instructions.remove(n1.getNext());
          // TODO: Fix hack
          // This checks if the next bytecode is an astore and if so replaces
          // with a type-appropriate store operation. This is not a general solution
          // and there are legal classfiles that this will not handle correctly
          if (n1.getNext().getOpcode() == Opcodes.ASTORE) {
            final int idx = ((VarInsnNode) n1.getNext()).var;
            instructions.remove(n1.getNext());
            instructions.insert(n1, BytecodeUtil.getStore(primName, idx));
            n1 = n1.getNext();
          }
        }
      } else if (n1.getOpcode() == Opcodes.ARETURN) {
        final TypeUtil.Type[] sig = TypeUtil.parseMethodDesc(method.desc);
        TypeUtil.Type returnType = sig[sig.length - 1];
        if (returnType.equals(type)) {
          n1 = n1.getPrevious();
          instructions.remove(n1.getNext());
          instructions.insert(n1, BytecodeUtil.getReturn(primName));
        }
      }
      n1 = n1.getNext();
    }

    updateDesc(method);
  }

  void updateDesc(MethodNode mn) {
    mn.desc = mn.desc.replace(String.format("L%s;", className), primDesc);
  }

  AbstractInsnNode getNextConcrete(AbstractInsnNode node) {
    node = node.getNext();
    while (node != null && node.getOpcode() < 0) {
      node = node.getNext();
    }
    return node;
  }
}
