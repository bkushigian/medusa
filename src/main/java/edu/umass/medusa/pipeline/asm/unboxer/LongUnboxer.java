package edu.umass.medusa.pipeline.asm.unboxer;

import edu.umass.medusa.exceptions.MultiPrecisionUnboxingException;
import edu.umass.medusa.util.TypeUtil;
import org.objectweb.asm.tree.MethodNode;

public class LongUnboxer extends Unboxer {

  final protected int primWidth = 2;

  public LongUnboxer() {
    super("Long", "long", "J", TypeUtil.LONG_BOXED_TYPE);
  }

  @Override
  public void unboxMethod(MethodNode method) {
    throw new MultiPrecisionUnboxingException(
        "Double: Haven't implemented multi-precision unboxing");
    //super.unboxMethod(method);
  }
}
