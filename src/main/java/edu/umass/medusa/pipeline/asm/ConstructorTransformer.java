package edu.umass.medusa.pipeline.asm;


import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.pipeline.TransformationPipe;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

/**
 * this class removes calls to Object constructors
 */
public class ConstructorTransformer extends TransformationPipe {

  @Override
  protected void flow(MutantIdentity mutant) throws Exception {
    // TODO
  }

  @Override
  protected void flow(MethodIdentity method) throws Exception {
    super.flow(method);
    MethodNode methodNode = method.method;
    if (!"<init>".equals(method.shortName)) {
      return;
    }

    final InsnList instructions = methodNode.instructions;
    AbstractInsnNode n1 = instructions.getFirst(), n2;

    assert n1 != null;

    while (n1.getOpcode() < 1) {
      n1 = n1.getNext();
      if (n1 == null) {
        return;
      }
    }

    n2 = n1.getNext();
    while (n2.getOpcode() < 1) {
      n2 = n2.getNext();
      if (n2 == null) {
        return;
      }
    }

    if (n1.getOpcode() == Opcodes.ALOAD && n2.getOpcode() == Opcodes.INVOKESPECIAL) {
      MethodInsnNode invokeSpecial = (MethodInsnNode) n2;
      if ("java/lang/Object".equals(invokeSpecial.owner) && "()V".equals(invokeSpecial.desc)) {
        instructions.remove(n1);
        instructions.remove(n2);
      }
      return;
    }
    // Construct new method node

  }
}
