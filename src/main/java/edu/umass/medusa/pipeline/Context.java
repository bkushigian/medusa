package edu.umass.medusa.pipeline;

import java.util.HashMap;
import java.util.Map;

public class Context {

  public final Map<String, String> properties;

  public Context() {
    properties = new HashMap<>();
  }
}
