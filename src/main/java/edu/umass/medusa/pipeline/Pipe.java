package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.tree.analysis.AnalyzerException;

import java.util.Set;

/**
 * A pipe takes as input a program format of the type F and returns a program format of the type G
 */
public interface Pipe {

  /**
   * transform/process an input program format
   *
   * @param input input program
   * @return program format after transforming/processing
   */
  void flow(ClassIdentity input) throws AnalyzerException;

  void flow(Set<ClassIdentity> inputs);
}
