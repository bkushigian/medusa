package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import org.objectweb.asm.tree.analysis.AnalyzerException;

import java.util.Set;

/**
 * A TransformationPipe creates a new program format from the given program format.
 */
public abstract class TransformationPipe implements Pipe {

  @Override
  public void flow(Set<ClassIdentity> inputs) {
    for (ClassIdentity input : inputs) {
      flow(input);
    }
  }

  /**
   * Flow a class down the pipe
   *
   * @param input input program
   */
  public void flow(ClassIdentity input) {
    for (MethodIdentity method : input.methods) {
      try {
        flow(method);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * flow a Method down the pipe
   */
  protected void flow(MethodIdentity method) throws Exception {
    for (MutantIdentity mutant : method.mutants) {
      flow(mutant);
    }
  }

  /**
   * Flow a Mutant down the pipe
   */
  protected abstract void flow(MutantIdentity mutant) throws Exception;
}
