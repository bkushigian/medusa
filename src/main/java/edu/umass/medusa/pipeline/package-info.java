/**
 * This package defines and implements the Pipeline component of Medusa's architecture.
 */
package edu.umass.medusa.pipeline;