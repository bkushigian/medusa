package edu.umass.medusa.pipeline;

import edu.umass.medusa.format.ClassIdentity;

import java.util.Set;

/**
 * A ProcPipe processes or collects information from the given program format. It either updates the
 * given Context object or annotates the given program format.
 */
public abstract class ProcPipe implements Pipe {

  @Override
  public abstract void flow(ClassIdentity input);

  @Override
  public void flow(Set<ClassIdentity> inputs) {
    for (ClassIdentity input : inputs) {
      flow(input);
    }
  }
}
