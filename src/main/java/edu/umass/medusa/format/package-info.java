/**
 * Defines the different program formats that needed by Medusa. Formats include
 * <ol>
 *     <li>
 *         {@code Identity}: {@code ClassIdentity}, {@code MethodIdentity}, and {@code MutantIdentity}
 *     </li>
 *     <li>
 *         {@code Bytecode}: This is provided by the {@code ASM} package which represents the a method
 *         in bytecode
 *     </li>
 *     <li>
 *         {@code CFG}: Control flow graph gives us control flow data about our method
 *     </li>
 *     <li>
 *         {@code Constraint} and {@code ConstraintGen}: These are in the {@code constraint} sub-package and
 *         represent the Z3 constraints that are generated by methods.
 *     </li>
 *
 * </ol>
 */
package edu.umass.medusa.format;