# Medusa's Program Formats

This package defines the various formats that Medusa will need in the Pipeline.

## Program Parts
We consider different parts of a program separately. These include
* Classes
* Methods
* Fields
These each need to be modelled separately.

## Identities
Programs have different parts, such as classes, methods, fields, etc. Each
program part is associated with an `Identity`; this `Identity` represents the
abstract _entity_ that is being considered. An `Identity` may be represented
by different `Format`s, such as the `ASMFormat` or the `CFGFormat`.

## Formats
The `Pipeline` transforms programs from one format to another. This is captured
in the `Format` type, where different program parts. Formats include:

* `ASMFormat`: This is a program format using the `ASM` representation. This
  will typically be the initial format that the `Pipeline` recognizes

* `CFGFormat`: A `CFGFormat` is a representation of a program in its CFG form.
  This primarily concerns methods, as reflected in the `MethodCFGFormat`. The
  `ClassCFGFormat` exists simply as a wrapper of a collection of `MethodCFGFormats`
  to keep reasoning about the pipeline simple.

* `ConstraintGenFormat`: This format allows for generation of SMT constraints

* `ConstraintFormat`: A wrapper around the actual Z3 constraints. These are no
longer unique to a program as they are approximations of the program rather
than a representation of the program itself.

## Mutants
Each Mutant operates on a `Format` instance, called `original`, and specifies
some format-dependent data and rules to produce a mutant. This concept is
still being fleshed out.
