package edu.umass.medusa.format;

/**
 * This interface represents an identity of a program, such as a class, a method, a basic block,
 * etc. Identites are used as an umbrella construct under which each {@code Format} of the identity
 * exists.
 *
 * For instance, a method can be represented as bytecode, as a CFG, as a constraint generator, or as
 * a constraints. These are all different representations of the same <em>thing</em>, and that thing
 * is what we refer to as the Identity.
 *
 * Another way to think of this is that a program format is a <em>format of something.</em> What is
 * that something? It's the identity.
 */
public interface Identity {

}
