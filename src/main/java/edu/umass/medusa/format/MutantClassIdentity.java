package edu.umass.medusa.format;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.io.IOException;

/**
 * This is an intermediary class used to create {@code MutantIdentities}. At some point this class
 * will be deprecated
 */
public class MutantClassIdentity extends ClassIdentity {

  public final ClassIdentity original;
  public final MutantIdentity mutantId;

  public MutantClassIdentity(ClassNode cn, ClassIdentity original, String mutatedMethodName) {
    super(cn);
    this.original = original;
    final MethodIdentity origMethod = original.getMethod(mutatedMethodName);
    final MethodIdentity mutantMethod = getMethod(mutatedMethodName);
    final MethodNode mutant = mutantMethod.method;
    this.mutantId = new MutantIdentity(origMethod, mutant);
  }

  public MutantClassIdentity(String className, String path, ClassIdentity original,
      String mutatedMethodName) throws IOException {
    super(className, path);
    this.original = original;
    final MethodIdentity origMethod = original.getMethod(mutatedMethodName);
    final MethodIdentity mutantMethod = getMethod(mutatedMethodName);
    final MethodNode mutant = mutantMethod.method;
    this.mutantId = new MutantIdentity(origMethod, mutant);
  }

  public MutantClassIdentity(File classfile, MethodIdentity origMethod) throws IOException {
    super(classfile);
    this.original = origMethod.classId;
    final MethodIdentity mutantMethod = getMethod(origMethod.name);
    this.mutantId = new MutantIdentity(origMethod, mutantMethod.method);
  }
}
