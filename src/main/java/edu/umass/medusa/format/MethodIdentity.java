package edu.umass.medusa.format;

import de.codesourcery.asm.controlflow.ControlFlowGraph;
import edu.umass.medusa.format.constraint.MethodConstraintGen;
import edu.umass.medusa.util.Enumerator;
import edu.umass.medusa.util.TypeUtil;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Method to be reasoned about. While this is conceptually more abstract than any of
 * the methods formats (such as ASM MethodNode or a CFG), the current implementation uses a
 * MethodNode as a constructor argument.
 */
public class MethodIdentity implements Identity {

  /**
   * Identity of the parent class
   */
  public final ClassIdentity classId;

  /**
   * The name of the owner ClassIdentity. This includes package data:
   *
   * {@code "package/name/ClassName"}
   */
  public final String owner;

  /**
   * This gives the type description of a method and is copied directly from the {@code MethodNode}
   * field of the same name.
   */
  public final String desc;

  /**
   * The {@code shortName} of a method is the unqualified identifier used to invoke the method, or
   * {@code "<init>"} for constructors. Thus in a class {@code MyClass} with a method {@code public
   * int foo(int i, int j){...}}, the shortName of {@code foo} is the string {@code "foo"}.
   *
   * By way of contrast, the {@code name} of {@code foo} is {@code "foo(II)I"}, and {@code foo}'s
   * {@code qualifiedName} would be {@code full/package/name/MyClass.foo(II)I}.
   */
  public final String shortName;

  /**
   * This is the <em>unqualified</em> name with type data; this is computed via
   *
   * {@code name = shortName + desc}
   *
   * and uniquely identifies this method withing its class.
   */
  public final String name;

  /**
   * This uniquely identifies this method in the entirety of the compilation unit. It is computed
   * via
   *
   * {@code qualifiedName = owner + "." + name}
   *
   * and includes
   * <ol>
   * <li>package data</li>
   * <li>owner data (i.e., class name)</li>
   * <li>method name</li>
   * <li>type and return data</li>
   * </ol>
   *
   * When in doubt of which identifying name to use, use this.
   */
  public final String qualifiedName;

  /**
   * All mutants of this method node.
   *
   * TODO: This should be changed to a list for easier access of the nth mutant
   */
  public final Set<MutantIdentity> mutants;

  /**
   * Create unique identifiers for mutants
   */
  final Enumerator mutantEnumerator = new Enumerator();

  /**
   * asm MethodNode of this method
   */
  public MethodNode method;
  public ControlFlowGraph cfg;
  public MethodConstraintGen gen = null;

  /**
   * Type of each parameter
   */
  public TypeUtil.Type[] paramTypes;

  /**
   * Type of the return value
   */
  public TypeUtil.Type returnType;

  /**
   * Create a new MethodIdentity with owner class {@code id} out of an ASM method node {@code
   * method}
   *
   * @param cid {@code ClassIdentity} of the owning class
   * @param method {@code MethodNode} that we are creating an identity for.
   */
  public MethodIdentity(ClassIdentity cid, MethodNode method) {
    classId = cid;
    this.method = method;
    mutants = new HashSet<>();
    cfg = null;
    shortName = method.name;
    desc = method.desc;
    owner = classId.name;
    name = shortName + desc;
    qualifiedName = owner + "." + name;

    final TypeUtil.Type[] types = TypeUtil.parseMethodDesc(desc);
    paramTypes = Arrays.copyOf(types, types.length - 1);
    returnType = types[types.length - 1];
  }

  /**
   * Add a mutant of this MethodIdentity to the mutants set.
   *
   * @return TODO: Handle no-such-file TODO: Handle classfile for wrong class
   */
  public boolean addMutant(MutantIdentity mutant) {
    return mutants.add(mutant);
  }

  /**
   * Given a classfile of a mutant, load it as a MutantIdentity and add it to this methods mutants.
   */
  public boolean addMutant(File classfile) throws IOException {
    MutantClassIdentity mcid = new MutantClassIdentity(classfile, this);
    mcid.mutantId.path = classfile.getPath();
    return addMutant(mcid.mutantId);
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", super.toString(), name);
  }

}
