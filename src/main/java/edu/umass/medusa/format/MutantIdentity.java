package edu.umass.medusa.format;

import edu.umass.medusa.format.constraint.MutantConstraintGen;
import edu.umass.medusa.solver.result.SolverResult;
import edu.umass.medusa.solver.result.SolverResultFactory;
import org.objectweb.asm.tree.MethodNode;

/**
 * This is the {@code Identity} of a mutant method.
 */
public class MutantIdentity extends MethodIdentity {

  /**
   * Original method that this mutant is mutating.
   */
  public final MethodIdentity original;

  /**
   * Result of an analysis
   */
  SolverResult result;

  /**
   * For debugging
   */
  public String path;

  /**
   * The unique identifier of this mutant. This can be used for later identification
   */
  final int ident;

  /**
   * Override the {@code gen} parameter from MethodIdentity.
   */
  public MutantConstraintGen gen = null;

  /**
   * Timed by strategy
   */
  public long time = 0;

  public MutantIdentity(MethodIdentity orig, MethodNode mutant) {
    super(orig.classId, mutant);
    SolverResultFactory factory = new SolverResultFactory();
    result = factory.makeUnknown();
    original = orig;
    ident = orig.mutantEnumerator.next();
  }

  /**
   * Record the result of an analysis
   *
   * @param result the result of an analysis of this mutant
   */
  public void setResult(SolverResult result) {
    this.result = result;
  }

  /**
   * Retrieve a recorded analysis
   *
   * @return the last recorded analysis, or {@code null} if none exists.
   */
  public SolverResult getResult() {
    return result;
  }

  /**
   * Record the time it took to reason about this mutant.
   */
  public void setTime(long time) {
    this.time = time;
  }
}
