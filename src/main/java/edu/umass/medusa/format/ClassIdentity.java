package edu.umass.medusa.format;

import edu.umass.medusa.format.constraint.ClassConstraintGen;
import edu.umass.medusa.util.ASMUtil;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This {@code Identity} denotes a single class to be reasoned about.
 */
public class ClassIdentity implements Identity {

  /**
   * The fully qualified name of a class <tt>org.example.package.ClassName</tt>
   */
  public final String name;

  /**
   * A list of methods belonging to this class
   */
  public final List<MethodIdentity> methods;

  /**
   * A list of fields belonging to this class
   */
  public final List<FieldIdentity> fields;

  /**
   * methodsByName map method names to the associated {@code MethodIdentity}. We store two forms of
   * indexing strings:
   * <ol>
   * <li>Class Offset Method Descriptions (or Absolute Method Descriptions) of the form
   * fully/qualified/package/ClassName.methodName(argTypes)retType</li>
   * <li>Relative Method Descriptions of the form methodName(argTypes)retType</li>
   * </ol>
   */
  public final Map<String, MethodIdentity> methodsByName;

  /**
   * A descriptor of where this class was loaded from
   */
  public String path = null;

  /**
   * The ASM {@code ClassNode} format.
   */
  public ClassNode classNode;

  /**
   * The constraint generator format for this {@code ClassIdentity}
   */
  public ClassConstraintGen gen;

  public ClassIdentity(ClassNode cn) {
    name = cn.name;
    classNode = cn;
    methodsByName = new HashMap<>();
    methods = new ArrayList<>(cn.methods.size());
    fields = new ArrayList<>(cn.fields.size());
    gen = null;
    populateMethodsAndFields();
  }

  public ClassIdentity(String className, String path) throws IOException {
    this(ASMUtil.readClass(className, path));
    this.path = path + "/" + className;
  }

  public ClassIdentity(File classfile) throws IOException {
    this(ASMUtil.readClass(classfile));
    this.path = classfile.getPath();
  }

  /**
   * Go through the classNode's methods and fields and create the appropriate Identities, adding
   * them to the {@code methods} and {@code fields} lists.
   *
   * This should only be used by constructors.
   */
  protected void populateMethodsAndFields() {
    for (Object m : classNode.methods) {
      MethodNode mn = (MethodNode) m;
      MethodIdentity mid = new MethodIdentity(this, mn);
      methods.add(mid);
      methodsByName.put(mid.qualifiedName, mid);
      methodsByName.put(mid.name, mid);
    }

    for (Object f : classNode.fields) {
      FieldNode fn = (FieldNode) f;
      fields.add(new FieldIdentity(this, fn));
    }
  }

  /**
   * Look up a method by its name, either qualified by classname or without a classname
   * qualification. Note that a method descriptor is needed in the name to differentiate between
   * overloaded methods.
   *
   * @param name name and descriptor of method to look up: "foo(IIZ)Ljava.lang.String;" or
   * "MyClass.foo(IIZ)Ljava.lang.String;"
   * @return the method with {@code name} if it exists, or {@code null} otherwise
   */
  public MethodIdentity getMethod(String name) {
    return methodsByName.get(name);
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", super.toString(), name);
  }
}
