package edu.umass.medusa.format;

import org.objectweb.asm.tree.FieldNode;

public class FieldIdentity implements Identity {

  public final ClassIdentity classId;
  public final FieldNode fieldNode;

  public FieldIdentity(ClassIdentity classId, FieldNode fieldNode) {
    this.classId = classId;
    this.fieldNode = fieldNode;
  }
}
