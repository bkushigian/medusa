package edu.umass.medusa.format.constraint;

import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.util.Enumerator;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.tree.MethodNode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * BlockConstraintGen generates {@code BlockConstraints} for a given block and stores them for later
 * use. This class also stores if the blocks are free or used, caching them for later use when they
 * are freed. This allows for Medusa to load new copies of a block constraint at will to enable
 * constraint forking and loop unrolling to arbitrary precision.
 */
public class BlockConstraintGen {

  final MethodConstraintGen method;
  final IBlock block;
  final Z3Session z3;
  final Enumerator blockConstraintEnumerator = new Enumerator();
  final Integer blockNumber;
  final Stack<BlockConstraint> free = new Stack<>();
  final Set<BlockConstraint> used = new HashSet<>();

  public BlockConstraintGen(MethodConstraintGen method, IBlock block, Z3Session z3) {
    this.method = method;
    this.block = block;
    this.z3 = z3;
    blockNumber = method.blockEnumerator.next();
  }

  /**
   * Get a free BlockConstraint. If none exists, create one.
   *
   * @return a free block constraint
   */
  public BlockConstraint getFree() {
    if (free.isEmpty()) {
      free.push(new BlockConstraint(this));
    }

    final BlockConstraint bc = free.pop();

    if (!used.add(bc)) {
      throw new IllegalStateException(
          "Schrodinger's Block Constraint: both free and used simultaneously!");
    }

    return bc;
  }

  public void free(BlockConstraint bc) {
    if (!used.remove(bc)) {
      throw new IllegalStateException("Cannot free an unused block constraint!");
    }

    free.push(bc);
  }

  public boolean isFree(BlockConstraint bc) {
    return used.contains(bc);
  }

  public MethodConstraintGen getMethodConstraintGen() {
    return method;
  }

  public MethodIdentity getMethodIdentity() {
    return method.method;
  }

  public MethodNode getMethodNode() {
    return method.method.method;
  }
}
