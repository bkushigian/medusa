/**
 * This package defines Constraints Generators and Constraints, which may be
 * respectively thought of as approximators and approximations of a program.
 *
 * A generator produces constraints, and this production may be modified in
 * some way to suit a particular purpose. For instance, in middle-out generation,
 * dynamic loop unrolling may need extra instances of basic blocks to be
 * produced during Medusa's execution. Constraint generators handle some basic
 * bookkeeping to make this efficient and easy.
 */
package edu.umass.medusa.format.constraint;