package edu.umass.medusa.format.constraint;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.constraint.state.State;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.tree.MethodNode;

/**
 * Generate constraints for a basic block.
 */
public class BlockConstraint {

  public final int n;
  final BoolExpr condition;
  final BoolExpr executed;

  private EdgeConstraint[] edges;
  private final MethodNode method;
  final State state;

  /**
   * The Z3 session to generate constraints from
   */
  final Z3Session z3;

  /**
   * Block we are modelling
   */
  final IBlock block;

  /**
   * The generator of {@code this}
   */
  final BlockConstraintGen generator;

  /**
   * This should be created by a BlockConstraintGen
   */
  BlockConstraint(BlockConstraintGen gen) {
    generator = gen;
    block = gen.block;
    z3 = gen.z3;
    method = gen.getMethodNode();
    n = gen.blockConstraintEnumerator.next();
    state = new State(z3, method.maxLocals);
    generator.method.bytecodeConstrainer.constrainBlock(state, method, block);
    condition = state.condition;
    executed = state.executed;
    edges = new EdgeConstraint[2];
    edges[0] = edges[1] = null;
  }

  /**
   * Free this from being used.
   */
  public void free() {
    generator.free(this);
    edges[0] = edges[1] = null;
  }

  /**
   * Is this block free? Let's find out!
   *
   * @return true if this block is free, false otherwise
   */
  public boolean isFree() {
    return generator.isFree(this);
  }

  /**
   * Get a free copy of BlockConstraint.
   *
   * @return copy of this
   */
  public BlockConstraint getFree() {
    return generator.getFree();
  }

  public EdgeConstraint addEdge(BlockConstraint target, boolean condition) {
    EdgeConstraint edge = new EdgeConstraint(this, target, condition);
    edges[condition ? 1 : 0] = edge;
    return edge;
  }

  public BoolExpr getBlockConstraints() {
    return state.getConstraints();
  }

  public EdgeConstraint trueEdge() {
    return edges[1];
  }

  public EdgeConstraint falseEdge() {
    return edges[0];
  }

  public boolean isTop() {
    return block.getRegularPredecessorCount() == 0;
  }

  public boolean isBot() {
    return block.getRegularSuccessorCount() == 0;
  }

  public boolean isTopOrBot() {
    return isTop() || isBot();
  }

  public BoolExpr getExecuted() {
    return executed;
  }

  public BitVecExpr peek() {
    return state.peek();
  }

  public IBlock getBlock() {
    return block;
  }
}
