package edu.umass.medusa.format.constraint;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.FPExpr;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.constraint.state.State;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.util.*;

/**
 * Defines the {@code emit} function which takes an {@code AbstractInsnNode} and a {@code State} and
 * updates the state appropriately.
 */
public class BytecodeConstrainer {

  private final Z3Session z3;

  private final HashMap<Integer, String> opNames = new HashMap<>();
  public BytecodeConstrainer(Z3Session z3) {
    this.z3 = z3;
    opNames.put(Opcodes.FADD, "fadd");
    opNames.put(Opcodes.FSUB, "fsub");
    opNames.put(Opcodes.FMUL, "fmul");
    opNames.put(Opcodes.FDIV, "fdiv");
    opNames.put(Opcodes.FNEG, "fneg");
    opNames.put(Opcodes.FCMPG, "fcmpg");
    opNames.put(Opcodes.FCMPL, "fcmpl");
  }


  public void constrainBlock(State state, MethodNode mn, IBlock block) {
    // This is fucking stupid. WTF doesn't a basic block have an ORDERED
    // structure with instruction values? WTF do I have to compute this??
    // This is dumb af. Seriously, wtf???

    final Set<Integer> instructionNums = block.getInstructionNums();
    final List<Integer> sorted = new ArrayList<>(instructionNums);
    Collections.sort(sorted);
    final AbstractInsnNode[] insns = new AbstractInsnNode[block.getByteCodeInstructionCount(mn)];
    final Iterator<Integer> iterator = sorted.iterator();
    for (int i = 0; iterator.hasNext(); ) {
      final AbstractInsnNode node = mn.instructions.get(iterator.next());
      if (node.getOpcode() > 0) {
        insns[i++] = node;
      }
    }
    // This entire method so far has been to get the list of instructions to
    // pass in to constrain. This is seriously dumb. The Block API needs to
    // be updated. There is no good reason why we don't store instruction ordering
    // data. This data is _important_. Nobody cares that instructions 2, 1, 3 are
    // in a block... they care that instructions 1, 2, 3 are in a block **IN THAT
    // GODDAMN ORDER**. Bytecode execution isn't fucking commutative, you git!

    // </rant>
    for (AbstractInsnNode insn : insns) {
      emit(state, insn);
    }

  }

  public BoolExpr emit(State state, AbstractInsnNode insn) {

    // The following are declared for convenience for the switch statements.
    VarInsnNode varNode;
    LdcInsnNode ldcNode;

    // Store bit vec exprs when constructing a long. For a long bin op this will involve
    // storing two long operands, an rhs and an lhs, as well as storing two words to use
    // while popping off the top of the opstack, the most significant word and the least
    // significant word.
    BitVecExpr msw, bv;
    FPExpr fp1, fp2, fp3;

    int opcode = insn.getOpcode();
    if (opcode < 1) {
      return null;
    }

    switch (insn.getOpcode()) {

      ////////////////////////////////////////////////////////////////////
      //               INTEGER ARITHMETIC INSTRUCTIONS                  //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.IADD:
        return state.push(z3.add(state.pop(), state.pop()));
      case Opcodes.ISUB:
        return state.push(z3.subtract(state.pop(), state.pop()));
      case Opcodes.IMUL:
        return state.push(z3.multiply(state.pop(), state.pop()));
      case Opcodes.IDIV:
        return state.push(z3.divide(state.pop(), state.pop()));
      case Opcodes.IREM:
        return state.push(z3.rem(state.pop(), state.pop()));
      case Opcodes.ISHL:
        return state.push(z3.shiftLeft(state.pop(), state.pop()));
      case Opcodes.ISHR:
        return state.push(z3.shiftRight(state.pop(), state.pop()));
      case Opcodes.IUSHR:
        return state.push(z3.unsignedShiftRight(state.pop(), state.pop()));
      case Opcodes.IAND:
        return state.push(z3.iand(state.pop(), state.pop()));
      case Opcodes.IOR:
        return state.push(z3.ior(state.pop(), state.pop()));
      case Opcodes.IXOR:
        return state.push(z3.ixor(state.pop(), state.pop()));
      case Opcodes.INEG:
        return state.push(z3.negative(state.pop()));
      case Opcodes.IINC:
        return state.push(z3.inc(state.pop()));

      ////////////////////////////////////////////////////////////////////
      //                INTEGER CONSTANT INSTRUCTIONS                   //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.ACONST_NULL:
      case Opcodes.ICONST_0:
        return state.push(z3.constants.ICONST_0);
      case Opcodes.ICONST_1:
        return state.push(z3.constants.ICONST_1);
      case Opcodes.ICONST_2:
        return state.push(z3.constants.ICONST_2);
      case Opcodes.ICONST_3:
        return state.push(z3.constants.ICONST_3);
      case Opcodes.ICONST_4:
        return state.push(z3.constants.ICONST_4);
      case Opcodes.ICONST_5:
        return state.push(z3.constants.ICONST_5);
      case Opcodes.ICONST_M1:
        return state.push(z3.constants.ICONST_M1);

      ///////////////////////////////////////////////////////////////////
      //                  INTEGER PUSH INSTRUCTIONS                    //
      ///////////////////////////////////////////////////////////////////
      case Opcodes.BIPUSH:
        // TODO: Check if is byte? It should be a signed byte...
        return state.push(z3.mkJavaInt(((IntInsnNode) insn).operand));

      case Opcodes.SIPUSH:
        // TODO: Check if is short? It should be a signed short...
        return state.push(z3.mkJavaInt(((IntInsnNode) insn).operand));

      ////////////////////////////////////////////////////////////////////
      //                  INT CONVERSION INSTRUCTIONS                   //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.I2B:
        return state.push(z3.ctx.mkBVAND(z3.constants.ICONST_255, state.pop()));

      case Opcodes.I2C:
        return state.push(z3.ctx.mkBVAND(z3.constants.ICONST_65536, state.pop()));

      case Opcodes.I2D:
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));

      case Opcodes.I2F:
        bv = state.pop();
        return state.pushFloat(z3.ctx.mkFPToFP(bv, z3.javaFloatSort));

      case Opcodes.I2L:
        msw = state.pop();
        return state.push(z3.ctx.mkSignExt(32, msw));

      case Opcodes.I2S:
        return state.push(z3.ctx.mkBVAND(z3.constants.ICONST_65535, state.pop()));

      ////////////////////////////////////////////////////////////////////
      //                 LONG ARITHMETIC INSTRUCTIONS                   //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.LADD:
        return state.pushLong(z3.add(state.popLong(), state.popLong()));

      case Opcodes.LSUB:
        return state.pushLong(z3.subtract(state.popLong(), state.popLong()));

      case Opcodes.LMUL:
        return state.pushLong(z3.multiply(state.popLong(), state.popLong()));

      case Opcodes.LDIV:
        return state.pushLong(z3.divide(state.popLong(), state.popLong()));

      case Opcodes.LREM:
        return state.pushLong(z3.rem(state.popLong(), state.popLong()));

      case Opcodes.LSHL:
        /*  Code: 0x79 (121)
         *  Stack: value1, value2 → result
         *  Description: bitwise shift left of a long value1 by int value2 positions
         */
        return state.pushLong(z3.shiftRight(state.popLong(), state.popLong()));

      case Opcodes.LSHR:
        /* Code: 0x7b (123)
         * Stack: value1, value2 → result
         * Description: bitwise shift right of a long value1 by int value2 positions
         */
        return state.pushLong(z3.shiftRight(state.popLong(), state.popLong()));

      case Opcodes.LUSHR:
        /* Code: 0x7d (125)
         * Stack: value1, value2 → result
         * Description: bitwise shift right of a long value1 by int value2 positions, unsigned
         */
        return state.pushLong(z3.unsignedShiftRight(state.popLong(), state.popLong()));

      case Opcodes.LAND:
        return state.pushLong(z3.iand(state.popLong(), state.popLong()));

      case Opcodes.LOR:
        return state.pushLong(z3.ior(state.popLong(), state.popLong()));

      case Opcodes.LXOR:
        return state.pushLong(z3.ixor(state.popLong(), state.popLong()));

      case Opcodes.LNEG:
        return state.pushLong(z3.negative(state.popLong()));

      ////////////////////////////////////////////////////////////////////
      //                 LONG CONVERSION INSTRUCTIONS                   //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.L2D:
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));
      case Opcodes.L2F:
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));
      case Opcodes.L2I:
        // TODO: Change this to reflect new architecture
        msw = state.pop();
        state.pop();   // Chop off the extra 32 bits
        return state.push(msw);

      ////////////////////////////////////////////////////////////////////
      //                  LONG CONSTANT INSTRUCTIONS                    //
      ////////////////////////////////////////////////////////////////////
      case Opcodes.LCONST_0:
        return z3.mkAnd(state.push(z3.constants.ICONST_0), state.push(z3.constants.ICONST_0));
      case Opcodes.LCONST_1:
        return z3.mkAnd(state.push(z3.constants.ICONST_1), state.push(z3.constants.ICONST_0));
      ///////////////////////////////////////////////////////////////////
      //                       LDC INSTRUCTIONS                        //
      ///////////////////////////////////////////////////////////////////
      case Opcodes.LDC:
        ldcNode = (LdcInsnNode) insn;
        Object cst = ldcNode.cst;
        if (cst instanceof Integer) {
          Integer val = (Integer) ldcNode.cst;
          return state.push(z3.mkJavaInt(val));
        } else if (cst instanceof Long) {
          Long val = (Long) ldcNode.cst;
          return state.pushLong(z3.mkJavaLong(val));
        } else if (cst instanceof Float) {
          Float val = (Float) ldcNode.cst;
          return state.pushFloat(z3.mkJavaFloat(val));
        } else {
          throw new RuntimeException(
              String.format("Opcode 0x%x is not implemented for cst type %s", opcode,
                  cst.getClass()));
        }

        ////////////////////////////////////////////////////////////////////
        //                 LONG COMPARISON INSTRUCTIONS                   //
        ////////////////////////////////////////////////////////////////////
      case Opcodes.LCMP:
        /* Code: 0x94 (148)
         * Stack: value1, value2 → result
         * Description:  push  0  if  the  two  longs are the same, 1 if value1 is greater than
         * value2, -1 otherwise
         */
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));

        ////////////////////////////////////////////////////////////////////
        //                FLOAT COMPARISON INSTRUCTIONS                   //
        ////////////////////////////////////////////////////////////////////
      case Opcodes.FCMPG:
        return emitFPBinOp(Opcodes.FCMPG, state);
      case Opcodes.FCMPL:
        return emitFPBinOp(Opcodes.FCMPL, state);

        ////////////////////////////////////////////////////////////////////
        //                 FLOAT CONSTANT INSTRUCTIONS                    //
        ////////////////////////////////////////////////////////////////////
      case Opcodes.FCONST_0:
        return state.pushFloat(z3.ctx.mkFP(0.0, z3.javaFloatSort));
      case Opcodes.FCONST_1:
        return state.pushFloat(z3.ctx.mkFP(1.0, z3.javaFloatSort));
      case Opcodes.FCONST_2:
        return state.pushFloat(z3.ctx.mkFP(2.0, z3.javaFloatSort));

        ////////////////////////////////////////////////////////////////////
        //                FLOAT ARITHMETIC INSTRUCTIONS                   //
        ////////////////////////////////////////////////////////////////////
      case Opcodes.FADD:
      case Opcodes.FSUB:
      case Opcodes.FMUL:
      case Opcodes.FDIV:
      case Opcodes.FREM:
        return emitFPBinOp(opcode, state);

      case Opcodes.FNEG:
        fp1 = z3.mkJavaFloat(z3.nameGen.get("fneg"));
        return z3.mkAnd(
            state.popFloat(fp1),
            state.pushFloat(z3.ctx.mkFPNeg(fp1))
        );

        ////////////////////////////////////////////////////////////////////
        //                 FLOAT CONVERSION INSTRUCTIONS                  //
        ////////////////////////////////////////////////////////////////////
      case Opcodes.F2D:
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));
      case Opcodes.F2I:
        fp1 = z3.mkJavaFloat("f2i");
        state.popFloat(fp1);
        return state.push(z3.ctx.mkFPToBV(z3.roundToZero, fp1, 32, true)); // TODO? What does this do?
        //throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));
      case Opcodes.F2L:
        // TODO
        throw new RuntimeException(String.format("Opcode 0x%x is not implemented", opcode));

        ///////////////////////////////////////////////////////////////////
        //                    VAR TABLE INSTRUCTIONS                     //
        ///////////////////////////////////////////////////////////////////
      case Opcodes.FLOAD:
      case Opcodes.ILOAD:
        varNode = (VarInsnNode) insn;
        return state.push(state.load(varNode.var));

      case Opcodes.FSTORE:
      case Opcodes.ISTORE:
        varNode = (VarInsnNode) insn;
        state.store(varNode.var, state.pop());
        return z3.ctx.mkTrue();     // XXX: This isn't the best solution but it works

      case Opcodes.LLOAD:
        /*
         * A long is comprised of two words, the most significant word and the least significant
         * word (MSW and LSW respectively). loading a long in our case corresponds to loading two
         * words to the stack in the order of LSW and MSW.
         *
         * Longs are stored in little-endian format in the vartable so this corresponds to loading
         * the varNode.var index first, and then the varNode.var + 1 index next, so that the stack
         * will go through the following transformations:
         * STACK_0: [...)
         * STACK_1: [... LSW)
         * STACK_2: [... LSW MSW)
         *
         *    INCREASING ADDRS -->
         * VAR_TABLE: [... LSW MSW ...]
         *
         */
        // TODO: Check this
        varNode = (VarInsnNode) insn;
        return z3
            .mkAnd(state.push(state.load(varNode.var)), state.push(state.load(varNode.var + 1)));

      case Opcodes.LSTORE:
        varNode = (VarInsnNode) insn;
        state.store(varNode.var + 1, state.pop());    // MSB goes to index i + 1
        state.store(varNode.var, state.pop());              // LSB goes to index i
        return z3.mkTrue();


      case Opcodes.DSTORE:
        // TODO
        throw new RuntimeException("DSTORE is not implemented");

      case Opcodes.DLOAD:
        // TODO
        throw new RuntimeException("DLOAD is not implemented");

      case Opcodes.ASTORE:
        varNode = (VarInsnNode) insn;
        state.store(varNode.var, state.pop());
        return z3.ctx.mkTrue();     // XXX: This isn't the best solution but it works

      case Opcodes.ALOAD:
        varNode = (VarInsnNode) insn;
        return state.push(state.load(varNode.var));

      ///////////////////////////////////////////////////////////////////
      //                      IFXXX INSTRUCTIONS                       //
      ///////////////////////////////////////////////////////////////////

      case Opcodes.IF_ACMPEQ:
        // TODO
        throw new RuntimeException("IF_ACMPEQ is not implemented");
      case Opcodes.IF_ACMPNE:
        // TODO
        throw new RuntimeException("IF_ACMPNE is not implemented");
      case Opcodes.IF_ICMPEQ:
        return state.setCondition(z3.mkEq(state.pop(), state.pop()));
      case Opcodes.IF_ICMPLE:
        return state.setCondition(z3.le(state.pop(), state.pop()));
      case Opcodes.IF_ICMPGE:
        return state.setCondition(z3.ge(state.pop(), state.pop()));
      case Opcodes.IF_ICMPGT:
        return state.setCondition(z3.gt(state.pop(), state.pop()));
      case Opcodes.IF_ICMPLT:
        return state.setCondition(z3.lt(state.pop(), state.pop()));
      case Opcodes.IF_ICMPNE:
        return state.setCondition(z3.mkNot(z3.mkEq(state.pop(), state.pop())));
      case Opcodes.IFNULL:
      case Opcodes.IFEQ:
        return state.setCondition(z3.mkEq(state.pop(), z3.constants.ICONST_0));
      case Opcodes.IFLE:
        return state.setCondition(z3.le(z3.constants.ICONST_0, state.pop()));
      case Opcodes.IFLT:
        return state.setCondition(z3.lt(z3.constants.ICONST_0, state.pop()));
      case Opcodes.IFGE:
        return state.setCondition(z3.ge(z3.constants.ICONST_0, state.pop()));
      case Opcodes.IFGT:
        return state.setCondition(z3.gt(z3.constants.ICONST_0, state.pop()));
      case Opcodes.IFNONNULL:
      case Opcodes.IFNE:
        return state.setCondition(z3.mkNot(z3.mkEq(state.pop(), z3.constants.ICONST_0)));
      case Opcodes.GOTO:
        return state.setCondition(z3.ctx.mkTrue());

      ///////////////////////////////////////////////////////////////////
      //                      RETURN INSTRUCTIONS                      //
      ///////////////////////////////////////////////////////////////////
      case Opcodes.FRETURN:
      case Opcodes.IRETURN:
      case Opcodes.ARETURN:
      case Opcodes.LRETURN:
      case Opcodes.RETURN:
        return z3.ctx.mkTrue();

      default:
        break;
    }
    throw new IllegalStateException(
        String.format("Unrecognized bytecode instruction 0x%x", insn.getOpcode()));
  }

  BitVecExpr popDoublePrec(State state) {
    return z3.ctx.mkConcat(state.pop(), state.pop());
  }

  /**
   * Given a floating point binary operation opcode and a state, generate the
   * constraints to execute the operation, making all necessary conversions to and
   * from BitVec form to interface with the stack
   * @param opcode the opcode of operation, must be in FADD, FDIV, FMUL, FREM, FSUB
   * @param state the state of the block we are working on
   * @return a boolean expression constraining the execution of this operation.
   */
  private BoolExpr emitFPBinOp(int opcode, State state) {
    String name = opNames.get(opcode);
    String opName1 = z3.nameGen.get(name + "-lhs");
    String opName2 = z3.nameGen.get(name + "-rhs");
    FPExpr fp1 = z3.mkJavaFloat(opName1),
           fp2 = z3.mkJavaFloat(opName2),
           fp3;
    BoolExpr be, pop1, pop2;
    BitVecExpr bve;

    switch (opcode) {
      case Opcodes.FADD:
        fp3 = z3.ctx.mkFPAdd(z3.roundingMode, fp1, fp2);
        break;
      case Opcodes.FDIV:
        fp3 = z3.ctx.mkFPDiv(z3.roundingMode, fp1, fp2);
        break;
      case Opcodes.FMUL:
        fp3 = z3.ctx.mkFPMul(z3.roundingMode, fp1, fp2);
        break;
      case Opcodes.FREM:
        fp3 = z3.ctx.mkFPRem(fp1, fp2);
        break;
      case Opcodes.FSUB:
        fp3 = z3.ctx.mkFPSub(z3.roundingMode, fp1, fp2);
        break;
      case Opcodes.FCMPG:
      case Opcodes.FCMPL:
        /*
         * From https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-6.html#jvms-6.5.fcmp_op:
         *
         * STACK: ..., value1, value2 --> ..., result
         *
         * Both value1 and value2 must be of type float. The values are popped from the operand
         * stack and undergo value set conversion (§2.8.3), resulting in value1' and value2'.
         * A floating-point comparison is performed:
         *
         *   - If value1 is greater than value2, the int value 1 is pushed onto the operand stack.
         *   - Otherwise, if value1 is equal to value2, the int value 0 is pushed onto the operand
         *     stack.
         *   - Otherwise, if value1 is less than value2, the int value -1 is pushed onto the
         *     operand stack.
         *   - Otherwise, at least one of value1 or value2 is NaN. The fcmpg instruction pushes
         *     the int value 1 onto the operand stack and the fcmpl instruction pushes the int
         *     value -1 onto the operand stack.
         *
         * Floating-point comparison is performed in accordance with IEEE 754. All values other than
         * NaN are ordered, with negative infinity less than all finite values and positive infinity
         * greater than all finite values. Positive zero and negative zero are considered equal.
         */
        BoolExpr pops = z3.mkAnd(state.popFloat(fp2), state.popFloat(fp1));
        //BoolExpr namedBe = z3.mkBoolExpr(opName1 + "-LT-" + opName2);
        //be = state.aliasExpr(z3.ctx.mkFPLt(fp1, fp2), namedBe);
        bve = (BitVecExpr)z3.mkITE(           // fp1 < fp2? push -1 : ...
            z3.ctx.mkFPGt(fp1, fp2),
            z3.mkJavaInt(1),
            z3.mkITE(z3.ctx.mkFPEq(fp1, fp2),
                     z3.mkJavaInt(0),
                     z3.mkITE(z3.ctx.mkFPLt(fp1, fp2),
                         z3.mkJavaInt(-1),
                         z3.mkJavaInt(opcode == Opcodes.FCMPG ? 1 : -1))));

        return z3.mkAnd(pops, state.push(bve));
      default:
        throw new RuntimeException("Unrecognized FP bin-op opcode: " + opcode);
    }

    return z3.mkAnd(state.popFloat(fp2), state.popFloat(fp1), state.pushFloat(fp3));
  }
}
