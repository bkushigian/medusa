package edu.umass.medusa.format.constraint;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.util.Z3Session;

public class ClassConstraintGen {

  final ClassIdentity classId;
  final Z3Session z3;

  public ClassConstraintGen(ClassIdentity classId, Z3Session z3) {
    this.classId = classId;
    this.z3 = z3;

    assert classId.gen == null;
    classId.gen = this;

    for (MethodIdentity m : classId.methods) {
      if ("<init>".equals(m.method.name)) {
        continue;
      }
      m.gen = new MethodConstraintGen(m, z3);
    }
  }
}
