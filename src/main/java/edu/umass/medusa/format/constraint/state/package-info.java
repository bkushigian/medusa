/**
 * This package defines the State class which is used to generate BlockConstraints
 */
package edu.umass.medusa.format.constraint.state;