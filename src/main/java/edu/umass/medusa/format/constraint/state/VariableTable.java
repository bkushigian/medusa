package edu.umass.medusa.format.constraint.state;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import edu.umass.medusa.util.Z3Session;

import java.util.LinkedList;
import java.util.List;

/**
 * The variable table stores local vars in a stack frame. The variable table has a fixed number of
 * cells allotted statically: each of these is represented by a {@code Cell} instance (which is
 * defined in this class). The {@code Cell} class tracks both
 * <ol>
 * <li>
 * <b>the initial value</b> held in the cell (which is unconstrained, allowing for a
 * previous basic block's vartable to be <em>propagated</em> to this table
 * </li>
 *
 * <li>
 * <b>the current value</b> held in the cell. This has two interpretations,
 * depending on who is doing the interpretation.
 *
 * <ul>
 * <li>If the VarTable is currently being
 * populated (i.e., its associated basic block is currently being constrained), then {@code current}
 * represents the most recent value in this cell that's been constrained thus far.
 * </li>
 * <li>Otherwise, if the VarTable and its associated basic block have been constrained
 * already, {@code current} represents the <em>final</em> value stored in the cell; that is, the
 * cell that is available at the end of this blocks execution.
 * </li>
 * </ul>
 * </li>
 * </ol>
 *
 * Each of the N cells in a variable table has a {@code Cell} instance allotted to it. The basic
 * interface is
 * <ul>
 * <li> {@code void store(int index, BitVecExpr bv)}: store bit vector value {@code bv}
 * in the cell at location {@code index}</li>
 * <li> {@code BitVecExpr load(int index)}: fetch the bit vector stored at {@code index}
 * and return it.</li>
 * <li> {@code getConstraints()} </li>
 * </ul>
 */
public class VariableTable {

  final int size;
  final Z3Session z3;
  final String name;
  final Cell[] entries;
  boolean hasConstraints;

  /**
   * @param z3 The z3 session to be used
   * @param size number of variable table entries
   */
  VariableTable(Z3Session z3, int size) {
    this.z3 = z3;
    this.size = size;
    this.name = z3.nameGen.get("vt");
    this.entries = new Cell[size];
    this.hasConstraints = false;
    for (int i = 0; i < size; ++i) {
      entries[i] = new Cell(i);
    }
  }


  /**
   * Store a new variable table entry at index {@code index}.
   *
   * @param index entry index to store to
   * @param bv bit vector expression to store
   * @throws IndexOutOfBoundsException when {@code index < 0 || size <= index}
   */
  void store(int index, BitVecExpr bv) {
    if (bv == null) {
      throw new NullPointerException(
          String.format("Cannot store a null BitVecExpr to %s", this.toString()));
    }

    if (0 <= index && index < size) {
      entries[index].store(bv);
      return;
    }

    throw new IndexOutOfBoundsException(
        String.format("%s cannot store to index %d: outside of range [0, %d)", this, index, size));
  }

  /**
   * Load the initial bit vector stored at location {@code index}
   *
   * @param index location to load from
   * @return value stored at location {@code index}
   */
  public BitVecExpr loadInitial(int index) {
    if (0 <= index && index < size) {
      return entries[index].initial;
    }
    throw new IndexOutOfBoundsException(
        "Found index " + index + " which is outside the bounds [0, " + size + ")");
  }

  /**
   * Load the value at location {@code index}
   *
   * @param index the location to load from
   * @return the bit vec stored at location {@code index}
   */
  BitVecExpr load(int index) {
    if (0 <= index && index < size) {
      return entries[index].current;
    }
    throw new IndexOutOfBoundsException(
        "Found index " + index + " which is outside the bounds [0, " + size + ")");
  }

  /**
   * Pipe the resultant state of this to the inital state of {@code other}, returing the relevant
   * constraints.
   *
   * @param other the successor block's variable table to connect to
   * @return the constraints enforcing that {@code other}'s initial variable table state is the
   * equal to {@code this} tables final state.
   */
  BoolExpr propagateState(VariableTable other) {
    if (size != other.size) {
      throw new IllegalStateException("Cannot propagate state to unequal sized VariableTable");
    }

    BoolExpr[] constraints = new BoolExpr[size];
    for (int i = 0; i < size; ++i) {
      constraints[i] = entries[i].propagateState(other.entries[i]);
    }

    return z3.mkAnd(constraints);
  }

  /**
   * Return the constraints describing the state transition of this variable table.
   *
   * @return the generated constraints transforming initial state into final state
   */
  BoolExpr getConstraints() {
    int constraintCount = 0;
    for (Cell c : entries) {
      constraintCount += c.constraints.size();
    }

    if (constraintCount == 0) {
      return z3.mkTrue();
    }

    BoolExpr[] constraints = new BoolExpr[constraintCount];
    int idx = 0;
    for (int i = 0; i < size; ++i) {
      final List<BoolExpr> cellConstraints = entries[i].constraints;
      for (int j = 0; j < cellConstraints.size(); ++j) {
        constraints[idx++] = cellConstraints.get(j);
      }
    }
    return z3.mkAnd(constraints);
  }

  /**
   * A Cell represents an entry in a JVM stackframe variable table over time and provides basic
   * functionality.
   */
  private class Cell {

    /**
     * {@code initial} represents the value present at the beginning of the basic block. This is set
     * by a blocks predecessor, or by the caller of this method when the block is the entry block.
     */
    final BitVecExpr initial;

    /**
     * {@code current} represents the value present in this cell presently. While a block is being
     * analyzed and constrained, this is the bit vec value that will be updated. Once a block is
     * finished, this will be the final value held in this cell.
     */
    BitVecExpr current;

    /**
     * The base of the unique generating names for this version of this cell
     */
    final String name;

    /**
     * The constraints generated over time to describe the states of this cell. This allows for a
     * more readable output and can be removed at a later time by not generating named cells but
     * rather storing the verbatim bit vec exprs and returning them on loads. This should result in
     * somewhat improved performance though at a cost to readability and debuggability
     */
    final List<BoolExpr> constraints;

    /**
     * The index is used to create a unique name.
     *
     * @param index the index in the variable table that this entry represents
     */
    Cell(int index) {
      name = String.format("%s_%s", VariableTable.this.name, String.valueOf(index));
      initial = (BitVecExpr) z3.ctx.mkConst(z3.nameGen.get(name), z3.javaIntSort);
      current = initial;
      constraints = new LinkedList<>();
    }

    /**
     * Add a new bit vector entry to the table and ensure
     */
    BoolExpr store(BitVecExpr value) {
      BoolExpr constraint = z3.mkEq(add(), value);
      constraints.add(constraint);
      hasConstraints = true;
      return constraint;
    }

    /**
     * Create a new bit vector with a unique name, addConstraints it to the var table entry, and
     * return it.
     */
    private BitVecExpr add() {
      current = (BitVecExpr) z3.ctx.mkConst(z3.nameGen.get(name), z3.javaIntSort);
      return current;
    }

    /**
     * Propagate this cell's final state to the next cell's initial state
     *
     * @param other the other cell whose initial value is our final value
     * @return the constraint that this.current = other.final
     */
    BoolExpr propagateState(Cell other) {
      return z3.mkEq(current, other.initial);
    }

    BoolExpr[] getConstraintsArray() {
      return constraints.toArray(new BoolExpr[constraints.size()]);
    }

    BoolExpr getConstraints() {
      return z3.mkAnd(getConstraintsArray());
    }
  }
}
