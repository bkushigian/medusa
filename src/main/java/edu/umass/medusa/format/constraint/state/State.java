package edu.umass.medusa.format.constraint.state;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FPExpr;
import edu.umass.medusa.util.Z3Session;


/**
 * Track JVM State. Currently only includes local state (op-stack, var table).
 *
 * Each basic block constraint has its own associated State to allow for blocks to be chained
 * together in different ways.
 *
 * Each {@code State} object stores the generated block constraints but does not store edge
 * constraints to facilitate decoupling of straight line and branching execution.
 */
public class State {

  final Z3Session z3;
  final VariableTable varTable;
  final OperandStack opStack;
  public final BoolExpr condition;
  public final BoolExpr executed;

  /**
   * While condition gives a readable handle, conditionConstraint gives the actual constraint to be
   * used.
   */
  BoolExpr conditionConstraint;

  public BoolExpr trueBranchConstraint = null;
  public BoolExpr falseBranchConstraint = null;
  public BoolExpr edgeConstraint = null;

  public State(Z3Session z3, int varTableSize) {
    // TODO: parametrize by the method being constrained for more descriptive names and debugging output
    this.z3 = z3;
    varTable = new VariableTable(z3, varTableSize);
    opStack = new OperandStack(z3);
    condition = z3.ctx.mkBoolConst(z3.nameGen.get("block-condition"));
    executed = z3.ctx.mkBoolConst(z3.nameGen.get("block-was-executed?"));
    conditionConstraint = null;
  }

  public BoolExpr propagateState(State other) {
    return z3.mkAnd(varTable.propagateState(other.varTable),
        opStack.propagateState(other.opStack));
  }

  /**
   * Pop the opstack
   */
  public BitVecExpr pop() {
    return opStack.pop();
  }

  public BitVecExpr popLong() {
    BitVecExpr msw = pop();
    BitVecExpr lsw = pop();
    return z3.ctx.mkConcat(msw, lsw);
  }

  /**
   * Push onto the opstack
   */
  public BoolExpr push(BitVecExpr bv) {
    assert bv.getSortSize() == 32;
    return opStack.push(bv);
  }

  public BoolExpr pushLong(BitVecExpr bv) {
    assert bv.getSortSize() == 64;
    BitVecExpr msw = z3.ctx.mkExtract(63, 32, bv);
    BitVecExpr lsw = z3.ctx.mkExtract(31, 0, bv);
    return z3.mkAnd(push(lsw), push(msw));
  }

  public BoolExpr pushFloat(FPExpr fp) {
    return opStack.pushFloat(fp);
  }

  /**
   * Pop the top value off the opstack, constrain it to be equal to the passed in FPExpr
   * @param fp the FPExpr that we are to constrain is equal to the popped value
   * @return constraint enforcing that we pop off the stack, and that for the popped BitVecExpr
   *         {@code bv} we have {@code (= fp ((_ to_fp 8 24) bv))}
   */
  public BoolExpr popFloat(FPExpr fp) {
    return opStack.popFloat(fp);
  }

  /**
   * Peek at the top of the current opstack
   *
   * @return the opstack's top value
   */
  public BitVecExpr peek() {
    return opStack.peek();
  }

  public BitVecExpr peek2() {
    return opStack.peek2();
  }

  public BitVecExpr peekLong() {
    return opStack.peekLong();
  }

  public void store(int index, BitVecExpr bv) {
    varTable.store(index, bv);
  }

  public BitVecExpr load(int index) {
    return varTable.load(index);
  }

  /**
   * @return the constraints modeling this state as a BoolExpr
   */
  public BoolExpr getConstraints() {
    // Note that the complicated if-then structure here is to simplify the output for debugging, minimizing the
    // nested (and and and and and) structure that was occurring before
    if (opStack.constraints.size() > 0) {
      final BoolExpr osc = opStack.getConstraints();
      if (varTable.hasConstraints) {
        final BoolExpr vtc = varTable.getConstraints();
        if (conditionConstraint == null) {
          return z3.mkAnd(osc, vtc);
        }
        return z3.mkAnd(osc, vtc, conditionConstraint);
      }
      if (conditionConstraint == null) {
        return osc;
      }
      return z3.mkAnd(osc, conditionConstraint);
    }
    if (varTable.hasConstraints) {
      final BoolExpr vtc = varTable.getConstraints();
      if (conditionConstraint == null) {
        return vtc;
      }
      return z3.mkAnd(vtc, conditionConstraint);
    }
    if (conditionConstraint == null) {
      return z3.mkTrue();
    }
    return conditionConstraint;
  }

  public BoolExpr getOpstackConstraints() {
    return opStack.getConstraints();
  }

  public BoolExpr getVartableConstraints() {
    return varTable.getConstraints();
  }

  public BoolExpr setCondition(BoolExpr c) {
    return (conditionConstraint = z3.mkEq(condition, c));
  }

  public BoolExpr getCondition() {
    return condition;
  }

  public BoolExpr aliasExpr(Expr expr, Expr alias) {
    BoolExpr result = z3.mkEq(expr, alias);
    opStack.constraints.add(result);    // XXX: This doesn't really belong here, but is placed here
                                        // for convenience. This should be stored in the state's
                                        // own constraint list (which doesn't exist yet).
    return result;
  }

  public BoolExpr constrainSuccessors(State trueState, State falseState) {
    if (falseState == null) {
      return constrainSuccessor(trueState);
    }
    // First, propagate the states and store them in trueEdge and falseEdge
    trueBranchConstraint = propagateState(trueState);
    falseBranchConstraint = propagateState(falseState);
    edgeConstraint =
        z3.mkAnd(z3.ctx.mkImplies(condition, z3.mkAnd(trueBranchConstraint,
            z3.mkEq(trueState.executed, z3.ctx.mkTrue()))),
            z3.ctx.mkImplies(z3.mkNot(condition), z3.mkAnd(falseBranchConstraint,
                z3.mkEq(falseState.executed, z3.ctx.mkTrue()))));
    return edgeConstraint;
  }

  public BoolExpr constrainSuccessor(State nextState) {
    trueBranchConstraint = propagateState(nextState);
    edgeConstraint = z3.ctx.mkImplies(
        executed,
        z3.mkAnd(z3.mkEq(nextState.executed, z3.ctx.mkTrue()), trueBranchConstraint));
    return edgeConstraint;
  }

  /**
   * When the associated BlockConstraint is freed we need to do some cleanup work, such as unsetting
   * successor states, etc.
   */
  public void reset() {
    trueBranchConstraint = null;
    falseBranchConstraint = null;
    edgeConstraint = null;
  }

  public void printConstraints(String header) {
    System.out.println(header);
    for (BoolExpr be : opStack.getConstraintsArray()) {
      System.out.println("    " + be);
    }

    System.out.println(getVartableConstraints());
    System.out.println("             State Condition: " + condition);
    System.out.println("      True Branch Constraint: " + trueBranchConstraint);
    System.out.println("     False Branch Constraint: " + falseBranchConstraint);
    System.out.println("             Edge Constraint: " + edgeConstraint);
    System.out.println(
        "================================================================================");

  }
}
