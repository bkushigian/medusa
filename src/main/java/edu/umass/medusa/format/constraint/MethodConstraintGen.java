package edu.umass.medusa.format.constraint;

import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.util.Enumerator;
import edu.umass.medusa.util.Z3Session;

import java.util.*;

public class MethodConstraintGen {

  final MethodIdentity method;
  final Z3Session z3;
  Enumerator blockEnumerator = new Enumerator();
  BytecodeConstrainer bytecodeConstrainer;
  IBlock initialBlock, terminalBlock;
  BlockConstraint initDefaultConstraint, termBlockConstraint;

  public MethodConstraintGen(MethodIdentity method, Z3Session z3) {
    this.method = method;
    this.z3 = z3;
    bytecodeConstrainer = new BytecodeConstrainer(z3);

    ControlFlowGraph g = method.cfg;
    for (IBlock b : g.getAllNodes()) {
      b.storeProperty("gen", new BlockConstraintGen(this, b, z3));
      if (b.getRegularPredecessorCount() == 0) {
        initialBlock = b;
      } else if (b.getRegularSuccessorCount() == 0) {
        terminalBlock = b;
      }
    }
  }


  public MethodConstraint generate() {
    return new MethodConstraint(this, -1); // TODO: This is a "I'm broken" value
  }

  /**
   * Generate a set of block constraints, one for each block.
   *
   * @return the generated constraints
   */
  public Set<BlockConstraint> generateBlockConstraints() {
    final List<IBlock> allNodes = new ArrayList<IBlock>();
    allNodes.addAll(method.cfg.getAllNodes());
    allNodes.sort(Comparator
        .comparingInt(a -> a.getInstructionNums().isEmpty() ? -1 : a.getFirstInstructionNum()));
    final Set<BlockConstraint> constraints = new HashSet<>();

    for (IBlock block : allNodes) {
      final BlockConstraintGen bc = (BlockConstraintGen) block.getProperty("gen");
      final BlockConstraint freeBc = bc.getFree();
      constraints.add(freeBc);
    }
    return constraints;
  }

  public Map<IBlock, BlockConstraint> generateIndexedBlockConstraints() {
    HashMap<IBlock, BlockConstraint> constraints = new HashMap<>();

    final List<IBlock> allNodes = new ArrayList<IBlock>();
    allNodes.addAll(method.cfg.getAllNodes());
    allNodes.sort((a, b) -> Integer.compare(
        a.getInstructionNums().isEmpty() ? -1 : a.getFirstInstructionNum(),
        b.getInstructionNums().isEmpty() ? -1 : b.getFirstInstructionNum()));

    for (IBlock block : allNodes) {
      final BlockConstraintGen bc = (BlockConstraintGen) block.getProperty("gen");
      final BlockConstraint freeBc = bc.getFree();
      constraints.put(block, freeBc);
    }

    return constraints;
  }
}
