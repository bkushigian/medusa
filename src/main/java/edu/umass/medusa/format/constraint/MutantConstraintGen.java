package edu.umass.medusa.format.constraint;

import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.util.Z3Session;

public class MutantConstraintGen extends MethodConstraintGen {

  public MutantConstraintGen(MethodIdentity method, Z3Session z3) {
    super(method, z3);
  }
}
