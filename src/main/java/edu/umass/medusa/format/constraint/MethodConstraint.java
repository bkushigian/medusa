package edu.umass.medusa.format.constraint;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.Edge;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.exceptions.IncompatibleSignatureException;
import edu.umass.medusa.util.Pair;
import edu.umass.medusa.util.TypeUtil;
import edu.umass.medusa.util.Z3Session;
import org.objectweb.asm.Opcodes;

import java.util.*;

/**
 * A MethodConstraint wraps a set of BlockConstraints, possibly duplicated during loop unrolling,
 * and provides an interface for traversing these, generating edge and block constraints, as well as
 * other convenience functionalities.
 *
 *
 * Currently, and this will change, loop unrolling is not supported. Still, with an eye towards the
 * future, the APIs allow for unrolling related values to be passed in, and to search for a
 * particular basic block constraint this must be done via a sequence of loop head-count pair.
 *
 * The idea behind this is the following: each block constrain may be reached through multiple
 * paths, and one copy of the constraint will suffice in the absence of loops. However, once a loop
 * is introduced, a block may be executed multiple times, and to look up the block constraint we
 * also need to know how many times each loop head has been executed prior to this being executed.
 *
 * For instance, consider the following code with loop unrolling depth 2:
 *
 * <pre>
 *     int calculateFour() {
 *         int total = 0;
 *         for (int i = 0; i < 2; ++i) {        // LOOP_1
 *             for (int j = 0; j < 2; ++j) {    // LOOP_2
 *                 total += 1;
 *             }
 *         }
 *         return total;
 *     }
 * </pre>
 *
 * Say that we want a copy of the constraint of the inner most block corresponding to the execution
 * statement {@code total += 1;}. This is ambiguous of course since this gets executed 4 times, and
 * each execution is in one-to-one correspondence with a value of i and a value of j, which in turn
 * is in bijective correspondence with the possible loop heads and the number of times they
 * occurred:
 * <ol>
 * <li>(LOOP_1, 1), (LOOP_2, 1)</li>
 * <li>(LOOP_1, 1), (LOOP_2, 2)</li>
 * <li>(LOOP_1, 2), (LOOP_2, 1)</li>
 * <li>(LOOP_1, 2), (LOOP_2, 2)</li>
 * </ol>
 *
 * More complicated structures may not retain this bijective correspondence but they have a slightly
 * weaker correspondence that each block is executed <em>at most</em> once for the possible
 * loop-head paths surrounding it. For example,
 *
 * <pre>
 *     int calculateFour() {
 *         int total = 0;
 *         for (int i = 0; i < 2; ++i) {        // LOOP_1
 *             for (int j = 0; j < 2; ++j) {    // LOOP_2
 *                 if (i % 2 == 0) {
 *                     total += 2;
 *                 }
 *             }
 *         }
 *         return total;
 *     }
 * </pre>
 *
 * the statement {@code total += 2;} is now executed twice, but really no matter what boolean
 * expression is placed in the {@code if (...)} expression, we know that the number of executions is
 * <em>at most</em> four, creating a reasonable upper bound that grows linearly with the possible
 * loop executions.
 */

public class MethodConstraint {

  /**
   * The constraint generator that created this method constraint
   */
  final MethodConstraintGen gen;

  /**
   * The MethodIdentity of the method we are constraining
   */
  final MethodIdentity methId;

  /**
   * The Z3 Session that this constraint belongs to
   */
  final Z3Session z3;

  /**
   * Exprs representing the parameters. Note that for a method
   * <pre>
   *     T foo(T1 p1, T2 p2, ..., Tn pn) { }
   * </pre>
   *
   * that paramExprs will have length n even though there is an implicit {@code this} parameter. It
   * is this casses responsibility to account for static/non-static status of the method we are
   * modelling.
   */
  final BitVecExpr[] paramExprs;

  final TypeUtil.Type[] paramTypes;

  /**
   * Expr representing the return value of this method.
   */
  final Expr retExpr;

  final TypeUtil.Type retType;

  /**
   * Control flow graph that is modeled for easy traversing
   */
  final ControlFlowGraph cfg;

  /**
   * Is this method static?
   */
  final Boolean isStatic;

  /**
   * Convenience handles for the top and bot blocks
   */
  final IBlock topBlock, botBlock;

  /**
   * This forwards the top block's 'executed' bool expr
   */
  public BoolExpr executed;

  Set<BlockConstraint> blockConstraints;

  Map<Edge, BoolExpr> edgeConstraintMap;

  /**
   * This maps the blocks to the constraints that are generated from them
   */
  public Map<IBlock, BlockConstraint> blockConstraintMap;

  final int loopUnrollDepth;

  /**
   * Convenience handles for the top and bot block constraints
   */
  BlockConstraint topConstraint, botConstraint;
  private Set<IBlock> loopHeads;
  private HashMap<IBlock, Set<IBlock>> loopTails;
  private HashMap<IBlock, Set<IBlock>> loopContents;
  private BoolExpr body;
  private BoolExpr edges;

  MethodConstraint(MethodConstraintGen gen, int loopUnrollDepth) {
    this.gen = gen;
    this.methId = gen.method;
    this.z3 = gen.z3;
    this.cfg = methId.cfg;
    this.isStatic = (methId.method.access & Opcodes.ACC_STATIC) != 0;
    this.loopUnrollDepth = loopUnrollDepth;
    this.loopHeads = null;
    this.loopTails = null;
    this.loopContents = null;
    this.topConstraint = null;
    this.botConstraint = null;
    this.blockConstraintMap = new HashMap<>();

    TypeUtil.Type[] types = TypeUtil.parseMethodDesc(methId.desc);
    this.paramTypes = Arrays.copyOf(types, types.length - 1);
    this.retType = types[types.length - 1];

    Pair<IBlock, IBlock> topAndBot = calculateTopAndBotBlocks();
    this.topBlock = topAndBot.first;
    this.botBlock = topAndBot.second;

    calculateBody();
    calculateEdges();

    // The following depends upon block constraints generated for topBlock, which occur during calculateBody
    this.paramExprs = calculateParamExprs();
    this.retExpr = calculateRetExpr();
  }


  protected Pair<IBlock, IBlock> calculateTopAndBotBlocks() {
    IBlock tb = null, bb = null;
    for (IBlock b : cfg.getAllNodes()) {
      if (b.getRegularPredecessorCount() == 0) {
        tb = b;
      }
      if (b.getRegularSuccessorCount() == 0) {
        bb = b;
      }
    }

    if (tb == null || bb == null) {
      throw new IllegalStateException("CFG does not have both a terminal and initial block");
    }

    return new Pair<>(tb, bb);
  }

  /**
   * Given the loop unrolling depth, calculate the body constraints and update the {@code
   * Pair<LoopHead, executionCount>[] => BlockConstraint} map TODO not implemented yet
   */
  protected void calculateBody() {
    if (loopHeads == null || loopTails == null || loopContents == null) {
      calculateLoopData();
    }

    if (!loopHeads.isEmpty()) {
      throw new RuntimeException("Loop unrolling is not implemented yet");
    }

    // get a copy of the block constraints
    // TODO: This will have to change when we deal with loop unrolling
    blockConstraints = gen.generateBlockConstraints();
    BlockConstraint top = null, bot = null;
    for (BlockConstraint bc : blockConstraints) {
      blockConstraintMap.put(bc.block, bc);
      if (bc.isTop()) {
        top = bc;
      } else if (bc.isBot()) {
        bot = bc;
      }
    }

    if (top == null || bot == null) {
      throw new IllegalStateException(
          "Missing either a top or a bot block---top: " + top + ", bot: " + bot);
    }
    topConstraint = top;
    botConstraint = bot;
    executed = top.executed;

    body = z3.mkAnd(
        (BoolExpr[]) blockConstraints
            .stream()
            .map(BlockConstraint::getBlockConstraints)
            .toArray(BoolExpr[]::new));
  }

  /**
   * Update true/false successor data, calculate the edge constraints, and store them in a map.
   *
   * TODO: This doesn't handle loop unrolling yet and assumes a unique block constraint for each
   * block
   */
  protected void calculateEdges() {
    edgeConstraintMap = new HashMap<>();
    List<BoolExpr> edgeConstraints = new ArrayList<>();
    for (IBlock b : cfg.getAllNodes()) {
      for (Edge e : b.getEdges()) {
        if (e.src.equals(b)) {
          BlockConstraint src = blockConstraintMap.get(e.src),
              dst = blockConstraintMap.get(e.dst);

          EdgeConstraint edge;
          BoolExpr constraint;
          if ("true".equals(e.metaData)) {
            edge = src.addEdge(dst, true);
            constraint = edge.conditionedConstraints();
          } else if ("false".equals(e.metaData)) {
            edge = src.addEdge(dst, false);
            constraint = edge.conditionedConstraints();
          } else if (e.metaData == null) {
            // Single edge from
            if (b.getRegularSuccessorCount() != 1) {
              throw new RuntimeException(
                  "No metadata found but multiple possible successors exist");
            }
            edge = src.addEdge(dst, true);
            constraint = edge.unconditionedConstraints();
          } else {
            throw new RuntimeException("Unrecognized metadata " + e.metaData);
          }
          edgeConstraints.add(constraint);
          edgeConstraintMap.put(e, constraint);
        }
      }
    }
    edges = z3.mkAnd(edgeConstraints.toArray(new BoolExpr[edgeConstraints.size()]));
  }

  /**
   * Calculate the loop heads
   */
  protected void calculateLoopData() {
    loopHeads = new HashSet<>();
    loopTails = new HashMap<>();
    loopContents = new HashMap<>();
    calculateLoopData(new Stack<>(), new HashSet<>(), topBlock, loopHeads, loopTails, loopContents);
  }

  /**
   * Perform a DFS to calculate loop-related data for loop unrolling.
   *
   * @param loopTails map block to loop head that succeeds it
   */
  protected void calculateLoopData(Stack<IBlock> stack,
      Set<IBlock> visited,
      IBlock block, Set<IBlock> loopHeads,
      HashMap<IBlock, Set<IBlock>> loopTails,
      HashMap<IBlock, Set<IBlock>> loopContents) {
    // Invariant 1: |stack| == |visited| at all times
    if (block == null || block.equals(botBlock)) {
      return;
    }

    // Invariant 2: if we have visited a block, we have visited exactly once
    if (visited.contains(block)) {
      IBlock head = block;        // For readability

      // Here we have reached a loop head, and the stack [head, a, b, ..., z] contains an execution
      // trace from the first execution of head to a back-edge
      IBlock last = stack.peek();     // Stack is non-empty since visited is non-empty (Invariant 1)
      loopHeads.add(head);           // Block is a loop head (Invariant II)

      // update loopTails, which records the set of back-edges in our CFG
      if (!loopTails.containsKey(last)) {
        loopTails.put(last, new HashSet<>());
      }
      loopTails.get(last).add(head);

      // Update loopContents, recording which blocks are contained in the loop with head `block`
      if (!loopContents.containsKey(head)) {
        loopContents.put(head, new HashSet<>());
      }
      Set<IBlock> contents = loopContents.get(head);
      for (IBlock b : stack) {
        contents.add(b);
        if (b.equals(head)) {
          break;
        }
      }

      return;
    }

    // We have not seen this block before, so add it to visited and stack and calculate its successors
    visited.add(block);
    stack.push(block);

    for (IBlock succ : block.getRegularSuccessors()) {
      calculateLoopData(stack, visited, succ, loopHeads, loopTails, loopContents);
    }

    visited.remove(block);
    stack.pop();
  }

  /**
   * Using the calculated constraints for the top block, extract the stored initial values in the
   * top state representing the parameters and store them in an array.
   */
  protected BitVecExpr[] calculateParamExprs() {
    return calculateParamExprs(topConstraint);
  }

  /**
   * Return a {@code BitVecExpr[]} containing the method parameters. Note that this does NOT
   * include a {@code this} parameter which should be handled separately.
   * @param topConstraint the constraint block representing the entry into the method
   * @return array of explicit method params
   */
  protected BitVecExpr[] calculateParamExprs(BlockConstraint topConstraint) {
    // todo: This does not currently handle a `this` parameter
    final int size = paramTypes.length;

    final BitVecExpr[] params = new BitVecExpr[size];
    final int offset = isStatic ? 0 : 1;
    for (int i = 0; i < size; ++i) {
      params[i] = topConstraint.state.load(i + offset);
    }
    return params;
  }

  /**
   * Given another MethodConstraint, create a boolean expression asserting that the input values are
   * the same
   */
  public BoolExpr identifyInputParams(MethodConstraint other) {
    if (!sigTypesAreCompatible(other)) {
      throw new IncompatibleSignatureException(paramTypes, other.paramTypes, retType,
          other.retType);
    }
    BoolExpr[] toAnd = new BoolExpr[other.paramExprs.length];
    for (int i = 0; i < other.paramExprs.length; ++i) {
      toAnd[i] = z3.mkEq(paramExprs[i], other.paramExprs[i]);
    }
    return z3.mkAnd(toAnd);
  }

  /**
   * Given another BlockConstraint, create a boolean expression asserting that the input values are
   * the same.
   */
  public BoolExpr identifyInputParams(BlockConstraint blockConstraint) {
    BoolExpr[] toAnd = new BoolExpr[paramExprs.length];
    Expr[] otherParamExprs = calculateParamExprs(blockConstraint);
    for (int i = 0; i < paramExprs.length; ++i) {
      toAnd[i] = z3.mkEq(paramExprs[i], otherParamExprs[i]);
    }
    return z3.mkAnd(toAnd);
  }

  /**
   * Given another MethodConstraint, create a boolean expression asserting that the return values
   * are different.
   */
  public BoolExpr separateReturnValues(MethodConstraint other) {
    if (!retTypesAreCompatible(other)) {
      throw new IncompatibleSignatureException(paramTypes, other.paramTypes, retType,
          other.retType);
    }
    return z3.mkNot(z3.mkEq(retExpr, other.retExpr));
  }

  private boolean sigTypesAreCompatible(MethodConstraint other) {
    return Arrays.equals(paramTypes, other.paramTypes);
  }

  private boolean retTypesAreCompatible(MethodConstraint other) {
    return retType.equals(other.retType);
  }

  private Expr calculateRetExpr() {
    return botConstraint.state.peek();
  }

  public BoolExpr getBlockConstraints() {
    return body;
  }

  public BoolExpr getEdgeConstraints() {
    return edges;
  }

  public Expr getReturnExpr() {
    return retExpr;
  }

  public BitVecExpr getParam(int i) {
    if (i < 0 || i >= paramExprs.length) {
      throw new IndexOutOfBoundsException(
          String.format(
              "Method Constraint for %s: Paramter index out of bounds:\n  index : %d\n  range : (0, %d)",
              methId.name, i, paramExprs.length));
    }

    return paramExprs[i];
  }

  public BoolExpr getConstraints() {
    return z3.mkAnd(getBlockConstraints(), getEdgeConstraints(), executed);
  }
}
