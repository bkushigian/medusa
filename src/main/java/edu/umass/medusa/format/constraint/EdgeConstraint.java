package edu.umass.medusa.format.constraint;

import com.microsoft.z3.BoolExpr;
import edu.umass.medusa.util.Z3Session;

/**
 * An edge between {@code BlockConstraint}s.
 */
public class EdgeConstraint {

  /**
   * Source constraint
   */
  private final BlockConstraint source;

  /**
   * Target constraint
   */
  private final BlockConstraint target;

  /**
   * Condition associated with this edge (is it a true edge or a false edge?)
   */
  private final boolean condition;

  /**
   * Z3Session that we are running.
   */
  private final Z3Session z3;

  /**
   * Create a true {@code EdgeConstraint} from {@code s} to target {@code t}.
   *
   * @param s source {@code BlockConstraint}
   * @param t target {@code BlockConstraint}
   */
  public EdgeConstraint(BlockConstraint s, BlockConstraint t) {
    this(s, t, true);
  }

  /**
   * Create an {@code EdgeConstraint} from {@code s} to target {@code t} with condition {@code
   * condition}
   *
   * @param s source {@code BlockConstraint}
   * @param t target {@code BlockConstraint}
   */
  public EdgeConstraint(BlockConstraint s, BlockConstraint t, boolean c) {
    assert s != null && t != null;
    source = s;
    target = t;
    z3 = s.z3;
    condition = c;
  }

  /**
   * Return the unconditioned constraints associated with the state propagation along this edge.
   * This includes
   * <ol>
   * <li>
   * The final state of {@code source} is asserted to be equal to the initial state of {@code
   * target}
   * </li>
   * <li>
   * {@code target.executed} is set to true
   * </li>
   * </ol>
   *
   * @return The unpredicated propgation of states from {@code source} to {@code target}, as well as
   * enforcing that {@code target} is executed.
   */
  public BoolExpr propagationConstraints() {
    BoolExpr stateProp = source.state.propagateState(target.state);
    return z3.mkAnd(stateProp, target.executed);
  }

  /**
   * Return the conditioned constraints associated with this edge being taken during execution. This
   * conditioning involves
   * <pre>
   *     (=> (and source.executed (= source.condition this.condition))
   *          this.unconditionedConstraints())
   * </pre>
   *
   * @return the conditioned edge constraints, predicated on {@code source.condition} and {@code
   * source.executed}.
   */
  public BoolExpr conditionedConstraints() {
    return z3.mkImplies(this.condition ? source.condition : z3.mkNot(source.condition),
        unconditionedConstraints());
  }

  /**
   * Like conditionedConstraints, but used when this edge is always taken. This allows us to not
   * include the source.condition check, reducing our formula size.
   * <pre>
   *     (=> source.executed propagationConstraints())
   * </pre>
   *
   * @return the unconditioned edge constraints, predicated on source.executed.
   */
  public BoolExpr unconditionedConstraints() {
    return z3.mkImplies(source.getExecuted(), propagationConstraints());
  }
}
