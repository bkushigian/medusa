package edu.umass.medusa.exceptions;

import edu.umass.medusa.util.TypeUtil;

public class IncompatibleSignatureException extends RuntimeException {

  public IncompatibleSignatureException(TypeUtil.Type[] sig1, TypeUtil.Type[] sig2,
      TypeUtil.Type ret1, TypeUtil.Type ret2) {
    super(String.format("Found incompatible signatures: (%s) => %s, (%s) => %s",
        typeSigToString(sig1), ret1.toString(),
        typeSigToString(sig2), ret2.toString()));
  }

  private static String typeSigToString(TypeUtil.Type[] sig) {
    StringBuilder sb = new StringBuilder("(");
    for (TypeUtil.Type t : sig) {
      sb.append(t).append(" ");
    }
    return sb.append(")").toString();
  }
}
