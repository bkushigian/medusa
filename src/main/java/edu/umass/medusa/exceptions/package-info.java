/**
 * This package defines Medusa-related exceptions
 */
package edu.umass.medusa.exceptions;