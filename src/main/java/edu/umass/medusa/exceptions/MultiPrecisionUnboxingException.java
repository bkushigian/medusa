package edu.umass.medusa.exceptions;

public class MultiPrecisionUnboxingException extends RuntimeException {

  public MultiPrecisionUnboxingException(String msg) {
    super(msg);
  }
}
