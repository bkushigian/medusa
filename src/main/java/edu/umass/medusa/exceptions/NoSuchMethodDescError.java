package edu.umass.medusa.exceptions;

public class NoSuchMethodDescError extends RuntimeException {

  public NoSuchMethodDescError(String msg) {
    super(msg);
  }
}
