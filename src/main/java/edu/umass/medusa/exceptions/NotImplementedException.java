package edu.umass.medusa.exceptions;

public class NotImplementedException extends RuntimeException {
  public NotImplementedException(String msg) {
    super(msg);
  }
}
