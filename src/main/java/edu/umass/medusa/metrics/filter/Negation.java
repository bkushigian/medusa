package edu.umass.medusa.metrics.filter;

public class Negation<T> implements Filter<T> {

  final Filter<T> filter;

  public Negation(Filter<T> filter) {
    this.filter = filter;
  }

  @Override
  public boolean keep(T t) {
    return !filter.keep(t);
  }
}
