package edu.umass.medusa.metrics.filter;

/**
 * This filter always returns true
 */
public class TrueFilter<T> implements Filter<T> {

  @Override
  public boolean keep(T t) {
    return true;
  }
}
