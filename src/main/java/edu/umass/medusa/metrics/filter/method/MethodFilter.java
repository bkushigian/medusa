package edu.umass.medusa.metrics.filter.method;

import edu.umass.medusa.metrics.filter.Filter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

public interface MethodFilter extends Filter<MethodNode> {

  default boolean keep(MethodNode method, ClassNode clss) {
    return keep(method);
  }
}
