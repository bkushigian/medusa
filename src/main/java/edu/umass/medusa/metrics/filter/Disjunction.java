package edu.umass.medusa.metrics.filter;

public class Disjunction<T> implements Filter<T> {

  public final Filter<T> left;
  public final Filter<T> right;

  public Disjunction(Filter<T> left, Filter<T> right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public boolean keep(T t) {
    return left.keep(t) || right.keep(t);
  }
}
