package edu.umass.medusa.metrics.filter;

public interface Filter<T> {

  boolean keep(T t);
}
