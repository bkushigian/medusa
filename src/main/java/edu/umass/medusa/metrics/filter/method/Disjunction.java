package edu.umass.medusa.metrics.filter.method;

import edu.umass.medusa.metrics.filter.Filter;
import org.objectweb.asm.tree.MethodNode;

public class Disjunction extends edu.umass.medusa.metrics.filter.Conjunction<MethodNode> implements
    MethodFilter {

  public Disjunction(Filter<MethodNode> f, Filter<MethodNode> g) {
    super(f, g);
  }
}
