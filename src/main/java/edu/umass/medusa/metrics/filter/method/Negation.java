package edu.umass.medusa.metrics.filter.method;

import org.objectweb.asm.tree.MethodNode;
import edu.umass.medusa.metrics.filter.Filter;

public class Negation extends edu.umass.medusa.metrics.filter.Negation<MethodNode> implements
    MethodFilter {

  public Negation(Filter<MethodNode> f) {
    super(f);

  }
}
