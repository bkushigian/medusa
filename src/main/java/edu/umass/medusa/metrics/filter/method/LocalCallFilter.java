package edu.umass.medusa.metrics.filter.method;

import edu.umass.medusa.util.bytecode.BytecodeUtil;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class LocalCallFilter implements MethodFilter {

  @Override
  public boolean keep(MethodNode method) {
    return false;
  }

  @Override
  public boolean keep(MethodNode method, ClassNode clss) {

    for (final AbstractInsnNode instr : method.instructions.toArray()) {
      if (BytecodeUtil.isCallInstr(instr)) {
        MethodInsnNode mn = (MethodInsnNode) instr;
        // TODO: Check this!
        if (!mn.owner.equals(clss.name)) {
          return false;
        }
      }
    }

    return true;
  }
}
