package edu.umass.medusa.metrics.filter.method;

import edu.umass.medusa.metrics.filter.Filter;
import org.objectweb.asm.tree.MethodNode;

public class Conjunction extends edu.umass.medusa.metrics.filter.Conjunction<MethodNode> implements
    MethodFilter {

  public Conjunction(Filter<MethodNode> f, Filter<MethodNode> g) {
    super(f, g);
  }
}
