package edu.umass.medusa.metrics.filter;

/**
 * This filter always returns false
 */
public class FalseFilter<T> implements Filter<T> {

  @Override
  public boolean keep(T t) {
    return false;
  }
}
