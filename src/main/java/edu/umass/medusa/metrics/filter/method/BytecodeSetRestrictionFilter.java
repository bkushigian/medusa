package edu.umass.medusa.metrics.filter.method;

import edu.umass.medusa.metrics.filter.Filter;
import edu.umass.medusa.util.bytecode.MutableBytecodeSet;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;


public class BytecodeSetRestrictionFilter implements MethodFilter {

  private final MutableBytecodeSet instructions;

  public BytecodeSetRestrictionFilter(MutableBytecodeSet instructions) {
    this.instructions = instructions;
  }

  @Override
  public boolean keep(MethodNode method) {
    final AbstractInsnNode[] instrs = method.instructions.toArray();
    for (AbstractInsnNode instr : instrs) {
      if (instructions.contains(instr.getOpcode())) {
        return false;
      }
    }
    return true;
  }
}
