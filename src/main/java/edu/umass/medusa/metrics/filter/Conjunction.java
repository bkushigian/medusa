package edu.umass.medusa.metrics.filter;

/**
 * Keeps precisely the values t that both left and right filters keep
 */
public class Conjunction<T> implements Filter<T> {

  public final Filter<T> left;
  public final Filter<T> right;

  public Conjunction(Filter<T> left, Filter<T> right) {
    this.left = left;
    this.right = right;
  }

  @Override
  public boolean keep(T t) {
    return left.keep(t) && right.keep(t);
  }
}
