package edu.umass.medusa.metrics.filter.method;

import org.objectweb.asm.tree.MethodNode;

import static org.objectweb.asm.Opcodes.ACC_STATIC;

public final class StaticMethodFilter implements MethodFilter {

  @Override
  public boolean keep(MethodNode method) {
    return (method.access & ACC_STATIC) != 0;
  }
}
