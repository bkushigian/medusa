package edu.umass.medusa.metrics;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.metrics.filter.method.MethodFilter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import static org.objectweb.asm.Opcodes.ACC_ABSTRACT;
import static org.objectweb.asm.Opcodes.ACC_INTERFACE;

public abstract class Analyzer {

  // To cut down on bloat, we can cut off the longest prefix for file paths
  public String longestPathPrefix = null;
  public Map<String, AnalyzerResult> results;
  /**
   * should we pre-process our ClassIdentites (i.e., unbox integers, etc) before analysis?
   */
  protected boolean preProcess = true;


  /**
   * Collect classfiles and return a map from paths to their associated ClassIdentities
   */
  public Map<String, ClassIdentity> collectClassfiles(String path) {
    File dir = new File(path);
    if (!dir.isDirectory()) {
      throw new RuntimeException(String.format("Path %s is not a directory", path));
    }
    return collectClassfiles(dir);
  }

  public abstract AnalyzerResult analyzeMethod(MethodNode method, ClassNode owner);

  /**
   * Collect classfiles
   */
  public Map<String, ClassIdentity> collectClassfiles(File file) {
    Map<String, ClassIdentity> results = new HashMap<>();
    if (file.isDirectory()) {
      final File[] children = file.listFiles();
      if (children == null) {
        return results;
      }

      for (File child : children) {
        results.putAll(collectClassfiles(child));
      }
    } else if (file.getName().endsWith(".class")) {

      String absPath = file.getAbsolutePath();
      try {
        results.put(absPath, new ClassIdentity(file));
      } catch (IOException e) {
        e.printStackTrace();
      }

    }

    return results;
  }

  protected Map<String, AnalyzerResult> analyzeClasses(Map<String, ClassIdentity> classIds,
      MethodFilter filter,
      BiConsumer<AnalyzerResult, String> action) {
    Map<String, AnalyzerResult> results = new HashMap<>();
    for (Map.Entry<String, ClassIdentity> e : classIds.entrySet()) {
      results.putAll(analyzeClass(e.getValue(), filter, action));
    }
    return results;
  }

  protected Map<String, AnalyzerResult> analyzeClass(ClassIdentity cid, MethodFilter filter,
      BiConsumer<AnalyzerResult, String> action) {
    return analyzeClass(cid.classNode, filter, action);
  }

  protected Map<String, AnalyzerResult> analyzeClass(ClassNode cn, MethodFilter filter,
      BiConsumer<AnalyzerResult, String> action) {
    Map<String, AnalyzerResult> results = new HashMap<>();
    for (Object m : cn.methods) {
      final MethodNode method = (MethodNode) m;
      if ((method.access & (ACC_ABSTRACT | ACC_INTERFACE)) != 0) {
        continue;
      }

      if (!filter.keep(method, cn)) {
        continue;
      }

      final AnalyzerResult result = analyzeMethod(method, cn);
      String key = getName(cn, method);
      results.put(key, result);
      updateLongestPathPrefix(key);

      action.accept(result, getName(cn, method));
    }
    return results;
  }

  protected void updateLongestPathPrefix(String key) {
    // update longestPathPrefix
    if (longestPathPrefix == null) {
      longestPathPrefix = key;
    } else {
      for (int i = 0; i < Math.min(longestPathPrefix.length(), key.length()); ++i) {
        if (longestPathPrefix.charAt(i) != key.charAt(i)) {
          longestPathPrefix = longestPathPrefix.substring(0, i);
          break;
        }
      }
    }
  }

  protected String getName(ClassNode cls, MethodNode method) {
    return cls.name + ":" + method.name + (method.desc == null ? "()" : method.desc);
  }

  public void writeResults(String path, Map<String, AnalyzerResult> results,
      String header, BiFunction<String, AnalyzerResult, String> action) {
    StringBuilder sb = new StringBuilder();
    SortedSet<String> sortedKeys = new TreeSet<>(results.keySet());

    // write header
    sb.append(header);
    // Compute length to chop off from start of each string
    final int ltrim = longestPathPrefix == null ? 0 : longestPathPrefix.length();
    for (String key : sortedKeys) {
      AnalyzerResult result = results.get(key);
      sb.append(action.apply(key.substring(ltrim), result));
    }

    File file = new File(path);
    try {
      if (!file.exists()) {
        file.createNewFile();
      }
      FileWriter writer = new FileWriter(file);
      writer.write(sb.toString());
      writer.flush();
      writer.close();
    } catch (IOException e) {
      System.err.printf("Couldn't create file %s\n", path);
    }
  }
}
