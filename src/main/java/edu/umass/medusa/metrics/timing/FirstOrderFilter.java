package edu.umass.medusa.metrics.timing;

import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.metrics.filter.Filter;
import edu.umass.medusa.util.CFGUtil;

public class FirstOrderFilter implements Filter<MethodIdentity> {

  final MethodIdentity mid;

  public FirstOrderFilter(MethodIdentity mid) {
    this.mid = mid;
  }


  @Override
  public boolean keep(MethodIdentity mid) {
    return CFGUtil.isAtomicCFGMutant(this.mid, mid);
  }
}
