package edu.umass.medusa.metrics.timing;


import com.microsoft.z3.Params;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantClassIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.metrics.filter.Filter;
import edu.umass.medusa.metrics.filter.TrueFilter;
import edu.umass.medusa.pipeline.Pipeline;
import edu.umass.medusa.solver.result.SolverResult;
import edu.umass.medusa.solver.strategies.ForkStrategy;
import edu.umass.medusa.solver.strategies.ImprovedNaiveStrategy;
import edu.umass.medusa.solver.strategies.NaiveStrategy;
import edu.umass.medusa.solver.strategies.Strategy;
import edu.umass.medusa.util.FSUtil;
import edu.umass.medusa.util.Z3Session;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * This class acts as a simple timing CLI where timing code can be stashed and used in an ad hoc
 * fashion.
 */
public class StrategyTimer {

  Z3Session z3;
  Pipeline pipeline;

  // OPTIONAL (defaults provided)
  Strategy strategy;
  String filterString;
  Boolean includeSetupTime;
  Boolean collectIndividualTimes;
  Filter<MethodIdentity> filter;
  int timeout = 0;

  // REQUIRED
  File origFile;
  File[] mutantFiles;
  String desc;
  boolean csv;

  // DERIVED
  MethodIdentity original;
  MutantIdentity[] mutants;
  long totalTime;


  public StrategyTimer() {
    reset();
  }

  public static void main(String[] args) {
    StrategyTimer timer = new StrategyTimer();
    try {
      timer.applyArgs(args);
      // NOTE: still need to check filter string
      timer.time();
      printResults(timer);
    } catch (IOException e) {
      e.printStackTrace();
      timer.usage("Caught an IOException");
    }
  }

  public static void printResults(StrategyTimer timer) {
    long t = timer.totalTime;
    int size = timer.mutants.length;
    int numReasoned = timer.strategy.getReasonedAbout();
    int numEquiv = 0;
    int numNEquiv = 0;
    int numUnknown = 0;
    long equivTime = 0;
    long nEquivTime = 0;
    long unknownTime = 0;
    for (MutantIdentity mid : timer.mutants) {
      final SolverResult result = mid.getResult();
      if (result == null) {
        ++numUnknown;
      } else if (result.isEquiv()) {
        equivTime += mid.time;
        ++numEquiv;
      } else if (result.isNotEquiv()) {
        nEquivTime += mid.time;
        ++numNEquiv;
      } else {
        unknownTime += mid.time;
        ++numUnknown;
      }
    }
    if (timer.csv) {
      double mean = (double) t / (double) numReasoned;
      double meanEq = (double) equivTime / (double) numEquiv;
      double meanNEq = (double) nEquivTime / (double) numNEquiv;
      StringJoiner joiner = new StringJoiner(",");
      joiner.add(timer.origFile.getName())                          // original file
          .add(timer.desc)                                      // desc
          .add(timer.strategy.type())                           // strategy type
          .add(timer.filterString)                              // filter
          .add(timer.includeSetupTime.toString())               // include setup
          .add(String.valueOf(size))                            // Num Reasoned
          .add(String.valueOf(numReasoned))                     // Num Reasoned
          .add(String.valueOf(numEquiv))
          .add(String.valueOf(numNEquiv))
          .add(String.valueOf(timer.totalTime))
          .add(String.valueOf(equivTime))
          .add(String.valueOf(nEquivTime))
          .add(numReasoned == 0 ? "N/A" : String.valueOf(mean))   // mean time
          .add(String.valueOf(meanEq))
          .add(String.valueOf(meanNEq));
      System.out.println(joiner.toString());

    } else {
      System.out.println("strategy type:            " + timer.strategy.type());
      System.out.println("filter type:              " + timer.filterString);
      System.out.println("included setup?           " + timer.includeSetupTime);
      System.out.println("reasoned about:           " + numReasoned + " of " + size + " mutants");
      System.out.println("total time:               " + t + "ms");
      System.out.println("average time per mutant:  " + (numReasoned == 0 ?
          "N/A" : (double) t / (double) numReasoned + "ms/mutant"));
      System.out.println("Mutant Equivalence Summary");
      System.out.println(
          "      EQUIV: " + numEquiv + ", average time: " + ((double) equivTime / (double) numEquiv)
              + "ms");
      System.out.println("     NEQUIV: " + numNEquiv + ", average time: " + ((double) nEquivTime
          / (double) numNEquiv) + "ms");
      if (numUnknown > 0) {
        System.out.println("    UNKNOWN: " + numUnknown + ", average time: " + ((double) unknownTime
            / (double) numUnknown) + "ms");
      }
    }

  }

  public static void writeCSV() {

  }

  /**
   * Time a strategy on a
   */
  public long time() throws IOException {
    switch (filterString) {
      case "first-order":
        filter = new FirstOrderFilter(original);
        break;
      case "all":
      case "true":
        filter = new TrueFilter<>();
        break;
      default:
        filter = new TrueFilter<>();
        break;
    }

    if (timeout > 0) {
      final Params params = z3.ctx.mkParams();
      params.add("timeout", timeout);
      z3.z3.setParameters(params);
    }

    long t0_setup = 0,        // Start time including setup time
        t0_noSetup = 0,      // Start time without setup
        end = 0;             // Final time

    t0_setup = System.currentTimeMillis();

    // XXX: This is extra work because we have to flow first, then
    //      remove rather than filtering as we go. Not sure of a simple fix
    addMutants(original, mutants);
    final Set<MutantIdentity> ms = new HashSet<>(original.mutants);
    pipeline.add(original.classId);
    pipeline.flow();

    for (MutantIdentity m : ms) {
      if (!filter.keep(m)) {
        original.mutants.remove(m);
      }
    }

    t0_noSetup = System.currentTimeMillis();
    strategy.applyToMethod(original);
    end = System.currentTimeMillis();

    totalTime = end - (includeSetupTime ? t0_setup : t0_noSetup);
    return totalTime;

  }

  private int addMutants(MethodIdentity mid, MutantIdentity[] mutants) {
    int total = 0;
    for (MutantIdentity m : mutants) {
      mid.addMutant(m);
      total++;
    }
    return total;
  }

  /**
   * Usage string: StrategyTimer --original path/to/Original.class --mutants path/to/Mutant1.class
   * [path/to/rootDir ...] [--filter {all|first-order}] [--strategy {fork|naive}]
   * [--include-setup-time]
   */
  private void applyArgs(String[] args) throws IOException {
    int i = 0;
    final int totalArgs = args.length;
    Deque<String> unparsed = new ArrayDeque<>();

    while (i < totalArgs) {
      String arg = args[i];
      String param;
      switch (arg) {
        case "--help":
        case "-h":
          usage("(Printing Help Menu)");
        case "--strategy":
        case "-s":
          param = args[++i];
          if ("fork".equals(param)) {
            strategy = new ForkStrategy(z3);
          } else if ("naive".equals(param)) {
            strategy = new NaiveStrategy(z3);
          } else if ("improved-naive".equals(param)) {
            strategy = new ImprovedNaiveStrategy(z3);
          } else {
            System.err.println(
                "Unrecognized strategy value: " + param + ", continuing with fork-strategy");
            strategy = new ForkStrategy(z3);
          }
          break;
        case "--timeout":
          param = args[++i];
          try {
            timeout = Integer.parseInt(param);
          } catch (Exception e) {
            usage("Couldn't parse --timeout arg as int: " + param);
          }
          break;
        case "--print-csv-headers":
          StringJoiner j = new StringJoiner(",");
          j.add("classfile")
              .add("desc")
              .add("strategy")
              .add("filter")
              .add("time-setup")
              .add("total")
              .add("reasoned")
              .add("equiv")
              .add("nequiv")
              .add("time")
              .add("equiv-time")
              .add("nequiv-time")
              .add("mean")
              .add("equiv-mean")
              .add("nequiv-mean");
          System.out.println(j.toString());
          System.exit(0);

        case "--csv":
          csv = true;
          break;
        case "--filter":
        case "-f":
          param = args[++i];
          if ("first-order".equals(param) || "all".equals(param)) {
            filterString = param;
          } else {
            filterString = "first-order";
          }
          break;
        case "--original":
        case "-o":
          param = args[++i];
          origFile = new File(param);
          break;

        case "--include-setup":
          includeSetupTime = true;
          break;
        case "--desc":
        case "-d":
          desc = args[++i];
          break;
        default:
          if (args[i].startsWith("-")) {
            usage("Parse Error: Unrecognized option `" + args[i] + "`");
          } else {
            unparsed.add(args[i]);
          }
      }
      ++i;
    }

    applyUnparsedArgs(unparsed);

    if (origFile == null || mutantFiles == null || desc == null) {
      usage("Missing required parameters");
    }

    if (!origFile.exists()) {
      usage("Could not find original classfile: " + origFile);
    }
    ClassIdentity cid = new ClassIdentity(origFile);
    // Collect mutantFiles
    mutantFiles = FSUtil.collectClassfiles(mutantFiles).toArray(new File[0]);
    original = cid.getMethod(desc);
    mutants = Arrays.stream(mutantFiles)
        .map(f -> {
          try {
            return new MutantClassIdentity(f, original);
          } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unchecked IO Exception: Couldn't find file " + f);
          }
        })
        .map(mcid -> mcid.mutantId)
        .toArray(MutantIdentity[]::new);
    if (original == null || mutants == null) {
      usage("Values were not initialized");
    }
  }

  private void applyUnparsedArgs(Deque<String> unparsed) {
    // Parse the unparsed args. These should present in the order
    // origFile, desc, mutantFiles
    if (origFile == null) {
      if (unparsed.isEmpty()) {
        usage("No origFile was specified");
      }
      origFile = new File(unparsed.poll());
    }

    if (desc == null) {
      if (unparsed.isEmpty()) {
        usage("No method descriptor was specified");
      }
      desc = unparsed.poll();
    }

    if (!unparsed.isEmpty()) {
      if (mutantFiles == null) {
        mutantFiles = Arrays.stream(unparsed.toArray(new String[0])).map(File::new)
            .toArray(File[]::new);
      } else {
        final File[] tmp = new File[unparsed.size() + mutantFiles.length];
        int j = 0;
        for (; j < mutantFiles.length; ++j) {
          tmp[j] = mutantFiles[j];
        }
        for (; j < tmp.length; ++j) {
          tmp[j] = new File(unparsed.poll());
        }
      }
    }
  }

  private void usage() {
    usage(null);
  }

  private void usage(String msg) {
    if (msg != null && !msg.isEmpty()) {
      System.err.println(msg);
    }
    System.err.println("USAGE");
    System.err.println("=====\n");
    System.err.println(
        "java StrategyTimer (--help|-h)\n" +
            "                   Print this usage message\n" +
            "java StrategyTimer [OPTIONS] orig desc mutant1 [mutant2 mutant3...]\n" +
            "                   run the timer on class located at `orig` with method of description `desc`\n"
            +
            "                   against the mutants gathered by recursively traversing `mutants...`\n\n"
            +
            "OVERVIEW\n" +
            "========\n\n" +
            "This is a very simple module to time  strategies  on  varying  mutants.  These  are  only\n"
            +
            "rough estimates and should not be given too much  weight.  However  they  are  relatively\n"
            +
            "accurate, and efforts have been made to time things appropriately (i.e., factor out setup\n"
            +
            "costs that are not in all candidate strategies, etc).\n\n" +
            "EXAMPLE USAGE\n" +
            "=============\n\n" +
            "" +
            "OPTIONS\n" +
            "=======\n" +
            "    -s STRATEGY\n" +
            "    --strategy STRATEGY: specify the strategy to use ('naive' or 'fork')\n\n" +
            "    -f FILTER\n" +
            "    --filter FILTER:     specify the method filter to use ('all' or 'first-order')\n\n"
            +
            "    -o ORIGINAL\n" +
            "    --original ORIGINAL: specify the location original program's classfile\n\n" +
            "    -d DESC\n" +
            "    --desc DESC:         provide the method description to use\n\n" +
            "    --include-setup:     flag marking that setup time should be included\n"
    );
    System.exit(1);
  }

  public void reset() {
    z3 = new Z3Session();
    pipeline = new Pipeline(z3);
    strategy = new ForkStrategy(z3);
    filterString = "first-order";
    includeSetupTime = false;
    collectIndividualTimes = false;
    filter = null;
    csv = false;

    // REQUIRED
    origFile = null;
    mutantFiles = null;
    desc = null;

    // DERIVED
    original = null;
    mutants = null;
    totalTime = -1;
  }
}
