package edu.umass.medusa.metrics.branch;

import edu.umass.medusa.metrics.AnalyzerResult;

public class JumpInsnResult extends AnalyzerResult {

  int numberOfJumpInsns;
  int totalInsns;
  int basicBlocks;

  /**
   * increments # of jump instructions by 1
   */
  public void incrementJumpInsn() {
    numberOfJumpInsns += 1;
  }

  public int getNumberOfJumpInsns() {
    return numberOfJumpInsns;
  }

  public void incrementTotalInsns() {
    totalInsns += 1;
  }

  public int getTotalInsns() {
    return totalInsns;
  }

  public int getBasicBlocks() { return basicBlocks; }
}
