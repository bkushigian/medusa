package edu.umass.medusa.metrics.branch;

import static org.objectweb.asm.Opcodes.ACC_ABSTRACT;
import static org.objectweb.asm.Opcodes.ACC_INTERFACE;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.metrics.Analyzer;
import edu.umass.medusa.metrics.AnalyzerResult;
import edu.umass.medusa.metrics.filter.method.MethodFilter;
import edu.umass.medusa.metrics.filter.method.TrueFilter;
import edu.umass.medusa.pipeline.PipeASM2CFG;
import java.util.function.BiConsumer;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiFunction;

public class JumpInsnAnalyzer extends Analyzer {

  PipeASM2CFG pipe = new PipeASM2CFG();
  protected String resultDir = "out/analysis-results";

  public JumpInsnResult analyzeMethod(MethodNode method, ClassNode owner) {
    JumpInsnResult result = new JumpInsnResult();
    final ListIterator<AbstractInsnNode> iterator = method.instructions.iterator();

    while (iterator.hasNext()) {
      final AbstractInsnNode insn = iterator.next();

      if (insn.getOpcode() >= 0 && insn.getType() == AbstractInsnNode.JUMP_INSN) {
        result.incrementJumpInsn();
      }

      if (insn.getOpcode() >= 0) {
        result.incrementTotalInsns();
      }
    }

    return result;
  }

  public Map<String, AnalyzerResult> analyzePath(String path) {
    final Map<String, ClassIdentity> cids = collectClassfiles(path);
    return analyzeClasses(cids, new TrueFilter(), (result, name) -> {
    });
  }

  void writeResults(String path, Map<String, AnalyzerResult> results) {
    String header = "file,jumps-insns,total-insns,basic-blocks\n";

    BiFunction<String, AnalyzerResult, String> fn = (key, aresult) -> {
      JumpInsnResult result = (JumpInsnResult) aresult;
      return key + "," + result.getNumberOfJumpInsns() + "," + result.getTotalInsns() + ","
          + result.getBasicBlocks() + "\n";
    };

    if (path == null || path.equals("")) {
      path = "results.csv";
    }
    super.writeResults(path, results, header, fn);
  }

  public static void main(String[] args) {
    String path = ".";
    if (args.length == 1) {
      path = args[0];
    }
    System.out.println("Analyzing source tree rooted at " + path);
    final JumpInsnAnalyzer analyzer = new JumpInsnAnalyzer();
    Map<String, AnalyzerResult> results = analyzer.analyzePath(path);

    // Write results
    File resultDir = new File(analyzer.resultDir);
    if (!resultDir.exists()) {
      resultDir.mkdirs();
    }
    if (!resultDir.isDirectory()) {
      throw new RuntimeException(
          "Cannot write to " + resultDir.getAbsolutePath() + ". Is not a directory");
    }

    Path writePath = Paths.get(resultDir.getPath(),
        "jumpinsn-analysis" + path.replace('/', '_')
            .replace(".", "") + "-results.csv");
    System.out.println("Writing results to " + writePath.toString());
    analyzer.writeResults(writePath.toString(), results);
  }

  protected Map<String, AnalyzerResult> analyzeClass(ClassIdentity cid, MethodFilter filter,
      BiConsumer<AnalyzerResult, String> action) {

    pipe.flow(cid);
    ClassNode cn = cid.classNode;
    final Map<String, AnalyzerResult> results= analyzeClass(cn, filter, action);

    for (Object o : cid.methods) {
      final MethodIdentity mid = (MethodIdentity) o;
      final MethodNode mn = mid.method;
      if ((mn.access & (ACC_ABSTRACT | ACC_INTERFACE)) != 0) {
        continue;
      }

      if (!filter.keep(mn, cn)) {
        continue;
      }
      final String key = getName(cn, mn);
      JumpInsnResult jir = (JumpInsnResult)results.get(key);
      if (jir != null) {
        jir.basicBlocks = mid.cfg.getAllNodes().size();
      }
    }
    return results;
  }
}
