package edu.umass.medusa.metrics.applicability;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.metrics.Analyzer;
import edu.umass.medusa.metrics.AnalyzerResult;
import edu.umass.medusa.metrics.filter.method.*;
import edu.umass.medusa.pipeline.ASMPipeline;
import edu.umass.medusa.util.bytecode.BytecodeUtil;
import edu.umass.medusa.util.bytecode.Implemented;
import edu.umass.medusa.util.bytecode.BytecodeSet;
import edu.umass.medusa.util.bytecode.MutableBytecodeSet;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.AbstractInsnNode;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiFunction;

/**
 * Given a CFG, tell if we can reason about it
 */
public class ApplicabilityAnalyzer extends Analyzer {

  // Flags
  public static final int UNIMPLEMENTED_INSTR = 1;
  public static final int LOOPS = 2;
  public static final int METHOD_CALL = 4;
  public static final int MAX_CALL_DEPTH_EXCEEDED = 8;
  public static final int UNKNOWN_CALL_DEPTH = 16;
  public static final int MIN_JUMP_NOT_MET = 32;

  public Set<String> applicable = new HashSet<>();
  Set<MutableBytecodeSet> botSets = new HashSet<>();
  public int[] instructionCount = new int[256];
  public int analyzed = 0;

  public int minJumpInst = 0;

  // Fields Set By Arguments
  protected MethodFilter filter;
  protected String path = "";

  // For displaying
  protected boolean showBotSets = false;
  protected boolean showApplicable = false;
  protected boolean showSummary = true;
  protected boolean showUnimplemented = false;

  /**
   * -1: do not analyze 0: do not analyze, but also do not use getters and setters heuristic 1:
   * inline calls to methods of depth less than or equal to zero 2: inline calls to methods of depth
   * less than or equal to one ... n: inline calls to methods of depth less than or equal to n - 1
   */
  protected int maxCallDepth = -1;

  /**
   * Which bytecodes should we pretend are implemented?
   */
  protected BytecodeSet pretendImplemented = new BytecodeSet();

  /**
   * Should we assume that getters and setters can be reasoned about?
   */
  protected boolean includeGettersAndSetters = false;

  /**
   * Where to write the results to?
   */
  protected String resultDir = null;

  // Fields for method inlining analysis.
  Map<String, Set<String>> caller2callee = new HashMap<>();
  Map<String, Set<String>> callee2caller = new HashMap<>();
  Map<String, ApplicabilityResult> resultsMap = new HashMap<>();

  /**
   * This stores the greatest known call depth so far. Defaults to -1.
   */
  Map<String, Integer> callDepths = new HashMap<>();
  Deque<String> callDepthKnown = new ArrayDeque<>();

  /**
   * Analyze methods on path passing through {@code filter} as if instructions in {@code
   * implemented} were implemented.
   */
  public Map<String, AnalyzerResult> analyze() {
    final Map<String, ClassIdentity> cids = collectClassfiles(path);
    if (preProcess) {
      (new ASMPipeline(new HashSet<>(cids.values()))).flow();
    }

    final Map<String, AnalyzerResult> results = analyzeClasses(cids, filter,
        (result, name) -> {
          if (((ApplicabilityResult) result).flag == 0) {
            applicable.add(name);
          }
        });
    propagateCallDepth();
    return results;
  }

  /**
   * Analyze a MethodNode
   */
  public ApplicabilityResult analyzeMethod(MethodNode method, ClassNode cn) {
    boolean trackCallDepth = maxCallDepth > -1;
    ApplicabilityResult result = new ApplicabilityResult();
    final String caller = getName(cn, method);
    resultsMap.put(caller, result);
    int jumps = 0;

    final ListIterator<AbstractInsnNode> iterator = method.instructions.iterator();
    while (iterator.hasNext()) {
      final AbstractInsnNode insn = iterator.next();
      final int opcode = insn.getOpcode();

      if (opcode < 0) {
        continue;
      }

      result.instructions.add(opcode);
      if (BytecodeUtil.isJumpInstr(opcode)) {
        ++jumps;
      }

      if (!isImplemented(opcode) && !pretendImplemented.contains(opcode)) {
        if (insn instanceof MethodInsnNode) {
          if (trackCallDepth) {
            MethodInsnNode methInsn = (MethodInsnNode) insn;
            String callee = getName(methInsn);
            addCallerCallee(caller, callee);
          } else if (!includeGettersAndSetters || !isGetterOrSetter(insn)) {
            result.unimplemented.add(opcode);
          }
        } else {
          result.unimplemented.add(opcode);
        }
      }
    }

    Set<String> callees = getCallees(caller);

    /* If there are _any_ unimplemented instructions, set a flag and continue.
     * Otherwise, if there is an unknown call depth, set an appropriate flag and
     * continue. Otherwise, if there are no other callers, update callDepth and
     * callDepthKnown
     */

    if (jumps < minJumpInst) {
      result.flag |= MIN_JUMP_NOT_MET;
    }
    if (!result.unimplemented.isEmpty()) {
      result.flag |= UNIMPLEMENTED_INSTR;
    } else if (callees != null && !callees.isEmpty()) {
      result.flag |= UNKNOWN_CALL_DEPTH;
    } else {
      callDepthKnown.add(caller);
      callDepths.put(caller, 0);
    }

    for (int opcode : result.unimplemented) {
      ++instructionCount[opcode];
    }

    analyzed += 1;
    return result;
  }

  /**
   * Determine if an instruction is implemented (or if we are pretending it is implemented).
   */
  private boolean isImplemented(int i) {
    return Implemented.isImplemented(i) || pretendImplemented.contains(i);
  }

  public boolean isGetter(AbstractInsnNode insn) {
    switch (insn.getOpcode()) {
      case Opcodes.INVOKESPECIAL:
      case Opcodes.INVOKESTATIC:
      case Opcodes.INVOKEVIRTUAL:
        String name = ((MethodInsnNode) insn).name;
        return name.startsWith("get")
            && name.length() > 3
            && Character.isUpperCase(name.charAt(3));
      default:
        return false;
    }
  }

  public boolean isSetter(AbstractInsnNode insn) {
    switch (insn.getOpcode()) {
      case Opcodes.INVOKESPECIAL:
      case Opcodes.INVOKESTATIC:
      case Opcodes.INVOKEVIRTUAL:
      case Opcodes.INVOKEINTERFACE:
        String name = ((MethodInsnNode) insn).name;
        return name.startsWith("set")
            && name.length() > 3
            && Character.isUpperCase(name.charAt(3));
      default:
        if (insn instanceof MethodInsnNode) {
          System.out.println("MethodInsnNode:" + insn);
        }
        return false;
    }
  }

  public boolean isGetterOrSetter(AbstractInsnNode insn) {
    return isGetter(insn) || isSetter(insn);
  }


  /**
   * Write results to a location.
   */
  void writeResults(String path, Map<String, AnalyzerResult> results) {
    String header = "file, hasLoops, flag, unimplemented, all instructions\n";

    BiFunction<String, AnalyzerResult, String> fn = (key, aresult) -> {
      ApplicabilityResult result = (ApplicabilityResult) aresult;
      return key + "," + result.loops + "," + result.flag + ","
          + result.unimplemented.toString() + ","
          + result.instructions.toString() + "\n";
    };

    if (path == null || path.equals("")) {
      path = "results.csv";
    }
    super.writeResults(path, results, header, fn);
  }

  /**
   * Print usage directions with an additional message as a header
   */
  private static void usage(String message) {
    System.out.println(message);
    usage();
  }

  /**
   * Print usage directions
   */
  private static void usage() {
    StringBuilder sb = new StringBuilder();
    sb.append(
        "usage: java ApplicabilityAnalyzer [--path PATH]\n" +
            "                                  [--filter (all|static|local-static)]\n" +
            "                                  [--instructions instrs]\n" +
            "                                  [--no-preproc]\n" +
            "                                  [--show-applicable]\n" +
            "                                  [--show-unimplemented]\n" +
            "                                  [--show-botsets]\n" +
            "                                  [--show-all]\n" +
            "                                  [--hide-summary]\n" +
            "                                  [--results RESULT-DIR]\n\n"
    );
    sb.append(
        "  --path: Path to traverse to collect classes to analyze\n"
    );
    sb.append(
        "  --filter: determines which methods to be considered. Defaults to all. This may\n" +
            "            be applied multiple times, the effect being a conjunction of conditions.\n\n"
            +
            "            Filters include:\n" +
            "            - all: This is the default and does not filter anything\n" +
            "            - local: Filter out all methods that make non-local calls\n" +
            "            - static: Filter out non-static methods\n" +
            "            - local-static: Equivalent to applying options `--filter local --filter static\n\n"
    );
    sb.append(
        "  --instructions: Give a list of instructions that we should pretend are\n" +
            "    implemented. This should be comma-separated with no spaces and may\n" +
            "    include ranges: `--instructions 0x01,0x19,0xb0..0xba,0xbf`\n"
    );
    sb.append(
        "  --no-preproc: Do not preprocesses the ASM of the analyzed methods.\n"
    );
    sb.append(
        "  --show-applicable: Show all methods that Medusa may be applied to\n"
    );
    sb.append(
        "  --show-unimplemented: Show all encountered unimplemented instructions and their stats\n"
    );
    sb.append(
        "  --show-botsets: Show the calculated botsets. These are minimal sets of\n" +
            "        instructions that will increase applicability\n"
    );
    sb.append(
        "  --show-all: Show botsets, unimplemented instructions, summary, and applicable\n" +
            "        methods\n"
    );
    sb.append(
        "  --hide-summary: Hide the stats summary that is given at the end of the analysis\n"
    );
    sb.append(
        "  --results: Where to write results. Defaults to 'out/analysis'\n"
    );
    System.out.println(sb.toString());
    System.exit(1);
  }

  /**
   * Parse and apply arguments.
   */
  private static void applyArgs(String[] args, ApplicabilityAnalyzer analyzer) {
    MethodFilter filter = new TrueFilter();
    BytecodeSet pretendImplemented = new BytecodeSet();
    String path = ".";

    for (int i = 0; i < args.length; ++i) {
      String arg = args[i];
      switch (arg) {
        case "--help":
          usage("help");
        case "--filter":
          ++i;
          if (i >= args.length) {
            usage();
          }

          String filterString = args[i];

          switch (filterString) {
            case "true":
              continue;
            case "local-static":
              filter = new Conjunction(filter,
                  new Conjunction(new LocalCallFilter(), new StaticMethodFilter()));
              break;
            case "static":
              filter = new Conjunction(filter, new StaticMethodFilter());
              break;
            case "local":
              filter = new Conjunction(filter, new LocalCallFilter());
              break;
            default:
              System.err.println("Unrecognized filter \"" + filterString + "\", using default");
          }
          break;

        case "--min-jumps":
          ++i;
          if (i >= args.length) {
            usage("Parse error: --min-jumps had no instructions following it");
          }
          try {
            analyzer.minJumpInst = Integer.parseInt(args[i]);
          } catch (Exception e) {
            throw new RuntimeException("Couldn't parse --min-jumps arg as int: " + args[i]);
          }
          break;
        case "--instructions":
          ++i;
          if (i >= args.length) {
            usage("Parse error: --instructions had no instructions following it");
          }
          String instructionsString = args[i];
          final String[] split = instructionsString.split(",");
          try {
            for (String s : split) {
              if (s.contains("..")) {
                String[] bounds = s.split("\\.\\.");
                if (!(bounds.length == 2)) {
                  usage("Bad instruction range found: " + s + ". Expect format \"lo..hi\"");
                }
                try {
                  Integer lo = Integer.decode(bounds[0]);
                  Integer hi = Integer.decode(bounds[1]);
                  if (lo > hi) {
                    throw new IllegalArgumentException(
                        "In instruction range " + s + " found lo > hi");
                  }
                  pretendImplemented = pretendImplemented.union(BytecodeSet.range(lo, hi));
                } catch (NumberFormatException e) {
                  e.printStackTrace();
                  usage("Bad instruction found");
                } catch (IllegalArgumentException e) {
                  e.printStackTrace();
                  usage("Bad instruction range found");
                }
              } else {
                try {
                  Integer n = Integer.decode(s);
                  pretendImplemented = pretendImplemented.union(new BytecodeSet(n));
                } catch (NumberFormatException e) {
                  e.printStackTrace();
                  usage("Bad instruction found");
                }
              }
            }
          } catch (NumberFormatException e) {
            e.printStackTrace();
            usage("Encountered a poorly formatted instruction in \"" + instructionsString + "\"");
          }
          break;

        case "--path":
          ++i;
          if (i >= args.length) {
            usage("Parse error: --path has no path following");
          }
          path = args[i];
          break;

        case "--no-preproc":
          analyzer.preProcess = false;
          break;

        case "--results":
          ++i;
          if (i >= args.length) {
            usage("Parse error: --results has no result directory following");
          }
          analyzer.resultDir = args[i];
          break;

        case "--getters":
        case "--setters":
          analyzer.includeGettersAndSetters = true;
          break;

        case "--show-botsets":
          analyzer.showBotSets = true;
          break;

        case "--show-applicable":
          analyzer.showApplicable = true;
          break;

        case "--show-unimplemented":
          analyzer.showUnimplemented = true;
          break;

        case "--hide-summary":
          analyzer.showSummary = false;
          break;

        case "--show-all":
          analyzer.showUnimplemented = analyzer.showSummary = analyzer.showBotSets = analyzer.showApplicable = true;
          break;

        case "--max-call-depth":
          ++i;
          if (i >= args.length) {
            usage("Parse error: --max-call-depth has no argument following it");
          }
          analyzer.maxCallDepth = Integer.parseInt(args[i]);
          break;

        default:
          usage("Unrecognized option: " + arg);
      }
    }
    analyzer.path = path;
    analyzer.filter = filter;
    analyzer.pretendImplemented = pretendImplemented;
  }

  public static void main(String[] args) {

    final ApplicabilityAnalyzer analyzer = new ApplicabilityAnalyzer();
    applyArgs(args, analyzer);

    Map<String, AnalyzerResult> results = analyzer.analyze();
    final String path = analyzer.path;

    if (analyzer.resultDir != null) {
      File resultDir = new File(analyzer.resultDir);
      if (!resultDir.exists()) {
        resultDir.mkdirs();
      }
      if (!resultDir.isDirectory()) {
        throw new RuntimeException(
            "Cannot write to " + resultDir.getAbsolutePath() + ". Is not a directory");
      }

      Path writePath = Paths.get(resultDir.toString(), "analysis" + path.replace('/', '_')
          .replace(".", "") + "-results.csv");
      System.out.println("Writing results to " + writePath.toString());
      analyzer.writeResults(writePath.toString(), results);
    }

    analyzer.printResults();
  }

  public void printResults() {
    if (showBotSets) {
      calculateBotSets();
      System.out.println("BotSets:");
      int j = 0;
      for (MutableBytecodeSet bs : this.botSets) {
        System.out.println("    (" + ++j + ") " + bs.toString());
      }
    }

    if (showUnimplemented) {
      System.out.println("Unimplemented Count:");
      System.out.println("    opcode   count   percentage");
      for (int i = 0; i < 256; ++i) {
        if (instructionCount[i] > 0) {
          System.out.printf("        %2x %6d     %6.2f%%\n", i, instructionCount[i],
              100 * (double) instructionCount[i] / (double) analyzed);
        }
      }
    }

    if (showApplicable) {
      System.out.println("\nApplicable methods\n==================\n");
      int j = 0;
      int width = (int) Math.floor(Math.log10(applicable.size())) + 1;
      for (String name : applicable) {
        System.out.printf("%" + width + "d  %s\n", ++j, name);
      }
    }

    if (showSummary) {
      System.out.println("\nSummary\n=======");
      System.out.printf("Classfiles Analyzed: %d\n", analyzed);
      System.out.printf("Total Applicable:    %d\n", applicable.size());
      System.out.printf("Percentage:          %f\n",
          100 * (double) applicable.size() / (double) analyzed);
      System.out.println("Longest prefix: " + longestPathPrefix);
    }
  }

  private void calculateBotSets() {
    // Update botSets
    for (AnalyzerResult e : results.values()) {
      ApplicabilityResult r = (ApplicabilityResult) e;
      MutableBytecodeSet unimplemented = r.unimplemented;
      if (unimplemented.isEmpty()) {
        continue;
      }
      Set<MutableBytecodeSet> toRemove = new HashSet<>();
      boolean isSubset = false;
      boolean isSupset = false;
      for (MutableBytecodeSet botSet : botSets) {
        if (unimplemented.isSubset(botSet)) {
          toRemove.add(botSet);
          isSubset = true;
        }
        if (unimplemented.isSuperset(botSet)) {
          isSupset = true;
          break;
        }
      }
      if (isSubset || (!isSupset)) {
        botSets.removeAll(toRemove);
        botSets.add(unimplemented);
      }
    }
  }

  /* Call Depth Analysis */

  private void propagateCallDepth() {
    while (!callDepthKnown.isEmpty()) {
      // Invariant: callDepthKnown ==> method is applicable
      final String callee = callDepthKnown.poll();
      final Set<String> callers = getCallers(callee);
      final Integer calleeDepth = callDepths.get(callee);
      // Now we mutate state. We remove callee from each caller
      for (String caller : callers) {
        getCallees(caller).remove(callee);
        final Integer depth = Math.max(calleeDepth + 1, getCallDepth(caller));
        setCallDepth(caller, depth);
      }
    }
  }

  private String getName(MethodInsnNode mn) {
    return mn.owner + ":" + mn.name + (mn.desc == null ? "()" : mn.desc);
  }

  private void addCallerCallee(String caller, String callee) {
    caller2callee.computeIfAbsent(caller, key -> new HashSet<>()).add(callee);
    callee2caller.computeIfAbsent(callee, key -> new HashSet<>()).add(caller);
  }

  private Set<String> getCallees(String caller) {
    final Set<String> callees = caller2callee.get(caller);
    return callees == null ? new HashSet<>() : callees;
  }

  private Set<String> getCallers(String callee) {
    final Set<String> callers = callee2caller.get(callee);
    return callers == null ? new HashSet<>() : callers;
  }

  private void setCallDepth(String caller, int depth) {
    callDepths.put(caller, depth);

    if (depth > maxCallDepth) {
      propagateMaxCallDepthExceeded(caller);
    } else {
      // Now, check if this is the last time this will be set
      final Set<String> callees = getCallees(caller);
      if (callees.isEmpty()) {
        final ApplicabilityResult r = resultsMap.get(caller);
        assert r != null; // This will probably trigger...
        r.flag &= ~UNKNOWN_CALL_DEPTH;
        if (r.flag == 0) {
          callDepthKnown.add(caller);
          applicable.add(caller);
        }
      }
    }
  }

  private Integer getCallDepth(String method) {
    Integer depth = callDepths.get(method);
    return depth == null ? -1 : depth;
  }

  /**
   * Recursively go to all callers and report that the max call depth has been exceeded.
   */
  private void propagateMaxCallDepthExceeded(String method) {
    for (String caller : getCallers(method)) {
      final ApplicabilityResult r = resultsMap.get(caller);
      if (r != null && (r.flag & MAX_CALL_DEPTH_EXCEEDED) == 0) {
        r.flag |= MAX_CALL_DEPTH_EXCEEDED;
        r.flag &= ~UNKNOWN_CALL_DEPTH;
        propagateMaxCallDepthExceeded(caller);
      }
    }
  }
}
