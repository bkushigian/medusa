package edu.umass.medusa.metrics.applicability;

import edu.umass.medusa.metrics.AnalyzerResult;
import edu.umass.medusa.util.bytecode.MutableBytecodeSet;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

import java.util.HashSet;
import java.util.Set;

/**
 * A convenience location to stash results. Treat as a struct.
 */
public class ApplicabilityResult extends AnalyzerResult {

  public Boolean loops = false;
  public MutableBytecodeSet instructions = new MutableBytecodeSet();
  public MutableBytecodeSet unimplemented = new MutableBytecodeSet();
  public Integer flag = 0;
  public Integer inlineDepth = -1;
  public String name;
}
