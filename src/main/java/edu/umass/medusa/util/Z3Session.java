package edu.umass.medusa.util;

import com.microsoft.z3.*;

import java.util.Arrays;

/**
 * {@code Z3Session} wraps a Z3 {@code Context} and a Z3 {@code Solver} object. Also includes a
 * {@code NameGen} to generate names as needed.
 *
 * This is a simple struct to streamline method signatures and to keep the {@code Context} keyword
 * out of namespaces. Therefore all fields are declared as {@code public final}.
 */
public class Z3Session {

  public final Context ctx;
  public final Solver z3;
  public final NameGen nameGen;

  ///////////////////////////////////////////////////////////////////////////
  // Z3 Sorts and Constants                                                //
  ///////////////////////////////////////////////////////////////////////////

  public final BitVecSort javaIntSort;
  public final BitVecSort javaLongSort;
  public final FPSort javaFloatSort;
  public final FPSort javaDoubleSort;
  public final ListSort opstackSort;
  public final FPRMExpr roundToZero;
  public final FPRMExpr roundingMode;
  private final FPRMNum roundToAway;

  public final Z3Constants constants;

  /**
   * Set up a new {@code Z3Session} with a new {@code Context} and a new {@code Solver}
   */
  public Z3Session() {
    this(new Context());
  }

  /**
   * Wrap an existing {@code Context} object and create a new associated {@code Solver} object from
   * it.
   *
   * @param ctx the {@code Context} to build a new {@code Solver} from.
   */
  public Z3Session(Context ctx) {
    this(ctx, ctx.mkSolver());
  }

  /**
   * Wrap an existing {@code Context} object and {@code Solver} object.
   *
   * @param ctx {@code Context} object to wrap
   * @param z3 {@code Solver} object to wrap; this should have been created by an invocation of
   * {@code ctx.mkSolver()}, where {@code ctx} is the {@code Context} being passed in as an
   * argument.
   */
  public Z3Session(Context ctx, Solver z3) {
    this(ctx, z3, new NameGen());
  }

  /**
   * Wrap existing {@code Context}, {@code Solver}, and {@code NameGen} objects.
   *
   * @param ctx {@code Context} object to wrap
   * @param z3 {@code Solver} object to wrap; this should have been created by an invocation of
   * {@code ctx.mkSolver()}, where {@code ctx} is the {@code Context} being passed in as an
   * argument.
   * @param nameGen {@code NameGen} to generate names
   */
  public Z3Session(Context ctx, Solver z3, NameGen nameGen) {
    this.ctx = ctx;
    this.z3 = z3;
    this.nameGen = nameGen;
    javaIntSort = ctx.mkBitVecSort(32);
    javaLongSort = ctx.mkBitVecSort(64);
    javaFloatSort = ctx.mkFPSort32();
    javaDoubleSort = ctx.mkFPSort64();
    opstackSort = ctx.mkListSort("opstack-sort", javaIntSort);
    roundToZero = ctx.mkFPRoundTowardZero();
    roundToAway = ctx.mkFPRoundNearestTiesToAway();
    roundingMode = roundToAway;
    constants = new Z3Constants();
  }

  ///////////////////////////////////////////////////////////////////////////
  // Utility Functions for Z3                                              //
  //                                                                       //
  // The following exposes an interface to the Z3 Context that takes care  //
  // of some of the basics such as handling bit vector sizes, etc.         //
  ///////////////////////////////////////////////////////////////////////////


  public BoolExpr mkBoolExpr(String name) {
    return (BoolExpr) ctx.mkConst(name, ctx.mkBoolSort());
  }

  public BitVecExpr mkJavaInt(String name) {
    return (BitVecExpr) ctx.mkConst(name, javaIntSort);
  }

  public BitVecExpr mkJavaInt(int value) {
    return ctx.mkBV(value, 32);
  }

  public BitVecExpr mkJavaLong(Long value) {
    return ctx.mkBV(value, 64);
  }

  public BitVecExpr mkJavaLong(String name) {
    return (BitVecExpr) ctx.mkConst(name, javaLongSort);
  }

  public FPNum mkJavaFloat(float value) {
    return ctx.mkFP(value, javaFloatSort);
  }

  public FPExpr mkJavaFloat(String name) {
    return (FPExpr) ctx.mkConst(name, javaFloatSort);
  }

  public Expr mkStack(String name) {
    return ctx.mkConst(nameGen.get(name), opstackSort);
  }

  public Expr mkStack() {
    return mkStack("stack");
  }

  public BoolExpr mkNot(BoolExpr b) {
    return ctx.mkNot(b);
  }

  public BoolExpr mkEq(Expr lhs, Expr rhs) {
    return ctx.mkEq(lhs, rhs);
  }

  public BoolExpr mkAnd(BoolExpr... es) {
    return ctx.mkAnd(es);
  }

  public BoolExpr mkOr(BoolExpr... es) {
    return ctx.mkOr(es);
  }

  public BoolExpr mkImplies(BoolExpr premise, BoolExpr conclusion) {
    return ctx.mkImplies(premise, conclusion);
  }

  public FuncDecl opstackTailDecl() {
    return opstackSort.getTailDecl();
  }

  public FuncDecl opstackHeadDecl() {
    return opstackSort.getHeadDecl();
  }

  ///////////////////////////////////////////////////////////////////////////
  // Forwarding the Z3 API                                                 //
  ///////////////////////////////////////////////////////////////////////////

  public void addConstraints(BoolExpr... constraints) {
    z3.add(constraints);
  }

  public BoolExpr parseSMTLIB2String(String s, Symbol[] sortNames, Sort[] sorts,
      Symbol[] declNames, FuncDecl[] funcDecls) {
    return ctx.parseSMTLIB2String(s, sortNames, sorts, declNames, funcDecls);
  }

  public void push() {
    z3.push();
  }

  public void push(BoolExpr... constraints) {
    z3.push();
    addConstraints(constraints);
  }

  public void pop() {
    z3.pop();
  }

  public Status check() {
    return z3.check();
  }

  public Status check(BoolExpr... constraints) {
    push(constraints);
    Status result = check();
    pop();
    return result;
  }

  public boolean isSat() {
    return check() == Status.SATISFIABLE;
  }

  public boolean isSat(BoolExpr... constraints) {
    return check(constraints) == Status.SATISFIABLE;
  }

  public boolean isUnsat() {
    return check() == Status.UNSATISFIABLE;
  }

  public boolean isUnsat(BoolExpr... constraints) {
    return check(constraints) == Status.UNSATISFIABLE;
  }

  public boolean isUnknown() {
    return check() == Status.UNKNOWN;
  }

  public boolean isUnknown(BoolExpr... constraints) {
    return check(constraints) == Status.UNKNOWN;
  }

  ///////////////////////////////////////////////////////////////////////////
  // Forwarding the Context API                                            //
  // NOTE: Binary operators have their lhs and rhs order switched. This is //
  // to allow for code such as `z3.sub(state.pop(), state.pop())` which    //
  // evaluates a stack of [bot top) to `top - bot`; in particular, the lhs //
  // is popped second and the rhs is popped first.                         //
  ///////////////////////////////////////////////////////////////////////////

  // Comparison Operators

  /**
   * Create a bit vec less-than-or-equal bool expr
   *
   * @param rhs right hand side
   * @param lhs left hand side
   * @return rhs <= lhs
   */
  public BoolExpr le(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSLE(lhs, rhs);
  }

  /**
   * Create a bit vec greater-than-or-equal bool expr
   *
   * @param rhs right hand side
   * @param lhs left hand side
   * @return rhs >= lhs
   */
  public BoolExpr ge(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSGE(lhs, rhs);
  }

  /**
   * Create a bit vec less-than bool expr
   *
   * @param rhs right hand side
   * @param lhs left hand side
   * @return rhs < lhs
   */
  public BoolExpr lt(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSLT(lhs, rhs);
  }

  /**
   * Create a bit vec greater-than bool expr
   *
   * @param rhs right hand side
   * @param lhs left hand side
   * @return rhs > lhs
   */
  public BoolExpr gt(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSGT(lhs, rhs);
  }

  // Arithemtic


  /**
   * Create a bit vec addition expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs + rhs
   */
  public BitVecExpr add(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVAdd(lhs, rhs);
  }

  /**
   * Create a bit vec subtraction expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs - rhs
   */
  public BitVecExpr subtract(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSub(lhs, rhs);
  }

  /**
   * Create a bit vec multiplication expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs * rhs
   */
  public BitVecExpr multiply(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVMul(lhs, rhs);
  }

  /**
   * Create a bit vec division expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs / rhs
   */
  public BitVecExpr divide(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSDiv(lhs, rhs);
  }

  /**
   * Create a bit vec signed-division expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs / rhs
   */
  public BitVecExpr signedDivide(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSDiv(lhs, rhs);
  }

  /**
   * Create a bit vec unsigned-division expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return (unsigned)lhs /  (unsigned)rhs
   */
  public BitVecExpr unsignedDivide(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVUDiv(lhs, rhs);
  }

  /**
   * Create a bit vec integer and
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs & rhs
   */
  public BitVecExpr iand(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVAND(lhs, rhs);
  }

  /**
   * Create a bit vec integer or
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs | rhs
   */
  public BitVecExpr ior(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVOR(lhs, rhs);
  }

  /**
   * Create a bit vec integer xor
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs ^ rhs
   */
  public BitVecExpr ixor(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVXOR(lhs, rhs);
  }

  /**
   * Create a bit vec arithmetic shift right expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs >> rhs (arithmetic/signed)
   */
  public BitVecExpr shiftRight(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVASHR(lhs, rhs);
  }

  /**
   * Create a bit vec logical shift right expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs >> rhs (logical/unsigned)
   */
  public BitVecExpr unsignedShiftRight(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVLSHR(lhs, rhs);
  }

  /**
   * Create a bit vec shift left expr
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs << rhs (signed-extended)
   */
  public BitVecExpr shiftLeft(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSHL(lhs, rhs);
  }

  /**
   * Create a bit vec signed remainder
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs % rhs (signed)
   */
  public BitVecExpr signedRem(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVSRem(lhs, rhs);
  }

  /**
   * Create a bit vec unsigned remainder
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs % rhs (unsigned)
   */
  public BitVecExpr unsignedRem(BitVecExpr rhs, BitVecExpr lhs) {
    return ctx.mkBVURem(lhs, rhs);
  }

  /**
   * Create a bit vec signed remainder (equivalent to signedRem)
   *
   * @param lhs left hand side
   * @param rhs right hand side
   * @return lhs % rhs (signed)
   */
  public BitVecExpr rem(BitVecExpr rhs, BitVecExpr lhs) {
    return signedRem(rhs, lhs);
  }

  /**
   * Create a bit vec negative
   *
   * @param bv bit vec to take a negative of
   * @return -bv
   */
  public BitVecExpr negative(BitVecExpr bv) {
    return ctx.mkBVMul(constants.ICONST_M1, bv);
  }

  /**
   * Increment a bit vec value
   *
   * @param bv bit vec to increment
   * @return ++bv
   */
  public BitVecExpr inc(BitVecExpr bv) {
    return ctx.mkBVAdd(constants.ICONST_1, bv);
  }

  public BoolExpr mkTrue() {
    return ctx.mkTrue();
  }

  public BoolExpr mkFalse() {
    return ctx.mkFalse();
  }

  public Model getModel() {
    return z3.getModel();
  }

  public String getSortedModelString() {
    return getSortedModelString(z3.getModel());
  }

  /**
   * Get the sorted model string for easy reference
   *
   * @param model the model whose string representation we wish to show
   * @return The sorted model string
   */
  public static String getSortedModelString(Model model) {
    return getSortedModelString(model.toString());
  }

  public static String getSortedModelString(String modelString) {
    // This is an ugly hack :). And by ugly I mean beautiful. And by beautiful I mean
    // I'm deeply ashamed, but also very lazy. And by lazy I mean I didn't put any
    // pants on this morning. So we're back to beautiful.

    String[] split = modelString.split("\\r?\\n");
    String[] combined = new String[split.length]; // combine multiline strings
    int cptr = 0;
    int sptr = 0;
    while (sptr < split.length) {
      if (combined[cptr] == null) {
        combined[cptr] = split[sptr];
      } else if (split[sptr].contains("->")) {  // This is an assignment in the model
        cptr += 1;          // Start a new combined string
        combined[cptr] = split[sptr];
      } else {
        combined[cptr] = combined[cptr] + "\n" + split[sptr];
      }
      sptr += 1;
    }

    String[] toSort = new String[cptr + 1];
    for (int i = 0; i < cptr + 1; ++i) {
      if (combined[i] == null) {
        System.out.println("Found null at index " + i);
      }
      toSort[i] = combined[i];

    }
    Arrays.sort(toSort);
    return String.join("\n", toSort);
  }

  public Expr mkITE(BoolExpr cond, Expr then, Expr elze) {
    return ctx.mkITE(cond, then, elze);
  }

  public class Z3Constants {

    public final BitVecNum ICONST_0 = ctx.mkBV(0, 32);

    // Positives
    public final BitVecNum ICONST_1 = ctx.mkBV(1, 32);
    public final BitVecNum ICONST_2 = ctx.mkBV(2, 32);
    public final BitVecNum ICONST_3 = ctx.mkBV(3, 32);
    public final BitVecNum ICONST_4 = ctx.mkBV(4, 32);
    public final BitVecNum ICONST_5 = ctx.mkBV(5, 32);
    public final BitVecNum ICONST_6 = ctx.mkBV(6, 32);
    public final BitVecNum ICONST_7 = ctx.mkBV(7, 32);
    public final BitVecNum ICONST_8 = ctx.mkBV(8, 32);
    public final BitVecNum ICONST_9 = ctx.mkBV(9, 32);
    public final BitVecNum ICONST_10 = ctx.mkBV(10, 32);
    public final BitVecNum ICONST_11 = ctx.mkBV(11, 32);
    public final BitVecNum ICONST_12 = ctx.mkBV(12, 32);
    public final BitVecNum ICONST_13 = ctx.mkBV(13, 32);
    public final BitVecNum ICONST_14 = ctx.mkBV(14, 32);
    public final BitVecNum ICONST_15 = ctx.mkBV(15, 32);
    public final BitVecNum ICONST_16 = ctx.mkBV(16, 32);
    public final BitVecNum ICONST_17 = ctx.mkBV(17, 32);
    public final BitVecNum ICONST_18 = ctx.mkBV(18, 32);
    public final BitVecNum ICONST_19 = ctx.mkBV(19, 32);
    public final BitVecNum ICONST_20 = ctx.mkBV(20, 32);
    public final BitVecNum ICONST_21 = ctx.mkBV(21, 32);
    public final BitVecNum ICONST_22 = ctx.mkBV(22, 32);
    public final BitVecNum ICONST_23 = ctx.mkBV(23, 32);
    public final BitVecNum ICONST_24 = ctx.mkBV(24, 32);
    public final BitVecNum ICONST_25 = ctx.mkBV(25, 32);
    public final BitVecNum ICONST_26 = ctx.mkBV(26, 32);
    public final BitVecNum ICONST_27 = ctx.mkBV(27, 32);
    public final BitVecNum ICONST_28 = ctx.mkBV(28, 32);
    public final BitVecNum ICONST_29 = ctx.mkBV(29, 32);
    public final BitVecNum ICONST_30 = ctx.mkBV(30, 32);
    public final BitVecNum ICONST_31 = ctx.mkBV(31, 32);
    public final BitVecNum ICONST_32 = ctx.mkBV(32, 32);

    public final BitVecNum ICONST_63 = ctx.mkBV(63, 32);
    public final BitVecNum ICONST_64 = ctx.mkBV(64, 32);

    public final BitVecNum ICONST_127 = ctx.mkBV(127, 32);
    public final BitVecNum ICONST_128 = ctx.mkBV(128, 32);

    public final BitVecNum ICONST_255 = ctx.mkBV(255, 32);
    public final BitVecNum ICONST_256 = ctx.mkBV(256, 32);

    public final BitVecNum ICONST_511 = ctx.mkBV(511, 32);
    public final BitVecNum ICONST_512 = ctx.mkBV(512, 32);

    public final BitVecNum ICONST_1023 = ctx.mkBV(1023, 32);
    public final BitVecNum ICONST_1024 = ctx.mkBV(1024, 32);

    public final BitVecNum ICONST_2047 = ctx.mkBV(2047, 32);
    public final BitVecNum ICONST_2048 = ctx.mkBV(2048, 32);

    public final BitVecNum ICONST_4095 = ctx.mkBV(4095, 32);
    public final BitVecNum ICONST_4096 = ctx.mkBV(4096, 32);

    public final BitVecNum ICONST_8191 = ctx.mkBV(8191, 32);
    public final BitVecNum ICONST_8192 = ctx.mkBV(8192, 32);

    public final BitVecNum ICONST_16383 = ctx.mkBV(16383, 32);
    public final BitVecNum ICONST_16384 = ctx.mkBV(16384, 32);

    public final BitVecNum ICONST_32767 = ctx.mkBV(32767, 32);
    public final BitVecNum ICONST_32768 = ctx.mkBV(32768, 32);

    public final BitVecNum ICONST_65535 = ctx.mkBV(65535, 32);
    public final BitVecNum ICONST_65536 = ctx.mkBV(65536, 32);

    // Negatives
    public final BitVecNum ICONST_M1 = ctx.mkBV(-1, 32);
    public final BitVecNum ICONST_M2 = ctx.mkBV(-2, 32);
    public final BitVecNum ICONST_M3 = ctx.mkBV(-3, 32);
    public final BitVecNum ICONST_M4 = ctx.mkBV(-4, 32);
    public final BitVecNum ICONST_M5 = ctx.mkBV(-5, 32);
    public final BitVecNum ICONST_M6 = ctx.mkBV(-6, 32);
    public final BitVecNum ICONST_M7 = ctx.mkBV(-7, 32);
    public final BitVecNum ICONST_M8 = ctx.mkBV(-8, 32);
    public final BitVecNum ICONST_M9 = ctx.mkBV(-9, 32);
    public final BitVecNum ICONST_M10 = ctx.mkBV(-10, 32);
    public final BitVecNum ICONST_M11 = ctx.mkBV(-11, 32);
    public final BitVecNum ICONST_M12 = ctx.mkBV(-12, 32);
    public final BitVecNum ICONST_M13 = ctx.mkBV(-13, 32);
    public final BitVecNum ICONST_M14 = ctx.mkBV(-14, 32);
    public final BitVecNum ICONST_M15 = ctx.mkBV(-15, 32);
    public final BitVecNum ICONST_M16 = ctx.mkBV(-16, 32);
    public final BitVecNum ICONST_M17 = ctx.mkBV(-17, 32);
    public final BitVecNum ICONST_M18 = ctx.mkBV(-18, 32);
    public final BitVecNum ICONST_M19 = ctx.mkBV(-19, 32);
    public final BitVecNum ICONST_M20 = ctx.mkBV(-20, 32);
    public final BitVecNum ICONST_M21 = ctx.mkBV(-21, 32);
    public final BitVecNum ICONST_M22 = ctx.mkBV(-22, 32);
    public final BitVecNum ICONST_M23 = ctx.mkBV(-23, 32);
    public final BitVecNum ICONST_M24 = ctx.mkBV(-24, 32);
    public final BitVecNum ICONST_M25 = ctx.mkBV(-25, 32);
    public final BitVecNum ICONST_M26 = ctx.mkBV(-26, 32);
    public final BitVecNum ICONST_M27 = ctx.mkBV(-27, 32);
    public final BitVecNum ICONST_M28 = ctx.mkBV(-28, 32);
    public final BitVecNum ICONST_M29 = ctx.mkBV(-29, 32);
    public final BitVecNum ICONST_M30 = ctx.mkBV(-30, 32);
    public final BitVecNum ICONST_M31 = ctx.mkBV(-31, 32);
    public final BitVecNum ICONST_M32 = ctx.mkBV(-32, 32);

    public final BitVecNum LCONST_0 = ctx.mkBV(0, 64);
    public final BitVecNum LCONST_1 = ctx.mkBV(1, 64);
    public final BitVecNum LCONST_2 = ctx.mkBV(2, 64);
    public final BitVecNum LCONST_3 = ctx.mkBV(3, 64);
    public final BitVecNum LCONST_4 = ctx.mkBV(4, 64);
    public final BitVecNum LCONST_5 = ctx.mkBV(5, 64);
    public final BitVecNum LCONST_6 = ctx.mkBV(6, 64);
    public final BitVecNum LCONST_7 = ctx.mkBV(7, 64);
    public final BitVecNum LCONST_8 = ctx.mkBV(8, 64);
    public final BitVecNum LCONST_9 = ctx.mkBV(9, 64);
    public final BitVecNum LCONST_10 = ctx.mkBV(10, 64);
    public final BitVecNum LCONST_11 = ctx.mkBV(11, 64);
    public final BitVecNum LCONST_12 = ctx.mkBV(12, 64);
    public final BitVecNum LCONST_13 = ctx.mkBV(13, 64);
    public final BitVecNum LCONST_14 = ctx.mkBV(14, 64);
    public final BitVecNum LCONST_15 = ctx.mkBV(15, 64);
    public final BitVecNum LCONST_16 = ctx.mkBV(16, 64);
    public final BitVecNum LCONST_17 = ctx.mkBV(17, 64);
    public final BitVecNum LCONST_18 = ctx.mkBV(18, 64);
    public final BitVecNum LCONST_19 = ctx.mkBV(19, 64);
    public final BitVecNum LCONST_20 = ctx.mkBV(20, 64);
    public final BitVecNum LCONST_21 = ctx.mkBV(21, 64);
    public final BitVecNum LCONST_22 = ctx.mkBV(22, 64);
    public final BitVecNum LCONST_23 = ctx.mkBV(23, 64);
    public final BitVecNum LCONST_24 = ctx.mkBV(24, 64);
    public final BitVecNum LCONST_25 = ctx.mkBV(25, 64);
    public final BitVecNum LCONST_26 = ctx.mkBV(26, 64);
    public final BitVecNum LCONST_27 = ctx.mkBV(27, 64);
    public final BitVecNum LCONST_28 = ctx.mkBV(28, 64);
    public final BitVecNum LCONST_29 = ctx.mkBV(29, 64);
    public final BitVecNum LCONST_30 = ctx.mkBV(30, 64);
    public final BitVecNum LCONST_31 = ctx.mkBV(31, 64);
    public final BitVecNum LCONST_32 = ctx.mkBV(32, 64);

    // Negatives
    public final BitVecNum LCONST_M1 = ctx.mkBV(-1, 64);
    public final BitVecNum LCONST_M2 = ctx.mkBV(-2, 64);
    public final BitVecNum LCONST_M3 = ctx.mkBV(-3, 64);
    public final BitVecNum LCONST_M4 = ctx.mkBV(-4, 64);
    public final BitVecNum LCONST_M5 = ctx.mkBV(-5, 64);
    public final BitVecNum LCONST_M6 = ctx.mkBV(-6, 64);
    public final BitVecNum LCONST_M7 = ctx.mkBV(-7, 64);
    public final BitVecNum LCONST_M8 = ctx.mkBV(-8, 64);
    public final BitVecNum LCONST_M9 = ctx.mkBV(-9, 64);
    public final BitVecNum LCONST_M10 = ctx.mkBV(-10, 64);
    public final BitVecNum LCONST_M11 = ctx.mkBV(-11, 64);
    public final BitVecNum LCONST_M12 = ctx.mkBV(-12, 64);
    public final BitVecNum LCONST_M13 = ctx.mkBV(-13, 64);
    public final BitVecNum LCONST_M14 = ctx.mkBV(-14, 64);
    public final BitVecNum LCONST_M15 = ctx.mkBV(-15, 64);
    public final BitVecNum LCONST_M16 = ctx.mkBV(-16, 64);
    public final BitVecNum LCONST_M17 = ctx.mkBV(-17, 64);
    public final BitVecNum LCONST_M18 = ctx.mkBV(-18, 64);
    public final BitVecNum LCONST_M19 = ctx.mkBV(-19, 64);
    public final BitVecNum LCONST_M20 = ctx.mkBV(-20, 64);
    public final BitVecNum LCONST_M21 = ctx.mkBV(-21, 64);
    public final BitVecNum LCONST_M22 = ctx.mkBV(-22, 64);
    public final BitVecNum LCONST_M23 = ctx.mkBV(-23, 64);
    public final BitVecNum LCONST_M24 = ctx.mkBV(-24, 64);
    public final BitVecNum LCONST_M25 = ctx.mkBV(-25, 64);
    public final BitVecNum LCONST_M26 = ctx.mkBV(-26, 64);
    public final BitVecNum LCONST_M27 = ctx.mkBV(-27, 64);
    public final BitVecNum LCONST_M28 = ctx.mkBV(-28, 64);
    public final BitVecNum LCONST_M29 = ctx.mkBV(-29, 64);
    public final BitVecNum LCONST_M30 = ctx.mkBV(-30, 64);
    public final BitVecNum LCONST_M31 = ctx.mkBV(-31, 64);
    public final BitVecNum LCONST_M32 = ctx.mkBV(-32, 64);


  }
}
