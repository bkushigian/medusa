package edu.umass.medusa.util;

import java.util.Iterator;

public class Enumerator implements Iterator<Integer> {

  private final int start;
  private final int step;
  private int current;

  public Enumerator() {
    this(0);
  }

  public Enumerator(int start) {
    this(start, 1);
  }

  public Enumerator(int start, int step) {
    this.start = start;
    this.step = step;
    current = start;
  }

  @Override
  public boolean hasNext() {
    return true;
  }

  @Override
  public Integer next() {
    Integer res = current;
    current += step;
    return res;
  }

  public void reset() {
    current = start;
  }
}
