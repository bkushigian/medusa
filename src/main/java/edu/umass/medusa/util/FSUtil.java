package edu.umass.medusa.util;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * A File System Utility class
 */
public class FSUtil {

  public static Set<File> collectWithExtension(File base, String ext) throws IOException {
    if (!base.exists()) {
      throw new IOException("Could not open file " + base + ": does not exist");
    }
    if (!ext.startsWith(".")) {
      ext = "." + ext;
    }

    Set<File> result = new HashSet<>();

    if (base.isDirectory()) {
      File[] children = base.listFiles();
      if (children == null) {
        return result;
      }
      for (File child : children) {
        result.addAll(collectWithExtension(child, ext));
      }
    } else {
      if (base.getName().endsWith(ext)) {
        result.add(base);
      }
    }

    return result;
  }

  public static Set<File> collectClassfiles(File base) throws IOException {
    return collectWithExtension(base, ".class");
  }

  public static Set<File> collectClassfiles(File[] bases) throws IOException {
    HashSet<File> result = new HashSet<>();
    for (File f : bases) {
      result.addAll(collectClassfiles(f));
    }
    return result;
  }
}
