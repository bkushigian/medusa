package edu.umass.medusa.util;


public class Pair<X, Y> {

  public final X first;
  public final Y second;

  public Pair(X fst, Y snd) {
    first = fst;
    second = snd;
  }
}
