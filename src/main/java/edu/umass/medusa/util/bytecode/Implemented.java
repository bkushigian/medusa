package edu.umass.medusa.util.bytecode;

import org.objectweb.asm.Opcodes;

/**
 * A place for us to record which VM instructions we have recorded (and where we can check
 * programmatically).
 */
public class Implemented {

  public static boolean isImplemented(int opCode) {
    switch (opCode) {
      case Opcodes.ILOAD:
      case Opcodes.ISTORE:
      case Opcodes.ALOAD:
      case Opcodes.ASTORE:

        // CONSTANT LOADING
      case Opcodes.BIPUSH:
      case Opcodes.SIPUSH:
      case Opcodes.LDC:

        // INTEGERS
      case Opcodes.ACONST_NULL:
      case Opcodes.ICONST_0:
      case Opcodes.ICONST_1:
      case Opcodes.ICONST_2:
      case Opcodes.ICONST_3:
      case Opcodes.ICONST_4:
      case Opcodes.ICONST_5:
      case Opcodes.ICONST_M1:
      case Opcodes.IADD:
      case Opcodes.ISUB:
      case Opcodes.IMUL:
      case Opcodes.IDIV:
      case Opcodes.IREM:
      case Opcodes.IAND:
      case Opcodes.IOR:
      case Opcodes.IXOR:
      case Opcodes.ISHL:
      case Opcodes.ISHR:
      case Opcodes.IUSHR:
      case Opcodes.INEG:
      case Opcodes.IINC:

        // LONG
      case Opcodes.LCONST_0:
      case Opcodes.LCONST_1:
      case Opcodes.LADD:
      case Opcodes.LSUB:
      case Opcodes.LMUL:
      case Opcodes.LDIV:
      case Opcodes.LREM:
      case Opcodes.LAND:
      case Opcodes.LOR:
      case Opcodes.LXOR:
      case Opcodes.LSHL:
      case Opcodes.LSHR:
      case Opcodes.LUSHR:
      case Opcodes.LNEG:

        // COMPARISONS
      case Opcodes.IF_ICMPEQ:
      case Opcodes.IF_ICMPNE:
      case Opcodes.IF_ICMPLT:
      case Opcodes.IF_ICMPLE:
      case Opcodes.IF_ICMPGE:
      case Opcodes.IF_ICMPGT:
      case Opcodes.IFNULL:
      case Opcodes.IFNONNULL:
      case Opcodes.IFEQ:
      case Opcodes.IFNE:
      case Opcodes.IFLE:
      case Opcodes.IFLT:
      case Opcodes.IFGE:
      case Opcodes.IFGT:
      case Opcodes.IRETURN:
      case Opcodes.ARETURN:
      case Opcodes.LRETURN:
      case Opcodes.FRETURN:
      case Opcodes.RETURN:
      case Opcodes.GOTO:
        return true;
      default:
        return false;
    }
  }
}
