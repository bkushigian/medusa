package edu.umass.medusa.util.bytecode;

import org.objectweb.asm.tree.AbstractInsnNode;

import java.util.*;

/**
 * This is an efficient representation of a subset of the 256 bytecode instructions.
 */
public class MutableBytecodeSet extends BytecodeSet {

  public static MutableBytecodeSet range(int low, int high) {
    if (low < 0 || high > 256 || low > high) {
      throw new IllegalArgumentException();
    }
    MutableBytecodeSet result = new MutableBytecodeSet();
    for (int i = low; i < high; ++i) {
      result.add(i);
    }
    return result;
  }

  public MutableBytecodeSet() {
  }

  public MutableBytecodeSet(BytecodeSet other) {
    for (int i = 0; i < 4; ++i) {
      flags[i] = other.flags[i];
    }
  }

  public MutableBytecodeSet(int... instructions) {
    for (int i : instructions) {
      if (i < 0 || i > 255) {
        throw new IllegalArgumentException("Instructions must be in range [0..255]");
      }
      add(i);
    }
  }

  @Override
  public BytecodeSet union(BytecodeSet other) {
    return new MutableBytecodeSet(super.union(other));
  }

  @Override
  public BytecodeSet intersection(BytecodeSet other) {
    return new MutableBytecodeSet(super.intersection(other));
  }

  @Override
  public BytecodeSet symmetricDiff(BytecodeSet other) {
    return new MutableBytecodeSet(super.symmetricDiff(other));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    SortedSet<Integer> sorted = new TreeSet<>(getInstructions());
    for (int i = 0; i < 256; ++i) {
      if (contains(i)) {
        sb.append(printHex ? Integer.toHexString(i) + " " : i + " ");
      }
    }
    return sb.toString();
  }


  @Override
  public boolean add(Integer opcode) {
    if (contains(opcode)) {
      return false;
    }
    ++size;
    flags[opcode / 64] |= (((long) 1) << (opcode % 64));
    return true;
  }

  public boolean add(AbstractInsnNode node) {
    return add(node.getOpcode());
  }

  @Override
  public boolean remove(Object o) {
    if (o instanceof Integer) {
      return remove((int) o);
    }
    if (o instanceof AbstractInsnNode) {
      return remove(((AbstractInsnNode) o).getOpcode());
    }
    return false;
  }

  public boolean remove(int opcode) {
    if (!contains(opcode)) {
      return false;
    }
    --size;
    flags[opcode / 64] ^= (((long) 1) << (opcode % 64));
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends Integer> c) {
    boolean result = false;
    for (Integer i : c) {
      result |= add(i);
    }
    return result;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    // TODO
    throw new UnsupportedOperationException();
  }
}
