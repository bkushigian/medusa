package edu.umass.medusa.util.bytecode;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import static org.objectweb.asm.Opcodes.*;
import org.objectweb.asm.Opcodes;

/**
 * A convenience class to store data about instructions
 */
public class BytecodeUtil {

  public final static BytecodeSet CALL_OPCODES = new BytecodeSet(INVOKEVIRTUAL, INVOKESPECIAL,
      INVOKESTATIC, INVOKEINTERFACE);
  public final static BytecodeSet INT_ARITH_OPCODES = new BytecodeSet(IADD, ISUB, IMUL, IDIV, IREM,
      ISHL, ISHR, IUSHR, IAND, IOR, IXOR, INEG, IINC);
  public final static BytecodeSet IPUSH_OPCODES = new BytecodeSet(SIPUSH, BIPUSH);
  public final static BytecodeSet CONTS_OPCODES = new BytecodeSet(ICONST_0, ICONST_1, ICONST_2,
      ICONST_3, ICONST_4, ICONST_5, ICONST_M1);
  public final static BytecodeSet LONG_ARITH_OPCODES = new BytecodeSet(LADD, LSUB, LMUL, LDIV, LREM,
      LSHL, LSHR, LUSHR, LAND, LOR, LXOR, LNEG);
  // TODO: Collect all binary ops here
  public final static BytecodeSet BIN_OP_OPCODES = new BytecodeSet(IADD, ISUB, IMUL, IDIV, IREM,
      ISHL, ISHR, IUSHR, IAND, IOR, IXOR,
      LADD, LSUB, LMUL, LDIV, LREM, LSHL, LSHR, LUSHR, LAND, LOR, LXOR);
  public final static BytecodeSet JUMP_OPCODES = new BytecodeSet(IFEQ, IFNE, IFLT, IFGE, IFGT, IFLE,
      IF_ICMPEQ,
      IF_ICMPNE, IF_ICMPLT, IF_ICMPGE, IF_ICMPGT, IF_ICMPLE,
      IF_ACMPEQ, IF_ACMPNE, GOTO, JSR, IFNULL, IFNONNULL);

  public final static BytecodeSet RETURN_OPCODES = BytecodeSet.range(IRETURN, ARETURN);
  public final static BytecodeSet IFXX_OPCODES = BytecodeSet.range(IFEQ, IFLE + 1);
  public final static BytecodeSet IF_ICMPXX_OPCODES = BytecodeSet.range(IF_ICMPEQ, IF_ICMPLE + 1);
  public final static BytecodeSet IF_ACMPXX_OPCODES = new BytecodeSet(IF_ACMPEQ, IF_ACMPNE);
  public final static BytecodeSet NEW_OPCODES = new BytecodeSet(NEW, NEWARRAY, ANEWARRAY);

  public static VarInsnNode getStore(String primName, int idx) {
    switch (primName.toLowerCase()) {
      case "boolean":
      case "bool":
      case "short":
      case "byte":
      case "char":
      case "integer":
      case "int":
        return istore(idx);
      case "long":
        return lstore(idx);
      case "double":
        return dstore(idx);
      case "float":
        return fstore(idx);
      default:
        throw new RuntimeException("Unrecognized prim type " + primName);
    }
  }

  public static VarInsnNode getLoad(String primName, int idx) {
    switch (primName.toLowerCase()) {
      case "boolean":
      case "bool":
      case "short":
      case "byte":
      case "char":
      case "integer":
      case "int":
        return iload(idx);
      case "long":
        return lload(idx);
      case "double":
        return dload(idx);
      case "float":
        return fload(idx);
      default:
        throw new RuntimeException("Unrecognized prim type " + primName);
    }
  }

  public static InsnNode getReturn(String primName) {
    switch (primName.toLowerCase()) {
      case "boolean":
      case "bool":
      case "short":
      case "byte":
      case "char":
      case "integer":
      case "int":
        return ireturn();
      case "long":
        return lreturn();
      case "double":
        return dreturn();
      case "float":
        return freturn();
      default:
        throw new RuntimeException("Unrecognized prim type " + primName);
    }
  }


  public static boolean isCallInstr(AbstractInsnNode instr) {
    return CALL_OPCODES.get(instr);
  }

  public static boolean isCallInstr(Integer instr) {
    return CALL_OPCODES.contains(instr);
  }

  public static boolean isJumpInstr(AbstractInsnNode instr) {
    return JUMP_OPCODES.get(instr);
  }

  public static boolean isJumpInstr(Integer instr) {
    return JUMP_OPCODES.contains(instr);
  }

  ////////////////////////////////////////////////////////////////////////////
  //                         Bytecode Generation                            //
  ////////////////////////////////////////////////////////////////////////////
  public static IntInsnNode bipush(int value) {
    return new IntInsnNode(BIPUSH, value);
  }

  ////////////////////////////////////////////////////////////////////////////
  //                  CONDITIONALS and JUMP INSTRUCTIONS                    //
  ////////////////////////////////////////////////////////////////////////////

  public static LabelNode label() {
    return new LabelNode();
  }

  public static LabelNode label(Label label) {
    return new LabelNode(label);
  }

  public static JumpInsnNode go2(LabelNode label) {
    return new JumpInsnNode(GOTO, label);
  }

  public static JumpInsnNode if_acmpeq(LabelNode label) {
    return new JumpInsnNode(IF_ACMPEQ, label);
  }

  public static JumpInsnNode if_icmpge(LabelNode label) {
    return new JumpInsnNode(IF_ICMPGE, label);
  }

  public static JumpInsnNode if_icmpgt(LabelNode label) {
    return new JumpInsnNode(IF_ICMPGT, label);
  }

  public static JumpInsnNode if_icmple(LabelNode label) {
    return new JumpInsnNode(IF_ICMPLE, label);
  }

  public static JumpInsnNode if_icmplt(LabelNode label) {
    return new JumpInsnNode(IF_ICMPLT, label);
  }

  public static JumpInsnNode if_icmpne(LabelNode label) {
    return new JumpInsnNode(IF_ICMPNE, label);
  }

  public static JumpInsnNode if_icmpeq(LabelNode label) {
    return new JumpInsnNode(IF_ICMPEQ, label);
  }

  public static JumpInsnNode ifeq(LabelNode label) {
    return new JumpInsnNode(IFEQ, label);
  }

  public static JumpInsnNode ifge(LabelNode label) {
    return new JumpInsnNode(IFGE, label);
  }

  public static JumpInsnNode ifgt(LabelNode label) {
    return new JumpInsnNode(IFGT, label);
  }

  public static JumpInsnNode ifle(LabelNode label) {
    return new JumpInsnNode(IFLE, label);
  }

  public static JumpInsnNode iflt(LabelNode label) {
    return new JumpInsnNode(IFLT, label);
  }

  public static JumpInsnNode ifne(LabelNode label) {
    return new JumpInsnNode(IFNE, label);
  }

  public static JumpInsnNode ifnonnull(LabelNode label) {
    return new JumpInsnNode(IFNONNULL, label);
  }

  public static JumpInsnNode ifnull(LabelNode label) {
    return new JumpInsnNode(IFNULL, label);
  }

  public static InsnNode ladd() {
    return new InsnNode(LADD);
  }

  public static InsnNode lsub() {
    return new InsnNode(LSUB);
  }

  public static InsnNode land() {
    return new InsnNode(LAND);
  }

  public static InsnNode ldiv() {
    return new InsnNode(LDIV);
  }

  public static InsnNode lmul() {
    return new InsnNode(LMUL);
  }

  public static InsnNode iadd() {
    return new InsnNode(IADD);
  }

  public static InsnNode iand() {
    return new InsnNode(IAND);
  }

  public static InsnNode iconst_m1() {
    return new InsnNode(ICONST_M1);
  }

  public static InsnNode iconst_0() {
    return new InsnNode(ICONST_0);
  }

  public static InsnNode iconst_1() {
    return new InsnNode(ICONST_1);
  }

  public static InsnNode iconst_2() {
    return new InsnNode(ICONST_2);
  }

  public static InsnNode iconst_3() {
    return new InsnNode(ICONST_3);
  }

  public static InsnNode iconst_4() {
    return new InsnNode(ICONST_4);
  }

  public static InsnNode iconst_5() {
    return new InsnNode(ICONST_5);
  }

  public static InsnNode dconst_0() {
    return new InsnNode(DCONST_0);
  }

  public static InsnNode dconst_1() {
    return new InsnNode(DCONST_1);
  }

  public static InsnNode fconst_0() {
    return new InsnNode(FCONST_0);
  }

  public static InsnNode fconst_1() {
    return new InsnNode(FCONST_1);
  }

  public static InsnNode fconst_2() {
    return new InsnNode(FCONST_2);
  }

  public static InsnNode lconst_0() {
    return new InsnNode(LCONST_0);
  }

  public static InsnNode lconst_1() {
    return new InsnNode(LCONST_1);
  }

  public static InsnNode idiv() {
    return new InsnNode(IDIV);
  }

  public static IincInsnNode iinc(int index, int amt) {
    return new IincInsnNode(index, amt);
  }

  public static VarInsnNode iload(int index) {
    return new VarInsnNode(ILOAD, index);
  }

  public static VarInsnNode lload(int index) {
    return new VarInsnNode(LLOAD, index);
  }

  public static VarInsnNode fload(int index) {
    return new VarInsnNode(FLOAD, index);
  }

  public static VarInsnNode dload(int index) {
    return new VarInsnNode(DLOAD, index);
  }

  public static VarInsnNode iload_0() {
    return new VarInsnNode(ILOAD, 0);
  }

  public static VarInsnNode iload_1() {
    return new VarInsnNode(ILOAD, 1);
  }

  public static VarInsnNode iload_2() {
    return new VarInsnNode(ILOAD, 2);
  }

  public static VarInsnNode iload_3() {
    return new VarInsnNode(ILOAD, 3);
  }

  public static InsnNode imul() {
    return new InsnNode(IMUL);
  }

  public static InsnNode ineg() {
    return new InsnNode(INEG);
  }

  public static InsnNode ior() {
    return new InsnNode(IOR);
  }

  public static InsnNode irem() {
    return new InsnNode(IREM);
  }

  public static InsnNode ireturn() {
    return new InsnNode(IRETURN);
  }

  public static InsnNode lreturn() {
    return new InsnNode(LRETURN);
  }

  public static InsnNode dreturn() {
    return new InsnNode(DRETURN);
  }

  public static InsnNode freturn() {
    return new InsnNode(FRETURN);
  }

  public static InsnNode ishl() {
    return new InsnNode(ISHL);
  }

  public static InsnNode ishr() {
    return new InsnNode(ISHR);
  }

  public static VarInsnNode fstore(int idx) {
    return new VarInsnNode(FSTORE, idx);
  }

  public static VarInsnNode dstore(int idx) {
    return new VarInsnNode(DSTORE, idx);
  }

  public static VarInsnNode istore(int idx) {
    return new VarInsnNode(ISTORE, idx);
  }

  public static VarInsnNode lstore(int idx) {
    return new VarInsnNode(LSTORE, idx);
  }

  public static InsnNode isub() {
    return new InsnNode(ISUB);
  }

  public static InsnNode iushr() {
    return new InsnNode(IUSHR);
  }

  public static InsnNode ixor() {
    return new InsnNode(IXOR);
  }

  public static IntInsnNode sipush(int value) {
    // TODO: Is this correct? The JVM specifies that SIPush is parametrized by two bytes instead of a single short
    return new IntInsnNode(SIPUSH, value);
  }

  public static IntInsnNode ldc(int value) {
    return new IntInsnNode(LDC, value);
  }

  /**
   * Determine the stack size delta from executing these instructions. For instructions:
   *
   * <ul>
   *   <li> DUP2 </li>
   *   <li> DUP2_X1 </li>
   *   <li> DUP2_X2 </li>
   *   <li> ATHROW </li>
   *   <li> INVOKEDYNAMIC </li>
   *   <li> INVOKEINTERFACE </li>
   *   <li> INVOKESPECIAL </li>
   *   <li> INVOKESTATIC </li>
   *   <li> INVOKEVIRTUAL </li>
   *   <li> MULTIANEWARRAY </li>
   *   <li> POP2 </li>
   * </ul>
   * this value cannot be known, and we return {@code Integer.MIN_VALUE}.
   * @param insn An instruction
   * @return the amount the stack size changes after executing this instruction if this can
   *         be determined from opcode alone; otherwise, {@code Integer.MIN_VALUE}.
   */
  public static int stackSizeDelta(final AbstractInsnNode insn) {
    switch (insn.getOpcode()) {
      case ACONST_NULL:
      case ALOAD:
      case BIPUSH:
      case DCONST_0:
      case DCONST_1:
      case DLOAD:
      case DUP:
      case DUP_X1:
      case DUP_X2:
      case FCONST_0:
      case FCONST_1:
      case FCONST_2:
      case FLOAD:
      case GETSTATIC:
      case ICONST_M1:
      case ICONST_0:
      case ICONST_1:
      case ICONST_2:
      case ICONST_3:
      case ICONST_4:
      case ICONST_5:
      case ILOAD:
      case JSR:
      case LCONST_0:
      case LCONST_1:
      case LDC:
      case LLOAD:
      case NEW:
      case SIPUSH:
        return 1;

      case AALOAD:
      case AASTORE:
      case ARETURN:
      case ASTORE:
      case BALOAD:
      case BASTORE:
      case CALOAD:
      case CASTORE:
      case DADD:
      case DALOAD:
      case DASTORE:
      case DCMPG:
      case DCMPL:
      case DDIV:
      case DMUL:
      case DREM:
      case DRETURN:
      case DSTORE:
      case DSUB:
      case FADD:
      case FALOAD:
      case FASTORE:
      case FCMPG:
      case FCMPL:
      case FDIV:
      case FMUL:
      case FREM:
      case FRETURN:
      case FSTORE:
      case FSUB:
      case IADD:
      case IALOAD:
      case IAND:
      case IASTORE:
      case IDIV:
      case IFEQ:
      case IFGE:
      case IFGT:
      case IFLE:
      case IFLT:
      case IFNE:
      case IFNONNULL:
      case IFNULL:
      case IMUL:
      case IOR:
      case IREM:
      case IRETURN:
      case ISHL:
      case ISHR:
      case ISTORE:
      case ISUB:
      case IUSHR:
      case IXOR:
      case LADD:
      case LALOAD:
      case LAND:
      case LASTORE:
      case LCMP:
      case LDIV:
      case LMUL:
      case LOOKUPSWITCH:
      case LOR:
      case LREM:
      case LRETURN:
      case LSHL:
      case LSHR:
      case LSTORE:
      case LSUB:
      case LUSHR:
      case LXOR:
      case MONITORENTER:
      case MONITOREXIT:
      case POP:
      case PUTSTATIC:
      case SALOAD:
      case SASTORE:
      case TABLESWITCH:
        return -1;

      case IF_ACMPEQ:
      case IF_ACMPNE:
      case IF_ICMPEQ:
      case IF_ICMPGE:
      case IF_ICMPGT:
      case IF_ICMPLE:
      case IF_ICMPLT:
      case IF_ICMPNE:
      case PUTFIELD:
        return -2;

      case DUP2:
      case DUP2_X1:
      case DUP2_X2:
      case ATHROW:
      case INVOKEDYNAMIC:
      case INVOKEINTERFACE:
      case INVOKESPECIAL:
      case INVOKESTATIC:
      case INVOKEVIRTUAL:
      case MULTIANEWARRAY:
      case POP2:
        return Integer.MIN_VALUE; // Unknown

      default:
        return 0;
    }
  }
}
