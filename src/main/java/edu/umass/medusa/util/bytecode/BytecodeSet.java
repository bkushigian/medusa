package edu.umass.medusa.util.bytecode;

import org.objectweb.asm.tree.AbstractInsnNode;

import java.util.*;

public class BytecodeSet implements Set<Integer> {

  public static boolean printHex = true;
  protected final long[] flags = new long[4];
  protected int size;

  public static BytecodeSet range(int low, int high) {
    if (low < 0 || high > 256 || low > high) {
      throw new IllegalArgumentException();
    }
    BytecodeSet result = new BytecodeSet();
    for (int i = low; i < high; ++i) {
      result._add(i);
    }
    return result;
  }

  public BytecodeSet() {
    for (int i = 0; i < 4; ++i) {
      flags[i] = 0;
    }
    size = 0;
  }

  public BytecodeSet(Integer... instructions) {
    for (Integer i : instructions) {
      if (i == null) {
        throw new NullPointerException();
      }
      if (i < 0 || i > 255) {
        throw new IllegalArgumentException("Instructions must be in range [0..255]");
      }
      _add(i);
    }
  }

  public boolean isSubset(BytecodeSet other) {
    for (int i = 0; i < 256; ++i) {
      if (contains(i) && !other.contains(i)) {
        return false;
      }
    }
    return true;
  }

  public boolean isSuperset(BytecodeSet other) {
    for (int i = 0; i < 256; ++i) {
      if (!contains(i) && other.contains(i)) {
        return false;
      }
    }
    return true;
  }

  public BytecodeSet union(BytecodeSet other) {
    BytecodeSet result = new BytecodeSet();
    for (int i = 0; i < 4; ++i) {
      result.flags[i] = flags[i] | other.flags[i];
    }
    return result;
  }

  public BytecodeSet intersection(BytecodeSet other) {
    BytecodeSet result = new BytecodeSet();
    for (int i = 0; i < 4; ++i) {
      result.flags[i] = flags[i] & other.flags[i];
    }
    return result;
  }

  public BytecodeSet symmetricDiff(BytecodeSet other) {
    BytecodeSet result = new BytecodeSet();
    for (int i = 0; i < 4; ++i) {
      result.flags[i] = flags[i] ^ other.flags[i];
    }
    return result;
  }

  @Override
  public boolean contains(Object o) {
    if (o instanceof Integer) {
      return contains((Integer) o);
    }
    if (o instanceof AbstractInsnNode) {
      return contains((AbstractInsnNode) o);
    }
    return false;
  }

  public boolean contains(int index) {
    if (index < 0 || index > 255) {
      return false;
    }
    return (flags[index / 64] & (((long) 1) << (index % 64))) != 0;
  }

  public boolean contains(Integer index) {
    if (index < 0 || index > 255) {
      return false;
    }
    return (flags[index / 64] & (((long) 1) << (index % 64))) != 0;
  }

  public boolean get(AbstractInsnNode instr) {
    return contains(instr.getOpcode());
  }

  private void _add(int index) {
    ++size;
    flags[index / 64] |= (((long) 1) << (index % 64));
  }

  public Set<Integer> getInstructions() {
    Set<Integer> result = new HashSet<>();
    for (int i = 0; i < 256; ++i) {
      if (contains(i)) {
        result.add(i);
      }
    }
    return result;
  }

  @Override
  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    SortedSet<Integer> sorted = new TreeSet<>(getInstructions());
    for (int i = 0; i < 256; ++i) {
      if (contains(i)) {
        sb.append(printHex ? Integer.toHexString(i) + " " : i + " ");
      }
    }
    return sb.toString();
  }


  @Override
  public Iterator<Integer> iterator() {
    return getInstructions().iterator();
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public boolean add(Integer integer) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    for (Object o : c) {
      if (!contains(o)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends Integer> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }
}
