package edu.umass.medusa.util;

import de.codesourcery.asm.controlflow.ControlFlowGraph;
import de.codesourcery.asm.controlflow.Edge;
import de.codesourcery.asm.controlflow.IBlock;
import org.objectweb.asm.tree.*;
import edu.umass.medusa.format.MethodIdentity;

import java.util.*;
import java.util.stream.Collectors;

public class CFGUtil {

  /**
   * return the instructions for a basic block in the order that they appear in the bytecode
   *
   * @param mn method node
   * @param bb basic block
   * @return an ordered list of instructions
   */
  public static List<AbstractInsnNode> getInstructionsInBasicBlock(MethodNode mn, IBlock bb) {
    @SuppressWarnings("unchecked") final ListIterator<AbstractInsnNode> iterator = mn.instructions
        .iterator();
    List<AbstractInsnNode> instList = new ArrayList<>();

    for (int index = 0; iterator.hasNext(); ++index) {
      final AbstractInsnNode instruction = iterator.next();

      if (bb.containsInstructionNum(index) && instruction.getOpcode() >= 0) {
        instList.add(instruction);
      }
    }

    return instList;
  }

  /**
   * check whether the two instruction are the same - jump instructions have a label which will most
   * likely point to different targets however we ignore that, as there is no good way to compare
   * other than the opcode - iinc instructions are compared to the var slot and increment - any
   * other instruction is compared based on opcode/var
   *
   * @param inst1 first instruction
   * @param inst2 second instruction
   * @return true if they are same according to the above assumptions/constraints, false otherwise
   */
  public static boolean sameInstruction(AbstractInsnNode inst1, AbstractInsnNode inst2) {
    if ((inst1.getType() != inst2.getType()) || (inst1.getOpcode() != inst2.getOpcode())) {
      return false;
    } else {
      switch (inst1.getType()) {
        case AbstractInsnNode.FIELD_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.FRAME:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.IINC_INSN:
          IincInsnNode iincInsnNode1 = (IincInsnNode) inst1;
          IincInsnNode iincInsnNode2 = (IincInsnNode) inst2;
          return iincInsnNode1.var == iincInsnNode2.var && iincInsnNode1.incr == iincInsnNode2.incr;

        case AbstractInsnNode.INSN:
          return true;

        case AbstractInsnNode.INT_INSN:
          IntInsnNode intInsnNode1 = (IntInsnNode) inst1;
          IntInsnNode intInsnNode2 = (IntInsnNode) inst2;
          return intInsnNode1.operand == intInsnNode2.operand;

        case AbstractInsnNode.INVOKE_DYNAMIC_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.JUMP_INSN:
          return true;

        case AbstractInsnNode.LABEL:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.LDC_INSN:
          LdcInsnNode ldcInsnNode1 = (LdcInsnNode) inst1;
          LdcInsnNode ldcInsnNode2 = (LdcInsnNode) inst2;
          return ldcInsnNode1.cst.equals(ldcInsnNode2.cst);

        case AbstractInsnNode.LINE:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.LOOKUPSWITCH_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.METHOD_INSN:
          MethodInsnNode n1 = (MethodInsnNode) inst1, n2 = (MethodInsnNode) inst2;
          // TODO: Do we need to handle field itf for interface?
          return n1.owner.equals(n2.owner) && n1.name.equals(n2.name) && n1.desc.equals(n2.desc);

        case AbstractInsnNode.MULTIANEWARRAY_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.TABLESWITCH_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.TYPE_INSN:
          throw new RuntimeException("not implemented");

        case AbstractInsnNode.VAR_INSN:
          VarInsnNode varInsnNode1 = (VarInsnNode) inst1;
          VarInsnNode varInsnNode2 = (VarInsnNode) inst2;
          return varInsnNode1.var == varInsnNode2.var;

        default:
          break;
      }

      return false;
    }
  }

  /**
   * true if both the basic blocks have the same contents
   *
   * @param bb1 basic block
   * @param bb2 basic block
   * @return true if the basic blocks are the same, false otherwise
   */
  public static boolean haveSameContents(MethodNode mn1, IBlock bb1, MethodNode mn2, IBlock bb2) {
    List<AbstractInsnNode> instListBB1 = getInstructionsInBasicBlock(mn1, bb1);
    List<AbstractInsnNode> instListBB2 = getInstructionsInBasicBlock(mn2, bb2);

    Iterator<AbstractInsnNode> iter1 = instListBB1.iterator();
    Iterator<AbstractInsnNode> iter2 = instListBB2.iterator();

    while (iter1.hasNext() && iter2.hasNext()) {
      if (!sameInstruction(iter1.next(), iter2.next())) {
        return false;
      }
    }

    return iter1.hasNext() == iter2.hasNext();
  }

  /**
   * Given a method {@code original} and mutant {@code mutant} with isomorphic CFGs, compute which
   * blocks are mutated and return pairs of (original,mutant) block pairs..
   */
  public static Set<Pair<IBlock, IBlock>> getMutantBlockPairs(MethodIdentity original,
      MethodIdentity mutant) {
    Set<Pair<IBlock, IBlock>> mutatedBlockPairs = new HashSet<>();
    Queue<Pair<IBlock, IBlock>> queue = new ArrayDeque<>();
    Set<IBlock> visited = new HashSet<>();
    queue.add(new Pair<>(original.cfg.getStart(), mutant.cfg.getStart()));

    while (!queue.isEmpty()) {
      Pair<IBlock, IBlock> p = queue.poll();
      assert p.first != null && p.second != null;

      if (!visited.contains(p.first) && !visited.contains(p.second)) {
        // mark both nodes visited
        visited.add(p.first);
        visited.add(p.second);

        if (!haveSameContents(original.method, p.first, mutant.method, p.second)) {
          mutatedBlockPairs.add(p);
        }

        updateBlockPairQueue(queue, p);
      }
    }
    return mutatedBlockPairs;
  }

  /**
   * count number of blocks that differ starting at the start basic block assuming the CFG of both
   * the methods are isomorphic
   *
   * @param original MethodIdentity for first method
   * @param mutant MethodIdentity for second method
   * @return number of basic blocks that differ
   */
  public static int countDifferingBlocks(MethodIdentity original, MethodIdentity mutant) {
    return getMutantBlockPairs(original, mutant).size();

  }

  /**
   * compare two method identities to check whether their CFGs are first order mutants a first order
   * mutant differs at only one basic block from the original version
   *
   * @param original first method
   * @param mutant second method
   * @return true if they are first order mutant, false otherwise
   */
  public static boolean isAtomicCFGMutant(MethodIdentity original, MethodIdentity mutant) {
    if (isIsomorphic(original.cfg, mutant.cfg)) {
      return countDifferingBlocks(original, mutant) == 1;
    } else {
      return false;
    }
  }

  /**
   * This and getEdgeToDst correct for a bad design in CFGUtil: every edge in the CFG has two Edge
   * instances: one that belongs to dst and one that belongs to src. This means that we need to
   * handle edges from predecessors differently from edges to successors. In particular, if we want
   * meta data (true edge vs false edge) we need to do some extra logic.
   *
   * @param edge (src, dest)
   * @return the edge that is stored by src
   */
  public static Edge getEdgeFromSrc(Edge edge) {
    for (Edge e : edge.src.getEdges()) {
      if (e.dst.equals(edge.dst)) {
        return e;
      }
    }
    throw new RuntimeException("Couldn't find corresponding edge");
  }

  /**
   * This and getEdgeFromSrc correct for a bad design in CFGUtil: every edge in the CFG has two Edge
   * instances: one that belongs to dst and one that belongs to src. This means that we need to
   * handle edges from predecessors differently from edges to successors. In particular, if we want
   * meta data (true edge vs false edge) we need to do some extra logic.
   *
   * @param edge (src, dest)
   * @return the edge that is stored by dst
   */
  public static Edge getEdgeToDst(Edge edge) {
    for (Edge e : edge.dst.getEdges()) {
      if (e.src.equals(edge.src)) {
        return e;
      }
    }
    throw new RuntimeException("Couldn't find corresponding edge");
  }

  /**
   * @param b edge source
   * @return the set of all outgoing edges from b
   */
  public static Set<Edge> getOutEdges(IBlock b) {
    return b.getEdges().stream()
        .filter(e -> e.src.equals(b))       // Get only edges from b to some dst
        .collect(Collectors.toSet());
  }

  /**
   * @param b edge target
   * @return the set of all incoming edges to b
   */
  public static Set<Edge> getInEdges(IBlock b) {
    return b.getEdges().stream()
        .filter(e -> e.dst.equals(b))       // Get only edges to b
        .map(CFGUtil::getEdgeFromSrc)       // Ensure that these have meta data
        .collect(Collectors.toSet());
  }

  /**
   * a helper function to return an outgoing edge with the specific metadata
   *
   * @param bb basic block
   * @param metadata metadata to look for
   * @return block which shares an edge to bb with this metadata if no such edge exists returns null
   */
  private static IBlock getOutgoing(IBlock bb, String metadata) {
    for (Edge edge : bb.getEdges()) {
      if (edge.metaData != null && edge.metaData.equals(metadata)) {
        return edge.dst;
      }
    }

    return null;
  }

  /**
   * return the true outgoing edge from the basic block
   *
   * @param bb basic block
   * @return outgoing true basic block
   */
  public static IBlock getTrueOutgoing(IBlock bb) {
    return getOutgoing(bb, "true");
  }

  /**
   * return the false outgoing edge from the basic block
   *
   * @param bb basic block
   * @return outgoing false basic block
   */
  public static IBlock getFalseOutgoing(IBlock bb) {
    return getOutgoing(bb, "false");
  }

  /**
   * label graph
   *
   * @param bb basic block
   * @param counter LabelCounter object
   * @param labels mapping from basic blocks to labels
   * @return true if finished successfully, false otherwise
   */
  private static boolean label(IBlock bb, LabelCounter counter, Map<IBlock, Integer> labels) {
    // do not label a basic block twice
    if (labels.containsKey(bb)) {
      return true;
    } else {
      // label this vertex
      labels.put(bb, counter.getUniqueLabel());

      // check the number of successors of this vertex
      if (bb.getRegularSuccessorCount() == 2) {
        return label(getTrueOutgoing(bb), counter, labels) && label(getFalseOutgoing(bb), counter,
            labels);
      } else if (bb.getRegularSuccessorCount() == 1) {
        return label(bb.getRegularSuccessor(), counter, labels);
      } else {
        return true; // this is the end block, we can stop searching
      }
    }
  }

  /**
   * generate a unique label
   */
  private static class LabelCounter {

    private int counter;

    public LabelCounter() {
      counter = 1;
    }

    public int getUniqueLabel() {
      return counter++;
    }
  }

  /**
   * check whether the given two graphs CFG are isomorphic
   *
   * @return true if the graphs are isomorphic, false otherwise
   */
  public static boolean isIsomorphic(ControlFlowGraph cfg1, ControlFlowGraph cfg2) {
    Map<IBlock, Integer> labelBB1 = new HashMap<>();
    Map<IBlock, Integer> labelBB2 = new HashMap<>();
    IBlock bb1 = cfg1.getStart(), bb2 = cfg2.getStart();

    // label first graph
    label(bb1, new LabelCounter(), labelBB1);

    // label second graph
    label(bb2, new LabelCounter(), labelBB2);

    // compare the labels on the two graphs
    // labels on the corresponding nodes in the graphs should match
    Queue<Pair<IBlock, IBlock>> queue = new ArrayDeque<>();
    queue.add(new Pair<>(bb1, bb2));

    Set<IBlock> visited = new HashSet<>();

    while (!queue.isEmpty()) {
      Pair<IBlock, IBlock> p = queue.poll();
      if (visited.contains(p.first)) {
        continue;
      }
      visited.add(p.first);

      // end of the graph, return true
      if ((p.first == null && p.second == null) ||
          (p.first.getRegularSuccessorCount() == 0 && p.second.getRegularSuccessorCount() == 0)) {
        continue;
      } else if (!labelBB1.get(p.first).equals(labelBB2.get(p.second))) {
        return false;
      }
      if (p.first.getRegularSuccessorCount() != p.second.getRegularSuccessorCount()) {
        return false;
      }

      updateBlockPairQueue(queue, p);
    }

    return true;
  }

  private static void updateBlockPairQueue(Queue<Pair<IBlock, IBlock>> queue,
      Pair<IBlock, IBlock> p) {
    if (p.first.getRegularSuccessorCount() == 1) {
      queue.add(new Pair<>(p.first.getRegularSuccessor(), p.second.getRegularSuccessor()));
    } else if (p.first.getRegularSuccessorCount() == 2) {
      queue.add(new Pair<>(getTrueOutgoing(p.first), getTrueOutgoing(p.second)));
      queue.add(new Pair<>(getFalseOutgoing(p.first), getFalseOutgoing(p.second)));
    }
  }
}
