package edu.umass.medusa.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Generate unique names as needed
 */
public class NameGen {

  private Map<String, Integer> baseNameCounts;

  public NameGen() {
    baseNameCounts = new HashMap<>();
  }

  public String get(String base, String... terms) {
    StringBuilder sb = new StringBuilder(base);
    for (String b : terms) {
      sb.append('_').append(b);
    }

    String baseName = sb.toString();

    if (!baseNameCounts.containsKey(baseName)) {
      baseNameCounts.put(baseName, 0);
    }

    sb.append('-').append(
        String.format("%x", baseNameCounts.put(baseName, baseNameCounts.get(baseName) + 1)));
    return sb.toString();
  }

  public String get() {
    return get("auto__");
  }

  public void reset() {
    baseNameCounts = new HashMap<>();
  }
}
