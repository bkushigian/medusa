package edu.umass.medusa.util;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.io.IOException;

public class ASMUtil {

  public static ClassReader createClassReader(String className, File[] classPath)
      throws IOException {
    return de.codesourcery.asm.util.ASMUtil.createClassReader(className, classPath);
  }


  public static ClassNode readClass(File classFile) throws IOException {
    String name = classFile.getName();
    if (name.contains(".")) {
      final String[] split = name.split("\\.");
      assert split.length == 2;
      assert "class".equals(split[1]);
      name = split[0];
    }
    String path = classFile.getParent();
    return readClass(name, path);
  }

  public static ClassNode readClass(String className, String path) throws IOException {
    if (path == null) {
      path = ".";
    }
    ClassReader reader = createClassReader(className, new File[]{new File(path)});
    ClassNode cn = new ClassNode();
    reader.accept(cn, 0);
    return cn;
  }

  /**
   * @param mn A method node to name
   * @return A string that will uniquely identify a method within its parent class
   */
  public static String getUniqueMethodName(MethodNode mn) {
    return mn.name + mn.desc;
  }

  public static String getUniqueClassOffsetMethodName(String pkgName, String className,
      String methodName, String methodDesc) {
    StringBuilder b = new StringBuilder(pkgName.replace('.', '/'));
    return b.append('/')
        .append(className)
        .append('.')
        .append(methodName)
        .append(methodDesc)
        .toString();
  }

  public static String getUniqueClassOffsetMethodName(ClassNode cn, MethodNode mn) {
    return cn.name + '.' + mn.name + mn.desc;
  }

  public static boolean isConstructor(MethodNode mn) {
    return "<init>".equals(mn.name);
  }
}
