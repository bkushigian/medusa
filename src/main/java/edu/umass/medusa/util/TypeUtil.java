package edu.umass.medusa.util;

import com.microsoft.z3.Context;
import com.microsoft.z3.Sort;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This helps with various type-related operations
 */
public class TypeUtil {

  private final static Map<String, ReferenceType> refTypeMap = new HashMap<>();
  private final static Map<String, PrimitiveType> primTypeMap = new HashMap<>();
  private final static Map<String, ArrayType> arrayTypeMap = new HashMap<>();

  /**
   * Primitive types such as {@code int}, {@code boolean}, etc.
   */
  public final static PrimitiveType
      BYTE_PRIM_TYPE    = new PrimitiveType(Primitives.BYTE),
      SHORT_PRIM_TYPE   = new PrimitiveType(Primitives.SHORT),
      INTEGER_PRIM_TYPE = new PrimitiveType(Primitives.INT),
      LONG_PRIM_TYPE    = new PrimitiveType(Primitives.LONG),
      DOUBLE_PRIM_TYPE  = new PrimitiveType(Primitives.DOUBLE),
      FLOAT_PRIM_TYPE   = new PrimitiveType(Primitives.FLOAT),
      BOOLEAN_PRIM_TYPE = new PrimitiveType(Primitives.BOOLEAN),
      CHAR_PRIM_TYPE    = new PrimitiveType(Primitives.CHAR);

  /**
   * Boxed types such as {@code Integer}, {@code Boolean}, etc.
   */
  public final static BoxedType
      BYTE_BOXED_TYPE    = new BoxedType(BYTE_PRIM_TYPE),
      SHORT_BOXED_TYPE   = new BoxedType(SHORT_PRIM_TYPE),
      INTEGER_BOXED_TYPE = new BoxedType(INTEGER_PRIM_TYPE),
      LONG_BOXED_TYPE    = new BoxedType(LONG_PRIM_TYPE),
      DOUBLE_BOXED_TYPE  = new BoxedType(DOUBLE_PRIM_TYPE),
      FLOAT_BOXED_TYPE   = new BoxedType(FLOAT_PRIM_TYPE),
      BOOLEAN_BOXED_TYPE = new BoxedType(BOOLEAN_PRIM_TYPE),
      CHAR_BOXED_TYPE    = new BoxedType(CHAR_PRIM_TYPE);

  private final static PrimitiveType[] primitiveTypes = {
      BYTE_PRIM_TYPE, SHORT_PRIM_TYPE,
      INTEGER_PRIM_TYPE, LONG_PRIM_TYPE,
      DOUBLE_PRIM_TYPE, FLOAT_PRIM_TYPE,
      BOOLEAN_PRIM_TYPE, CHAR_PRIM_TYPE
  };

  private final static BoxedType[] boxedTypes = {
      BYTE_BOXED_TYPE, SHORT_BOXED_TYPE,
      INTEGER_BOXED_TYPE, LONG_BOXED_TYPE,
      DOUBLE_BOXED_TYPE, FLOAT_BOXED_TYPE,
      BOOLEAN_BOXED_TYPE, CHAR_BOXED_TYPE
  };

  static {
    for (BoxedType bt : boxedTypes) {
      refTypeMap.put(bt.desc, bt);
    }

    for (PrimitiveType pt : primitiveTypes) {
      primTypeMap.put(pt.desc(), pt);
    }
  }

  /**
   * Given a well-formed type descriptor, return a Type object reflecting this
   *
   * @return Type object representing the type descriptor {@code desc}
   */
  public static Type parseTypeDesc(String desc) {
    Pair<String, Type> pair = parseNext(desc);
    if (pair.first != null && !pair.first.isEmpty()) {
      throw new RuntimeException("Invalid type descriptor: " + desc);
    }
    return pair.second;
  }

  /**
   * Given a well-formed method descriptor (T1T2T3...)RT, return an array of Types
   * {@code {T1, T2, T3, ..., RT}}, where the last entry is the return type.
   */
  public static Type[] parseMethodDesc(final String desc) {
    Queue<Type> queue = new LinkedBlockingQueue<>();
    String currentDesc = desc;
    while (currentDesc != null && !"".equals(currentDesc)) {
      final Pair<String, Type> pair = parseNext(currentDesc);
      currentDesc = pair.first;
      final Type type = pair.second;
      queue.add(type);
    }

    Type[] types = new Type[queue.size()];
    int i = 0;
    for (Type t : queue) {
      types[i++] = t;
    }
    return types;
  }

  /**
   * Count the number of arguments in a method descriptor
   * @param desc method descriptor
   * @return number of arguments in {@code desc}
   */
  public static int countParameters(String desc) {
    return parseMethodDesc(desc).length - 1;
  }

  /**
   * Helper method
   * @param desc descriptor
   * @return {@code Pair<String, Type>} containing the unparsed string and the parsed type
   */
  private static Pair<String, Type> parseNext(String desc) {
    String nextString = desc;
    int nextStart;

    if (desc == null || desc.isEmpty()) {
      return null;
    }

    char c = desc.charAt(0);

    switch (c) {
      case 'V':
        if (desc.length() != 1) {
          throw new RuntimeException(
              "Void type descriptor \"V\" may only occur as a return type: " + desc);
        }
        return new Pair<>("", new VoidType());
      case '(':
      case ')':
        return parseNext(desc.substring(1));
      case 'L':
        nextStart = desc.indexOf(';');
        nextString = desc.substring(nextStart + 1);
        // TODO: Handle new ReferenceType for boxed types
        return new Pair<>(nextString, makeRefType(desc.substring(0, nextStart + 1)));
      case '[':
        nextString = desc.substring(1);
        Pair<String, Type> pair = parseNext(nextString);
        return new Pair<>(pair.first, new ArrayType(pair.second));
      case 'B':
      case 'S':
      case 'I':
      case 'J':
      case 'D':
      case 'F':
      case 'Z':
      case 'C':
        nextString = desc.substring(1);
        PrimitiveType pt = primTypeMap.get(String.valueOf(c));
        if (pt == null) {
          throw new RuntimeException();
        }
        return new Pair<>(nextString, pt);
      default:
        throw new RuntimeException("Illegal Type Description:" + desc);
    }
  }

  /**
   * Helper method to parser that performs a lookup to determine if a {@code ReferenceType} exists
   * for descriptor {@code desc}, constructing one and storing it for later use if need be.
   * {@code ReferenceType} descriptors are of the form {@code Lfoo.bar.baz.ClassName;}.
   *
   * @param desc the descriptor we are turning into a {@code ReferenceType}
   * @return The {@code ReferenceType} matching {@code desc}
   */
  private static ReferenceType makeRefType(String desc) {
    if (!refTypeMap.containsKey(desc)) {
      refTypeMap.put(desc, new ReferenceType(desc));
    }
    return refTypeMap.get(desc);
  }

  public static ArrayType makeArrayType(String desc) {
    if (!arrayTypeMap.containsKey(desc)) {
      arrayTypeMap.put(desc, new ArrayType(parseTypeDesc(desc.substring(1))));
    }

    return arrayTypeMap.get(desc);
  }

  // Define type system
  public enum Primitives {
    BYTE   ("B", "Byte"     , 8 ),
    SHORT  ("S", "Short"    , 16),
    INT    ("I", "Integer"  , 32),
    LONG   ("J", "Long"     , 64),
    DOUBLE ("D", "Double"   , 64),
    FLOAT  ("F", "Float"    , 32),
    CHAR   ("C", "Character", 16),
    BOOLEAN("Z", "Boolean"  , 8 );

    public final String desc;
    public final String boxDesc;
    public final int width;

    Primitives(String d, String b, int width) {
      this.desc = d;
      this.boxDesc = "Ljava/lang/" + b + ";";
      this.width = width;
    }
  }

  public static Primitives getPrimFromDesc(char c) {
    switch (c) {
      case 'B':
        return Primitives.BYTE;
      case 'S':
        return Primitives.SHORT;
      case 'I':
        return Primitives.INT;
      case 'J':
        return Primitives.LONG;
      case 'D':
        return Primitives.DOUBLE;
      case 'F':
        return Primitives.FLOAT;
      case 'C':
        return Primitives.CHAR;
      case 'Z':
        return Primitives.BOOLEAN;
      default:
        throw new RuntimeException("Unrecognized primitive descriptor: " + c);
    }
  }


  public interface Type {

    String desc();

    /**
     * @return {@code true} if this is a primitive type.
     */
    boolean isPrimitive();

    /**
     * @return {@code true} if this is an array type.
     */
    boolean isArray();

    /**
     * @return {@code true} if this is a boxed type.
     */
    boolean isBoxed();

    /**
     * @return {@code true} if this is a reference type.
     */
    boolean isReference();

    /**
     * @return {@code true} if this is a void return type
     */
    boolean isVoid();

    /**
     * Get the Z3 sort associated with this type if possible
     *
     * @return Z3 sort representing this type
     */
    Sort z3Sort(Context ctx) throws RuntimeException;
  }

  public static class PrimitiveType implements Type {

    public final Primitives primitiveType;
    private final String desc;

    PrimitiveType(Primitives prim) {
      this.primitiveType = prim;
      this.desc = prim.desc;
    }

    public BoxedType box() {
      return new BoxedType(this);
    }

    @Override
    public String desc() {
      return desc;
    }

    @Override
    public boolean isPrimitive() {
      return true;
    }

    @Override
    public boolean isArray() {
      return false;
    }

    @Override
    public boolean isBoxed() {
      return false;
    }

    @Override
    public boolean isReference() {
      return false;
    }

    @Override
    public boolean isVoid() {
      return false;
    }

    @Override
    public Sort z3Sort(Context ctx) throws RuntimeException {
      return ctx.mkBitVecSort(primitiveType.width);
    }

    @Override
    public String toString() {
      return "Prim[" + desc + "]";
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof PrimitiveType) {
        PrimitiveType other = (PrimitiveType) obj;
        return desc.equals(other.desc()) && primitiveType.equals(other.primitiveType);
      }
      return false;
    }
  }

  public static class ReferenceType implements Type {

    public final String desc;

    ReferenceType(String desc) {
      this.desc = desc;
    }

    @Override
    public String desc() {
      return desc;
    }

    @Override
    public boolean isPrimitive() {
      return false;
    }

    @Override
    public boolean isArray() {
      return false;
    }

    @Override
    public boolean isBoxed() {
      return false;
    }

    @Override
    public boolean isReference() {
      return true;
    }

    @Override
    public boolean isVoid() {
      return false;
    }

    @Override
    public String toString() {
      return "RefType[" + desc + "]";
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof ReferenceType) {
        ReferenceType other = (ReferenceType) obj;
        return (isBoxed() == other.isBoxed()) && (isArray() == other.isArray())
            && (desc.equals(other.desc));
      }
      return false;
    }

    @Override
    public Sort z3Sort(Context ctx) throws RuntimeException {
      throw new RuntimeException("RefTypes do not have a z3Sort");
    }
  }

  public static class ArrayType extends ReferenceType {

    public final Type type;

    ArrayType(Type type) {
      super("[" + type.desc());
      this.type = type;
    }

    @Override
    public String toString() {
      return "ArrayType[" + desc + "]";
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof ArrayType) {
        ArrayType at = (ArrayType) obj;
        return type.equals(at.type) && desc.equals(at.desc);
      }
      return false;
    }
  }

  public static class BoxedType extends ReferenceType {

    public final PrimitiveType baseType;

    BoxedType(PrimitiveType prim) {
      super(prim.primitiveType.boxDesc);
      baseType = prim;
    }

    PrimitiveType unbox() {
      return baseType;
    }

    @Override
    public boolean isBoxed() {
      return true;
    }

    @Override
    public String toString() {
      return "BoxedType[" + desc + "]";
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof BoxedType) {
        BoxedType bt = (BoxedType) obj;
        return baseType.equals(bt.baseType) && desc.equals(bt.desc);
      }
      return false;
    }
  }

  public static class VoidType implements Type {

    @Override
    public String desc() {
      return "V";
    }

    @Override
    public boolean isPrimitive() {
      return false;
    }

    @Override
    public boolean isArray() {
      return false;
    }

    @Override
    public boolean isBoxed() {
      return false;
    }

    @Override
    public boolean isReference() {
      return false;
    }

    @Override
    public boolean isVoid() {
      return true;
    }

    @Override
    public Sort z3Sort(Context ctx) throws RuntimeException {
      throw new RuntimeException("VoidType does not have a sort");
    }

    @Override
    public String toString() {
      return "VoidType";
    }

    @Override
    public boolean equals(Object obj) {
      return obj instanceof VoidType;
    }
  }
}
