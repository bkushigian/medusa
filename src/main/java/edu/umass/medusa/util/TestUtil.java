package edu.umass.medusa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestUtil {

  public static final String ROOT = System.getProperty("user.dir");
  public static final String RESOURCES = ROOT + "/resources";
  public static final String TEST_SRC = RESOURCES + "/java/test-src";
  public static final String TEST_CLASSES = RESOURCES + "/java/test-classes";
  public static final String TRIANGLE_CLASSES = TEST_CLASSES + "/triangle";
  public static final String TRIANGLE = TRIANGLE_CLASSES + "/Triangle.class";
  public static final String TRIANGLE_MUTANT_CLASSES = TRIANGLE_CLASSES + "/mutants";
  public static final String FORK_STRATEGY_CLASSES = TEST_CLASSES + "/fork-strat-tests";
  public static final String FORK_STRATEGY = FORK_STRATEGY_CLASSES + "/ForkStrategyTest.class";
  public static final String FORK_STRATEGY_MUTANT_CLASSES = FORK_STRATEGY_CLASSES + "/mutants";
  public static final String OUT = ROOT + "/out";

  public static final String FORK_STRATEGY2_CLASSES = TEST_CLASSES + "/fork-strat-tests2";
  public static final String FORK_STRATEGY2 = FORK_STRATEGY2_CLASSES + "/ForkStrategyTest.class";
  public static final String FORK_STRATEGY2_MUTANT_CLASSES = FORK_STRATEGY2_CLASSES + "/mutants";

  public static String getTestClass(String className) {
    return TEST_CLASSES + "/" + className;
  }

  public static String getTriangleMutantClass(int n) {
    return getTriangleMutantDir(n) + "/Triangle.class";
  }


  public static String getForkStrategyMutantClass(int n) {
    return getForkStrategyMutantDir(n) + "/ForkStrategyTest.class";
  }

  public static String getTriangleMutantDir(int n) {
    return String.format("%s/%d/triangle", TRIANGLE_MUTANT_CLASSES, n);
  }

  public static String getForkStrategyMutantDir(int n) {
    return String.format("%s/%d", FORK_STRATEGY_MUTANT_CLASSES, n);
  }

  public static String getForkStrategy2MutantDir(int n) {
    return String.format("%s/%d", FORK_STRATEGY2_MUTANT_CLASSES, n);
  }

  /**
   * @return a list of File objects for each Triangle mutant
   */
  public static File[] getTriangleMutantFiles() throws NoSuchFileException {
    return getMutantFiles("Triangle", TRIANGLE_MUTANT_CLASSES, "/triangle");
  }

  public static File[] getForkStrategyMutantFiles() throws NoSuchFileException {
    return getMutantFiles("ForkStrategyTest", FORK_STRATEGY_MUTANT_CLASSES, null);
  }

  public static File[] getForkStrategy2MutantFiles() throws NoSuchFileException {
    return getMutantFiles("ForkStrategyTest", FORK_STRATEGY2_MUTANT_CLASSES, null);
  }

  /**
   * @param name name of the class, such as `Triangle` for Triangle.class
   * @param searchPath where the mutants are located. These should be in directories from 1 to N.
   * @param pathOffset This is the package qualifier. For Triangle, which is in package triangle,
   * the offset if "/triangle" or simply "triangle".
   *
   * Thus, if the directory structure is
   * <pre>
   *                   location/1/triangle/Triangle.class
   *                   location/2/triangle/Triangle.class
   *                   ...
   *                   location/n/triangle/Triangle.class
   *                   </pre>
   *
   * The call should be {@code getMutantFiles(Triangle, path/to/location, /triangle);}
   * @return array of mutant class files
   * @throws NoSuchFileException if a file can't be found
   */
  public static File[] getMutantFiles(String name, String searchPath, String pathOffset)
      throws NoSuchFileException {
    if (pathOffset == null) {
      pathOffset = "";
    } else {
      // XXX: Very inefficient
      if (!pathOffset.startsWith("/")) {
        pathOffset = "/" + pathOffset;
      }
      while (pathOffset.endsWith("/")) {
        pathOffset = pathOffset.substring(0, pathOffset.length() - 1);
      }
    }

    if (!name.startsWith("/")) {
      name = "/" + name;
    }

    if (!name.endsWith(".class")) {
      name = name + ".class";
    }

    File mutantsBaseDir = new File(searchPath);
    if (!mutantsBaseDir.exists()) {
      throw new NoSuchFileException(searchPath, null, "Couldn't find directory " + searchPath);
    }

    File[] mutantDirs = mutantsBaseDir.listFiles();
    assert mutantDirs != null;
    File[] mutants = new File[mutantDirs.length];

    for (int i = 0; i < mutants.length; ++i) {
      final File dir = mutantDirs[i];
      if (!dir.exists()) {
        throw new NoSuchFileException(
            dir.getAbsolutePath(),
            null,
            String.format("Mutant directory %s does not exist. Perhaps you need to compile the "
                    + name + " mutants first? For Triangle, try `compile-triangle.sh`.",
                dir.getAbsolutePath()));
      }
      final String dirPath = dir.getAbsolutePath();
      final String mutantPath = dirPath + pathOffset + name;
      mutants[i] = new File(mutantPath);
      if (!mutants[i].exists()) {
        throw new NoSuchFileException(mutants[i].getAbsolutePath(),
            null,
            "Compiled mutant does not exist. Try running `compile-triangle.sh`");
      }
    }
    return mutants;

  }


  public static HashMap<Integer, Boolean> triangleEquivalence() {
    File eqFile = new File(TEST_SRC + "/triangle/equivalence.csv");
    return readEquivalenceFile(eqFile);
  }

  public static HashMap<Integer, Boolean> pipelineEquivalence() {
    File eqFile = new File(TEST_SRC + "/pipeline-test-equivalence.csv");
    return readEquivalenceFile(eqFile);

  }

  public static HashMap<Integer, Boolean> forkStrategyEquivalence() {
    File eqFile = new File(TEST_SRC + "/fork-strat-tests/equivalence.csv");
    return readEquivalenceFile(eqFile);
  }

  public static HashMap<Integer, Boolean> forkStrategy2Equivalence() {
    File eqFile = new File(TEST_SRC + "/fork-strat-tests2/equivalence.csv");
    return readEquivalenceFile(eqFile);
  }

  private static HashMap<Integer, Boolean> readEquivalenceFile(File file) {
    List<String> lines = new ArrayList<>(128);
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = br.readLine()) != null) {
        lines.add(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }

    HashMap<Integer, Boolean> equivMap = new HashMap<>();
    for (String line : lines) {
      int comma = line.indexOf(',');
      if (comma >= 0) {
        final String k = line.substring(0, comma),
            v = line.substring(comma + 1);
        try {
          final int key = Integer.parseInt(k);
          equivMap.put(key, v.trim().equals("E"));

        } catch (Exception e) {
          System.err.println("Couldn't parse line " + line);
        }
      }
    }
    return equivMap;

  }

  public static File getTriangleMutant(int i) throws NoSuchFileException {
    return new File(getTriangleMutantDir(i) + "/Triangle.class");
  }
}
