package edu.umass.medusa.solver;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.util.Z3Session;

import java.util.Set;

public class MedusaSolver {

  public final Z3Session z3;

  public MedusaSolver(Z3Session z3) {
    this.z3 = z3;
  }

  public void solve(Set<ClassIdentity> classes) {

  }
}
