package edu.umass.medusa.solver.result;

public enum ResultType {
  EQUIV,
  NEQUIV,
  UNKNOWN
}
