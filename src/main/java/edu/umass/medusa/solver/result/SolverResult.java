package edu.umass.medusa.solver.result;

import com.microsoft.z3.Model;
import edu.umass.medusa.exceptions.NoModelException;

/**
 * This represents the result of Medusa's reasoning about a mutant.
 */
public abstract class SolverResult {

  /**
   * For debugging, this holds the textual assertion string generated from Z3
   */
  public String assertionString;
  protected ResultType resultType;

  protected SolverResult(ResultType tp) {
    resultType = tp;
  }

  /**
   * Return the type of result--this is for switch statements;
   *
   * @return the result type of this {@code SolverResult}
   */
  ResultType getResultType() {
    return resultType;
  }

  /**
   * @return true if Medusa proved the mutant was equivalent
   */
  boolean provedEquivalent() {
    return false;
  }

  /**
   * @return true if Medusa proved the mutant was not equivalent
   */
  boolean provedNotEquivalent() {
    return false;
  }

  /**
   * @return true iff not provedNotEquivalent() and not provedEquivalent()
   */
  boolean unknown() {
    return false;
  }

  /**
   * @return a model demonstrating non-equivalence
   * @throws NoModelException if there is no model to return
   */
  public abstract Model getModel();

  public boolean isUnknown() {
    return resultType == ResultType.UNKNOWN;
  }

  public boolean isEquiv() {
    return resultType == ResultType.EQUIV;
  }

  public boolean isNotEquiv() {
    return resultType == ResultType.NEQUIV;
  }

  @Override
  public String toString() {
    if (isEquiv()) {
      return "EQUIV";
    }
    if (isNotEquiv()) {
      return "NEQUIV";
    }
    if (isUnknown()) {
      return "UNKNOWN";
    }
    return "error";
  }
}
