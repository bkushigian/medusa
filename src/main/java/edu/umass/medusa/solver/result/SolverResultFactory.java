package edu.umass.medusa.solver.result;

import com.microsoft.z3.Model;
import com.microsoft.z3.Status;
import edu.umass.medusa.exceptions.NoModelException;
import edu.umass.medusa.util.Z3Session;

public class SolverResultFactory {

  private Equivalent equiv;
  private Unknown unknown;

  public SolverResultFactory() {
    equiv = new Equivalent();
    unknown = new Unknown();
  }

  public SolverResult make(Status status, Z3Session z3) {
    switch (status) {
      case SATISFIABLE:
        return makeNotEquivalent(z3.getModel());
      case UNSATISFIABLE:
        return makeEquivalent();
      case UNKNOWN:
        return makeUnknown();
    }
    return makeUnknown();
  }

  public SolverResult makeEquivalent() {
    return equiv;
  }

  public SolverResult makeNotEquivalent(Model model) {
    return new NotEquivalent(model);
  }

  public SolverResult makeUnknown() {
    return unknown;
  }

  public class Equivalent extends SolverResult {

    private Equivalent() {
      super(ResultType.EQUIV);
    }

    @Override
    public Model getModel() {
      throw new NoModelException();
    }

    @Override
    boolean provedEquivalent() {
      return true;
    }
  }

  public class Unknown extends SolverResult {

    private Unknown() {
      super(ResultType.UNKNOWN);
    }

    @Override
    public Model getModel() {
      throw new NoModelException();
    }

    @Override
    boolean unknown() {
      return true;
    }
  }

  public class NotEquivalent extends SolverResult {

    Model model;

    private NotEquivalent(Model model) {
      super(ResultType.NEQUIV);
      this.model = model;
    }

    @Override
    public Model getModel() {
      return model;
    }

    @Override
    boolean provedNotEquivalent() {
      return true;
    }
  }

}
