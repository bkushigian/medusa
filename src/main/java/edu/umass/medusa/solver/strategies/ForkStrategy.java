package edu.umass.medusa.solver.strategies;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Status;
import de.codesourcery.asm.controlflow.Edge;
import de.codesourcery.asm.controlflow.IBlock;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.BlockConstraint;
import edu.umass.medusa.format.constraint.BlockConstraintGen;
import edu.umass.medusa.format.constraint.EdgeConstraint;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.solver.result.SolverResultFactory;
import edu.umass.medusa.util.CFGUtil;
import edu.umass.medusa.util.Pair;
import edu.umass.medusa.util.Z3Session;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The Fork strategy reuses origin's constraints to gain optimizations and organizes visited mutants
 * for performance benefits
 */
public class ForkStrategy extends Strategy {

  final Z3Session z3;
  final SolverResultFactory factory;
  Set<MutantIdentity> reasonedAbout;

  public ForkStrategy(Z3Session z3) {
    this.z3 = z3;
    this.factory = new SolverResultFactory();
    reasonedAbout = new HashSet<>();
  }

  @Override
  public void applyToClass(ClassIdentity clazz) {
    for (MethodIdentity meth : clazz.methods) {
      applyToMethod(meth);
    }
  }

  @Override
  public void applyToMethod(MethodIdentity method) {
    if (method.mutants == null || method.mutants.size() == 0) {
      return;
    }
    System.out.println("Applying to method" + method.name);
    long to = System.currentTimeMillis();
    // Get the TOP and BOT blocks
    IBlock top = method.cfg.getStart(), bot = method.cfg.getEnd();

    /* (1) Compute a partial ordering on blocks by reachability */
    final Deque<IBlock> ordering = calculateBlockOrder(method);

    /* (2) Compute the mutated nodes. For now this only happens on FO CFG mutants */

    final Map<IBlock, Set<MutantIdentity>> mapBlocksToMutants = new HashMap<>();
    final Map<MutantIdentity, IBlock> mapMutantsToBlocks = new HashMap<>();
    final List<MutantIdentity> isomorphicMutants = method.mutants.stream()
        .filter(m -> CFGUtil.isIsomorphic(method.cfg, m.cfg))
        .collect(Collectors.toList());

    for (MutantIdentity mutant : isomorphicMutants) {
      final Set<Pair<IBlock, IBlock>> mutantBlockPairs = CFGUtil
          .getMutantBlockPairs(method, mutant);
      if (mutantBlockPairs.size() == 1) {
        final Pair<IBlock, IBlock> pair = mutantBlockPairs.iterator().next();
        mapBlocksToMutants.computeIfAbsent(pair.first, k -> new HashSet<>()).add(mutant);
        mapMutantsToBlocks.put(mutant, pair.second);
      }
    }

    /* (3) Create two sets of constraints. The first constraints, which we will call
     * the ground constraints, represent ground truth and are used as a reference. The
     * second set of constraints, which we call the forkedConstraints, represent the forked
     * execution after the mutant has been executed.*/
    MethodConstraint groundConstraint = method.gen.generate();
    final Map<IBlock, BlockConstraint> forkedBlockConstraints = method.gen
        .generateIndexedBlockConstraints();

    /* (4) Push a new Z3 scope to work on the origin method. Keep track of the depth to
     * sanity-check at the end of the procedure. */
    int depth = 0;      // For sanity checking our scope depth
    z3.push();
    ++depth;

    /* (5) Constrain the original method */
    z3.addConstraints(groundConstraint.getConstraints());

    /* (6) a. In reverse order (from BOT to TOP), add each block constraint and its outgoing edges.
     *     b. Then, identify the parameters and separate the return values
     *     c. Assert that both methods are executed */
    Deque<IBlock> blockVisitOrder = new ArrayDeque<>();

    // b. Identify inputs and separate outputs
    final Expr rv1 = z3.ctx.mkConst("rv1", z3.javaIntSort);
    final Expr rv2 = z3.ctx.mkConst("rv2", z3.javaIntSort);
    z3.addConstraints(z3.mkEq(rv1, groundConstraint.getReturnExpr()),
        z3.mkEq(rv2, forkedBlockConstraints.get(bot).peek()));
    z3.addConstraints(groundConstraint.identifyInputParams(forkedBlockConstraints.get(top)),
        z3.mkNot(z3.mkEq(rv1, rv2))); // TODO: Don't identify input params: fork top block instead
    // c. Assert method execution
    z3.addConstraints(groundConstraint.executed, forkedBlockConstraints.get(top).getExecuted());
    // a.
    for (final IBlock b : ordering) {
      blockVisitOrder.push(b);
      z3.push();
      ++depth;
      final BlockConstraint bc = forkedBlockConstraints.get(b);
      z3.addConstraints(bc.getBlockConstraints());

      BlockConstraint trueSucc, falseSucc;
      switch (b.getRegularSuccessorCount()) {
        case 0:
          break;
        case 1:
          trueSucc = forkedBlockConstraints.get(b.getRegularSuccessor());
          z3.addConstraints(new EdgeConstraint(bc, trueSucc).unconditionedConstraints());
          break;
        case 2:
          trueSucc = forkedBlockConstraints.get(CFGUtil.getTrueOutgoing(b));
          falseSucc = forkedBlockConstraints.get(CFGUtil.getFalseOutgoing(b));
          z3.addConstraints(
              new EdgeConstraint(bc, trueSucc, true).conditionedConstraints(),
              new EdgeConstraint(bc, falseSucc, false).conditionedConstraints()
          );
          break;
        default:
          throw new RuntimeException(
              "Cannot handle block constraints with more than two successors!");
      }
    }

    // Currently the top scope stores the top block and its single outgoing edge

    /* (7) For each block b, pop off the top Z3 scope to remove b's constraints, and
     * then iterate through all fo-mutants of b.
     *
     * TODO: This pop() will affect other forking constriants that we want to preserve.
     *
     * For each FO mutant mid, push a new scope and get the relevant mutated block. Constrain
     * the block
     *
     * INVARIANT: At the start of each loop, b and its outgoing edges will be in the top
     * Z3 scope. We pop this so that their constraints no longer affect our calculations.
     *
     * INVARIANT: At the start of each loop, any incoming edges to B are forked edges
     * (e.g., they are from groundConstraints).
     */

    final Map<IBlock, Set<BoolExpr>> forkedEdges = new HashMap<>();

    for (IBlock b : blockVisitOrder) {
      // Pop b's block constraints and outgoing constraints. This clears all
      // forked constraints as well, which we add back next
      z3.pop();
      --depth;

      // Forget about the forking edge constraints that target b
      forkedEdges.remove(b);

      // Add the remaining forking edge constraint sto Z3
      for (Set<BoolExpr> edges : forkedEdges.values()) {
        z3.addConstraints(edges.toArray(new BoolExpr[0]));
      }

      final Set<MutantIdentity> blockMutants = mapBlocksToMutants.get(b);
      final Set<Edge> inEdges = CFGUtil.getInEdges(b);
      final Set<Edge> outEdges = CFGUtil.getOutEdges(b);

      /* Now we need to take all of the edges out from b and fork them to our second copy of the constraints.
       * We store them in an indexed map so that we can easily update this collection as we continue with our
       * pops.
       */
      for (Edge e : outEdges) {
        final BlockConstraint src = groundConstraint.blockConstraintMap.get(e.src);
        final BlockConstraint trg = forkedBlockConstraints.get(e.dst);
        final BoolExpr edgeConstraint = createEdgeConstraint(e, src, trg);
        forkedEdges.computeIfAbsent(e.dst, k -> new HashSet<>()).add(edgeConstraint);
      }

      if (blockMutants != null) {
        handleBlockMutants(mapMutantsToBlocks, groundConstraint, forkedBlockConstraints,
            blockMutants, inEdges, outEdges);
      }
    }

    long tf = System.currentTimeMillis();
    System.out.println("Total time: " + (tf - to));
    z3.pop();               // Clear original's constraints
    --depth;
    assert depth == 0;
  }

  private void handleBlockMutants(Map<MutantIdentity, IBlock> mapMutantsToBlocks,
      MethodConstraint groundConstraint,
      Map<IBlock, BlockConstraint> forkedBlockConstraints,
      Set<MutantIdentity> blockMutants,
      Set<Edge> inEdges,
      Set<Edge> outEdges) {
    for (MutantIdentity mid : blockMutants) {

      if (verbosity > 0) {
        System.out.println("(" + (reasonedAbout.size() + 1) + ") Applying to mutant " + mid.path );
      }
      long t0 = System.currentTimeMillis();
      z3.push();

      // Get the mutated block and obtain a constraint
      final IBlock mutatedBlock = mapMutantsToBlocks.get(mid);
      BlockConstraintGen gen = (BlockConstraintGen) mutatedBlock.getProperty("gen");
      final BlockConstraint bc = gen.getFree();
      // Add block constraints

      z3.addConstraints(bc.getBlockConstraints());

      // Create edges to mutated block
      for (Edge e : inEdges) {
        BlockConstraint src = groundConstraint.blockConstraintMap.get(e.src);
        BoolExpr edgeConstraint = createEdgeConstraint(e, src, bc);

        z3.addConstraints(edgeConstraint);
      }

      // Create edges from mutated block
      for (Edge e : outEdges) {
        BlockConstraint dst = forkedBlockConstraints.get(e.dst);
        BoolExpr edgeConstraint = createEdgeConstraint(e, bc, dst);

        z3.addConstraints(edgeConstraint);
      }

      // Now, check sat
      final Status status = z3.check();
      long tf = System.currentTimeMillis();
      mid.setResult(factory.make(status, z3));
      mid.setTime(tf - t0);
      mid.getResult().assertionString = z3.z3.toString();
      z3.pop();
      reasonedAbout.add(mid);
      if (verbosity > 1)  {
        System.out.println("    result: " + mid.getResult() + " in time " + mid.time);
      }

    }
  }

  private BoolExpr createEdgeConstraint(Edge e, BlockConstraint src, BlockConstraint trg) {
    BoolExpr edgeConstraint;
    if ("true".equals(e.metaData) || e.metaData == null) {
      if (src.getBlock().getRegularSuccessorCount() == 2) {
        edgeConstraint = new EdgeConstraint(src, trg).conditionedConstraints();
      } else {
        edgeConstraint = new EdgeConstraint(src, trg).unconditionedConstraints();
      }
    } else if ("false".equals(e.metaData)) {
      edgeConstraint = new EdgeConstraint(src, trg, false).conditionedConstraints();
    } else {
      throw new RuntimeException("Unhandled edge: " + e.toString());
    }
    return edgeConstraint;
  }

  @Override
  public void applyToMutant(MutantIdentity mutant) {

  }

  @Override
  public String type() {
    return "constraint-forking";
  }

  /**
   * This calculates an ordering of the blocks b1, b2, ..., bN, such that bi may not occur before bj
   * if there is an execution trace in which block  bj is executed before bi is executed (note, this
   * refers to the <em>first</em> execution in the method.)
   *
   * This is a partial order in general, and the relation defined by this partial ordering is
   * extended to provide a linear total ordering of the blocks.
   *
   * For instance, consider the following code snippet:
   *
   * <pre>
   *     // BLOCK 1
   *     int y = 0;
   *     if (x < 0) {
   *         // BLOCK 2
   *         y = 1;
   *     } else {
   *         // BLOCK 3
   *         y = 2;
   *     }
   *     // BLOCK 4
   *     return y;
   * </pre>
   *
   * Here we have four blocks (not including the entry/exit blocks) where
   * <ul>
   * <li> B1 is always executed first</li>
   * <li> B4 is always executed last</li>
   * </ul>
   *
   * Thus we have B1 < B2, B3 < B4, and the only ambiguity remaining is whether B2 < B3. This is
   * resolved in an arbitrary and unimportant fashion, say B2 < B3, and a linear ordering of blocks
   * is returned.
   *
   * @return A linear total ordering of blocks that respects the natural partial ordering of the
   * blocks.
   */
  public Deque<IBlock> calculateBlockOrder(MethodIdentity method) {
    Deque<IBlock> deque = new ArrayDeque<>(method.cfg.getAllNodes().size());
    Map<IBlock, Set<IBlock>> domSets = new HashMap<>();
    calculateDomSets(method.cfg.getStart(), new HashSet<>(), domSets);

    final IBlock start = method.cfg.getStart();
    // The downward expanding boundary of the program
    Queue<IBlock> boundary = new LinkedList<>();
    // The blocks that have been consumed by the boundary
    Set<IBlock> consumed = new HashSet<>();
    boundary.add(start);
    consumed.add(start);

    while (!boundary.isEmpty()) {
      IBlock candidate = boundary.poll();
      boolean isNext = true;

      for (IBlock b : boundary) {
        if (domSets.get(b).contains(candidate)) {
          isNext = false;
          break;
        }
      }

      if (isNext) {
        deque.push(candidate);
        for (IBlock b : candidate.getRegularSuccessors()) {
          if (!consumed.contains(b)) {
            boundary.add(b);
            consumed.add(b);
          }
        }
      } else {
        boundary.add(candidate);
      }
    }

    return deque;
  }

  /**
   * Calculate a total ordering on the IBlocks that is consistent with the ordering induces by
   * relation
   *
   * A <= B     whenever A may be executed before B
   *
   * The main action of this method is to populate domSets recusively. The domSet parameter contains
   * the dominator data for each node, where A dominates B whenever A < B
   *
   * WARNING: this relies on the **unverified** property of Javac that any back-edges in a CFG must
   * pass to a visited block. That is, at the bottom of a loop we always jump back to a basic block
   * that we have seen before. This seems clear by inspecting Java's control flow structures (for,
   * while, do-while, for-each, etc) but this needs to be verified at some point.
   *
   * @param current The current block being considered
   * @param visited The set of blocks that have been visited in the current execution trace
   * @param domSets The dominator sets: B \in dom(A) whenever A < B
   * @return the domSet of current.
   */
  private Set<IBlock> calculateDomSets(IBlock current, Set<IBlock> visited,
      Map<IBlock, Set<IBlock>> domSets) {

    // (1) If we have already calculated the dominated set for current, return it
    if (domSets.containsKey(current)) {
      return domSets.get(current);
    } else {
      domSets.put(current, new HashSet<IBlock>());
    }

    Set<IBlock> domSet = domSets.get(current);

    // (2) Add the current block to visited so that we don't loop forever
    visited.add(current);

    // (3) For each successor block, if we have not already visited it...
    for (IBlock b : current.getRegularSuccessors()) {
      if (!visited.contains(b)) {
        // (a) add it to the domSet
        domSet.add(b);

        // (b) if we have not already visited it, calculate its domSet
        domSet.addAll(calculateDomSets(b, visited, domSets));
      }
    }

    // (4) We are done here, so remove current from visited
    visited.remove(current);
    return domSet;
  }

  public void reset() {
    reasonedAbout = new HashSet<>();
  }

  @Override
  public int getReasonedAbout() {
    return reasonedAbout.size();
  }
}
