package edu.umass.medusa.solver.strategies;

import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.solver.result.SolverResultFactory;

import java.util.Collection;

/**
 * A {@code Strategy} works on a method and its collection of mutants.
 */
public abstract class Strategy {

  SolverResultFactory resultFactory;

  protected int verbosity = 0;

  Strategy() {
    resultFactory = new SolverResultFactory();
  }

  public void applyToClasses(Collection<ClassIdentity> classes) {
    applyToClasses(classes.toArray(new ClassIdentity[classes.size()]));
  }

  public void applyToClasses(ClassIdentity[] classes) {
    for (ClassIdentity cid : classes) {
      if (verbosity > 1) {
        System.out.println("Applying to class: " + cid.name + ", " + cid.path);
      }
      applyToClass(cid);
    }
  }

  public abstract void applyToClass(ClassIdentity clazz);

  public void applyToMethods(Collection<MethodIdentity> methods) {
    applyToMethods(new MethodIdentity[methods.size()]);
  }

  public void applyToMethods(MethodIdentity[] methods) {
    for (MethodIdentity mid : methods) {
      applyToMethod(mid);
    }
  }

  public abstract void applyToMethod(MethodIdentity method);


  public void applyToMutants(Collection<MutantIdentity> mutants) {
    applyToMutants(mutants.toArray(new MutantIdentity[mutants.size()]));
  }

  public void applyToMutants(MutantIdentity[] mutants) {
    for (MutantIdentity mid : mutants) {
      if (mid.getResult().isUnknown()) {
        System.out.println("      Applying to mutant: " + mid.name);
        applyToMutant(mid);
      }
    }
  }

  public abstract void applyToMutant(MutantIdentity mutant);

  public abstract String type();

  public abstract void reset();

  public abstract int getReasonedAbout();

  public int getVerbosity() {
    return verbosity;
  }

  public void setVerbosity(int verbosity) {
    if (verbosity >= 0 && verbosity < 5) this.verbosity = verbosity;
  }
}
