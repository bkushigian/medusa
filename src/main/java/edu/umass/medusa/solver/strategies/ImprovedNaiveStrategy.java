package edu.umass.medusa.solver.strategies;

import com.microsoft.z3.Status;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.solver.result.SolverResult;
import edu.umass.medusa.util.Z3Session;

public class ImprovedNaiveStrategy extends NaiveStrategy {

  MethodConstraint mconstraint;

  public ImprovedNaiveStrategy(Z3Session z3) {
    super(z3);
  }

  @Override
  public void applyToMethod(MethodIdentity method) {
    mconstraint = method.gen.generate();
    z3.push(mconstraint.getConstraints());
    if (verbosity > 2) {
      System.out.println("  Applying to method: " + method.name);
    }
    long t0 = System.currentTimeMillis();
    for (MutantIdentity mut : method.mutants) {
      SolverResult result = applyToMutant(mut, mconstraint);
      long tf = System.currentTimeMillis();
      mut.setResult(result);
    }
    z3.pop();
  }

  @Override
  public SolverResult applyToMutant(MutantIdentity mutant, MethodConstraint origConstraint) {
    MethodConstraint mutConstraint = mutant.gen.generate();
    long t0 = System.currentTimeMillis();
    if (verbosity > 0) {
      System.out.println("(" + (totalReasonedAbout + 1) + ") Applying to mutant " + mutant.path );
    }
    z3.push(
        mutConstraint.getConstraints(),
        origConstraint.identifyInputParams(mutConstraint),
        origConstraint.separateReturnValues(mutConstraint)
    );

    final Status status = z3.check();
    long tf = System.currentTimeMillis();
    SolverResult result = resultFactory.make(status, z3);
    mutant.setTime(tf - t0);
    z3.pop();
    if (verbosity > 1)  {
      System.out.println("    result: " + result + " in time " + mutant.time);
    }

    ++totalReasonedAbout;
    return result;
  }

  public String type() {
    return "improved-naive";
  }
}
