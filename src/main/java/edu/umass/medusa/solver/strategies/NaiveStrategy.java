package edu.umass.medusa.solver.strategies;

import com.microsoft.z3.Status;
import edu.umass.medusa.format.ClassIdentity;
import edu.umass.medusa.format.MethodIdentity;
import edu.umass.medusa.format.MutantIdentity;
import edu.umass.medusa.format.constraint.MethodConstraint;
import edu.umass.medusa.format.constraint.MethodConstraintGen;
import edu.umass.medusa.solver.result.SolverResult;
import edu.umass.medusa.util.Z3Session;

/**
 * This is the basic no-frills strategy that identifies input parameters, separates output
 * parameters, and lets Z3 do the rest. This is the baseline least efficient Strategy.
 */
public class NaiveStrategy extends Strategy {

  protected final Z3Session z3;
  int totalReasonedAbout = 0;

  public NaiveStrategy(Z3Session z3) {
    super();
    this.z3 = z3;
  }

  @Override
  public void applyToClass(ClassIdentity clazz) {
    for (MethodIdentity meth : clazz.methods) {
      applyToMethod(meth);
    }

  }

  @Override
  public void applyToMethod(MethodIdentity method) {
    if ("<init>()V".equals(method.name)) {
      return;
    }
    System.out.println("Applying to method " + method.name);
    long to = System.currentTimeMillis();
    final MethodConstraintGen gen = method.gen;
    MethodConstraint methodConstraint = gen.generate();
    for (MutantIdentity mut : method.mutants) {
      SolverResult result = applyToMutant(mut, methodConstraint);
      mut.setResult(result);
    }
    long tf = System.currentTimeMillis();
    System.out.println("Total time: " + (tf - to));

  }

  @Override
  public void applyToMutant(MutantIdentity mutant) {
    final MethodIdentity method = mutant.original;
    final MethodConstraintGen gen = method.gen;
    final MethodConstraint methConstraint = gen.generate();
    SolverResult result = applyToMutant(mutant, methConstraint);
    mutant.setResult(result);
  }

  @Override
  public String type() {
    return "naive";
  }

  @Override
  public void reset() {
    totalReasonedAbout = 0;
  }

  @Override
  public int getReasonedAbout() {
    return totalReasonedAbout;
  }

  public SolverResult applyToMutant(MutantIdentity mutant, MethodConstraint origConstraint) {
    if (verbosity > 0) {
      System.out.println("(" + (totalReasonedAbout + 1) + ") Applying to mutant " + mutant.path);
    }
    MethodConstraint mutConstraint = mutant.gen.generate();
    long t0 = System.currentTimeMillis();
    z3.push();
    z3.addConstraints(
        origConstraint.getConstraints(),
        mutConstraint.getConstraints(),
        origConstraint.identifyInputParams(mutConstraint),
        origConstraint.separateReturnValues(mutConstraint)
    );

    final Status status = z3.check();
    long tf = System.currentTimeMillis();
    SolverResult result = resultFactory.make(status, z3);
    mutant.setTime(tf - t0);
    z3.pop();
    if (verbosity > 1)  {
      System.out.println("    result: " + result + " in time " + mutant.time);
    }

    ++totalReasonedAbout;
    return result;
  }
}
